﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

namespace Harmony
{
    public class Program2 : MonoBehaviour
    {
        //private Type2 type2;
        //private Difficulty diff;
        
        public AudioSource messageSFX;
        public AudioSource leetSFX;

        private static float normTypeSpeed = 0.03f;
        public bool inCoroutine = false;
        public TMP_Text startText;
        int i;

        void Start()
        {
            //type2 = GameObject.FindGameObjectWithTag("GameManager").GetComponent<Type2>();
            //diff = GameObject.FindGameObjectWithTag("GameManager").GetComponent<Difficulty>();

            StartCoroutine(Loading());
        }

        private void Update()
        {
            if (UnityEngine.Input.GetKeyDown(KeyCode.Escape))
            {
                SceneManager.LoadScene(0);
            }
        }

        IEnumerator Loading()
        {
            i = 0;
            while (i <= 4)
            {
                startText.text = "         WELCOME TO GejrOS, logged in with root\n>_";
                yield return new WaitForSeconds(0.5f);
                startText.text = "         WELCOME TO GejrOS, logged in with root\n>";
                yield return new WaitForSeconds(0.5f);
                i++;
            }
            messageSFX.Play();
            Type2.TypeText(Difficulty.a, startText, normTypeSpeed);
            while (inCoroutine == true)
            {
                yield return new WaitForSeconds(0.1f);
            }
            yield return new WaitForSeconds(5f);
            leetSFX.Play();

            Type2.TypeText(Difficulty.b, startText, 0.5f);
            while (inCoroutine == true)
            {
                yield return new WaitForSeconds(0.1f);
            }
            SceneManager.LoadScene(4);
        }
    }
}