﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Harmony;

namespace Harmony
{
    public class Type : MonoBehaviour {
        public static Type instance;
        private static Program program;

        private static IEnumerator coroutine;

        private void Awake()
        {
            program = GameObject.FindGameObjectWithTag("GameManager").GetComponent<Program>();
            instance = this;
        }

        public static void TypeText(string message, TMP_Text textDes, float speed)
        {
            instance.StartCoroutine(AnimateText(message, textDes, speed));
        }
        
        static IEnumerator AnimateText(string message, TMP_Text textDes, float speed)
        {
            program.inCoroutine = true;
            for (int i = 0; i < (message.Length + 1); i++)
            {
                textDes.text = message.Substring(0, i);
                yield return new WaitForSeconds(speed);
            }
            program.inCoroutine = false;
        }
    }
}

