﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Harmony;

namespace Harmony
{
    public class Type3 : MonoBehaviour {
        public static Type3 instance;
        private static Program3 program3;

        private static IEnumerator coroutine;

        private void Awake()
        {
            program3 = GameObject.FindGameObjectWithTag("GameManager").GetComponent<Program3>();
            instance = this;
        }

        public static void TypeText(string message, TMP_Text textDes, float speed)
        {
            instance.StartCoroutine(AnimateText(message, textDes, speed));
        }
        
        static IEnumerator AnimateText(string message, TMP_Text textDes, float speed)
        {
            program3.inCoroutine = true;
            for (int i = 0; i < (message.Length + 1); i++)
            {
                textDes.text = message.Substring(0, i);
                yield return new WaitForSeconds(speed);
            }
            program3.inCoroutine = false;
        }
    }
}

