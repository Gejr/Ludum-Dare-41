﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

namespace Harmony
{
    public class Program3 : MonoBehaviour
    {
        //private Type3 type3;
        //private Doggo doggo;
        
        public AudioSource messageSFX;
        public AudioSource doggoSFX;

        private static float normTypeSpeed = 0.03f;
        public bool inCoroutine = false;
        public TMP_Text startText;
        int i;

        void Start()
        {
            //type3 = GameObject.FindGameObjectWithTag("GameManager").GetComponent<Type3>();
            //doggo = GameObject.FindGameObjectWithTag("GameManager").GetComponent<Doggo>();

            StartCoroutine(Loading());
        }
        private void Update()
        {
            if (UnityEngine.Input.GetKeyDown(KeyCode.Escape))
            {
                SceneManager.LoadScene(0);
            }
        }
        IEnumerator Loading()
        {
            i = 0;
            while (i <= 4)
            {
                startText.text = "         WELCOME TO GejrOS, logged in with root\n>_";
                yield return new WaitForSeconds(0.5f);
                startText.text = "         WELCOME TO GejrOS, logged in with root\n>";
                yield return new WaitForSeconds(0.5f);
                i++;
            }
            messageSFX.Play();
            Type3.TypeText(Doggo.a, startText, normTypeSpeed);
            while (inCoroutine == true)
            {
                yield return new WaitForSeconds(0.1f);
            }
            yield return new WaitForSeconds(11f);
            doggoSFX.Play();
            Type3.TypeText(Doggo.b, startText, 0.5f);
            while (inCoroutine == true)
            {
                yield return new WaitForSeconds(0.1f);
            }
            PlayerPrefs.SetInt("Completed", 1);
            PlayerPrefs.Save();
            SceneManager.LoadScene(0);
        }
    }
}