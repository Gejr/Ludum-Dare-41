﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;
using UnityEngine.SceneManagement;

public class Battleimp : MonoBehaviour {

    public Slider slider;
    public Slider guySlider;
    public TMP_Text attackText;
    public TMP_Text dogHealth;
    public TMP_Text guyHealth;

    public Image black;
    public Animator animator;
    public AudioSource DogDieSFX;
    public AudioSource GuydDieSFX;

    public GameObject GUI;
    public GameObject GameOver;
    private Animimp anim;
    System.Random rnd = new System.Random();

    private bool myTurn = true;
    private bool ableToAttack = false;
    private bool enemyAbleToAttack = false;
    private bool isReady = false;
    private bool isDone = false;
    public bool isDead;

    public float CurrentHealth { get; set; }
    public float MaxHealth { get; set; }
    public float CurrentHealthGuy { get; set; }
    public float MaxHealthGuy { get; set; }

    void Start () {
        anim = GameObject.FindGameObjectWithTag("GameManager").GetComponent<Animimp>();

        GUI.SetActive(false);
        GameOver.SetActive(false);
        isDead = false;

        MaxHealth = 200f;
        CurrentHealth = MaxHealth;
        dogHealth.text = "Dog love-o-meter : " + CurrentHealth.ToString();
        slider.value = CalcHealth();

        MaxHealthGuy = 20f;
        CurrentHealthGuy = MaxHealthGuy;
        guyHealth.text = "Guy love-o-meter : " + CurrentHealthGuy.ToString();
        guySlider.value = CalcHealthGuy();
    }
    
    void Update () {
        if(anim.isDone == true && isDone == false)
        {
            isReady = true;
        }

        if (isReady == true && isDone == false)
        {
            GUI.SetActive(true);
            isDone = true;
        }

        if(myTurn == true)
        {
            ableToAttack = true;
        }
        if(myTurn == false)
        {
            enemyAbleToAttack = true;
            StartCoroutine(EnemyAttack());
        }
        if (UnityEngine.Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
        }
    }

    public void Attack(string move)
    {
        if(ableToAttack == true)
        {
            myTurn = false;
            ableToAttack = false;

            //PLAY SOUND
            float rndDMG = rnd.Next(1, 5);
            if (rndDMG >= 4.8f)
            {
                attackText.text = "<color=#18b7b3>You " + move + " the dog, it's very effective!";
            }
            if (rndDMG <= 4f)
            {
                attackText.text = "<color=#18b7b3>You " + move + " the dog, it's effective!";
            }
            if (rndDMG <= 2.5f)
            {
                attackText.text = "<color=#18b7b3>You " + move + " the dog, it's neutral!";
            }
            if (rndDMG <= 1.5f)
            {
                attackText.text = "<color=#18b7b3>You " + move + " the dog, it's ineffective!";
            }
            DealDamage(rndDMG);
        }
        else
        {
            return;
        }
        
    }

    void DealDamage(float damageValue)
    {
        CurrentHealth -= damageValue;
        slider.value = CalcHealth();
        dogHealth.text = "Dog love-o-meter : " + CurrentHealth.ToString();

        if (CurrentHealth <= 0)
        {
            myTurn = true;
            ableToAttack = true;
            enemyAbleToAttack = false;
            GUI.SetActive(false);
        }
        if(slider.value <= 0)
        {
            myTurn = true;
            ableToAttack = true;
            enemyAbleToAttack = false;
        }
    }

    float CalcHealth()
    {
        return CurrentHealth / MaxHealth;
    }

    IEnumerator EnemyAttack()
    {
        if(enemyAbleToAttack == true)
        {
            myTurn = true;
            enemyAbleToAttack = false;
            yield return new WaitForSeconds(1f);
            //PLAY SOUND
            float rndDMG = rnd.Next(1, 5);
            if (rndDMG >= 4.8f)
            {
                attackText.text = "<color=#861a1a>The dog licked you, it's very effective!";
            }
            if (rndDMG <= 4f)
            {
                attackText.text = "<color=#861a1a>The dog eye begged you, it's effective!";
            }
            if (rndDMG <= 2.5f)
            {
                attackText.text = "<color=#861a1a>The dog roll arounds, it's neutral!";
            }
            if (rndDMG <= 1.5f)
            {
                attackText.text = "<color=#861a1a>The dog sat down, it's ineffective!";
            }
            EnemyDealDamage(rndDMG);
        }
    }

    float CalcHealthGuy()
    {
        return CurrentHealthGuy / MaxHealthGuy;
    }

    void EnemyDealDamage(float damageValue)
    {
        CurrentHealthGuy -= damageValue;
        guySlider.value = CalcHealthGuy();
        guyHealth.text = "Guy love-o-meter : " + CurrentHealthGuy.ToString();

        if (CurrentHealthGuy <= 0)
        {
            myTurn = true;
            ableToAttack = true;
            enemyAbleToAttack = false;
            GUI.SetActive(false);
            DieGuy();
        }
        if (guySlider.value <= 0)
        {
            myTurn = true;
            ableToAttack = true;
            enemyAbleToAttack = false;
            DieGuy();
        }
    }

    IEnumerator Continue()
    {
        yield return new WaitForSeconds(5);
        animator.SetBool("Fade", true);
        yield return new WaitUntil(() => black.color.a == 1);
        SceneManager.LoadScene(3);
    }

    void DieGuy()
    {
        GuydDieSFX.Play();
        attackText.text = "You fell asleep, because you didn't love the dog enough!";
        GUI.SetActive(false);
        StartCoroutine(Continue());
    }

    public void Again()
    {
        SceneManager.LoadScene(2);
    }
    public void Giveup()
    {
        SceneManager.LoadScene(0);
    }
}
