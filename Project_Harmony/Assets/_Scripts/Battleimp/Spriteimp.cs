﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spriteimp : MonoBehaviour {
    private Battleimp battle;
    public Animator anim;
    private bool stahp;

    // Use this for initialization
    void Start () {
        battle = GameObject.FindGameObjectWithTag("GameManager").GetComponent<Battleimp>();

    }

    // Update is called once per frame
    void Update () {
		if(battle.isDead == true && stahp == false)
        {
            PlayAnim();
        }
	}

    void PlayAnim()
    {
        stahp = true;
        anim.Play("die");
    }
}
