﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Harmony
{
    public class BootSeq : MonoBehaviour
    {
        [HideInInspector]
        public static string BootSequence =
            "GejrOS version 2.4.18-14.134.X (bhfcompile@com.gejrs.GejrOS)\n" +
            "gcc version 2.96 20000731 (GejrOS 12.34.2.1092)\n" +
            "Nuffloader #dlader Tue Oct 26 13:33:54 CET 2002\n" +
            "BIOS-provided physical RAM-package map:\n" +
            "  BIOS-esp8266: 0000000000000000 - 000000000009fc00 (usable)\n" +
            "  BIOS-esp8266: 000000000009fc00 - 00000000000a0000 (reserved)\n" +
            "  BIOS-esp8266: 00000000000ce000 - 00000000000d2000 (reserved)\n" +
            "  BIOS-esp8266: 00000000000f0000 - 000000003fff0000 (reserved)\n" +
            "  BIOS-esp8266: 0000001000000000 - 0000000031000000 (usable)\n" +
            "  BIOS-esp8266: 000003ffff000000 - 0000037ddef00000 (ACPI data)\n" +
            "  BIOS-esp8266: 00000000000ce000 - 00000000000d2000 (ACPI NVS)\n" +
            "128MB HIGHMEM available.\n" +
            "86MB LOWMEM available.\n" +
            "Kernel command: BOOT_IMAGE=GejrOS ro root=302 BOOT_FILE=/boot/dxgejros-2.4.18-14.iso\n" +
            "Initializing CPU#0\n" +
            "Detected 456 MHz processor\n" +
            "CPU: Common caps: 3febfbff 00000000 00000000 00000000\n" +
            "BOOTING .\n" +
            "BOOTING ..\n" +
            "BOOTING ..."
        ;
    }
}