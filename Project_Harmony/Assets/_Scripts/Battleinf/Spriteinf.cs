﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spriteinf : MonoBehaviour {
    private Battleinf battle;
    public Animator anim;
    private bool stahp;

    // Use this for initialization
    void Start () {
        battle = GameObject.FindGameObjectWithTag("GameManager").GetComponent<Battleinf>();

    }

    // Update is called once per frame
    void Update () {
		if(battle.isDead == true && stahp == false)
        {
            PlayAnim();
        }
	}

    void PlayAnim()
    {
        stahp = true;
        anim.Play("die");
    }
}
