﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animinf : MonoBehaviour {

    public GameObject quad1;
    public GameObject quad2;

    public GameObject dog;
    public GameObject guy;

    public bool isDone;
    private int i;
    private float speed = 1f;
    private float startTime;
    private float journeyLength;
    private float journeyLength2;
    private float journeyLength3;
    private float journeyLength4;
    void Start()
    {
        startTime = Time.time;
        journeyLength = Vector3.Distance(quad1.transform.position, new Vector3(-16.55f, -0.013f, 0));
        journeyLength2 = Vector3.Distance(quad2.transform.position, new Vector3(16.42f, -0.016f, 0f));
        journeyLength3 = Vector3.Distance(dog.transform.position, new Vector3(-4f, 5.82f, 0.5f));
        journeyLength4 = Vector3.Distance(guy.transform.position, new Vector3(6.64f, -2.01f, 0.5f));
        StartCoroutine(TimeTracker());
        //PLAY SOUND
    }
    void Update()
    {
        float distCovered = (Time.time - startTime) * speed / 10;
        float distCoveredplay = (Time.time - startTime) * speed / 10;
        float fracJourney = distCovered / journeyLength;
        float fracJourney2 = distCovered / journeyLength2;
        float fracJourney3 = distCoveredplay / journeyLength3;
        float fracJourney4 = distCoveredplay / journeyLength4;
        quad1.transform.position = Vector3.Lerp(quad1.transform.position, new Vector3(-16.55f, -0.013f, 0), fracJourney);
        quad2.transform.position = Vector3.Lerp(quad2.transform.position, new Vector3(16.42f, -0.016f, 0f), fracJourney2);
        dog.transform.position = Vector3.Lerp(dog.transform.position, new Vector3(-4f, 5.82f, 0.5f), fracJourney3);
        guy.transform.position = Vector3.Lerp(guy.transform.position, new Vector3(6.64f, -2.01f, 0.5f), fracJourney4*2);
    }
    IEnumerator TimeTracker()
    {
        while(i <= 4)
        {
            i++;
            yield return new WaitForSeconds(1);
        }
        isDone = true;
    }
}
