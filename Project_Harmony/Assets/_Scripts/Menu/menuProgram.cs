﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

namespace Harmony
{
    public class menuProgram : MonoBehaviour
    {
        public TMP_Text startText;
        public GameObject doggoHuntBTN;
        private string str1;
        private string str2;


        void Start()
        {
            str1 = "         WELCOME TO GejrOS, logged in with root\n>_";
            str2 = "         WELCOME TO GejrOS, logged in with root\n>";
            doggoHuntBTN.SetActive(false);
            StartCoroutine(Loading());
        }
        private void Update()
        {
            if (PlayerPrefs.GetInt("Completed") == 2)
            {
                str1 = "         WELCOME TO GejrOS, logged in with root\n>module loaded: doggo-game\n>_";
                str2 = "         WELCOME TO GejrOS, logged in with root\n>module loaded: doggo-game\n>";
                doggoHuntBTN.SetActive(true);
            }
            if (UnityEngine.Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
        }

        IEnumerator Loading()
        {
            while (true)
            {
                startText.text = str1;
                yield return new WaitForSeconds(0.5f);
                startText.text = str2;
                yield return new WaitForSeconds(0.5f);
            }
        }

        public void StartGame()
        {
            SceneManager.LoadScene(1);
        }

        public void QuitGame()
        {
            Application.Quit();
        }

        public void DoggoHunt()
        {
            SceneManager.LoadScene(6);
        }
    }
}