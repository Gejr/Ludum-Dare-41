﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

namespace Harmony
{
    public class Program : MonoBehaviour
    {

        //private Type type;
        //private BootSeq bootSeq;
        //private Input input;

        public AudioSource bootSFX;
        public AudioSource messageSFX;
        public AudioSource leetSFX;

        private static float normTypeSpeed = 0.03f;
        public bool inCoroutine = false;
        public TMP_Text startText;
        int i;

        void Start()
        {
            //type = GameObject.FindGameObjectWithTag("GameManager").GetComponent<Type>();
            //bootSeq = GameObject.FindGameObjectWithTag("GameManager").GetComponent<BootSeq>();
            //input = GameObject.FindGameObjectWithTag("GameManager").GetComponent<Input>();

            StartCoroutine(Loading());
        }

        private void Update()
        {
            if (UnityEngine.Input.GetKeyDown(KeyCode.Escape))
            {
                SceneManager.LoadScene(0);
            }
        }

        IEnumerator Loading()
        {
            yield return new WaitForSeconds(1.5f);
            Type.TypeText(BootSeq.BootSequence, startText, 0.001f);
            StartCoroutine(PlayBootSFX());
            while (inCoroutine == true)
            {
                yield return new WaitForSeconds(0.1f);
            }
            i = 0;
            while (i <= 3)
            {
                startText.text = "         WELCOME TO GejrOS, please login using root!\n>_";
                yield return new WaitForSeconds(0.5f);
                startText.text = "         WELCOME TO GejrOS, please login using root!\n>";
                yield return new WaitForSeconds(0.5f);
                i++;
            }
            messageSFX.Play();
            Type.TypeText(Input.a, startText, normTypeSpeed);
            while (inCoroutine == true)
            {
                yield return new WaitForSeconds(0.1f);
            }
            i = 0;
            while (i <= 5)
            {
                startText.text = ">UNK8WN@127.0.0.1 :: Hello?\n>UNK8WN@127.0.0.1 :: Is anybody there?\n>_";
                yield return new WaitForSeconds(0.5f);
                startText.text = ">UNK8WN@127.0.0.1 :: Hello?\n>UNK8WN@127.0.0.1 :: Is anybody there?\n>";
                yield return new WaitForSeconds(0.5f);
                i++;
            }
            messageSFX.Play();
            Type.TypeText(Input.b, startText, normTypeSpeed);
            while (inCoroutine == true)
            {
                yield return new WaitForSeconds(0.1f);
            }
            i = 0;
            while (i <= 7)
            {
                startText.text = ">UNK8WN@127.0.0.1 :: Fine don't answer me...\n>UNK8WN@127.0.0.1 :: I do however need you to do me a favor\n>UNK8WN@127.0.0.1 :: If you don't I will inform the police that you stole this computer\n>UNK8WN@127.0.0.1 :: Understand?\n>UNK8WN@127.0.0.1 :: Good, now I need you to hack this computer, I have uploaded a crack script for you\n>UNK8WN@127.0.0.1 :: So it should be simple enough.\n>\n>UNK8WN@127.0.0.1 :: All you need to do is to defeat the dog, eazy!\n>_";
                yield return new WaitForSeconds(0.5f);
                startText.text = ">UNK8WN@127.0.0.1 :: Fine don't answer me...\n>UNK8WN@127.0.0.1 :: I do however need you to do me a favor\n>UNK8WN@127.0.0.1 :: If you don't I will inform the police that you stole this computer\n>UNK8WN@127.0.0.1 :: Understand?\n>UNK8WN@127.0.0.1 :: Good, now I need you to hack this computer, I have uploaded a crack script for you\n>UNK8WN@127.0.0.1 :: So it should be simple enough.\n>\n>UNK8WN@127.0.0.1 :: All you need to do is to defeat the dog, eazy!\n>";
                yield return new WaitForSeconds(0.5f);
                i++;
            }
            i = 0;
            while (i < 1)
            {
                startText.text = ">UNK8WN@127.0.0.1 :: Fine don't answer me...\n>UNK8WN@127.0.0.1 :: I do however need you to do me a favor\n>UNK8WN@127.0.0.1 :: If you don't I will inform the police that you stole this computer\n>UNK8WN@127.0.0.1 :: Understand?\n>UNK8WN@127.0.0.1 :: Good, now I need you to hack this computer, I have uploaded a crack script for you\n>UNK8WN@127.0.0.1 :: So it should be simple enough.\n>\n>UNK8WN@127.0.0.1 :: All you need to do is to defeat the dog, eazy!\n>c";
                yield return new WaitForSeconds(0.3f);
                startText.text = ">UNK8WN@127.0.0.1 :: Fine don't answer me...\n>UNK8WN@127.0.0.1 :: I do however need you to do me a favor\n>UNK8WN@127.0.0.1 :: If you don't I will inform the police that you stole this computer\n>UNK8WN@127.0.0.1 :: Understand?\n>UNK8WN@127.0.0.1 :: Good, now I need you to hack this computer, I have uploaded a crack script for you\n>UNK8WN@127.0.0.1 :: So it should be simple enough.\n>\n>UNK8WN@127.0.0.1 :: All you need to do is to defeat the dog, eazy!\n>cl";
                yield return new WaitForSeconds(0.3f);
                startText.text = ">UNK8WN@127.0.0.1 :: Fine don't answer me...\n>UNK8WN@127.0.0.1 :: I do however need you to do me a favor\n>UNK8WN@127.0.0.1 :: If you don't I will inform the police that you stole this computer\n>UNK8WN@127.0.0.1 :: Understand?\n>UNK8WN@127.0.0.1 :: Good, now I need you to hack this computer, I have uploaded a crack script for you\n>UNK8WN@127.0.0.1 :: So it should be simple enough.\n>\n>UNK8WN@127.0.0.1 :: All you need to do is to defeat the dog, eazy!\n>cla";
                yield return new WaitForSeconds(0.3f);
                startText.text = ">UNK8WN@127.0.0.1 :: Fine don't answer me...\n>UNK8WN@127.0.0.1 :: I do however need you to do me a favor\n>UNK8WN@127.0.0.1 :: If you don't I will inform the police that you stole this computer\n>UNK8WN@127.0.0.1 :: Understand?\n>UNK8WN@127.0.0.1 :: Good, now I need you to hack this computer, I have uploaded a crack script for you\n>UNK8WN@127.0.0.1 :: So it should be simple enough.\n>\n>UNK8WN@127.0.0.1 :: All you need to do is to defeat the dog, eazy!\n>clae";
                yield return new WaitForSeconds(0.3f);
                startText.text = ">UNK8WN@127.0.0.1 :: Fine don't answer me...\n>UNK8WN@127.0.0.1 :: I do however need you to do me a favor\n>UNK8WN@127.0.0.1 :: If you don't I will inform the police that you stole this computer\n>UNK8WN@127.0.0.1 :: Understand?\n>UNK8WN@127.0.0.1 :: Good, now I need you to hack this computer, I have uploaded a crack script for you\n>UNK8WN@127.0.0.1 :: So it should be simple enough.\n>\n>UNK8WN@127.0.0.1 :: All you need to do is to defeat the dog, eazy!\n>cla";
                yield return new WaitForSeconds(0.3f);
                startText.text = ">UNK8WN@127.0.0.1 :: Fine don't answer me...\n>UNK8WN@127.0.0.1 :: I do however need you to do me a favor\n>UNK8WN@127.0.0.1 :: If you don't I will inform the police that you stole this computer\n>UNK8WN@127.0.0.1 :: Understand?\n>UNK8WN@127.0.0.1 :: Good, now I need you to hack this computer, I have uploaded a crack script for you\n>UNK8WN@127.0.0.1 :: So it should be simple enough.\n>\n>UNK8WN@127.0.0.1 :: All you need to do is to defeat the dog, eazy!\n>cl";
                yield return new WaitForSeconds(0.3f);
                startText.text = ">UNK8WN@127.0.0.1 :: Fine don't answer me...\n>UNK8WN@127.0.0.1 :: I do however need you to do me a favor\n>UNK8WN@127.0.0.1 :: If you don't I will inform the police that you stole this computer\n>UNK8WN@127.0.0.1 :: Understand?\n>UNK8WN@127.0.0.1 :: Good, now I need you to hack this computer, I have uploaded a crack script for you\n>UNK8WN@127.0.0.1 :: So it should be simple enough.\n>\n>UNK8WN@127.0.0.1 :: All you need to do is to defeat the dog, eazy!\n>cle";
                yield return new WaitForSeconds(0.3f);
                startText.text = ">UNK8WN@127.0.0.1 :: Fine don't answer me...\n>UNK8WN@127.0.0.1 :: I do however need you to do me a favor\n>UNK8WN@127.0.0.1 :: If you don't I will inform the police that you stole this computer\n>UNK8WN@127.0.0.1 :: Understand?\n>UNK8WN@127.0.0.1 :: Good, now I need you to hack this computer, I have uploaded a crack script for you\n>UNK8WN@127.0.0.1 :: So it should be simple enough.\n>\n>UNK8WN@127.0.0.1 :: All you need to do is to defeat the dog, eazy!\n>clea";
                yield return new WaitForSeconds(0.3f);
                startText.text = ">UNK8WN@127.0.0.1 :: Fine don't answer me...\n>UNK8WN@127.0.0.1 :: I do however need you to do me a favor\n>UNK8WN@127.0.0.1 :: If you don't I will inform the police that you stole this computer\n>UNK8WN@127.0.0.1 :: Understand?\n>UNK8WN@127.0.0.1 :: Good, now I need you to hack this computer, I have uploaded a crack script for you\n>UNK8WN@127.0.0.1 :: So it should be simple enough.\n>\n>UNK8WN@127.0.0.1 :: All you need to do is to defeat the dog, eazy!\n>clear";
                yield return new WaitForSeconds(0.3f);
                startText.text = ">_";
                yield return new WaitForSeconds(0.5f);
                startText.text = ">";
                yield return new WaitForSeconds(0.5f);
                startText.text = ">_";
                yield return new WaitForSeconds(0.5f);
                startText.text = ">";
                i++;
            }
            leetSFX.Play();
            Type.TypeText(Input.c, startText, 0.5f);
            while (inCoroutine == true)
            {
                yield return new WaitForSeconds(0.1f);
            }
            SceneManager.LoadScene(2);
        }

        IEnumerator PlayBootSFX()
        {
            yield return new WaitForSeconds(14);
            bootSFX.Play();
        }
    }
}