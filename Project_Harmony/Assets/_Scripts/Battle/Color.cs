﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Color : MonoBehaviour {

    public Slider slider;
    public Image img;
    public Color32 col;
    public float val;
    private void Start()
    {
        col = new Color32(20, 229, 106, 255);
    }
    // Update is called once per frame
    void Update () {
        val = slider.value;
        if(slider.value <= 1f)
        {
            col = new Color32(20, 229, 106, 255);
            img.color = col;
        }
		if(slider.value <= 0.4f)
        {
            col = new Color32(219, 209, 73, 255);
            img.color = col;
        }
        if (slider.value <= 0.2f)
        {
            col = new Color32(205, 32, 34, 255);
            img.color = col;
        }
    }
}
