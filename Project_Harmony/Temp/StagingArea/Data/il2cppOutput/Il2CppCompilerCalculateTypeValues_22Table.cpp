﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Anim
struct Anim_t271495542;
// Animimp
struct Animimp_t3408691587;
// Animinf
struct Animinf_t989049961;
// Battle
struct Battle_t2191861408;
// Battleimp
struct Battleimp_t158148094;
// Battleinf
struct Battleinf_t1732126188;
// Harmony.Program
struct Program_t1866495201;
// Harmony.Program2
struct Program2_t3485103329;
// Harmony.Program3
struct Program3_t1528788193;
// Harmony.menuProgram
struct menuProgram_t465836879;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule>
struct List_1_t3418373063;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.DelegateData
struct DelegateData_t1677132599;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Random
struct Random_t108471755;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Void
struct Void_t1185182177;
// TMPro.TMP_Text
struct TMP_Text_t2599618874;
// UnityEngine.Analytics.EventTrigger/OnTrigger
struct OnTrigger_t4184125570;
// UnityEngine.Analytics.TrackableField
struct TrackableField_t1772682203;
// UnityEngine.Analytics.TriggerListContainer
struct TriggerListContainer_t2032715483;
// UnityEngine.Analytics.TriggerMethod
struct TriggerMethod_t582536534;
// UnityEngine.Analytics.ValueProperty
struct ValueProperty_t1868393739;
// UnityEngine.Animator
struct Animator_t434523843;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.Slider
struct Slider_t3903728902;




#ifndef U3CMODULEU3E_T692745545_H
#define U3CMODULEU3E_T692745545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745545 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745545_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CTIMETRACKERU3EC__ITERATOR0_T1744683265_H
#define U3CTIMETRACKERU3EC__ITERATOR0_T1744683265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Anim/<TimeTracker>c__Iterator0
struct  U3CTimeTrackerU3Ec__Iterator0_t1744683265  : public RuntimeObject
{
public:
	// Anim Anim/<TimeTracker>c__Iterator0::$this
	Anim_t271495542 * ___U24this_0;
	// System.Object Anim/<TimeTracker>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Anim/<TimeTracker>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Anim/<TimeTracker>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CTimeTrackerU3Ec__Iterator0_t1744683265, ___U24this_0)); }
	inline Anim_t271495542 * get_U24this_0() const { return ___U24this_0; }
	inline Anim_t271495542 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Anim_t271495542 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CTimeTrackerU3Ec__Iterator0_t1744683265, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CTimeTrackerU3Ec__Iterator0_t1744683265, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CTimeTrackerU3Ec__Iterator0_t1744683265, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTIMETRACKERU3EC__ITERATOR0_T1744683265_H
#ifndef U3CTIMETRACKERU3EC__ITERATOR0_T3157006910_H
#define U3CTIMETRACKERU3EC__ITERATOR0_T3157006910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Animimp/<TimeTracker>c__Iterator0
struct  U3CTimeTrackerU3Ec__Iterator0_t3157006910  : public RuntimeObject
{
public:
	// Animimp Animimp/<TimeTracker>c__Iterator0::$this
	Animimp_t3408691587 * ___U24this_0;
	// System.Object Animimp/<TimeTracker>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Animimp/<TimeTracker>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Animimp/<TimeTracker>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CTimeTrackerU3Ec__Iterator0_t3157006910, ___U24this_0)); }
	inline Animimp_t3408691587 * get_U24this_0() const { return ___U24this_0; }
	inline Animimp_t3408691587 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Animimp_t3408691587 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CTimeTrackerU3Ec__Iterator0_t3157006910, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CTimeTrackerU3Ec__Iterator0_t3157006910, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CTimeTrackerU3Ec__Iterator0_t3157006910, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTIMETRACKERU3EC__ITERATOR0_T3157006910_H
#ifndef U3CTIMETRACKERU3EC__ITERATOR0_T1213291940_H
#define U3CTIMETRACKERU3EC__ITERATOR0_T1213291940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Animinf/<TimeTracker>c__Iterator0
struct  U3CTimeTrackerU3Ec__Iterator0_t1213291940  : public RuntimeObject
{
public:
	// Animinf Animinf/<TimeTracker>c__Iterator0::$this
	Animinf_t989049961 * ___U24this_0;
	// System.Object Animinf/<TimeTracker>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Animinf/<TimeTracker>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Animinf/<TimeTracker>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CTimeTrackerU3Ec__Iterator0_t1213291940, ___U24this_0)); }
	inline Animinf_t989049961 * get_U24this_0() const { return ___U24this_0; }
	inline Animinf_t989049961 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Animinf_t989049961 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CTimeTrackerU3Ec__Iterator0_t1213291940, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CTimeTrackerU3Ec__Iterator0_t1213291940, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CTimeTrackerU3Ec__Iterator0_t1213291940, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTIMETRACKERU3EC__ITERATOR0_T1213291940_H
#ifndef U3CCONTINUEU3EC__ITERATOR1_T642798636_H
#define U3CCONTINUEU3EC__ITERATOR1_T642798636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Battle/<Continue>c__Iterator1
struct  U3CContinueU3Ec__Iterator1_t642798636  : public RuntimeObject
{
public:
	// Battle Battle/<Continue>c__Iterator1::$this
	Battle_t2191861408 * ___U24this_0;
	// System.Object Battle/<Continue>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Battle/<Continue>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 Battle/<Continue>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CContinueU3Ec__Iterator1_t642798636, ___U24this_0)); }
	inline Battle_t2191861408 * get_U24this_0() const { return ___U24this_0; }
	inline Battle_t2191861408 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Battle_t2191861408 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CContinueU3Ec__Iterator1_t642798636, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CContinueU3Ec__Iterator1_t642798636, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CContinueU3Ec__Iterator1_t642798636, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONTINUEU3EC__ITERATOR1_T642798636_H
#ifndef U3CDIEU3EC__ITERATOR2_T558276709_H
#define U3CDIEU3EC__ITERATOR2_T558276709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Battle/<DIe>c__Iterator2
struct  U3CDIeU3Ec__Iterator2_t558276709  : public RuntimeObject
{
public:
	// Battle Battle/<DIe>c__Iterator2::$this
	Battle_t2191861408 * ___U24this_0;
	// System.Object Battle/<DIe>c__Iterator2::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Battle/<DIe>c__Iterator2::$disposing
	bool ___U24disposing_2;
	// System.Int32 Battle/<DIe>c__Iterator2::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CDIeU3Ec__Iterator2_t558276709, ___U24this_0)); }
	inline Battle_t2191861408 * get_U24this_0() const { return ___U24this_0; }
	inline Battle_t2191861408 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Battle_t2191861408 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDIeU3Ec__Iterator2_t558276709, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDIeU3Ec__Iterator2_t558276709, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDIeU3Ec__Iterator2_t558276709, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDIEU3EC__ITERATOR2_T558276709_H
#ifndef U3CENEMYATTACKU3EC__ITERATOR0_T1682970516_H
#define U3CENEMYATTACKU3EC__ITERATOR0_T1682970516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Battle/<EnemyAttack>c__Iterator0
struct  U3CEnemyAttackU3Ec__Iterator0_t1682970516  : public RuntimeObject
{
public:
	// System.Single Battle/<EnemyAttack>c__Iterator0::<rndDMG>__1
	float ___U3CrndDMGU3E__1_0;
	// Battle Battle/<EnemyAttack>c__Iterator0::$this
	Battle_t2191861408 * ___U24this_1;
	// System.Object Battle/<EnemyAttack>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Battle/<EnemyAttack>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Battle/<EnemyAttack>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrndDMGU3E__1_0() { return static_cast<int32_t>(offsetof(U3CEnemyAttackU3Ec__Iterator0_t1682970516, ___U3CrndDMGU3E__1_0)); }
	inline float get_U3CrndDMGU3E__1_0() const { return ___U3CrndDMGU3E__1_0; }
	inline float* get_address_of_U3CrndDMGU3E__1_0() { return &___U3CrndDMGU3E__1_0; }
	inline void set_U3CrndDMGU3E__1_0(float value)
	{
		___U3CrndDMGU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CEnemyAttackU3Ec__Iterator0_t1682970516, ___U24this_1)); }
	inline Battle_t2191861408 * get_U24this_1() const { return ___U24this_1; }
	inline Battle_t2191861408 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Battle_t2191861408 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CEnemyAttackU3Ec__Iterator0_t1682970516, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CEnemyAttackU3Ec__Iterator0_t1682970516, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CEnemyAttackU3Ec__Iterator0_t1682970516, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CENEMYATTACKU3EC__ITERATOR0_T1682970516_H
#ifndef U3CCONTINUEU3EC__ITERATOR1_T2046304316_H
#define U3CCONTINUEU3EC__ITERATOR1_T2046304316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Battleimp/<Continue>c__Iterator1
struct  U3CContinueU3Ec__Iterator1_t2046304316  : public RuntimeObject
{
public:
	// Battleimp Battleimp/<Continue>c__Iterator1::$this
	Battleimp_t158148094 * ___U24this_0;
	// System.Object Battleimp/<Continue>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Battleimp/<Continue>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 Battleimp/<Continue>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CContinueU3Ec__Iterator1_t2046304316, ___U24this_0)); }
	inline Battleimp_t158148094 * get_U24this_0() const { return ___U24this_0; }
	inline Battleimp_t158148094 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Battleimp_t158148094 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CContinueU3Ec__Iterator1_t2046304316, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CContinueU3Ec__Iterator1_t2046304316, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CContinueU3Ec__Iterator1_t2046304316, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONTINUEU3EC__ITERATOR1_T2046304316_H
#ifndef U3CENEMYATTACKU3EC__ITERATOR0_T889231986_H
#define U3CENEMYATTACKU3EC__ITERATOR0_T889231986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Battleimp/<EnemyAttack>c__Iterator0
struct  U3CEnemyAttackU3Ec__Iterator0_t889231986  : public RuntimeObject
{
public:
	// System.Single Battleimp/<EnemyAttack>c__Iterator0::<rndDMG>__1
	float ___U3CrndDMGU3E__1_0;
	// Battleimp Battleimp/<EnemyAttack>c__Iterator0::$this
	Battleimp_t158148094 * ___U24this_1;
	// System.Object Battleimp/<EnemyAttack>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Battleimp/<EnemyAttack>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Battleimp/<EnemyAttack>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrndDMGU3E__1_0() { return static_cast<int32_t>(offsetof(U3CEnemyAttackU3Ec__Iterator0_t889231986, ___U3CrndDMGU3E__1_0)); }
	inline float get_U3CrndDMGU3E__1_0() const { return ___U3CrndDMGU3E__1_0; }
	inline float* get_address_of_U3CrndDMGU3E__1_0() { return &___U3CrndDMGU3E__1_0; }
	inline void set_U3CrndDMGU3E__1_0(float value)
	{
		___U3CrndDMGU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CEnemyAttackU3Ec__Iterator0_t889231986, ___U24this_1)); }
	inline Battleimp_t158148094 * get_U24this_1() const { return ___U24this_1; }
	inline Battleimp_t158148094 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Battleimp_t158148094 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CEnemyAttackU3Ec__Iterator0_t889231986, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CEnemyAttackU3Ec__Iterator0_t889231986, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CEnemyAttackU3Ec__Iterator0_t889231986, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CENEMYATTACKU3EC__ITERATOR0_T889231986_H
#ifndef U3CCONTINUEU3EC__ITERATOR1_T1090546628_H
#define U3CCONTINUEU3EC__ITERATOR1_T1090546628_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Battleinf/<Continue>c__Iterator1
struct  U3CContinueU3Ec__Iterator1_t1090546628  : public RuntimeObject
{
public:
	// Battleinf Battleinf/<Continue>c__Iterator1::$this
	Battleinf_t1732126188 * ___U24this_0;
	// System.Object Battleinf/<Continue>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Battleinf/<Continue>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 Battleinf/<Continue>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CContinueU3Ec__Iterator1_t1090546628, ___U24this_0)); }
	inline Battleinf_t1732126188 * get_U24this_0() const { return ___U24this_0; }
	inline Battleinf_t1732126188 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Battleinf_t1732126188 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CContinueU3Ec__Iterator1_t1090546628, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CContinueU3Ec__Iterator1_t1090546628, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CContinueU3Ec__Iterator1_t1090546628, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONTINUEU3EC__ITERATOR1_T1090546628_H
#ifndef U3CDIEU3EC__ITERATOR2_T3004093797_H
#define U3CDIEU3EC__ITERATOR2_T3004093797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Battleinf/<DIe>c__Iterator2
struct  U3CDIeU3Ec__Iterator2_t3004093797  : public RuntimeObject
{
public:
	// Battleinf Battleinf/<DIe>c__Iterator2::$this
	Battleinf_t1732126188 * ___U24this_0;
	// System.Object Battleinf/<DIe>c__Iterator2::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Battleinf/<DIe>c__Iterator2::$disposing
	bool ___U24disposing_2;
	// System.Int32 Battleinf/<DIe>c__Iterator2::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CDIeU3Ec__Iterator2_t3004093797, ___U24this_0)); }
	inline Battleinf_t1732126188 * get_U24this_0() const { return ___U24this_0; }
	inline Battleinf_t1732126188 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Battleinf_t1732126188 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDIeU3Ec__Iterator2_t3004093797, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDIeU3Ec__Iterator2_t3004093797, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDIeU3Ec__Iterator2_t3004093797, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDIEU3EC__ITERATOR2_T3004093797_H
#ifndef U3CENEMYATTACKU3EC__ITERATOR0_T2884124416_H
#define U3CENEMYATTACKU3EC__ITERATOR0_T2884124416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Battleinf/<EnemyAttack>c__Iterator0
struct  U3CEnemyAttackU3Ec__Iterator0_t2884124416  : public RuntimeObject
{
public:
	// System.Single Battleinf/<EnemyAttack>c__Iterator0::<rndDMG>__1
	float ___U3CrndDMGU3E__1_0;
	// Battleinf Battleinf/<EnemyAttack>c__Iterator0::$this
	Battleinf_t1732126188 * ___U24this_1;
	// System.Object Battleinf/<EnemyAttack>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Battleinf/<EnemyAttack>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Battleinf/<EnemyAttack>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrndDMGU3E__1_0() { return static_cast<int32_t>(offsetof(U3CEnemyAttackU3Ec__Iterator0_t2884124416, ___U3CrndDMGU3E__1_0)); }
	inline float get_U3CrndDMGU3E__1_0() const { return ___U3CrndDMGU3E__1_0; }
	inline float* get_address_of_U3CrndDMGU3E__1_0() { return &___U3CrndDMGU3E__1_0; }
	inline void set_U3CrndDMGU3E__1_0(float value)
	{
		___U3CrndDMGU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CEnemyAttackU3Ec__Iterator0_t2884124416, ___U24this_1)); }
	inline Battleinf_t1732126188 * get_U24this_1() const { return ___U24this_1; }
	inline Battleinf_t1732126188 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Battleinf_t1732126188 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CEnemyAttackU3Ec__Iterator0_t2884124416, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CEnemyAttackU3Ec__Iterator0_t2884124416, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CEnemyAttackU3Ec__Iterator0_t2884124416, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CENEMYATTACKU3EC__ITERATOR0_T2884124416_H
#ifndef U3CLOADINGU3EC__ITERATOR0_T3071600265_H
#define U3CLOADINGU3EC__ITERATOR0_T3071600265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Program/<Loading>c__Iterator0
struct  U3CLoadingU3Ec__Iterator0_t3071600265  : public RuntimeObject
{
public:
	// Harmony.Program Harmony.Program/<Loading>c__Iterator0::$this
	Program_t1866495201 * ___U24this_0;
	// System.Object Harmony.Program/<Loading>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Harmony.Program/<Loading>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Harmony.Program/<Loading>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t3071600265, ___U24this_0)); }
	inline Program_t1866495201 * get_U24this_0() const { return ___U24this_0; }
	inline Program_t1866495201 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Program_t1866495201 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t3071600265, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t3071600265, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t3071600265, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADINGU3EC__ITERATOR0_T3071600265_H
#ifndef U3CPLAYBOOTSFXU3EC__ITERATOR1_T3195710289_H
#define U3CPLAYBOOTSFXU3EC__ITERATOR1_T3195710289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Program/<PlayBootSFX>c__Iterator1
struct  U3CPlayBootSFXU3Ec__Iterator1_t3195710289  : public RuntimeObject
{
public:
	// Harmony.Program Harmony.Program/<PlayBootSFX>c__Iterator1::$this
	Program_t1866495201 * ___U24this_0;
	// System.Object Harmony.Program/<PlayBootSFX>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Harmony.Program/<PlayBootSFX>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 Harmony.Program/<PlayBootSFX>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CPlayBootSFXU3Ec__Iterator1_t3195710289, ___U24this_0)); }
	inline Program_t1866495201 * get_U24this_0() const { return ___U24this_0; }
	inline Program_t1866495201 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Program_t1866495201 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CPlayBootSFXU3Ec__Iterator1_t3195710289, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CPlayBootSFXU3Ec__Iterator1_t3195710289, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CPlayBootSFXU3Ec__Iterator1_t3195710289, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPLAYBOOTSFXU3EC__ITERATOR1_T3195710289_H
#ifndef U3CLOADINGU3EC__ITERATOR0_T2822561722_H
#define U3CLOADINGU3EC__ITERATOR0_T2822561722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Program2/<Loading>c__Iterator0
struct  U3CLoadingU3Ec__Iterator0_t2822561722  : public RuntimeObject
{
public:
	// Harmony.Program2 Harmony.Program2/<Loading>c__Iterator0::$this
	Program2_t3485103329 * ___U24this_0;
	// System.Object Harmony.Program2/<Loading>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Harmony.Program2/<Loading>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Harmony.Program2/<Loading>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t2822561722, ___U24this_0)); }
	inline Program2_t3485103329 * get_U24this_0() const { return ___U24this_0; }
	inline Program2_t3485103329 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Program2_t3485103329 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t2822561722, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t2822561722, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t2822561722, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADINGU3EC__ITERATOR0_T2822561722_H
#ifndef U3CLOADINGU3EC__ITERATOR0_T2437702791_H
#define U3CLOADINGU3EC__ITERATOR0_T2437702791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Program3/<Loading>c__Iterator0
struct  U3CLoadingU3Ec__Iterator0_t2437702791  : public RuntimeObject
{
public:
	// Harmony.Program3 Harmony.Program3/<Loading>c__Iterator0::$this
	Program3_t1528788193 * ___U24this_0;
	// System.Object Harmony.Program3/<Loading>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Harmony.Program3/<Loading>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Harmony.Program3/<Loading>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t2437702791, ___U24this_0)); }
	inline Program3_t1528788193 * get_U24this_0() const { return ___U24this_0; }
	inline Program3_t1528788193 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Program3_t1528788193 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t2437702791, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t2437702791, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t2437702791, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADINGU3EC__ITERATOR0_T2437702791_H
#ifndef U3CANIMATETEXTU3EC__ITERATOR0_T429204900_H
#define U3CANIMATETEXTU3EC__ITERATOR0_T429204900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Type/<AnimateText>c__Iterator0
struct  U3CAnimateTextU3Ec__Iterator0_t429204900  : public RuntimeObject
{
public:
	// System.Int32 Harmony.Type/<AnimateText>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// System.String Harmony.Type/<AnimateText>c__Iterator0::message
	String_t* ___message_1;
	// TMPro.TMP_Text Harmony.Type/<AnimateText>c__Iterator0::textDes
	TMP_Text_t2599618874 * ___textDes_2;
	// System.Single Harmony.Type/<AnimateText>c__Iterator0::speed
	float ___speed_3;
	// System.Object Harmony.Type/<AnimateText>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Harmony.Type/<AnimateText>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Harmony.Type/<AnimateText>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t429204900, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t429204900, ___message_1)); }
	inline String_t* get_message_1() const { return ___message_1; }
	inline String_t** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(String_t* value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier((&___message_1), value);
	}

	inline static int32_t get_offset_of_textDes_2() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t429204900, ___textDes_2)); }
	inline TMP_Text_t2599618874 * get_textDes_2() const { return ___textDes_2; }
	inline TMP_Text_t2599618874 ** get_address_of_textDes_2() { return &___textDes_2; }
	inline void set_textDes_2(TMP_Text_t2599618874 * value)
	{
		___textDes_2 = value;
		Il2CppCodeGenWriteBarrier((&___textDes_2), value);
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t429204900, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t429204900, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t429204900, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t429204900, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATETEXTU3EC__ITERATOR0_T429204900_H
#ifndef U3CANIMATETEXTU3EC__ITERATOR0_T589720723_H
#define U3CANIMATETEXTU3EC__ITERATOR0_T589720723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Type2/<AnimateText>c__Iterator0
struct  U3CAnimateTextU3Ec__Iterator0_t589720723  : public RuntimeObject
{
public:
	// System.Int32 Harmony.Type2/<AnimateText>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// System.String Harmony.Type2/<AnimateText>c__Iterator0::message
	String_t* ___message_1;
	// TMPro.TMP_Text Harmony.Type2/<AnimateText>c__Iterator0::textDes
	TMP_Text_t2599618874 * ___textDes_2;
	// System.Single Harmony.Type2/<AnimateText>c__Iterator0::speed
	float ___speed_3;
	// System.Object Harmony.Type2/<AnimateText>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Harmony.Type2/<AnimateText>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Harmony.Type2/<AnimateText>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t589720723, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t589720723, ___message_1)); }
	inline String_t* get_message_1() const { return ___message_1; }
	inline String_t** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(String_t* value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier((&___message_1), value);
	}

	inline static int32_t get_offset_of_textDes_2() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t589720723, ___textDes_2)); }
	inline TMP_Text_t2599618874 * get_textDes_2() const { return ___textDes_2; }
	inline TMP_Text_t2599618874 ** get_address_of_textDes_2() { return &___textDes_2; }
	inline void set_textDes_2(TMP_Text_t2599618874 * value)
	{
		___textDes_2 = value;
		Il2CppCodeGenWriteBarrier((&___textDes_2), value);
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t589720723, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t589720723, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t589720723, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t589720723, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATETEXTU3EC__ITERATOR0_T589720723_H
#ifndef U3CANIMATETEXTU3EC__ITERATOR0_T772139121_H
#define U3CANIMATETEXTU3EC__ITERATOR0_T772139121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Type3/<AnimateText>c__Iterator0
struct  U3CAnimateTextU3Ec__Iterator0_t772139121  : public RuntimeObject
{
public:
	// System.Int32 Harmony.Type3/<AnimateText>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// System.String Harmony.Type3/<AnimateText>c__Iterator0::message
	String_t* ___message_1;
	// TMPro.TMP_Text Harmony.Type3/<AnimateText>c__Iterator0::textDes
	TMP_Text_t2599618874 * ___textDes_2;
	// System.Single Harmony.Type3/<AnimateText>c__Iterator0::speed
	float ___speed_3;
	// System.Object Harmony.Type3/<AnimateText>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Harmony.Type3/<AnimateText>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Harmony.Type3/<AnimateText>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t772139121, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t772139121, ___message_1)); }
	inline String_t* get_message_1() const { return ___message_1; }
	inline String_t** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(String_t* value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier((&___message_1), value);
	}

	inline static int32_t get_offset_of_textDes_2() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t772139121, ___textDes_2)); }
	inline TMP_Text_t2599618874 * get_textDes_2() const { return ___textDes_2; }
	inline TMP_Text_t2599618874 ** get_address_of_textDes_2() { return &___textDes_2; }
	inline void set_textDes_2(TMP_Text_t2599618874 * value)
	{
		___textDes_2 = value;
		Il2CppCodeGenWriteBarrier((&___textDes_2), value);
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t772139121, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t772139121, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t772139121, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t772139121, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATETEXTU3EC__ITERATOR0_T772139121_H
#ifndef U3CLOADINGU3EC__ITERATOR0_T2464126437_H
#define U3CLOADINGU3EC__ITERATOR0_T2464126437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.menuProgram/<Loading>c__Iterator0
struct  U3CLoadingU3Ec__Iterator0_t2464126437  : public RuntimeObject
{
public:
	// Harmony.menuProgram Harmony.menuProgram/<Loading>c__Iterator0::$this
	menuProgram_t465836879 * ___U24this_0;
	// System.Object Harmony.menuProgram/<Loading>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Harmony.menuProgram/<Loading>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Harmony.menuProgram/<Loading>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t2464126437, ___U24this_0)); }
	inline menuProgram_t465836879 * get_U24this_0() const { return ___U24this_0; }
	inline menuProgram_t465836879 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(menuProgram_t465836879 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t2464126437, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t2464126437, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t2464126437, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADINGU3EC__ITERATOR0_T2464126437_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef TRACKABLETRIGGER_T621205209_H
#define TRACKABLETRIGGER_T621205209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableTrigger
struct  TrackableTrigger_t621205209  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityEngine.Analytics.TrackableTrigger::m_Target
	GameObject_t1113636619 * ___m_Target_0;
	// System.String UnityEngine.Analytics.TrackableTrigger::m_MethodPath
	String_t* ___m_MethodPath_1;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_Target_0)); }
	inline GameObject_t1113636619 * get_m_Target_0() const { return ___m_Target_0; }
	inline GameObject_t1113636619 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(GameObject_t1113636619 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_MethodPath_1() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_MethodPath_1)); }
	inline String_t* get_m_MethodPath_1() const { return ___m_MethodPath_1; }
	inline String_t** get_address_of_m_MethodPath_1() { return &___m_MethodPath_1; }
	inline void set_m_MethodPath_1(String_t* value)
	{
		___m_MethodPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_MethodPath_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLETRIGGER_T621205209_H
#ifndef TRIGGERLISTCONTAINER_T2032715483_H
#define TRIGGERLISTCONTAINER_T2032715483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerListContainer
struct  TriggerListContainer_t2032715483  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule> UnityEngine.Analytics.TriggerListContainer::m_Rules
	List_1_t3418373063 * ___m_Rules_0;

public:
	inline static int32_t get_offset_of_m_Rules_0() { return static_cast<int32_t>(offsetof(TriggerListContainer_t2032715483, ___m_Rules_0)); }
	inline List_1_t3418373063 * get_m_Rules_0() const { return ___m_Rules_0; }
	inline List_1_t3418373063 ** get_address_of_m_Rules_0() { return &___m_Rules_0; }
	inline void set_m_Rules_0(List_1_t3418373063 * value)
	{
		___m_Rules_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLISTCONTAINER_T2032715483_H
#ifndef TRIGGERMETHOD_T582536534_H
#define TRIGGERMETHOD_T582536534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerMethod
struct  TriggerMethod_t582536534  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERMETHOD_T582536534_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef COLOR32_T2600501292_H
#define COLOR32_T2600501292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t2600501292 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T2600501292_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef TRIGGERBOOL_T501031542_H
#define TRIGGERBOOL_T501031542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerBool
struct  TriggerBool_t501031542 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerBool::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerBool_t501031542, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERBOOL_T501031542_H
#ifndef TRIGGERLIFECYCLEEVENT_T3193146760_H
#define TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerLifecycleEvent
struct  TriggerLifecycleEvent_t3193146760 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerLifecycleEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerLifecycleEvent_t3193146760, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifndef TRIGGEROPERATOR_T3611898925_H
#define TRIGGEROPERATOR_T3611898925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerOperator
struct  TriggerOperator_t3611898925 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerOperator::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerOperator_t3611898925, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGEROPERATOR_T3611898925_H
#ifndef TRIGGERTYPE_T105272677_H
#define TRIGGERTYPE_T105272677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerType
struct  TriggerType_t105272677 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerType_t105272677, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERTYPE_T105272677_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef EVENTTRIGGER_T2527451695_H
#define EVENTTRIGGER_T2527451695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger
struct  EventTrigger_t2527451695  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_IsTriggerExpanded
	bool ___m_IsTriggerExpanded_0;
	// UnityEngine.Analytics.TriggerType UnityEngine.Analytics.EventTrigger::m_Type
	int32_t ___m_Type_1;
	// UnityEngine.Analytics.TriggerLifecycleEvent UnityEngine.Analytics.EventTrigger::m_LifecycleEvent
	int32_t ___m_LifecycleEvent_2;
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_ApplyRules
	bool ___m_ApplyRules_3;
	// UnityEngine.Analytics.TriggerListContainer UnityEngine.Analytics.EventTrigger::m_Rules
	TriggerListContainer_t2032715483 * ___m_Rules_4;
	// UnityEngine.Analytics.TriggerBool UnityEngine.Analytics.EventTrigger::m_TriggerBool
	int32_t ___m_TriggerBool_5;
	// System.Single UnityEngine.Analytics.EventTrigger::m_InitTime
	float ___m_InitTime_6;
	// System.Single UnityEngine.Analytics.EventTrigger::m_RepeatTime
	float ___m_RepeatTime_7;
	// System.Int32 UnityEngine.Analytics.EventTrigger::m_Repetitions
	int32_t ___m_Repetitions_8;
	// System.Int32 UnityEngine.Analytics.EventTrigger::repetitionCount
	int32_t ___repetitionCount_9;
	// UnityEngine.Analytics.EventTrigger/OnTrigger UnityEngine.Analytics.EventTrigger::m_TriggerFunction
	OnTrigger_t4184125570 * ___m_TriggerFunction_10;
	// UnityEngine.Analytics.TriggerMethod UnityEngine.Analytics.EventTrigger::m_Method
	TriggerMethod_t582536534 * ___m_Method_11;

public:
	inline static int32_t get_offset_of_m_IsTriggerExpanded_0() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_IsTriggerExpanded_0)); }
	inline bool get_m_IsTriggerExpanded_0() const { return ___m_IsTriggerExpanded_0; }
	inline bool* get_address_of_m_IsTriggerExpanded_0() { return &___m_IsTriggerExpanded_0; }
	inline void set_m_IsTriggerExpanded_0(bool value)
	{
		___m_IsTriggerExpanded_0 = value;
	}

	inline static int32_t get_offset_of_m_Type_1() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Type_1)); }
	inline int32_t get_m_Type_1() const { return ___m_Type_1; }
	inline int32_t* get_address_of_m_Type_1() { return &___m_Type_1; }
	inline void set_m_Type_1(int32_t value)
	{
		___m_Type_1 = value;
	}

	inline static int32_t get_offset_of_m_LifecycleEvent_2() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_LifecycleEvent_2)); }
	inline int32_t get_m_LifecycleEvent_2() const { return ___m_LifecycleEvent_2; }
	inline int32_t* get_address_of_m_LifecycleEvent_2() { return &___m_LifecycleEvent_2; }
	inline void set_m_LifecycleEvent_2(int32_t value)
	{
		___m_LifecycleEvent_2 = value;
	}

	inline static int32_t get_offset_of_m_ApplyRules_3() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_ApplyRules_3)); }
	inline bool get_m_ApplyRules_3() const { return ___m_ApplyRules_3; }
	inline bool* get_address_of_m_ApplyRules_3() { return &___m_ApplyRules_3; }
	inline void set_m_ApplyRules_3(bool value)
	{
		___m_ApplyRules_3 = value;
	}

	inline static int32_t get_offset_of_m_Rules_4() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Rules_4)); }
	inline TriggerListContainer_t2032715483 * get_m_Rules_4() const { return ___m_Rules_4; }
	inline TriggerListContainer_t2032715483 ** get_address_of_m_Rules_4() { return &___m_Rules_4; }
	inline void set_m_Rules_4(TriggerListContainer_t2032715483 * value)
	{
		___m_Rules_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_4), value);
	}

	inline static int32_t get_offset_of_m_TriggerBool_5() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerBool_5)); }
	inline int32_t get_m_TriggerBool_5() const { return ___m_TriggerBool_5; }
	inline int32_t* get_address_of_m_TriggerBool_5() { return &___m_TriggerBool_5; }
	inline void set_m_TriggerBool_5(int32_t value)
	{
		___m_TriggerBool_5 = value;
	}

	inline static int32_t get_offset_of_m_InitTime_6() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_InitTime_6)); }
	inline float get_m_InitTime_6() const { return ___m_InitTime_6; }
	inline float* get_address_of_m_InitTime_6() { return &___m_InitTime_6; }
	inline void set_m_InitTime_6(float value)
	{
		___m_InitTime_6 = value;
	}

	inline static int32_t get_offset_of_m_RepeatTime_7() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_RepeatTime_7)); }
	inline float get_m_RepeatTime_7() const { return ___m_RepeatTime_7; }
	inline float* get_address_of_m_RepeatTime_7() { return &___m_RepeatTime_7; }
	inline void set_m_RepeatTime_7(float value)
	{
		___m_RepeatTime_7 = value;
	}

	inline static int32_t get_offset_of_m_Repetitions_8() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Repetitions_8)); }
	inline int32_t get_m_Repetitions_8() const { return ___m_Repetitions_8; }
	inline int32_t* get_address_of_m_Repetitions_8() { return &___m_Repetitions_8; }
	inline void set_m_Repetitions_8(int32_t value)
	{
		___m_Repetitions_8 = value;
	}

	inline static int32_t get_offset_of_repetitionCount_9() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___repetitionCount_9)); }
	inline int32_t get_repetitionCount_9() const { return ___repetitionCount_9; }
	inline int32_t* get_address_of_repetitionCount_9() { return &___repetitionCount_9; }
	inline void set_repetitionCount_9(int32_t value)
	{
		___repetitionCount_9 = value;
	}

	inline static int32_t get_offset_of_m_TriggerFunction_10() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerFunction_10)); }
	inline OnTrigger_t4184125570 * get_m_TriggerFunction_10() const { return ___m_TriggerFunction_10; }
	inline OnTrigger_t4184125570 ** get_address_of_m_TriggerFunction_10() { return &___m_TriggerFunction_10; }
	inline void set_m_TriggerFunction_10(OnTrigger_t4184125570 * value)
	{
		___m_TriggerFunction_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_TriggerFunction_10), value);
	}

	inline static int32_t get_offset_of_m_Method_11() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Method_11)); }
	inline TriggerMethod_t582536534 * get_m_Method_11() const { return ___m_Method_11; }
	inline TriggerMethod_t582536534 ** get_address_of_m_Method_11() { return &___m_Method_11; }
	inline void set_m_Method_11(TriggerMethod_t582536534 * value)
	{
		___m_Method_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Method_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTRIGGER_T2527451695_H
#ifndef TRIGGERRULE_T1946298321_H
#define TRIGGERRULE_T1946298321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerRule
struct  TriggerRule_t1946298321  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.TrackableField UnityEngine.Analytics.TriggerRule::m_Target
	TrackableField_t1772682203 * ___m_Target_0;
	// UnityEngine.Analytics.TriggerOperator UnityEngine.Analytics.TriggerRule::m_Operator
	int32_t ___m_Operator_1;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value
	ValueProperty_t1868393739 * ___m_Value_2;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value2
	ValueProperty_t1868393739 * ___m_Value2_3;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Target_0)); }
	inline TrackableField_t1772682203 * get_m_Target_0() const { return ___m_Target_0; }
	inline TrackableField_t1772682203 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(TrackableField_t1772682203 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_Operator_1() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Operator_1)); }
	inline int32_t get_m_Operator_1() const { return ___m_Operator_1; }
	inline int32_t* get_address_of_m_Operator_1() { return &___m_Operator_1; }
	inline void set_m_Operator_1(int32_t value)
	{
		___m_Operator_1 = value;
	}

	inline static int32_t get_offset_of_m_Value_2() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value_2)); }
	inline ValueProperty_t1868393739 * get_m_Value_2() const { return ___m_Value_2; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value_2() { return &___m_Value_2; }
	inline void set_m_Value_2(ValueProperty_t1868393739 * value)
	{
		___m_Value_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_2), value);
	}

	inline static int32_t get_offset_of_m_Value2_3() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value2_3)); }
	inline ValueProperty_t1868393739 * get_m_Value2_3() const { return ___m_Value2_3; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value2_3() { return &___m_Value2_3; }
	inline void set_m_Value2_3(ValueProperty_t1868393739 * value)
	{
		___m_Value2_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERRULE_T1946298321_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef ONTRIGGER_T4184125570_H
#define ONTRIGGER_T4184125570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger/OnTrigger
struct  OnTrigger_t4184125570  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTRIGGER_T4184125570_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef ANIM_T271495542_H
#define ANIM_T271495542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Anim
struct  Anim_t271495542  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject Anim::quad1
	GameObject_t1113636619 * ___quad1_4;
	// UnityEngine.GameObject Anim::quad2
	GameObject_t1113636619 * ___quad2_5;
	// UnityEngine.GameObject Anim::dog
	GameObject_t1113636619 * ___dog_6;
	// UnityEngine.GameObject Anim::guy
	GameObject_t1113636619 * ___guy_7;
	// System.Boolean Anim::isDone
	bool ___isDone_8;
	// System.Int32 Anim::i
	int32_t ___i_9;
	// System.Single Anim::speed
	float ___speed_10;
	// System.Single Anim::startTime
	float ___startTime_11;
	// System.Single Anim::journeyLength
	float ___journeyLength_12;
	// System.Single Anim::journeyLength2
	float ___journeyLength2_13;
	// System.Single Anim::journeyLength3
	float ___journeyLength3_14;
	// System.Single Anim::journeyLength4
	float ___journeyLength4_15;

public:
	inline static int32_t get_offset_of_quad1_4() { return static_cast<int32_t>(offsetof(Anim_t271495542, ___quad1_4)); }
	inline GameObject_t1113636619 * get_quad1_4() const { return ___quad1_4; }
	inline GameObject_t1113636619 ** get_address_of_quad1_4() { return &___quad1_4; }
	inline void set_quad1_4(GameObject_t1113636619 * value)
	{
		___quad1_4 = value;
		Il2CppCodeGenWriteBarrier((&___quad1_4), value);
	}

	inline static int32_t get_offset_of_quad2_5() { return static_cast<int32_t>(offsetof(Anim_t271495542, ___quad2_5)); }
	inline GameObject_t1113636619 * get_quad2_5() const { return ___quad2_5; }
	inline GameObject_t1113636619 ** get_address_of_quad2_5() { return &___quad2_5; }
	inline void set_quad2_5(GameObject_t1113636619 * value)
	{
		___quad2_5 = value;
		Il2CppCodeGenWriteBarrier((&___quad2_5), value);
	}

	inline static int32_t get_offset_of_dog_6() { return static_cast<int32_t>(offsetof(Anim_t271495542, ___dog_6)); }
	inline GameObject_t1113636619 * get_dog_6() const { return ___dog_6; }
	inline GameObject_t1113636619 ** get_address_of_dog_6() { return &___dog_6; }
	inline void set_dog_6(GameObject_t1113636619 * value)
	{
		___dog_6 = value;
		Il2CppCodeGenWriteBarrier((&___dog_6), value);
	}

	inline static int32_t get_offset_of_guy_7() { return static_cast<int32_t>(offsetof(Anim_t271495542, ___guy_7)); }
	inline GameObject_t1113636619 * get_guy_7() const { return ___guy_7; }
	inline GameObject_t1113636619 ** get_address_of_guy_7() { return &___guy_7; }
	inline void set_guy_7(GameObject_t1113636619 * value)
	{
		___guy_7 = value;
		Il2CppCodeGenWriteBarrier((&___guy_7), value);
	}

	inline static int32_t get_offset_of_isDone_8() { return static_cast<int32_t>(offsetof(Anim_t271495542, ___isDone_8)); }
	inline bool get_isDone_8() const { return ___isDone_8; }
	inline bool* get_address_of_isDone_8() { return &___isDone_8; }
	inline void set_isDone_8(bool value)
	{
		___isDone_8 = value;
	}

	inline static int32_t get_offset_of_i_9() { return static_cast<int32_t>(offsetof(Anim_t271495542, ___i_9)); }
	inline int32_t get_i_9() const { return ___i_9; }
	inline int32_t* get_address_of_i_9() { return &___i_9; }
	inline void set_i_9(int32_t value)
	{
		___i_9 = value;
	}

	inline static int32_t get_offset_of_speed_10() { return static_cast<int32_t>(offsetof(Anim_t271495542, ___speed_10)); }
	inline float get_speed_10() const { return ___speed_10; }
	inline float* get_address_of_speed_10() { return &___speed_10; }
	inline void set_speed_10(float value)
	{
		___speed_10 = value;
	}

	inline static int32_t get_offset_of_startTime_11() { return static_cast<int32_t>(offsetof(Anim_t271495542, ___startTime_11)); }
	inline float get_startTime_11() const { return ___startTime_11; }
	inline float* get_address_of_startTime_11() { return &___startTime_11; }
	inline void set_startTime_11(float value)
	{
		___startTime_11 = value;
	}

	inline static int32_t get_offset_of_journeyLength_12() { return static_cast<int32_t>(offsetof(Anim_t271495542, ___journeyLength_12)); }
	inline float get_journeyLength_12() const { return ___journeyLength_12; }
	inline float* get_address_of_journeyLength_12() { return &___journeyLength_12; }
	inline void set_journeyLength_12(float value)
	{
		___journeyLength_12 = value;
	}

	inline static int32_t get_offset_of_journeyLength2_13() { return static_cast<int32_t>(offsetof(Anim_t271495542, ___journeyLength2_13)); }
	inline float get_journeyLength2_13() const { return ___journeyLength2_13; }
	inline float* get_address_of_journeyLength2_13() { return &___journeyLength2_13; }
	inline void set_journeyLength2_13(float value)
	{
		___journeyLength2_13 = value;
	}

	inline static int32_t get_offset_of_journeyLength3_14() { return static_cast<int32_t>(offsetof(Anim_t271495542, ___journeyLength3_14)); }
	inline float get_journeyLength3_14() const { return ___journeyLength3_14; }
	inline float* get_address_of_journeyLength3_14() { return &___journeyLength3_14; }
	inline void set_journeyLength3_14(float value)
	{
		___journeyLength3_14 = value;
	}

	inline static int32_t get_offset_of_journeyLength4_15() { return static_cast<int32_t>(offsetof(Anim_t271495542, ___journeyLength4_15)); }
	inline float get_journeyLength4_15() const { return ___journeyLength4_15; }
	inline float* get_address_of_journeyLength4_15() { return &___journeyLength4_15; }
	inline void set_journeyLength4_15(float value)
	{
		___journeyLength4_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIM_T271495542_H
#ifndef ANIMIMP_T3408691587_H
#define ANIMIMP_T3408691587_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Animimp
struct  Animimp_t3408691587  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject Animimp::quad1
	GameObject_t1113636619 * ___quad1_4;
	// UnityEngine.GameObject Animimp::quad2
	GameObject_t1113636619 * ___quad2_5;
	// UnityEngine.GameObject Animimp::dog
	GameObject_t1113636619 * ___dog_6;
	// UnityEngine.GameObject Animimp::guy
	GameObject_t1113636619 * ___guy_7;
	// System.Boolean Animimp::isDone
	bool ___isDone_8;
	// System.Int32 Animimp::i
	int32_t ___i_9;
	// System.Single Animimp::speed
	float ___speed_10;
	// System.Single Animimp::startTime
	float ___startTime_11;
	// System.Single Animimp::journeyLength
	float ___journeyLength_12;
	// System.Single Animimp::journeyLength2
	float ___journeyLength2_13;
	// System.Single Animimp::journeyLength3
	float ___journeyLength3_14;
	// System.Single Animimp::journeyLength4
	float ___journeyLength4_15;

public:
	inline static int32_t get_offset_of_quad1_4() { return static_cast<int32_t>(offsetof(Animimp_t3408691587, ___quad1_4)); }
	inline GameObject_t1113636619 * get_quad1_4() const { return ___quad1_4; }
	inline GameObject_t1113636619 ** get_address_of_quad1_4() { return &___quad1_4; }
	inline void set_quad1_4(GameObject_t1113636619 * value)
	{
		___quad1_4 = value;
		Il2CppCodeGenWriteBarrier((&___quad1_4), value);
	}

	inline static int32_t get_offset_of_quad2_5() { return static_cast<int32_t>(offsetof(Animimp_t3408691587, ___quad2_5)); }
	inline GameObject_t1113636619 * get_quad2_5() const { return ___quad2_5; }
	inline GameObject_t1113636619 ** get_address_of_quad2_5() { return &___quad2_5; }
	inline void set_quad2_5(GameObject_t1113636619 * value)
	{
		___quad2_5 = value;
		Il2CppCodeGenWriteBarrier((&___quad2_5), value);
	}

	inline static int32_t get_offset_of_dog_6() { return static_cast<int32_t>(offsetof(Animimp_t3408691587, ___dog_6)); }
	inline GameObject_t1113636619 * get_dog_6() const { return ___dog_6; }
	inline GameObject_t1113636619 ** get_address_of_dog_6() { return &___dog_6; }
	inline void set_dog_6(GameObject_t1113636619 * value)
	{
		___dog_6 = value;
		Il2CppCodeGenWriteBarrier((&___dog_6), value);
	}

	inline static int32_t get_offset_of_guy_7() { return static_cast<int32_t>(offsetof(Animimp_t3408691587, ___guy_7)); }
	inline GameObject_t1113636619 * get_guy_7() const { return ___guy_7; }
	inline GameObject_t1113636619 ** get_address_of_guy_7() { return &___guy_7; }
	inline void set_guy_7(GameObject_t1113636619 * value)
	{
		___guy_7 = value;
		Il2CppCodeGenWriteBarrier((&___guy_7), value);
	}

	inline static int32_t get_offset_of_isDone_8() { return static_cast<int32_t>(offsetof(Animimp_t3408691587, ___isDone_8)); }
	inline bool get_isDone_8() const { return ___isDone_8; }
	inline bool* get_address_of_isDone_8() { return &___isDone_8; }
	inline void set_isDone_8(bool value)
	{
		___isDone_8 = value;
	}

	inline static int32_t get_offset_of_i_9() { return static_cast<int32_t>(offsetof(Animimp_t3408691587, ___i_9)); }
	inline int32_t get_i_9() const { return ___i_9; }
	inline int32_t* get_address_of_i_9() { return &___i_9; }
	inline void set_i_9(int32_t value)
	{
		___i_9 = value;
	}

	inline static int32_t get_offset_of_speed_10() { return static_cast<int32_t>(offsetof(Animimp_t3408691587, ___speed_10)); }
	inline float get_speed_10() const { return ___speed_10; }
	inline float* get_address_of_speed_10() { return &___speed_10; }
	inline void set_speed_10(float value)
	{
		___speed_10 = value;
	}

	inline static int32_t get_offset_of_startTime_11() { return static_cast<int32_t>(offsetof(Animimp_t3408691587, ___startTime_11)); }
	inline float get_startTime_11() const { return ___startTime_11; }
	inline float* get_address_of_startTime_11() { return &___startTime_11; }
	inline void set_startTime_11(float value)
	{
		___startTime_11 = value;
	}

	inline static int32_t get_offset_of_journeyLength_12() { return static_cast<int32_t>(offsetof(Animimp_t3408691587, ___journeyLength_12)); }
	inline float get_journeyLength_12() const { return ___journeyLength_12; }
	inline float* get_address_of_journeyLength_12() { return &___journeyLength_12; }
	inline void set_journeyLength_12(float value)
	{
		___journeyLength_12 = value;
	}

	inline static int32_t get_offset_of_journeyLength2_13() { return static_cast<int32_t>(offsetof(Animimp_t3408691587, ___journeyLength2_13)); }
	inline float get_journeyLength2_13() const { return ___journeyLength2_13; }
	inline float* get_address_of_journeyLength2_13() { return &___journeyLength2_13; }
	inline void set_journeyLength2_13(float value)
	{
		___journeyLength2_13 = value;
	}

	inline static int32_t get_offset_of_journeyLength3_14() { return static_cast<int32_t>(offsetof(Animimp_t3408691587, ___journeyLength3_14)); }
	inline float get_journeyLength3_14() const { return ___journeyLength3_14; }
	inline float* get_address_of_journeyLength3_14() { return &___journeyLength3_14; }
	inline void set_journeyLength3_14(float value)
	{
		___journeyLength3_14 = value;
	}

	inline static int32_t get_offset_of_journeyLength4_15() { return static_cast<int32_t>(offsetof(Animimp_t3408691587, ___journeyLength4_15)); }
	inline float get_journeyLength4_15() const { return ___journeyLength4_15; }
	inline float* get_address_of_journeyLength4_15() { return &___journeyLength4_15; }
	inline void set_journeyLength4_15(float value)
	{
		___journeyLength4_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMIMP_T3408691587_H
#ifndef ANIMINF_T989049961_H
#define ANIMINF_T989049961_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Animinf
struct  Animinf_t989049961  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject Animinf::quad1
	GameObject_t1113636619 * ___quad1_4;
	// UnityEngine.GameObject Animinf::quad2
	GameObject_t1113636619 * ___quad2_5;
	// UnityEngine.GameObject Animinf::dog
	GameObject_t1113636619 * ___dog_6;
	// UnityEngine.GameObject Animinf::guy
	GameObject_t1113636619 * ___guy_7;
	// System.Boolean Animinf::isDone
	bool ___isDone_8;
	// System.Int32 Animinf::i
	int32_t ___i_9;
	// System.Single Animinf::speed
	float ___speed_10;
	// System.Single Animinf::startTime
	float ___startTime_11;
	// System.Single Animinf::journeyLength
	float ___journeyLength_12;
	// System.Single Animinf::journeyLength2
	float ___journeyLength2_13;
	// System.Single Animinf::journeyLength3
	float ___journeyLength3_14;
	// System.Single Animinf::journeyLength4
	float ___journeyLength4_15;

public:
	inline static int32_t get_offset_of_quad1_4() { return static_cast<int32_t>(offsetof(Animinf_t989049961, ___quad1_4)); }
	inline GameObject_t1113636619 * get_quad1_4() const { return ___quad1_4; }
	inline GameObject_t1113636619 ** get_address_of_quad1_4() { return &___quad1_4; }
	inline void set_quad1_4(GameObject_t1113636619 * value)
	{
		___quad1_4 = value;
		Il2CppCodeGenWriteBarrier((&___quad1_4), value);
	}

	inline static int32_t get_offset_of_quad2_5() { return static_cast<int32_t>(offsetof(Animinf_t989049961, ___quad2_5)); }
	inline GameObject_t1113636619 * get_quad2_5() const { return ___quad2_5; }
	inline GameObject_t1113636619 ** get_address_of_quad2_5() { return &___quad2_5; }
	inline void set_quad2_5(GameObject_t1113636619 * value)
	{
		___quad2_5 = value;
		Il2CppCodeGenWriteBarrier((&___quad2_5), value);
	}

	inline static int32_t get_offset_of_dog_6() { return static_cast<int32_t>(offsetof(Animinf_t989049961, ___dog_6)); }
	inline GameObject_t1113636619 * get_dog_6() const { return ___dog_6; }
	inline GameObject_t1113636619 ** get_address_of_dog_6() { return &___dog_6; }
	inline void set_dog_6(GameObject_t1113636619 * value)
	{
		___dog_6 = value;
		Il2CppCodeGenWriteBarrier((&___dog_6), value);
	}

	inline static int32_t get_offset_of_guy_7() { return static_cast<int32_t>(offsetof(Animinf_t989049961, ___guy_7)); }
	inline GameObject_t1113636619 * get_guy_7() const { return ___guy_7; }
	inline GameObject_t1113636619 ** get_address_of_guy_7() { return &___guy_7; }
	inline void set_guy_7(GameObject_t1113636619 * value)
	{
		___guy_7 = value;
		Il2CppCodeGenWriteBarrier((&___guy_7), value);
	}

	inline static int32_t get_offset_of_isDone_8() { return static_cast<int32_t>(offsetof(Animinf_t989049961, ___isDone_8)); }
	inline bool get_isDone_8() const { return ___isDone_8; }
	inline bool* get_address_of_isDone_8() { return &___isDone_8; }
	inline void set_isDone_8(bool value)
	{
		___isDone_8 = value;
	}

	inline static int32_t get_offset_of_i_9() { return static_cast<int32_t>(offsetof(Animinf_t989049961, ___i_9)); }
	inline int32_t get_i_9() const { return ___i_9; }
	inline int32_t* get_address_of_i_9() { return &___i_9; }
	inline void set_i_9(int32_t value)
	{
		___i_9 = value;
	}

	inline static int32_t get_offset_of_speed_10() { return static_cast<int32_t>(offsetof(Animinf_t989049961, ___speed_10)); }
	inline float get_speed_10() const { return ___speed_10; }
	inline float* get_address_of_speed_10() { return &___speed_10; }
	inline void set_speed_10(float value)
	{
		___speed_10 = value;
	}

	inline static int32_t get_offset_of_startTime_11() { return static_cast<int32_t>(offsetof(Animinf_t989049961, ___startTime_11)); }
	inline float get_startTime_11() const { return ___startTime_11; }
	inline float* get_address_of_startTime_11() { return &___startTime_11; }
	inline void set_startTime_11(float value)
	{
		___startTime_11 = value;
	}

	inline static int32_t get_offset_of_journeyLength_12() { return static_cast<int32_t>(offsetof(Animinf_t989049961, ___journeyLength_12)); }
	inline float get_journeyLength_12() const { return ___journeyLength_12; }
	inline float* get_address_of_journeyLength_12() { return &___journeyLength_12; }
	inline void set_journeyLength_12(float value)
	{
		___journeyLength_12 = value;
	}

	inline static int32_t get_offset_of_journeyLength2_13() { return static_cast<int32_t>(offsetof(Animinf_t989049961, ___journeyLength2_13)); }
	inline float get_journeyLength2_13() const { return ___journeyLength2_13; }
	inline float* get_address_of_journeyLength2_13() { return &___journeyLength2_13; }
	inline void set_journeyLength2_13(float value)
	{
		___journeyLength2_13 = value;
	}

	inline static int32_t get_offset_of_journeyLength3_14() { return static_cast<int32_t>(offsetof(Animinf_t989049961, ___journeyLength3_14)); }
	inline float get_journeyLength3_14() const { return ___journeyLength3_14; }
	inline float* get_address_of_journeyLength3_14() { return &___journeyLength3_14; }
	inline void set_journeyLength3_14(float value)
	{
		___journeyLength3_14 = value;
	}

	inline static int32_t get_offset_of_journeyLength4_15() { return static_cast<int32_t>(offsetof(Animinf_t989049961, ___journeyLength4_15)); }
	inline float get_journeyLength4_15() const { return ___journeyLength4_15; }
	inline float* get_address_of_journeyLength4_15() { return &___journeyLength4_15; }
	inline void set_journeyLength4_15(float value)
	{
		___journeyLength4_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMINF_T989049961_H
#ifndef BATTLE_T2191861408_H
#define BATTLE_T2191861408_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Battle
struct  Battle_t2191861408  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Slider Battle::slider
	Slider_t3903728902 * ___slider_4;
	// UnityEngine.UI.Slider Battle::guySlider
	Slider_t3903728902 * ___guySlider_5;
	// TMPro.TMP_Text Battle::attackText
	TMP_Text_t2599618874 * ___attackText_6;
	// TMPro.TMP_Text Battle::dogHealth
	TMP_Text_t2599618874 * ___dogHealth_7;
	// TMPro.TMP_Text Battle::guyHealth
	TMP_Text_t2599618874 * ___guyHealth_8;
	// UnityEngine.UI.Image Battle::black
	Image_t2670269651 * ___black_9;
	// UnityEngine.Animator Battle::animator
	Animator_t434523843 * ___animator_10;
	// UnityEngine.AudioSource Battle::DogDieSFX
	AudioSource_t3935305588 * ___DogDieSFX_11;
	// UnityEngine.AudioSource Battle::GuydDieSFX
	AudioSource_t3935305588 * ___GuydDieSFX_12;
	// UnityEngine.GameObject Battle::GUI
	GameObject_t1113636619 * ___GUI_13;
	// UnityEngine.GameObject Battle::GameOver
	GameObject_t1113636619 * ___GameOver_14;
	// Anim Battle::anim
	Anim_t271495542 * ___anim_15;
	// System.Random Battle::rnd
	Random_t108471755 * ___rnd_16;
	// System.Boolean Battle::myTurn
	bool ___myTurn_17;
	// System.Boolean Battle::ableToAttack
	bool ___ableToAttack_18;
	// System.Boolean Battle::enemyAbleToAttack
	bool ___enemyAbleToAttack_19;
	// System.Boolean Battle::isReady
	bool ___isReady_20;
	// System.Boolean Battle::isDone
	bool ___isDone_21;
	// System.Boolean Battle::isDead
	bool ___isDead_22;
	// System.Single Battle::<CurrentHealth>k__BackingField
	float ___U3CCurrentHealthU3Ek__BackingField_23;
	// System.Single Battle::<MaxHealth>k__BackingField
	float ___U3CMaxHealthU3Ek__BackingField_24;
	// System.Single Battle::<CurrentHealthGuy>k__BackingField
	float ___U3CCurrentHealthGuyU3Ek__BackingField_25;
	// System.Single Battle::<MaxHealthGuy>k__BackingField
	float ___U3CMaxHealthGuyU3Ek__BackingField_26;

public:
	inline static int32_t get_offset_of_slider_4() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___slider_4)); }
	inline Slider_t3903728902 * get_slider_4() const { return ___slider_4; }
	inline Slider_t3903728902 ** get_address_of_slider_4() { return &___slider_4; }
	inline void set_slider_4(Slider_t3903728902 * value)
	{
		___slider_4 = value;
		Il2CppCodeGenWriteBarrier((&___slider_4), value);
	}

	inline static int32_t get_offset_of_guySlider_5() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___guySlider_5)); }
	inline Slider_t3903728902 * get_guySlider_5() const { return ___guySlider_5; }
	inline Slider_t3903728902 ** get_address_of_guySlider_5() { return &___guySlider_5; }
	inline void set_guySlider_5(Slider_t3903728902 * value)
	{
		___guySlider_5 = value;
		Il2CppCodeGenWriteBarrier((&___guySlider_5), value);
	}

	inline static int32_t get_offset_of_attackText_6() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___attackText_6)); }
	inline TMP_Text_t2599618874 * get_attackText_6() const { return ___attackText_6; }
	inline TMP_Text_t2599618874 ** get_address_of_attackText_6() { return &___attackText_6; }
	inline void set_attackText_6(TMP_Text_t2599618874 * value)
	{
		___attackText_6 = value;
		Il2CppCodeGenWriteBarrier((&___attackText_6), value);
	}

	inline static int32_t get_offset_of_dogHealth_7() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___dogHealth_7)); }
	inline TMP_Text_t2599618874 * get_dogHealth_7() const { return ___dogHealth_7; }
	inline TMP_Text_t2599618874 ** get_address_of_dogHealth_7() { return &___dogHealth_7; }
	inline void set_dogHealth_7(TMP_Text_t2599618874 * value)
	{
		___dogHealth_7 = value;
		Il2CppCodeGenWriteBarrier((&___dogHealth_7), value);
	}

	inline static int32_t get_offset_of_guyHealth_8() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___guyHealth_8)); }
	inline TMP_Text_t2599618874 * get_guyHealth_8() const { return ___guyHealth_8; }
	inline TMP_Text_t2599618874 ** get_address_of_guyHealth_8() { return &___guyHealth_8; }
	inline void set_guyHealth_8(TMP_Text_t2599618874 * value)
	{
		___guyHealth_8 = value;
		Il2CppCodeGenWriteBarrier((&___guyHealth_8), value);
	}

	inline static int32_t get_offset_of_black_9() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___black_9)); }
	inline Image_t2670269651 * get_black_9() const { return ___black_9; }
	inline Image_t2670269651 ** get_address_of_black_9() { return &___black_9; }
	inline void set_black_9(Image_t2670269651 * value)
	{
		___black_9 = value;
		Il2CppCodeGenWriteBarrier((&___black_9), value);
	}

	inline static int32_t get_offset_of_animator_10() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___animator_10)); }
	inline Animator_t434523843 * get_animator_10() const { return ___animator_10; }
	inline Animator_t434523843 ** get_address_of_animator_10() { return &___animator_10; }
	inline void set_animator_10(Animator_t434523843 * value)
	{
		___animator_10 = value;
		Il2CppCodeGenWriteBarrier((&___animator_10), value);
	}

	inline static int32_t get_offset_of_DogDieSFX_11() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___DogDieSFX_11)); }
	inline AudioSource_t3935305588 * get_DogDieSFX_11() const { return ___DogDieSFX_11; }
	inline AudioSource_t3935305588 ** get_address_of_DogDieSFX_11() { return &___DogDieSFX_11; }
	inline void set_DogDieSFX_11(AudioSource_t3935305588 * value)
	{
		___DogDieSFX_11 = value;
		Il2CppCodeGenWriteBarrier((&___DogDieSFX_11), value);
	}

	inline static int32_t get_offset_of_GuydDieSFX_12() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___GuydDieSFX_12)); }
	inline AudioSource_t3935305588 * get_GuydDieSFX_12() const { return ___GuydDieSFX_12; }
	inline AudioSource_t3935305588 ** get_address_of_GuydDieSFX_12() { return &___GuydDieSFX_12; }
	inline void set_GuydDieSFX_12(AudioSource_t3935305588 * value)
	{
		___GuydDieSFX_12 = value;
		Il2CppCodeGenWriteBarrier((&___GuydDieSFX_12), value);
	}

	inline static int32_t get_offset_of_GUI_13() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___GUI_13)); }
	inline GameObject_t1113636619 * get_GUI_13() const { return ___GUI_13; }
	inline GameObject_t1113636619 ** get_address_of_GUI_13() { return &___GUI_13; }
	inline void set_GUI_13(GameObject_t1113636619 * value)
	{
		___GUI_13 = value;
		Il2CppCodeGenWriteBarrier((&___GUI_13), value);
	}

	inline static int32_t get_offset_of_GameOver_14() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___GameOver_14)); }
	inline GameObject_t1113636619 * get_GameOver_14() const { return ___GameOver_14; }
	inline GameObject_t1113636619 ** get_address_of_GameOver_14() { return &___GameOver_14; }
	inline void set_GameOver_14(GameObject_t1113636619 * value)
	{
		___GameOver_14 = value;
		Il2CppCodeGenWriteBarrier((&___GameOver_14), value);
	}

	inline static int32_t get_offset_of_anim_15() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___anim_15)); }
	inline Anim_t271495542 * get_anim_15() const { return ___anim_15; }
	inline Anim_t271495542 ** get_address_of_anim_15() { return &___anim_15; }
	inline void set_anim_15(Anim_t271495542 * value)
	{
		___anim_15 = value;
		Il2CppCodeGenWriteBarrier((&___anim_15), value);
	}

	inline static int32_t get_offset_of_rnd_16() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___rnd_16)); }
	inline Random_t108471755 * get_rnd_16() const { return ___rnd_16; }
	inline Random_t108471755 ** get_address_of_rnd_16() { return &___rnd_16; }
	inline void set_rnd_16(Random_t108471755 * value)
	{
		___rnd_16 = value;
		Il2CppCodeGenWriteBarrier((&___rnd_16), value);
	}

	inline static int32_t get_offset_of_myTurn_17() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___myTurn_17)); }
	inline bool get_myTurn_17() const { return ___myTurn_17; }
	inline bool* get_address_of_myTurn_17() { return &___myTurn_17; }
	inline void set_myTurn_17(bool value)
	{
		___myTurn_17 = value;
	}

	inline static int32_t get_offset_of_ableToAttack_18() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___ableToAttack_18)); }
	inline bool get_ableToAttack_18() const { return ___ableToAttack_18; }
	inline bool* get_address_of_ableToAttack_18() { return &___ableToAttack_18; }
	inline void set_ableToAttack_18(bool value)
	{
		___ableToAttack_18 = value;
	}

	inline static int32_t get_offset_of_enemyAbleToAttack_19() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___enemyAbleToAttack_19)); }
	inline bool get_enemyAbleToAttack_19() const { return ___enemyAbleToAttack_19; }
	inline bool* get_address_of_enemyAbleToAttack_19() { return &___enemyAbleToAttack_19; }
	inline void set_enemyAbleToAttack_19(bool value)
	{
		___enemyAbleToAttack_19 = value;
	}

	inline static int32_t get_offset_of_isReady_20() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___isReady_20)); }
	inline bool get_isReady_20() const { return ___isReady_20; }
	inline bool* get_address_of_isReady_20() { return &___isReady_20; }
	inline void set_isReady_20(bool value)
	{
		___isReady_20 = value;
	}

	inline static int32_t get_offset_of_isDone_21() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___isDone_21)); }
	inline bool get_isDone_21() const { return ___isDone_21; }
	inline bool* get_address_of_isDone_21() { return &___isDone_21; }
	inline void set_isDone_21(bool value)
	{
		___isDone_21 = value;
	}

	inline static int32_t get_offset_of_isDead_22() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___isDead_22)); }
	inline bool get_isDead_22() const { return ___isDead_22; }
	inline bool* get_address_of_isDead_22() { return &___isDead_22; }
	inline void set_isDead_22(bool value)
	{
		___isDead_22 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentHealthU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___U3CCurrentHealthU3Ek__BackingField_23)); }
	inline float get_U3CCurrentHealthU3Ek__BackingField_23() const { return ___U3CCurrentHealthU3Ek__BackingField_23; }
	inline float* get_address_of_U3CCurrentHealthU3Ek__BackingField_23() { return &___U3CCurrentHealthU3Ek__BackingField_23; }
	inline void set_U3CCurrentHealthU3Ek__BackingField_23(float value)
	{
		___U3CCurrentHealthU3Ek__BackingField_23 = value;
	}

	inline static int32_t get_offset_of_U3CMaxHealthU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___U3CMaxHealthU3Ek__BackingField_24)); }
	inline float get_U3CMaxHealthU3Ek__BackingField_24() const { return ___U3CMaxHealthU3Ek__BackingField_24; }
	inline float* get_address_of_U3CMaxHealthU3Ek__BackingField_24() { return &___U3CMaxHealthU3Ek__BackingField_24; }
	inline void set_U3CMaxHealthU3Ek__BackingField_24(float value)
	{
		___U3CMaxHealthU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentHealthGuyU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___U3CCurrentHealthGuyU3Ek__BackingField_25)); }
	inline float get_U3CCurrentHealthGuyU3Ek__BackingField_25() const { return ___U3CCurrentHealthGuyU3Ek__BackingField_25; }
	inline float* get_address_of_U3CCurrentHealthGuyU3Ek__BackingField_25() { return &___U3CCurrentHealthGuyU3Ek__BackingField_25; }
	inline void set_U3CCurrentHealthGuyU3Ek__BackingField_25(float value)
	{
		___U3CCurrentHealthGuyU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_U3CMaxHealthGuyU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___U3CMaxHealthGuyU3Ek__BackingField_26)); }
	inline float get_U3CMaxHealthGuyU3Ek__BackingField_26() const { return ___U3CMaxHealthGuyU3Ek__BackingField_26; }
	inline float* get_address_of_U3CMaxHealthGuyU3Ek__BackingField_26() { return &___U3CMaxHealthGuyU3Ek__BackingField_26; }
	inline void set_U3CMaxHealthGuyU3Ek__BackingField_26(float value)
	{
		___U3CMaxHealthGuyU3Ek__BackingField_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLE_T2191861408_H
#ifndef BATTLEIMP_T158148094_H
#define BATTLEIMP_T158148094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Battleimp
struct  Battleimp_t158148094  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Slider Battleimp::slider
	Slider_t3903728902 * ___slider_4;
	// UnityEngine.UI.Slider Battleimp::guySlider
	Slider_t3903728902 * ___guySlider_5;
	// TMPro.TMP_Text Battleimp::attackText
	TMP_Text_t2599618874 * ___attackText_6;
	// TMPro.TMP_Text Battleimp::dogHealth
	TMP_Text_t2599618874 * ___dogHealth_7;
	// TMPro.TMP_Text Battleimp::guyHealth
	TMP_Text_t2599618874 * ___guyHealth_8;
	// UnityEngine.UI.Image Battleimp::black
	Image_t2670269651 * ___black_9;
	// UnityEngine.Animator Battleimp::animator
	Animator_t434523843 * ___animator_10;
	// UnityEngine.AudioSource Battleimp::DogDieSFX
	AudioSource_t3935305588 * ___DogDieSFX_11;
	// UnityEngine.AudioSource Battleimp::GuydDieSFX
	AudioSource_t3935305588 * ___GuydDieSFX_12;
	// UnityEngine.GameObject Battleimp::GUI
	GameObject_t1113636619 * ___GUI_13;
	// UnityEngine.GameObject Battleimp::GameOver
	GameObject_t1113636619 * ___GameOver_14;
	// Animimp Battleimp::anim
	Animimp_t3408691587 * ___anim_15;
	// System.Random Battleimp::rnd
	Random_t108471755 * ___rnd_16;
	// System.Boolean Battleimp::myTurn
	bool ___myTurn_17;
	// System.Boolean Battleimp::ableToAttack
	bool ___ableToAttack_18;
	// System.Boolean Battleimp::enemyAbleToAttack
	bool ___enemyAbleToAttack_19;
	// System.Boolean Battleimp::isReady
	bool ___isReady_20;
	// System.Boolean Battleimp::isDone
	bool ___isDone_21;
	// System.Boolean Battleimp::isDead
	bool ___isDead_22;
	// System.Single Battleimp::<CurrentHealth>k__BackingField
	float ___U3CCurrentHealthU3Ek__BackingField_23;
	// System.Single Battleimp::<MaxHealth>k__BackingField
	float ___U3CMaxHealthU3Ek__BackingField_24;
	// System.Single Battleimp::<CurrentHealthGuy>k__BackingField
	float ___U3CCurrentHealthGuyU3Ek__BackingField_25;
	// System.Single Battleimp::<MaxHealthGuy>k__BackingField
	float ___U3CMaxHealthGuyU3Ek__BackingField_26;

public:
	inline static int32_t get_offset_of_slider_4() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___slider_4)); }
	inline Slider_t3903728902 * get_slider_4() const { return ___slider_4; }
	inline Slider_t3903728902 ** get_address_of_slider_4() { return &___slider_4; }
	inline void set_slider_4(Slider_t3903728902 * value)
	{
		___slider_4 = value;
		Il2CppCodeGenWriteBarrier((&___slider_4), value);
	}

	inline static int32_t get_offset_of_guySlider_5() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___guySlider_5)); }
	inline Slider_t3903728902 * get_guySlider_5() const { return ___guySlider_5; }
	inline Slider_t3903728902 ** get_address_of_guySlider_5() { return &___guySlider_5; }
	inline void set_guySlider_5(Slider_t3903728902 * value)
	{
		___guySlider_5 = value;
		Il2CppCodeGenWriteBarrier((&___guySlider_5), value);
	}

	inline static int32_t get_offset_of_attackText_6() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___attackText_6)); }
	inline TMP_Text_t2599618874 * get_attackText_6() const { return ___attackText_6; }
	inline TMP_Text_t2599618874 ** get_address_of_attackText_6() { return &___attackText_6; }
	inline void set_attackText_6(TMP_Text_t2599618874 * value)
	{
		___attackText_6 = value;
		Il2CppCodeGenWriteBarrier((&___attackText_6), value);
	}

	inline static int32_t get_offset_of_dogHealth_7() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___dogHealth_7)); }
	inline TMP_Text_t2599618874 * get_dogHealth_7() const { return ___dogHealth_7; }
	inline TMP_Text_t2599618874 ** get_address_of_dogHealth_7() { return &___dogHealth_7; }
	inline void set_dogHealth_7(TMP_Text_t2599618874 * value)
	{
		___dogHealth_7 = value;
		Il2CppCodeGenWriteBarrier((&___dogHealth_7), value);
	}

	inline static int32_t get_offset_of_guyHealth_8() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___guyHealth_8)); }
	inline TMP_Text_t2599618874 * get_guyHealth_8() const { return ___guyHealth_8; }
	inline TMP_Text_t2599618874 ** get_address_of_guyHealth_8() { return &___guyHealth_8; }
	inline void set_guyHealth_8(TMP_Text_t2599618874 * value)
	{
		___guyHealth_8 = value;
		Il2CppCodeGenWriteBarrier((&___guyHealth_8), value);
	}

	inline static int32_t get_offset_of_black_9() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___black_9)); }
	inline Image_t2670269651 * get_black_9() const { return ___black_9; }
	inline Image_t2670269651 ** get_address_of_black_9() { return &___black_9; }
	inline void set_black_9(Image_t2670269651 * value)
	{
		___black_9 = value;
		Il2CppCodeGenWriteBarrier((&___black_9), value);
	}

	inline static int32_t get_offset_of_animator_10() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___animator_10)); }
	inline Animator_t434523843 * get_animator_10() const { return ___animator_10; }
	inline Animator_t434523843 ** get_address_of_animator_10() { return &___animator_10; }
	inline void set_animator_10(Animator_t434523843 * value)
	{
		___animator_10 = value;
		Il2CppCodeGenWriteBarrier((&___animator_10), value);
	}

	inline static int32_t get_offset_of_DogDieSFX_11() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___DogDieSFX_11)); }
	inline AudioSource_t3935305588 * get_DogDieSFX_11() const { return ___DogDieSFX_11; }
	inline AudioSource_t3935305588 ** get_address_of_DogDieSFX_11() { return &___DogDieSFX_11; }
	inline void set_DogDieSFX_11(AudioSource_t3935305588 * value)
	{
		___DogDieSFX_11 = value;
		Il2CppCodeGenWriteBarrier((&___DogDieSFX_11), value);
	}

	inline static int32_t get_offset_of_GuydDieSFX_12() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___GuydDieSFX_12)); }
	inline AudioSource_t3935305588 * get_GuydDieSFX_12() const { return ___GuydDieSFX_12; }
	inline AudioSource_t3935305588 ** get_address_of_GuydDieSFX_12() { return &___GuydDieSFX_12; }
	inline void set_GuydDieSFX_12(AudioSource_t3935305588 * value)
	{
		___GuydDieSFX_12 = value;
		Il2CppCodeGenWriteBarrier((&___GuydDieSFX_12), value);
	}

	inline static int32_t get_offset_of_GUI_13() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___GUI_13)); }
	inline GameObject_t1113636619 * get_GUI_13() const { return ___GUI_13; }
	inline GameObject_t1113636619 ** get_address_of_GUI_13() { return &___GUI_13; }
	inline void set_GUI_13(GameObject_t1113636619 * value)
	{
		___GUI_13 = value;
		Il2CppCodeGenWriteBarrier((&___GUI_13), value);
	}

	inline static int32_t get_offset_of_GameOver_14() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___GameOver_14)); }
	inline GameObject_t1113636619 * get_GameOver_14() const { return ___GameOver_14; }
	inline GameObject_t1113636619 ** get_address_of_GameOver_14() { return &___GameOver_14; }
	inline void set_GameOver_14(GameObject_t1113636619 * value)
	{
		___GameOver_14 = value;
		Il2CppCodeGenWriteBarrier((&___GameOver_14), value);
	}

	inline static int32_t get_offset_of_anim_15() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___anim_15)); }
	inline Animimp_t3408691587 * get_anim_15() const { return ___anim_15; }
	inline Animimp_t3408691587 ** get_address_of_anim_15() { return &___anim_15; }
	inline void set_anim_15(Animimp_t3408691587 * value)
	{
		___anim_15 = value;
		Il2CppCodeGenWriteBarrier((&___anim_15), value);
	}

	inline static int32_t get_offset_of_rnd_16() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___rnd_16)); }
	inline Random_t108471755 * get_rnd_16() const { return ___rnd_16; }
	inline Random_t108471755 ** get_address_of_rnd_16() { return &___rnd_16; }
	inline void set_rnd_16(Random_t108471755 * value)
	{
		___rnd_16 = value;
		Il2CppCodeGenWriteBarrier((&___rnd_16), value);
	}

	inline static int32_t get_offset_of_myTurn_17() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___myTurn_17)); }
	inline bool get_myTurn_17() const { return ___myTurn_17; }
	inline bool* get_address_of_myTurn_17() { return &___myTurn_17; }
	inline void set_myTurn_17(bool value)
	{
		___myTurn_17 = value;
	}

	inline static int32_t get_offset_of_ableToAttack_18() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___ableToAttack_18)); }
	inline bool get_ableToAttack_18() const { return ___ableToAttack_18; }
	inline bool* get_address_of_ableToAttack_18() { return &___ableToAttack_18; }
	inline void set_ableToAttack_18(bool value)
	{
		___ableToAttack_18 = value;
	}

	inline static int32_t get_offset_of_enemyAbleToAttack_19() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___enemyAbleToAttack_19)); }
	inline bool get_enemyAbleToAttack_19() const { return ___enemyAbleToAttack_19; }
	inline bool* get_address_of_enemyAbleToAttack_19() { return &___enemyAbleToAttack_19; }
	inline void set_enemyAbleToAttack_19(bool value)
	{
		___enemyAbleToAttack_19 = value;
	}

	inline static int32_t get_offset_of_isReady_20() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___isReady_20)); }
	inline bool get_isReady_20() const { return ___isReady_20; }
	inline bool* get_address_of_isReady_20() { return &___isReady_20; }
	inline void set_isReady_20(bool value)
	{
		___isReady_20 = value;
	}

	inline static int32_t get_offset_of_isDone_21() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___isDone_21)); }
	inline bool get_isDone_21() const { return ___isDone_21; }
	inline bool* get_address_of_isDone_21() { return &___isDone_21; }
	inline void set_isDone_21(bool value)
	{
		___isDone_21 = value;
	}

	inline static int32_t get_offset_of_isDead_22() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___isDead_22)); }
	inline bool get_isDead_22() const { return ___isDead_22; }
	inline bool* get_address_of_isDead_22() { return &___isDead_22; }
	inline void set_isDead_22(bool value)
	{
		___isDead_22 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentHealthU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___U3CCurrentHealthU3Ek__BackingField_23)); }
	inline float get_U3CCurrentHealthU3Ek__BackingField_23() const { return ___U3CCurrentHealthU3Ek__BackingField_23; }
	inline float* get_address_of_U3CCurrentHealthU3Ek__BackingField_23() { return &___U3CCurrentHealthU3Ek__BackingField_23; }
	inline void set_U3CCurrentHealthU3Ek__BackingField_23(float value)
	{
		___U3CCurrentHealthU3Ek__BackingField_23 = value;
	}

	inline static int32_t get_offset_of_U3CMaxHealthU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___U3CMaxHealthU3Ek__BackingField_24)); }
	inline float get_U3CMaxHealthU3Ek__BackingField_24() const { return ___U3CMaxHealthU3Ek__BackingField_24; }
	inline float* get_address_of_U3CMaxHealthU3Ek__BackingField_24() { return &___U3CMaxHealthU3Ek__BackingField_24; }
	inline void set_U3CMaxHealthU3Ek__BackingField_24(float value)
	{
		___U3CMaxHealthU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentHealthGuyU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___U3CCurrentHealthGuyU3Ek__BackingField_25)); }
	inline float get_U3CCurrentHealthGuyU3Ek__BackingField_25() const { return ___U3CCurrentHealthGuyU3Ek__BackingField_25; }
	inline float* get_address_of_U3CCurrentHealthGuyU3Ek__BackingField_25() { return &___U3CCurrentHealthGuyU3Ek__BackingField_25; }
	inline void set_U3CCurrentHealthGuyU3Ek__BackingField_25(float value)
	{
		___U3CCurrentHealthGuyU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_U3CMaxHealthGuyU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___U3CMaxHealthGuyU3Ek__BackingField_26)); }
	inline float get_U3CMaxHealthGuyU3Ek__BackingField_26() const { return ___U3CMaxHealthGuyU3Ek__BackingField_26; }
	inline float* get_address_of_U3CMaxHealthGuyU3Ek__BackingField_26() { return &___U3CMaxHealthGuyU3Ek__BackingField_26; }
	inline void set_U3CMaxHealthGuyU3Ek__BackingField_26(float value)
	{
		___U3CMaxHealthGuyU3Ek__BackingField_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLEIMP_T158148094_H
#ifndef BATTLEINF_T1732126188_H
#define BATTLEINF_T1732126188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Battleinf
struct  Battleinf_t1732126188  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Slider Battleinf::slider
	Slider_t3903728902 * ___slider_4;
	// UnityEngine.UI.Slider Battleinf::guySlider
	Slider_t3903728902 * ___guySlider_5;
	// TMPro.TMP_Text Battleinf::attackText
	TMP_Text_t2599618874 * ___attackText_6;
	// TMPro.TMP_Text Battleinf::nameText
	TMP_Text_t2599618874 * ___nameText_7;
	// TMPro.TMP_Text Battleinf::dogHealth
	TMP_Text_t2599618874 * ___dogHealth_8;
	// TMPro.TMP_Text Battleinf::guyHealth
	TMP_Text_t2599618874 * ___guyHealth_9;
	// UnityEngine.SpriteRenderer Battleinf::dogSprite
	SpriteRenderer_t3235626157 * ___dogSprite_10;
	// UnityEngine.UI.Image Battleinf::black
	Image_t2670269651 * ___black_11;
	// UnityEngine.Animator Battleinf::animator
	Animator_t434523843 * ___animator_12;
	// UnityEngine.AudioSource Battleinf::DogDieSFX
	AudioSource_t3935305588 * ___DogDieSFX_13;
	// UnityEngine.AudioSource Battleinf::GuydDieSFX
	AudioSource_t3935305588 * ___GuydDieSFX_14;
	// UnityEngine.GameObject Battleinf::GUI
	GameObject_t1113636619 * ___GUI_15;
	// UnityEngine.GameObject Battleinf::GameOver
	GameObject_t1113636619 * ___GameOver_16;
	// Animinf Battleinf::anim
	Animinf_t989049961 * ___anim_17;
	// System.Random Battleinf::rnd
	Random_t108471755 * ___rnd_18;
	// System.Boolean Battleinf::myTurn
	bool ___myTurn_19;
	// System.Boolean Battleinf::ableToAttack
	bool ___ableToAttack_20;
	// System.Boolean Battleinf::enemyAbleToAttack
	bool ___enemyAbleToAttack_21;
	// System.Boolean Battleinf::isReady
	bool ___isReady_22;
	// System.Boolean Battleinf::isDone
	bool ___isDone_23;
	// System.Boolean Battleinf::isDead
	bool ___isDead_24;
	// System.Byte Battleinf::r
	uint8_t ___r_25;
	// System.Byte Battleinf::g
	uint8_t ___g_26;
	// System.Byte Battleinf::b
	uint8_t ___b_27;
	// UnityEngine.Color32 Battleinf::col
	Color32_t2600501292  ___col_28;
	// System.Int32 Battleinf::n
	int32_t ___n_29;
	// System.Single Battleinf::<CurrentHealth>k__BackingField
	float ___U3CCurrentHealthU3Ek__BackingField_30;
	// System.Single Battleinf::<MaxHealth>k__BackingField
	float ___U3CMaxHealthU3Ek__BackingField_31;
	// System.Single Battleinf::<CurrentHealthGuy>k__BackingField
	float ___U3CCurrentHealthGuyU3Ek__BackingField_32;
	// System.Single Battleinf::<MaxHealthGuy>k__BackingField
	float ___U3CMaxHealthGuyU3Ek__BackingField_33;

public:
	inline static int32_t get_offset_of_slider_4() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___slider_4)); }
	inline Slider_t3903728902 * get_slider_4() const { return ___slider_4; }
	inline Slider_t3903728902 ** get_address_of_slider_4() { return &___slider_4; }
	inline void set_slider_4(Slider_t3903728902 * value)
	{
		___slider_4 = value;
		Il2CppCodeGenWriteBarrier((&___slider_4), value);
	}

	inline static int32_t get_offset_of_guySlider_5() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___guySlider_5)); }
	inline Slider_t3903728902 * get_guySlider_5() const { return ___guySlider_5; }
	inline Slider_t3903728902 ** get_address_of_guySlider_5() { return &___guySlider_5; }
	inline void set_guySlider_5(Slider_t3903728902 * value)
	{
		___guySlider_5 = value;
		Il2CppCodeGenWriteBarrier((&___guySlider_5), value);
	}

	inline static int32_t get_offset_of_attackText_6() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___attackText_6)); }
	inline TMP_Text_t2599618874 * get_attackText_6() const { return ___attackText_6; }
	inline TMP_Text_t2599618874 ** get_address_of_attackText_6() { return &___attackText_6; }
	inline void set_attackText_6(TMP_Text_t2599618874 * value)
	{
		___attackText_6 = value;
		Il2CppCodeGenWriteBarrier((&___attackText_6), value);
	}

	inline static int32_t get_offset_of_nameText_7() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___nameText_7)); }
	inline TMP_Text_t2599618874 * get_nameText_7() const { return ___nameText_7; }
	inline TMP_Text_t2599618874 ** get_address_of_nameText_7() { return &___nameText_7; }
	inline void set_nameText_7(TMP_Text_t2599618874 * value)
	{
		___nameText_7 = value;
		Il2CppCodeGenWriteBarrier((&___nameText_7), value);
	}

	inline static int32_t get_offset_of_dogHealth_8() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___dogHealth_8)); }
	inline TMP_Text_t2599618874 * get_dogHealth_8() const { return ___dogHealth_8; }
	inline TMP_Text_t2599618874 ** get_address_of_dogHealth_8() { return &___dogHealth_8; }
	inline void set_dogHealth_8(TMP_Text_t2599618874 * value)
	{
		___dogHealth_8 = value;
		Il2CppCodeGenWriteBarrier((&___dogHealth_8), value);
	}

	inline static int32_t get_offset_of_guyHealth_9() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___guyHealth_9)); }
	inline TMP_Text_t2599618874 * get_guyHealth_9() const { return ___guyHealth_9; }
	inline TMP_Text_t2599618874 ** get_address_of_guyHealth_9() { return &___guyHealth_9; }
	inline void set_guyHealth_9(TMP_Text_t2599618874 * value)
	{
		___guyHealth_9 = value;
		Il2CppCodeGenWriteBarrier((&___guyHealth_9), value);
	}

	inline static int32_t get_offset_of_dogSprite_10() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___dogSprite_10)); }
	inline SpriteRenderer_t3235626157 * get_dogSprite_10() const { return ___dogSprite_10; }
	inline SpriteRenderer_t3235626157 ** get_address_of_dogSprite_10() { return &___dogSprite_10; }
	inline void set_dogSprite_10(SpriteRenderer_t3235626157 * value)
	{
		___dogSprite_10 = value;
		Il2CppCodeGenWriteBarrier((&___dogSprite_10), value);
	}

	inline static int32_t get_offset_of_black_11() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___black_11)); }
	inline Image_t2670269651 * get_black_11() const { return ___black_11; }
	inline Image_t2670269651 ** get_address_of_black_11() { return &___black_11; }
	inline void set_black_11(Image_t2670269651 * value)
	{
		___black_11 = value;
		Il2CppCodeGenWriteBarrier((&___black_11), value);
	}

	inline static int32_t get_offset_of_animator_12() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___animator_12)); }
	inline Animator_t434523843 * get_animator_12() const { return ___animator_12; }
	inline Animator_t434523843 ** get_address_of_animator_12() { return &___animator_12; }
	inline void set_animator_12(Animator_t434523843 * value)
	{
		___animator_12 = value;
		Il2CppCodeGenWriteBarrier((&___animator_12), value);
	}

	inline static int32_t get_offset_of_DogDieSFX_13() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___DogDieSFX_13)); }
	inline AudioSource_t3935305588 * get_DogDieSFX_13() const { return ___DogDieSFX_13; }
	inline AudioSource_t3935305588 ** get_address_of_DogDieSFX_13() { return &___DogDieSFX_13; }
	inline void set_DogDieSFX_13(AudioSource_t3935305588 * value)
	{
		___DogDieSFX_13 = value;
		Il2CppCodeGenWriteBarrier((&___DogDieSFX_13), value);
	}

	inline static int32_t get_offset_of_GuydDieSFX_14() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___GuydDieSFX_14)); }
	inline AudioSource_t3935305588 * get_GuydDieSFX_14() const { return ___GuydDieSFX_14; }
	inline AudioSource_t3935305588 ** get_address_of_GuydDieSFX_14() { return &___GuydDieSFX_14; }
	inline void set_GuydDieSFX_14(AudioSource_t3935305588 * value)
	{
		___GuydDieSFX_14 = value;
		Il2CppCodeGenWriteBarrier((&___GuydDieSFX_14), value);
	}

	inline static int32_t get_offset_of_GUI_15() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___GUI_15)); }
	inline GameObject_t1113636619 * get_GUI_15() const { return ___GUI_15; }
	inline GameObject_t1113636619 ** get_address_of_GUI_15() { return &___GUI_15; }
	inline void set_GUI_15(GameObject_t1113636619 * value)
	{
		___GUI_15 = value;
		Il2CppCodeGenWriteBarrier((&___GUI_15), value);
	}

	inline static int32_t get_offset_of_GameOver_16() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___GameOver_16)); }
	inline GameObject_t1113636619 * get_GameOver_16() const { return ___GameOver_16; }
	inline GameObject_t1113636619 ** get_address_of_GameOver_16() { return &___GameOver_16; }
	inline void set_GameOver_16(GameObject_t1113636619 * value)
	{
		___GameOver_16 = value;
		Il2CppCodeGenWriteBarrier((&___GameOver_16), value);
	}

	inline static int32_t get_offset_of_anim_17() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___anim_17)); }
	inline Animinf_t989049961 * get_anim_17() const { return ___anim_17; }
	inline Animinf_t989049961 ** get_address_of_anim_17() { return &___anim_17; }
	inline void set_anim_17(Animinf_t989049961 * value)
	{
		___anim_17 = value;
		Il2CppCodeGenWriteBarrier((&___anim_17), value);
	}

	inline static int32_t get_offset_of_rnd_18() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___rnd_18)); }
	inline Random_t108471755 * get_rnd_18() const { return ___rnd_18; }
	inline Random_t108471755 ** get_address_of_rnd_18() { return &___rnd_18; }
	inline void set_rnd_18(Random_t108471755 * value)
	{
		___rnd_18 = value;
		Il2CppCodeGenWriteBarrier((&___rnd_18), value);
	}

	inline static int32_t get_offset_of_myTurn_19() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___myTurn_19)); }
	inline bool get_myTurn_19() const { return ___myTurn_19; }
	inline bool* get_address_of_myTurn_19() { return &___myTurn_19; }
	inline void set_myTurn_19(bool value)
	{
		___myTurn_19 = value;
	}

	inline static int32_t get_offset_of_ableToAttack_20() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___ableToAttack_20)); }
	inline bool get_ableToAttack_20() const { return ___ableToAttack_20; }
	inline bool* get_address_of_ableToAttack_20() { return &___ableToAttack_20; }
	inline void set_ableToAttack_20(bool value)
	{
		___ableToAttack_20 = value;
	}

	inline static int32_t get_offset_of_enemyAbleToAttack_21() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___enemyAbleToAttack_21)); }
	inline bool get_enemyAbleToAttack_21() const { return ___enemyAbleToAttack_21; }
	inline bool* get_address_of_enemyAbleToAttack_21() { return &___enemyAbleToAttack_21; }
	inline void set_enemyAbleToAttack_21(bool value)
	{
		___enemyAbleToAttack_21 = value;
	}

	inline static int32_t get_offset_of_isReady_22() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___isReady_22)); }
	inline bool get_isReady_22() const { return ___isReady_22; }
	inline bool* get_address_of_isReady_22() { return &___isReady_22; }
	inline void set_isReady_22(bool value)
	{
		___isReady_22 = value;
	}

	inline static int32_t get_offset_of_isDone_23() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___isDone_23)); }
	inline bool get_isDone_23() const { return ___isDone_23; }
	inline bool* get_address_of_isDone_23() { return &___isDone_23; }
	inline void set_isDone_23(bool value)
	{
		___isDone_23 = value;
	}

	inline static int32_t get_offset_of_isDead_24() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___isDead_24)); }
	inline bool get_isDead_24() const { return ___isDead_24; }
	inline bool* get_address_of_isDead_24() { return &___isDead_24; }
	inline void set_isDead_24(bool value)
	{
		___isDead_24 = value;
	}

	inline static int32_t get_offset_of_r_25() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___r_25)); }
	inline uint8_t get_r_25() const { return ___r_25; }
	inline uint8_t* get_address_of_r_25() { return &___r_25; }
	inline void set_r_25(uint8_t value)
	{
		___r_25 = value;
	}

	inline static int32_t get_offset_of_g_26() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___g_26)); }
	inline uint8_t get_g_26() const { return ___g_26; }
	inline uint8_t* get_address_of_g_26() { return &___g_26; }
	inline void set_g_26(uint8_t value)
	{
		___g_26 = value;
	}

	inline static int32_t get_offset_of_b_27() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___b_27)); }
	inline uint8_t get_b_27() const { return ___b_27; }
	inline uint8_t* get_address_of_b_27() { return &___b_27; }
	inline void set_b_27(uint8_t value)
	{
		___b_27 = value;
	}

	inline static int32_t get_offset_of_col_28() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___col_28)); }
	inline Color32_t2600501292  get_col_28() const { return ___col_28; }
	inline Color32_t2600501292 * get_address_of_col_28() { return &___col_28; }
	inline void set_col_28(Color32_t2600501292  value)
	{
		___col_28 = value;
	}

	inline static int32_t get_offset_of_n_29() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___n_29)); }
	inline int32_t get_n_29() const { return ___n_29; }
	inline int32_t* get_address_of_n_29() { return &___n_29; }
	inline void set_n_29(int32_t value)
	{
		___n_29 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentHealthU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___U3CCurrentHealthU3Ek__BackingField_30)); }
	inline float get_U3CCurrentHealthU3Ek__BackingField_30() const { return ___U3CCurrentHealthU3Ek__BackingField_30; }
	inline float* get_address_of_U3CCurrentHealthU3Ek__BackingField_30() { return &___U3CCurrentHealthU3Ek__BackingField_30; }
	inline void set_U3CCurrentHealthU3Ek__BackingField_30(float value)
	{
		___U3CCurrentHealthU3Ek__BackingField_30 = value;
	}

	inline static int32_t get_offset_of_U3CMaxHealthU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___U3CMaxHealthU3Ek__BackingField_31)); }
	inline float get_U3CMaxHealthU3Ek__BackingField_31() const { return ___U3CMaxHealthU3Ek__BackingField_31; }
	inline float* get_address_of_U3CMaxHealthU3Ek__BackingField_31() { return &___U3CMaxHealthU3Ek__BackingField_31; }
	inline void set_U3CMaxHealthU3Ek__BackingField_31(float value)
	{
		___U3CMaxHealthU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentHealthGuyU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___U3CCurrentHealthGuyU3Ek__BackingField_32)); }
	inline float get_U3CCurrentHealthGuyU3Ek__BackingField_32() const { return ___U3CCurrentHealthGuyU3Ek__BackingField_32; }
	inline float* get_address_of_U3CCurrentHealthGuyU3Ek__BackingField_32() { return &___U3CCurrentHealthGuyU3Ek__BackingField_32; }
	inline void set_U3CCurrentHealthGuyU3Ek__BackingField_32(float value)
	{
		___U3CCurrentHealthGuyU3Ek__BackingField_32 = value;
	}

	inline static int32_t get_offset_of_U3CMaxHealthGuyU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___U3CMaxHealthGuyU3Ek__BackingField_33)); }
	inline float get_U3CMaxHealthGuyU3Ek__BackingField_33() const { return ___U3CMaxHealthGuyU3Ek__BackingField_33; }
	inline float* get_address_of_U3CMaxHealthGuyU3Ek__BackingField_33() { return &___U3CMaxHealthGuyU3Ek__BackingField_33; }
	inline void set_U3CMaxHealthGuyU3Ek__BackingField_33(float value)
	{
		___U3CMaxHealthGuyU3Ek__BackingField_33 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLEINF_T1732126188_H
#ifndef COLOR_T2591083401_H
#define COLOR_T2591083401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Color
struct  Color_t2591083401  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Slider Color::slider
	Slider_t3903728902 * ___slider_4;
	// UnityEngine.UI.Image Color::img
	Image_t2670269651 * ___img_5;
	// UnityEngine.Color32 Color::col
	Color32_t2600501292  ___col_6;
	// System.Single Color::val
	float ___val_7;

public:
	inline static int32_t get_offset_of_slider_4() { return static_cast<int32_t>(offsetof(Color_t2591083401, ___slider_4)); }
	inline Slider_t3903728902 * get_slider_4() const { return ___slider_4; }
	inline Slider_t3903728902 ** get_address_of_slider_4() { return &___slider_4; }
	inline void set_slider_4(Slider_t3903728902 * value)
	{
		___slider_4 = value;
		Il2CppCodeGenWriteBarrier((&___slider_4), value);
	}

	inline static int32_t get_offset_of_img_5() { return static_cast<int32_t>(offsetof(Color_t2591083401, ___img_5)); }
	inline Image_t2670269651 * get_img_5() const { return ___img_5; }
	inline Image_t2670269651 ** get_address_of_img_5() { return &___img_5; }
	inline void set_img_5(Image_t2670269651 * value)
	{
		___img_5 = value;
		Il2CppCodeGenWriteBarrier((&___img_5), value);
	}

	inline static int32_t get_offset_of_col_6() { return static_cast<int32_t>(offsetof(Color_t2591083401, ___col_6)); }
	inline Color32_t2600501292  get_col_6() const { return ___col_6; }
	inline Color32_t2600501292 * get_address_of_col_6() { return &___col_6; }
	inline void set_col_6(Color32_t2600501292  value)
	{
		___col_6 = value;
	}

	inline static int32_t get_offset_of_val_7() { return static_cast<int32_t>(offsetof(Color_t2591083401, ___val_7)); }
	inline float get_val_7() const { return ___val_7; }
	inline float* get_address_of_val_7() { return &___val_7; }
	inline void set_val_7(float value)
	{
		___val_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2591083401_H
#ifndef COLORIMP_T1969227374_H
#define COLORIMP_T1969227374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorimp
struct  Colorimp_t1969227374  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Slider Colorimp::slider
	Slider_t3903728902 * ___slider_4;
	// UnityEngine.UI.Image Colorimp::img
	Image_t2670269651 * ___img_5;
	// UnityEngine.Color32 Colorimp::col
	Color32_t2600501292  ___col_6;
	// System.Single Colorimp::val
	float ___val_7;

public:
	inline static int32_t get_offset_of_slider_4() { return static_cast<int32_t>(offsetof(Colorimp_t1969227374, ___slider_4)); }
	inline Slider_t3903728902 * get_slider_4() const { return ___slider_4; }
	inline Slider_t3903728902 ** get_address_of_slider_4() { return &___slider_4; }
	inline void set_slider_4(Slider_t3903728902 * value)
	{
		___slider_4 = value;
		Il2CppCodeGenWriteBarrier((&___slider_4), value);
	}

	inline static int32_t get_offset_of_img_5() { return static_cast<int32_t>(offsetof(Colorimp_t1969227374, ___img_5)); }
	inline Image_t2670269651 * get_img_5() const { return ___img_5; }
	inline Image_t2670269651 ** get_address_of_img_5() { return &___img_5; }
	inline void set_img_5(Image_t2670269651 * value)
	{
		___img_5 = value;
		Il2CppCodeGenWriteBarrier((&___img_5), value);
	}

	inline static int32_t get_offset_of_col_6() { return static_cast<int32_t>(offsetof(Colorimp_t1969227374, ___col_6)); }
	inline Color32_t2600501292  get_col_6() const { return ___col_6; }
	inline Color32_t2600501292 * get_address_of_col_6() { return &___col_6; }
	inline void set_col_6(Color32_t2600501292  value)
	{
		___col_6 = value;
	}

	inline static int32_t get_offset_of_val_7() { return static_cast<int32_t>(offsetof(Colorimp_t1969227374, ___val_7)); }
	inline float get_val_7() const { return ___val_7; }
	inline float* get_address_of_val_7() { return &___val_7; }
	inline void set_val_7(float value)
	{
		___val_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORIMP_T1969227374_H
#ifndef COLORINF_T4284197021_H
#define COLORINF_T4284197021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorinf
struct  Colorinf_t4284197021  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Slider Colorinf::slider
	Slider_t3903728902 * ___slider_4;
	// UnityEngine.UI.Image Colorinf::img
	Image_t2670269651 * ___img_5;
	// UnityEngine.Color32 Colorinf::col
	Color32_t2600501292  ___col_6;
	// System.Single Colorinf::val
	float ___val_7;

public:
	inline static int32_t get_offset_of_slider_4() { return static_cast<int32_t>(offsetof(Colorinf_t4284197021, ___slider_4)); }
	inline Slider_t3903728902 * get_slider_4() const { return ___slider_4; }
	inline Slider_t3903728902 ** get_address_of_slider_4() { return &___slider_4; }
	inline void set_slider_4(Slider_t3903728902 * value)
	{
		___slider_4 = value;
		Il2CppCodeGenWriteBarrier((&___slider_4), value);
	}

	inline static int32_t get_offset_of_img_5() { return static_cast<int32_t>(offsetof(Colorinf_t4284197021, ___img_5)); }
	inline Image_t2670269651 * get_img_5() const { return ___img_5; }
	inline Image_t2670269651 ** get_address_of_img_5() { return &___img_5; }
	inline void set_img_5(Image_t2670269651 * value)
	{
		___img_5 = value;
		Il2CppCodeGenWriteBarrier((&___img_5), value);
	}

	inline static int32_t get_offset_of_col_6() { return static_cast<int32_t>(offsetof(Colorinf_t4284197021, ___col_6)); }
	inline Color32_t2600501292  get_col_6() const { return ___col_6; }
	inline Color32_t2600501292 * get_address_of_col_6() { return &___col_6; }
	inline void set_col_6(Color32_t2600501292  value)
	{
		___col_6 = value;
	}

	inline static int32_t get_offset_of_val_7() { return static_cast<int32_t>(offsetof(Colorinf_t4284197021, ___val_7)); }
	inline float get_val_7() const { return ___val_7; }
	inline float* get_address_of_val_7() { return &___val_7; }
	inline void set_val_7(float value)
	{
		___val_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORINF_T4284197021_H
#ifndef BOOTSEQ_T2506222928_H
#define BOOTSEQ_T2506222928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.BootSeq
struct  BootSeq_t2506222928  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct BootSeq_t2506222928_StaticFields
{
public:
	// System.String Harmony.BootSeq::BootSequence
	String_t* ___BootSequence_4;

public:
	inline static int32_t get_offset_of_BootSequence_4() { return static_cast<int32_t>(offsetof(BootSeq_t2506222928_StaticFields, ___BootSequence_4)); }
	inline String_t* get_BootSequence_4() const { return ___BootSequence_4; }
	inline String_t** get_address_of_BootSequence_4() { return &___BootSequence_4; }
	inline void set_BootSequence_4(String_t* value)
	{
		___BootSequence_4 = value;
		Il2CppCodeGenWriteBarrier((&___BootSequence_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOTSEQ_T2506222928_H
#ifndef DIFFICULTY_T3960432064_H
#define DIFFICULTY_T3960432064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Difficulty
struct  Difficulty_t3960432064  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Difficulty_t3960432064_StaticFields
{
public:
	// System.String Harmony.Difficulty::a
	String_t* ___a_4;
	// System.String Harmony.Difficulty::b
	String_t* ___b_5;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Difficulty_t3960432064_StaticFields, ___a_4)); }
	inline String_t* get_a_4() const { return ___a_4; }
	inline String_t** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(String_t* value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(Difficulty_t3960432064_StaticFields, ___b_5)); }
	inline String_t* get_b_5() const { return ___b_5; }
	inline String_t** get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(String_t* value)
	{
		___b_5 = value;
		Il2CppCodeGenWriteBarrier((&___b_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIFFICULTY_T3960432064_H
#ifndef DOGGO_T4070438711_H
#define DOGGO_T4070438711_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Doggo
struct  Doggo_t4070438711  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Doggo_t4070438711_StaticFields
{
public:
	// System.String Harmony.Doggo::a
	String_t* ___a_4;
	// System.String Harmony.Doggo::b
	String_t* ___b_5;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Doggo_t4070438711_StaticFields, ___a_4)); }
	inline String_t* get_a_4() const { return ___a_4; }
	inline String_t** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(String_t* value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(Doggo_t4070438711_StaticFields, ___b_5)); }
	inline String_t* get_b_5() const { return ___b_5; }
	inline String_t** get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(String_t* value)
	{
		___b_5 = value;
		Il2CppCodeGenWriteBarrier((&___b_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOGGO_T4070438711_H
#ifndef INPUT_T3716682282_H
#define INPUT_T3716682282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Input
struct  Input_t3716682282  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Input_t3716682282_StaticFields
{
public:
	// System.String Harmony.Input::a
	String_t* ___a_4;
	// System.String Harmony.Input::b
	String_t* ___b_5;
	// System.String Harmony.Input::c
	String_t* ___c_6;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Input_t3716682282_StaticFields, ___a_4)); }
	inline String_t* get_a_4() const { return ___a_4; }
	inline String_t** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(String_t* value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(Input_t3716682282_StaticFields, ___b_5)); }
	inline String_t* get_b_5() const { return ___b_5; }
	inline String_t** get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(String_t* value)
	{
		___b_5 = value;
		Il2CppCodeGenWriteBarrier((&___b_5), value);
	}

	inline static int32_t get_offset_of_c_6() { return static_cast<int32_t>(offsetof(Input_t3716682282_StaticFields, ___c_6)); }
	inline String_t* get_c_6() const { return ___c_6; }
	inline String_t** get_address_of_c_6() { return &___c_6; }
	inline void set_c_6(String_t* value)
	{
		___c_6 = value;
		Il2CppCodeGenWriteBarrier((&___c_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUT_T3716682282_H
#ifndef PROGRAM_T1866495201_H
#define PROGRAM_T1866495201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Program
struct  Program_t1866495201  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioSource Harmony.Program::bootSFX
	AudioSource_t3935305588 * ___bootSFX_4;
	// UnityEngine.AudioSource Harmony.Program::messageSFX
	AudioSource_t3935305588 * ___messageSFX_5;
	// UnityEngine.AudioSource Harmony.Program::leetSFX
	AudioSource_t3935305588 * ___leetSFX_6;
	// System.Boolean Harmony.Program::inCoroutine
	bool ___inCoroutine_8;
	// TMPro.TMP_Text Harmony.Program::startText
	TMP_Text_t2599618874 * ___startText_9;
	// System.Int32 Harmony.Program::i
	int32_t ___i_10;

public:
	inline static int32_t get_offset_of_bootSFX_4() { return static_cast<int32_t>(offsetof(Program_t1866495201, ___bootSFX_4)); }
	inline AudioSource_t3935305588 * get_bootSFX_4() const { return ___bootSFX_4; }
	inline AudioSource_t3935305588 ** get_address_of_bootSFX_4() { return &___bootSFX_4; }
	inline void set_bootSFX_4(AudioSource_t3935305588 * value)
	{
		___bootSFX_4 = value;
		Il2CppCodeGenWriteBarrier((&___bootSFX_4), value);
	}

	inline static int32_t get_offset_of_messageSFX_5() { return static_cast<int32_t>(offsetof(Program_t1866495201, ___messageSFX_5)); }
	inline AudioSource_t3935305588 * get_messageSFX_5() const { return ___messageSFX_5; }
	inline AudioSource_t3935305588 ** get_address_of_messageSFX_5() { return &___messageSFX_5; }
	inline void set_messageSFX_5(AudioSource_t3935305588 * value)
	{
		___messageSFX_5 = value;
		Il2CppCodeGenWriteBarrier((&___messageSFX_5), value);
	}

	inline static int32_t get_offset_of_leetSFX_6() { return static_cast<int32_t>(offsetof(Program_t1866495201, ___leetSFX_6)); }
	inline AudioSource_t3935305588 * get_leetSFX_6() const { return ___leetSFX_6; }
	inline AudioSource_t3935305588 ** get_address_of_leetSFX_6() { return &___leetSFX_6; }
	inline void set_leetSFX_6(AudioSource_t3935305588 * value)
	{
		___leetSFX_6 = value;
		Il2CppCodeGenWriteBarrier((&___leetSFX_6), value);
	}

	inline static int32_t get_offset_of_inCoroutine_8() { return static_cast<int32_t>(offsetof(Program_t1866495201, ___inCoroutine_8)); }
	inline bool get_inCoroutine_8() const { return ___inCoroutine_8; }
	inline bool* get_address_of_inCoroutine_8() { return &___inCoroutine_8; }
	inline void set_inCoroutine_8(bool value)
	{
		___inCoroutine_8 = value;
	}

	inline static int32_t get_offset_of_startText_9() { return static_cast<int32_t>(offsetof(Program_t1866495201, ___startText_9)); }
	inline TMP_Text_t2599618874 * get_startText_9() const { return ___startText_9; }
	inline TMP_Text_t2599618874 ** get_address_of_startText_9() { return &___startText_9; }
	inline void set_startText_9(TMP_Text_t2599618874 * value)
	{
		___startText_9 = value;
		Il2CppCodeGenWriteBarrier((&___startText_9), value);
	}

	inline static int32_t get_offset_of_i_10() { return static_cast<int32_t>(offsetof(Program_t1866495201, ___i_10)); }
	inline int32_t get_i_10() const { return ___i_10; }
	inline int32_t* get_address_of_i_10() { return &___i_10; }
	inline void set_i_10(int32_t value)
	{
		___i_10 = value;
	}
};

struct Program_t1866495201_StaticFields
{
public:
	// System.Single Harmony.Program::normTypeSpeed
	float ___normTypeSpeed_7;

public:
	inline static int32_t get_offset_of_normTypeSpeed_7() { return static_cast<int32_t>(offsetof(Program_t1866495201_StaticFields, ___normTypeSpeed_7)); }
	inline float get_normTypeSpeed_7() const { return ___normTypeSpeed_7; }
	inline float* get_address_of_normTypeSpeed_7() { return &___normTypeSpeed_7; }
	inline void set_normTypeSpeed_7(float value)
	{
		___normTypeSpeed_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRAM_T1866495201_H
#ifndef PROGRAM2_T3485103329_H
#define PROGRAM2_T3485103329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Program2
struct  Program2_t3485103329  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioSource Harmony.Program2::messageSFX
	AudioSource_t3935305588 * ___messageSFX_4;
	// UnityEngine.AudioSource Harmony.Program2::leetSFX
	AudioSource_t3935305588 * ___leetSFX_5;
	// System.Boolean Harmony.Program2::inCoroutine
	bool ___inCoroutine_7;
	// TMPro.TMP_Text Harmony.Program2::startText
	TMP_Text_t2599618874 * ___startText_8;
	// System.Int32 Harmony.Program2::i
	int32_t ___i_9;

public:
	inline static int32_t get_offset_of_messageSFX_4() { return static_cast<int32_t>(offsetof(Program2_t3485103329, ___messageSFX_4)); }
	inline AudioSource_t3935305588 * get_messageSFX_4() const { return ___messageSFX_4; }
	inline AudioSource_t3935305588 ** get_address_of_messageSFX_4() { return &___messageSFX_4; }
	inline void set_messageSFX_4(AudioSource_t3935305588 * value)
	{
		___messageSFX_4 = value;
		Il2CppCodeGenWriteBarrier((&___messageSFX_4), value);
	}

	inline static int32_t get_offset_of_leetSFX_5() { return static_cast<int32_t>(offsetof(Program2_t3485103329, ___leetSFX_5)); }
	inline AudioSource_t3935305588 * get_leetSFX_5() const { return ___leetSFX_5; }
	inline AudioSource_t3935305588 ** get_address_of_leetSFX_5() { return &___leetSFX_5; }
	inline void set_leetSFX_5(AudioSource_t3935305588 * value)
	{
		___leetSFX_5 = value;
		Il2CppCodeGenWriteBarrier((&___leetSFX_5), value);
	}

	inline static int32_t get_offset_of_inCoroutine_7() { return static_cast<int32_t>(offsetof(Program2_t3485103329, ___inCoroutine_7)); }
	inline bool get_inCoroutine_7() const { return ___inCoroutine_7; }
	inline bool* get_address_of_inCoroutine_7() { return &___inCoroutine_7; }
	inline void set_inCoroutine_7(bool value)
	{
		___inCoroutine_7 = value;
	}

	inline static int32_t get_offset_of_startText_8() { return static_cast<int32_t>(offsetof(Program2_t3485103329, ___startText_8)); }
	inline TMP_Text_t2599618874 * get_startText_8() const { return ___startText_8; }
	inline TMP_Text_t2599618874 ** get_address_of_startText_8() { return &___startText_8; }
	inline void set_startText_8(TMP_Text_t2599618874 * value)
	{
		___startText_8 = value;
		Il2CppCodeGenWriteBarrier((&___startText_8), value);
	}

	inline static int32_t get_offset_of_i_9() { return static_cast<int32_t>(offsetof(Program2_t3485103329, ___i_9)); }
	inline int32_t get_i_9() const { return ___i_9; }
	inline int32_t* get_address_of_i_9() { return &___i_9; }
	inline void set_i_9(int32_t value)
	{
		___i_9 = value;
	}
};

struct Program2_t3485103329_StaticFields
{
public:
	// System.Single Harmony.Program2::normTypeSpeed
	float ___normTypeSpeed_6;

public:
	inline static int32_t get_offset_of_normTypeSpeed_6() { return static_cast<int32_t>(offsetof(Program2_t3485103329_StaticFields, ___normTypeSpeed_6)); }
	inline float get_normTypeSpeed_6() const { return ___normTypeSpeed_6; }
	inline float* get_address_of_normTypeSpeed_6() { return &___normTypeSpeed_6; }
	inline void set_normTypeSpeed_6(float value)
	{
		___normTypeSpeed_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRAM2_T3485103329_H
#ifndef PROGRAM3_T1528788193_H
#define PROGRAM3_T1528788193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Program3
struct  Program3_t1528788193  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioSource Harmony.Program3::messageSFX
	AudioSource_t3935305588 * ___messageSFX_4;
	// UnityEngine.AudioSource Harmony.Program3::doggoSFX
	AudioSource_t3935305588 * ___doggoSFX_5;
	// System.Boolean Harmony.Program3::inCoroutine
	bool ___inCoroutine_7;
	// TMPro.TMP_Text Harmony.Program3::startText
	TMP_Text_t2599618874 * ___startText_8;
	// System.Int32 Harmony.Program3::i
	int32_t ___i_9;

public:
	inline static int32_t get_offset_of_messageSFX_4() { return static_cast<int32_t>(offsetof(Program3_t1528788193, ___messageSFX_4)); }
	inline AudioSource_t3935305588 * get_messageSFX_4() const { return ___messageSFX_4; }
	inline AudioSource_t3935305588 ** get_address_of_messageSFX_4() { return &___messageSFX_4; }
	inline void set_messageSFX_4(AudioSource_t3935305588 * value)
	{
		___messageSFX_4 = value;
		Il2CppCodeGenWriteBarrier((&___messageSFX_4), value);
	}

	inline static int32_t get_offset_of_doggoSFX_5() { return static_cast<int32_t>(offsetof(Program3_t1528788193, ___doggoSFX_5)); }
	inline AudioSource_t3935305588 * get_doggoSFX_5() const { return ___doggoSFX_5; }
	inline AudioSource_t3935305588 ** get_address_of_doggoSFX_5() { return &___doggoSFX_5; }
	inline void set_doggoSFX_5(AudioSource_t3935305588 * value)
	{
		___doggoSFX_5 = value;
		Il2CppCodeGenWriteBarrier((&___doggoSFX_5), value);
	}

	inline static int32_t get_offset_of_inCoroutine_7() { return static_cast<int32_t>(offsetof(Program3_t1528788193, ___inCoroutine_7)); }
	inline bool get_inCoroutine_7() const { return ___inCoroutine_7; }
	inline bool* get_address_of_inCoroutine_7() { return &___inCoroutine_7; }
	inline void set_inCoroutine_7(bool value)
	{
		___inCoroutine_7 = value;
	}

	inline static int32_t get_offset_of_startText_8() { return static_cast<int32_t>(offsetof(Program3_t1528788193, ___startText_8)); }
	inline TMP_Text_t2599618874 * get_startText_8() const { return ___startText_8; }
	inline TMP_Text_t2599618874 ** get_address_of_startText_8() { return &___startText_8; }
	inline void set_startText_8(TMP_Text_t2599618874 * value)
	{
		___startText_8 = value;
		Il2CppCodeGenWriteBarrier((&___startText_8), value);
	}

	inline static int32_t get_offset_of_i_9() { return static_cast<int32_t>(offsetof(Program3_t1528788193, ___i_9)); }
	inline int32_t get_i_9() const { return ___i_9; }
	inline int32_t* get_address_of_i_9() { return &___i_9; }
	inline void set_i_9(int32_t value)
	{
		___i_9 = value;
	}
};

struct Program3_t1528788193_StaticFields
{
public:
	// System.Single Harmony.Program3::normTypeSpeed
	float ___normTypeSpeed_6;

public:
	inline static int32_t get_offset_of_normTypeSpeed_6() { return static_cast<int32_t>(offsetof(Program3_t1528788193_StaticFields, ___normTypeSpeed_6)); }
	inline float get_normTypeSpeed_6() const { return ___normTypeSpeed_6; }
	inline float* get_address_of_normTypeSpeed_6() { return &___normTypeSpeed_6; }
	inline void set_normTypeSpeed_6(float value)
	{
		___normTypeSpeed_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRAM3_T1528788193_H
#ifndef TYPE_T4058249690_H
#define TYPE_T4058249690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Type
struct  Type_t4058249690  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Type_t4058249690_StaticFields
{
public:
	// Harmony.Type Harmony.Type::instance
	Type_t4058249690 * ___instance_4;
	// Harmony.Program Harmony.Type::program
	Program_t1866495201 * ___program_5;
	// System.Collections.IEnumerator Harmony.Type::coroutine
	RuntimeObject* ___coroutine_6;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(Type_t4058249690_StaticFields, ___instance_4)); }
	inline Type_t4058249690 * get_instance_4() const { return ___instance_4; }
	inline Type_t4058249690 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(Type_t4058249690 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_program_5() { return static_cast<int32_t>(offsetof(Type_t4058249690_StaticFields, ___program_5)); }
	inline Program_t1866495201 * get_program_5() const { return ___program_5; }
	inline Program_t1866495201 ** get_address_of_program_5() { return &___program_5; }
	inline void set_program_5(Program_t1866495201 * value)
	{
		___program_5 = value;
		Il2CppCodeGenWriteBarrier((&___program_5), value);
	}

	inline static int32_t get_offset_of_coroutine_6() { return static_cast<int32_t>(offsetof(Type_t4058249690_StaticFields, ___coroutine_6)); }
	inline RuntimeObject* get_coroutine_6() const { return ___coroutine_6; }
	inline RuntimeObject** get_address_of_coroutine_6() { return &___coroutine_6; }
	inline void set_coroutine_6(RuntimeObject* value)
	{
		___coroutine_6 = value;
		Il2CppCodeGenWriteBarrier((&___coroutine_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T4058249690_H
#ifndef TYPE2_T2499874421_H
#define TYPE2_T2499874421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Type2
struct  Type2_t2499874421  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Type2_t2499874421_StaticFields
{
public:
	// Harmony.Type2 Harmony.Type2::instance
	Type2_t2499874421 * ___instance_4;
	// Harmony.Program2 Harmony.Type2::program2
	Program2_t3485103329 * ___program2_5;
	// System.Collections.IEnumerator Harmony.Type2::coroutine
	RuntimeObject* ___coroutine_6;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(Type2_t2499874421_StaticFields, ___instance_4)); }
	inline Type2_t2499874421 * get_instance_4() const { return ___instance_4; }
	inline Type2_t2499874421 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(Type2_t2499874421 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_program2_5() { return static_cast<int32_t>(offsetof(Type2_t2499874421_StaticFields, ___program2_5)); }
	inline Program2_t3485103329 * get_program2_5() const { return ___program2_5; }
	inline Program2_t3485103329 ** get_address_of_program2_5() { return &___program2_5; }
	inline void set_program2_5(Program2_t3485103329 * value)
	{
		___program2_5 = value;
		Il2CppCodeGenWriteBarrier((&___program2_5), value);
	}

	inline static int32_t get_offset_of_coroutine_6() { return static_cast<int32_t>(offsetof(Type2_t2499874421_StaticFields, ___coroutine_6)); }
	inline RuntimeObject* get_coroutine_6() const { return ___coroutine_6; }
	inline RuntimeObject** get_address_of_coroutine_6() { return &___coroutine_6; }
	inline void set_coroutine_6(RuntimeObject* value)
	{
		___coroutine_6 = value;
		Il2CppCodeGenWriteBarrier((&___coroutine_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE2_T2499874421_H
#ifndef TYPE3_T2499874422_H
#define TYPE3_T2499874422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Type3
struct  Type3_t2499874422  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Type3_t2499874422_StaticFields
{
public:
	// Harmony.Type3 Harmony.Type3::instance
	Type3_t2499874422 * ___instance_4;
	// Harmony.Program3 Harmony.Type3::program3
	Program3_t1528788193 * ___program3_5;
	// System.Collections.IEnumerator Harmony.Type3::coroutine
	RuntimeObject* ___coroutine_6;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(Type3_t2499874422_StaticFields, ___instance_4)); }
	inline Type3_t2499874422 * get_instance_4() const { return ___instance_4; }
	inline Type3_t2499874422 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(Type3_t2499874422 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_program3_5() { return static_cast<int32_t>(offsetof(Type3_t2499874422_StaticFields, ___program3_5)); }
	inline Program3_t1528788193 * get_program3_5() const { return ___program3_5; }
	inline Program3_t1528788193 ** get_address_of_program3_5() { return &___program3_5; }
	inline void set_program3_5(Program3_t1528788193 * value)
	{
		___program3_5 = value;
		Il2CppCodeGenWriteBarrier((&___program3_5), value);
	}

	inline static int32_t get_offset_of_coroutine_6() { return static_cast<int32_t>(offsetof(Type3_t2499874422_StaticFields, ___coroutine_6)); }
	inline RuntimeObject* get_coroutine_6() const { return ___coroutine_6; }
	inline RuntimeObject** get_address_of_coroutine_6() { return &___coroutine_6; }
	inline void set_coroutine_6(RuntimeObject* value)
	{
		___coroutine_6 = value;
		Il2CppCodeGenWriteBarrier((&___coroutine_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE3_T2499874422_H
#ifndef MENUPROGRAM_T465836879_H
#define MENUPROGRAM_T465836879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.menuProgram
struct  menuProgram_t465836879  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_Text Harmony.menuProgram::startText
	TMP_Text_t2599618874 * ___startText_4;
	// UnityEngine.GameObject Harmony.menuProgram::doggoHuntBTN
	GameObject_t1113636619 * ___doggoHuntBTN_5;
	// System.String Harmony.menuProgram::str1
	String_t* ___str1_6;
	// System.String Harmony.menuProgram::str2
	String_t* ___str2_7;

public:
	inline static int32_t get_offset_of_startText_4() { return static_cast<int32_t>(offsetof(menuProgram_t465836879, ___startText_4)); }
	inline TMP_Text_t2599618874 * get_startText_4() const { return ___startText_4; }
	inline TMP_Text_t2599618874 ** get_address_of_startText_4() { return &___startText_4; }
	inline void set_startText_4(TMP_Text_t2599618874 * value)
	{
		___startText_4 = value;
		Il2CppCodeGenWriteBarrier((&___startText_4), value);
	}

	inline static int32_t get_offset_of_doggoHuntBTN_5() { return static_cast<int32_t>(offsetof(menuProgram_t465836879, ___doggoHuntBTN_5)); }
	inline GameObject_t1113636619 * get_doggoHuntBTN_5() const { return ___doggoHuntBTN_5; }
	inline GameObject_t1113636619 ** get_address_of_doggoHuntBTN_5() { return &___doggoHuntBTN_5; }
	inline void set_doggoHuntBTN_5(GameObject_t1113636619 * value)
	{
		___doggoHuntBTN_5 = value;
		Il2CppCodeGenWriteBarrier((&___doggoHuntBTN_5), value);
	}

	inline static int32_t get_offset_of_str1_6() { return static_cast<int32_t>(offsetof(menuProgram_t465836879, ___str1_6)); }
	inline String_t* get_str1_6() const { return ___str1_6; }
	inline String_t** get_address_of_str1_6() { return &___str1_6; }
	inline void set_str1_6(String_t* value)
	{
		___str1_6 = value;
		Il2CppCodeGenWriteBarrier((&___str1_6), value);
	}

	inline static int32_t get_offset_of_str2_7() { return static_cast<int32_t>(offsetof(menuProgram_t465836879, ___str2_7)); }
	inline String_t* get_str2_7() const { return ___str2_7; }
	inline String_t** get_address_of_str2_7() { return &___str2_7; }
	inline void set_str2_7(String_t* value)
	{
		___str2_7 = value;
		Il2CppCodeGenWriteBarrier((&___str2_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUPROGRAM_T465836879_H
#ifndef SPRITE_T2249211761_H
#define SPRITE_T2249211761_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sprite
struct  Sprite_t2249211761  : public MonoBehaviour_t3962482529
{
public:
	// Battle Sprite::battle
	Battle_t2191861408 * ___battle_4;
	// UnityEngine.Animator Sprite::anim
	Animator_t434523843 * ___anim_5;
	// System.Boolean Sprite::stahp
	bool ___stahp_6;

public:
	inline static int32_t get_offset_of_battle_4() { return static_cast<int32_t>(offsetof(Sprite_t2249211761, ___battle_4)); }
	inline Battle_t2191861408 * get_battle_4() const { return ___battle_4; }
	inline Battle_t2191861408 ** get_address_of_battle_4() { return &___battle_4; }
	inline void set_battle_4(Battle_t2191861408 * value)
	{
		___battle_4 = value;
		Il2CppCodeGenWriteBarrier((&___battle_4), value);
	}

	inline static int32_t get_offset_of_anim_5() { return static_cast<int32_t>(offsetof(Sprite_t2249211761, ___anim_5)); }
	inline Animator_t434523843 * get_anim_5() const { return ___anim_5; }
	inline Animator_t434523843 ** get_address_of_anim_5() { return &___anim_5; }
	inline void set_anim_5(Animator_t434523843 * value)
	{
		___anim_5 = value;
		Il2CppCodeGenWriteBarrier((&___anim_5), value);
	}

	inline static int32_t get_offset_of_stahp_6() { return static_cast<int32_t>(offsetof(Sprite_t2249211761, ___stahp_6)); }
	inline bool get_stahp_6() const { return ___stahp_6; }
	inline bool* get_address_of_stahp_6() { return &___stahp_6; }
	inline void set_stahp_6(bool value)
	{
		___stahp_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITE_T2249211761_H
#ifndef SPRITEIMP_T3952321968_H
#define SPRITEIMP_T3952321968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spriteimp
struct  Spriteimp_t3952321968  : public MonoBehaviour_t3962482529
{
public:
	// Battleimp Spriteimp::battle
	Battleimp_t158148094 * ___battle_4;
	// UnityEngine.Animator Spriteimp::anim
	Animator_t434523843 * ___anim_5;
	// System.Boolean Spriteimp::stahp
	bool ___stahp_6;

public:
	inline static int32_t get_offset_of_battle_4() { return static_cast<int32_t>(offsetof(Spriteimp_t3952321968, ___battle_4)); }
	inline Battleimp_t158148094 * get_battle_4() const { return ___battle_4; }
	inline Battleimp_t158148094 ** get_address_of_battle_4() { return &___battle_4; }
	inline void set_battle_4(Battleimp_t158148094 * value)
	{
		___battle_4 = value;
		Il2CppCodeGenWriteBarrier((&___battle_4), value);
	}

	inline static int32_t get_offset_of_anim_5() { return static_cast<int32_t>(offsetof(Spriteimp_t3952321968, ___anim_5)); }
	inline Animator_t434523843 * get_anim_5() const { return ___anim_5; }
	inline Animator_t434523843 ** get_address_of_anim_5() { return &___anim_5; }
	inline void set_anim_5(Animator_t434523843 * value)
	{
		___anim_5 = value;
		Il2CppCodeGenWriteBarrier((&___anim_5), value);
	}

	inline static int32_t get_offset_of_stahp_6() { return static_cast<int32_t>(offsetof(Spriteimp_t3952321968, ___stahp_6)); }
	inline bool get_stahp_6() const { return ___stahp_6; }
	inline bool* get_address_of_stahp_6() { return &___stahp_6; }
	inline void set_stahp_6(bool value)
	{
		___stahp_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEIMP_T3952321968_H
#ifndef SPRITEINF_T2378343838_H
#define SPRITEINF_T2378343838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spriteinf
struct  Spriteinf_t2378343838  : public MonoBehaviour_t3962482529
{
public:
	// Battleinf Spriteinf::battle
	Battleinf_t1732126188 * ___battle_4;
	// UnityEngine.Animator Spriteinf::anim
	Animator_t434523843 * ___anim_5;
	// System.Boolean Spriteinf::stahp
	bool ___stahp_6;

public:
	inline static int32_t get_offset_of_battle_4() { return static_cast<int32_t>(offsetof(Spriteinf_t2378343838, ___battle_4)); }
	inline Battleinf_t1732126188 * get_battle_4() const { return ___battle_4; }
	inline Battleinf_t1732126188 ** get_address_of_battle_4() { return &___battle_4; }
	inline void set_battle_4(Battleinf_t1732126188 * value)
	{
		___battle_4 = value;
		Il2CppCodeGenWriteBarrier((&___battle_4), value);
	}

	inline static int32_t get_offset_of_anim_5() { return static_cast<int32_t>(offsetof(Spriteinf_t2378343838, ___anim_5)); }
	inline Animator_t434523843 * get_anim_5() const { return ___anim_5; }
	inline Animator_t434523843 ** get_address_of_anim_5() { return &___anim_5; }
	inline void set_anim_5(Animator_t434523843 * value)
	{
		___anim_5 = value;
		Il2CppCodeGenWriteBarrier((&___anim_5), value);
	}

	inline static int32_t get_offset_of_stahp_6() { return static_cast<int32_t>(offsetof(Spriteinf_t2378343838, ___stahp_6)); }
	inline bool get_stahp_6() const { return ___stahp_6; }
	inline bool* get_address_of_stahp_6() { return &___stahp_6; }
	inline void set_stahp_6(bool value)
	{
		___stahp_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEINF_T2378343838_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (TriggerType_t105272677)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2200[5] = 
{
	TriggerType_t105272677::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (TriggerListContainer_t2032715483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2201[1] = 
{
	TriggerListContainer_t2032715483::get_offset_of_m_Rules_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (EventTrigger_t2527451695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2202[12] = 
{
	EventTrigger_t2527451695::get_offset_of_m_IsTriggerExpanded_0(),
	EventTrigger_t2527451695::get_offset_of_m_Type_1(),
	EventTrigger_t2527451695::get_offset_of_m_LifecycleEvent_2(),
	EventTrigger_t2527451695::get_offset_of_m_ApplyRules_3(),
	EventTrigger_t2527451695::get_offset_of_m_Rules_4(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerBool_5(),
	EventTrigger_t2527451695::get_offset_of_m_InitTime_6(),
	EventTrigger_t2527451695::get_offset_of_m_RepeatTime_7(),
	EventTrigger_t2527451695::get_offset_of_m_Repetitions_8(),
	EventTrigger_t2527451695::get_offset_of_repetitionCount_9(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerFunction_10(),
	EventTrigger_t2527451695::get_offset_of_m_Method_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (OnTrigger_t4184125570), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (TrackableTrigger_t621205209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2204[2] = 
{
	TrackableTrigger_t621205209::get_offset_of_m_Target_0(),
	TrackableTrigger_t621205209::get_offset_of_m_MethodPath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (TriggerMethod_t582536534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (TriggerRule_t1946298321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2206[4] = 
{
	TriggerRule_t1946298321::get_offset_of_m_Target_0(),
	TriggerRule_t1946298321::get_offset_of_m_Operator_1(),
	TriggerRule_t1946298321::get_offset_of_m_Value_2(),
	TriggerRule_t1946298321::get_offset_of_m_Value2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (U3CModuleU3E_t692745545), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (Anim_t271495542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2208[12] = 
{
	Anim_t271495542::get_offset_of_quad1_4(),
	Anim_t271495542::get_offset_of_quad2_5(),
	Anim_t271495542::get_offset_of_dog_6(),
	Anim_t271495542::get_offset_of_guy_7(),
	Anim_t271495542::get_offset_of_isDone_8(),
	Anim_t271495542::get_offset_of_i_9(),
	Anim_t271495542::get_offset_of_speed_10(),
	Anim_t271495542::get_offset_of_startTime_11(),
	Anim_t271495542::get_offset_of_journeyLength_12(),
	Anim_t271495542::get_offset_of_journeyLength2_13(),
	Anim_t271495542::get_offset_of_journeyLength3_14(),
	Anim_t271495542::get_offset_of_journeyLength4_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (U3CTimeTrackerU3Ec__Iterator0_t1744683265), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2209[4] = 
{
	U3CTimeTrackerU3Ec__Iterator0_t1744683265::get_offset_of_U24this_0(),
	U3CTimeTrackerU3Ec__Iterator0_t1744683265::get_offset_of_U24current_1(),
	U3CTimeTrackerU3Ec__Iterator0_t1744683265::get_offset_of_U24disposing_2(),
	U3CTimeTrackerU3Ec__Iterator0_t1744683265::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (Battle_t2191861408), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2210[23] = 
{
	Battle_t2191861408::get_offset_of_slider_4(),
	Battle_t2191861408::get_offset_of_guySlider_5(),
	Battle_t2191861408::get_offset_of_attackText_6(),
	Battle_t2191861408::get_offset_of_dogHealth_7(),
	Battle_t2191861408::get_offset_of_guyHealth_8(),
	Battle_t2191861408::get_offset_of_black_9(),
	Battle_t2191861408::get_offset_of_animator_10(),
	Battle_t2191861408::get_offset_of_DogDieSFX_11(),
	Battle_t2191861408::get_offset_of_GuydDieSFX_12(),
	Battle_t2191861408::get_offset_of_GUI_13(),
	Battle_t2191861408::get_offset_of_GameOver_14(),
	Battle_t2191861408::get_offset_of_anim_15(),
	Battle_t2191861408::get_offset_of_rnd_16(),
	Battle_t2191861408::get_offset_of_myTurn_17(),
	Battle_t2191861408::get_offset_of_ableToAttack_18(),
	Battle_t2191861408::get_offset_of_enemyAbleToAttack_19(),
	Battle_t2191861408::get_offset_of_isReady_20(),
	Battle_t2191861408::get_offset_of_isDone_21(),
	Battle_t2191861408::get_offset_of_isDead_22(),
	Battle_t2191861408::get_offset_of_U3CCurrentHealthU3Ek__BackingField_23(),
	Battle_t2191861408::get_offset_of_U3CMaxHealthU3Ek__BackingField_24(),
	Battle_t2191861408::get_offset_of_U3CCurrentHealthGuyU3Ek__BackingField_25(),
	Battle_t2191861408::get_offset_of_U3CMaxHealthGuyU3Ek__BackingField_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (U3CEnemyAttackU3Ec__Iterator0_t1682970516), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2211[5] = 
{
	U3CEnemyAttackU3Ec__Iterator0_t1682970516::get_offset_of_U3CrndDMGU3E__1_0(),
	U3CEnemyAttackU3Ec__Iterator0_t1682970516::get_offset_of_U24this_1(),
	U3CEnemyAttackU3Ec__Iterator0_t1682970516::get_offset_of_U24current_2(),
	U3CEnemyAttackU3Ec__Iterator0_t1682970516::get_offset_of_U24disposing_3(),
	U3CEnemyAttackU3Ec__Iterator0_t1682970516::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (U3CContinueU3Ec__Iterator1_t642798636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2212[4] = 
{
	U3CContinueU3Ec__Iterator1_t642798636::get_offset_of_U24this_0(),
	U3CContinueU3Ec__Iterator1_t642798636::get_offset_of_U24current_1(),
	U3CContinueU3Ec__Iterator1_t642798636::get_offset_of_U24disposing_2(),
	U3CContinueU3Ec__Iterator1_t642798636::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (U3CDIeU3Ec__Iterator2_t558276709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2213[4] = 
{
	U3CDIeU3Ec__Iterator2_t558276709::get_offset_of_U24this_0(),
	U3CDIeU3Ec__Iterator2_t558276709::get_offset_of_U24current_1(),
	U3CDIeU3Ec__Iterator2_t558276709::get_offset_of_U24disposing_2(),
	U3CDIeU3Ec__Iterator2_t558276709::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (Color_t2591083401), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2214[4] = 
{
	Color_t2591083401::get_offset_of_slider_4(),
	Color_t2591083401::get_offset_of_img_5(),
	Color_t2591083401::get_offset_of_col_6(),
	Color_t2591083401::get_offset_of_val_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (Sprite_t2249211761), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2215[3] = 
{
	Sprite_t2249211761::get_offset_of_battle_4(),
	Sprite_t2249211761::get_offset_of_anim_5(),
	Sprite_t2249211761::get_offset_of_stahp_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (Animimp_t3408691587), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2216[12] = 
{
	Animimp_t3408691587::get_offset_of_quad1_4(),
	Animimp_t3408691587::get_offset_of_quad2_5(),
	Animimp_t3408691587::get_offset_of_dog_6(),
	Animimp_t3408691587::get_offset_of_guy_7(),
	Animimp_t3408691587::get_offset_of_isDone_8(),
	Animimp_t3408691587::get_offset_of_i_9(),
	Animimp_t3408691587::get_offset_of_speed_10(),
	Animimp_t3408691587::get_offset_of_startTime_11(),
	Animimp_t3408691587::get_offset_of_journeyLength_12(),
	Animimp_t3408691587::get_offset_of_journeyLength2_13(),
	Animimp_t3408691587::get_offset_of_journeyLength3_14(),
	Animimp_t3408691587::get_offset_of_journeyLength4_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (U3CTimeTrackerU3Ec__Iterator0_t3157006910), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2217[4] = 
{
	U3CTimeTrackerU3Ec__Iterator0_t3157006910::get_offset_of_U24this_0(),
	U3CTimeTrackerU3Ec__Iterator0_t3157006910::get_offset_of_U24current_1(),
	U3CTimeTrackerU3Ec__Iterator0_t3157006910::get_offset_of_U24disposing_2(),
	U3CTimeTrackerU3Ec__Iterator0_t3157006910::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (Battleimp_t158148094), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2218[23] = 
{
	Battleimp_t158148094::get_offset_of_slider_4(),
	Battleimp_t158148094::get_offset_of_guySlider_5(),
	Battleimp_t158148094::get_offset_of_attackText_6(),
	Battleimp_t158148094::get_offset_of_dogHealth_7(),
	Battleimp_t158148094::get_offset_of_guyHealth_8(),
	Battleimp_t158148094::get_offset_of_black_9(),
	Battleimp_t158148094::get_offset_of_animator_10(),
	Battleimp_t158148094::get_offset_of_DogDieSFX_11(),
	Battleimp_t158148094::get_offset_of_GuydDieSFX_12(),
	Battleimp_t158148094::get_offset_of_GUI_13(),
	Battleimp_t158148094::get_offset_of_GameOver_14(),
	Battleimp_t158148094::get_offset_of_anim_15(),
	Battleimp_t158148094::get_offset_of_rnd_16(),
	Battleimp_t158148094::get_offset_of_myTurn_17(),
	Battleimp_t158148094::get_offset_of_ableToAttack_18(),
	Battleimp_t158148094::get_offset_of_enemyAbleToAttack_19(),
	Battleimp_t158148094::get_offset_of_isReady_20(),
	Battleimp_t158148094::get_offset_of_isDone_21(),
	Battleimp_t158148094::get_offset_of_isDead_22(),
	Battleimp_t158148094::get_offset_of_U3CCurrentHealthU3Ek__BackingField_23(),
	Battleimp_t158148094::get_offset_of_U3CMaxHealthU3Ek__BackingField_24(),
	Battleimp_t158148094::get_offset_of_U3CCurrentHealthGuyU3Ek__BackingField_25(),
	Battleimp_t158148094::get_offset_of_U3CMaxHealthGuyU3Ek__BackingField_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (U3CEnemyAttackU3Ec__Iterator0_t889231986), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2219[5] = 
{
	U3CEnemyAttackU3Ec__Iterator0_t889231986::get_offset_of_U3CrndDMGU3E__1_0(),
	U3CEnemyAttackU3Ec__Iterator0_t889231986::get_offset_of_U24this_1(),
	U3CEnemyAttackU3Ec__Iterator0_t889231986::get_offset_of_U24current_2(),
	U3CEnemyAttackU3Ec__Iterator0_t889231986::get_offset_of_U24disposing_3(),
	U3CEnemyAttackU3Ec__Iterator0_t889231986::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (U3CContinueU3Ec__Iterator1_t2046304316), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2220[4] = 
{
	U3CContinueU3Ec__Iterator1_t2046304316::get_offset_of_U24this_0(),
	U3CContinueU3Ec__Iterator1_t2046304316::get_offset_of_U24current_1(),
	U3CContinueU3Ec__Iterator1_t2046304316::get_offset_of_U24disposing_2(),
	U3CContinueU3Ec__Iterator1_t2046304316::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (Colorimp_t1969227374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2221[4] = 
{
	Colorimp_t1969227374::get_offset_of_slider_4(),
	Colorimp_t1969227374::get_offset_of_img_5(),
	Colorimp_t1969227374::get_offset_of_col_6(),
	Colorimp_t1969227374::get_offset_of_val_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (Spriteimp_t3952321968), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2222[3] = 
{
	Spriteimp_t3952321968::get_offset_of_battle_4(),
	Spriteimp_t3952321968::get_offset_of_anim_5(),
	Spriteimp_t3952321968::get_offset_of_stahp_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (Animinf_t989049961), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2223[12] = 
{
	Animinf_t989049961::get_offset_of_quad1_4(),
	Animinf_t989049961::get_offset_of_quad2_5(),
	Animinf_t989049961::get_offset_of_dog_6(),
	Animinf_t989049961::get_offset_of_guy_7(),
	Animinf_t989049961::get_offset_of_isDone_8(),
	Animinf_t989049961::get_offset_of_i_9(),
	Animinf_t989049961::get_offset_of_speed_10(),
	Animinf_t989049961::get_offset_of_startTime_11(),
	Animinf_t989049961::get_offset_of_journeyLength_12(),
	Animinf_t989049961::get_offset_of_journeyLength2_13(),
	Animinf_t989049961::get_offset_of_journeyLength3_14(),
	Animinf_t989049961::get_offset_of_journeyLength4_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (U3CTimeTrackerU3Ec__Iterator0_t1213291940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2224[4] = 
{
	U3CTimeTrackerU3Ec__Iterator0_t1213291940::get_offset_of_U24this_0(),
	U3CTimeTrackerU3Ec__Iterator0_t1213291940::get_offset_of_U24current_1(),
	U3CTimeTrackerU3Ec__Iterator0_t1213291940::get_offset_of_U24disposing_2(),
	U3CTimeTrackerU3Ec__Iterator0_t1213291940::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (Battleinf_t1732126188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2225[30] = 
{
	Battleinf_t1732126188::get_offset_of_slider_4(),
	Battleinf_t1732126188::get_offset_of_guySlider_5(),
	Battleinf_t1732126188::get_offset_of_attackText_6(),
	Battleinf_t1732126188::get_offset_of_nameText_7(),
	Battleinf_t1732126188::get_offset_of_dogHealth_8(),
	Battleinf_t1732126188::get_offset_of_guyHealth_9(),
	Battleinf_t1732126188::get_offset_of_dogSprite_10(),
	Battleinf_t1732126188::get_offset_of_black_11(),
	Battleinf_t1732126188::get_offset_of_animator_12(),
	Battleinf_t1732126188::get_offset_of_DogDieSFX_13(),
	Battleinf_t1732126188::get_offset_of_GuydDieSFX_14(),
	Battleinf_t1732126188::get_offset_of_GUI_15(),
	Battleinf_t1732126188::get_offset_of_GameOver_16(),
	Battleinf_t1732126188::get_offset_of_anim_17(),
	Battleinf_t1732126188::get_offset_of_rnd_18(),
	Battleinf_t1732126188::get_offset_of_myTurn_19(),
	Battleinf_t1732126188::get_offset_of_ableToAttack_20(),
	Battleinf_t1732126188::get_offset_of_enemyAbleToAttack_21(),
	Battleinf_t1732126188::get_offset_of_isReady_22(),
	Battleinf_t1732126188::get_offset_of_isDone_23(),
	Battleinf_t1732126188::get_offset_of_isDead_24(),
	Battleinf_t1732126188::get_offset_of_r_25(),
	Battleinf_t1732126188::get_offset_of_g_26(),
	Battleinf_t1732126188::get_offset_of_b_27(),
	Battleinf_t1732126188::get_offset_of_col_28(),
	Battleinf_t1732126188::get_offset_of_n_29(),
	Battleinf_t1732126188::get_offset_of_U3CCurrentHealthU3Ek__BackingField_30(),
	Battleinf_t1732126188::get_offset_of_U3CMaxHealthU3Ek__BackingField_31(),
	Battleinf_t1732126188::get_offset_of_U3CCurrentHealthGuyU3Ek__BackingField_32(),
	Battleinf_t1732126188::get_offset_of_U3CMaxHealthGuyU3Ek__BackingField_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (U3CEnemyAttackU3Ec__Iterator0_t2884124416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2226[5] = 
{
	U3CEnemyAttackU3Ec__Iterator0_t2884124416::get_offset_of_U3CrndDMGU3E__1_0(),
	U3CEnemyAttackU3Ec__Iterator0_t2884124416::get_offset_of_U24this_1(),
	U3CEnemyAttackU3Ec__Iterator0_t2884124416::get_offset_of_U24current_2(),
	U3CEnemyAttackU3Ec__Iterator0_t2884124416::get_offset_of_U24disposing_3(),
	U3CEnemyAttackU3Ec__Iterator0_t2884124416::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (U3CContinueU3Ec__Iterator1_t1090546628), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2227[4] = 
{
	U3CContinueU3Ec__Iterator1_t1090546628::get_offset_of_U24this_0(),
	U3CContinueU3Ec__Iterator1_t1090546628::get_offset_of_U24current_1(),
	U3CContinueU3Ec__Iterator1_t1090546628::get_offset_of_U24disposing_2(),
	U3CContinueU3Ec__Iterator1_t1090546628::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (U3CDIeU3Ec__Iterator2_t3004093797), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2228[4] = 
{
	U3CDIeU3Ec__Iterator2_t3004093797::get_offset_of_U24this_0(),
	U3CDIeU3Ec__Iterator2_t3004093797::get_offset_of_U24current_1(),
	U3CDIeU3Ec__Iterator2_t3004093797::get_offset_of_U24disposing_2(),
	U3CDIeU3Ec__Iterator2_t3004093797::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (Colorinf_t4284197021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2229[4] = 
{
	Colorinf_t4284197021::get_offset_of_slider_4(),
	Colorinf_t4284197021::get_offset_of_img_5(),
	Colorinf_t4284197021::get_offset_of_col_6(),
	Colorinf_t4284197021::get_offset_of_val_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (Spriteinf_t2378343838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2230[3] = 
{
	Spriteinf_t2378343838::get_offset_of_battle_4(),
	Spriteinf_t2378343838::get_offset_of_anim_5(),
	Spriteinf_t2378343838::get_offset_of_stahp_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (menuProgram_t465836879), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2231[4] = 
{
	menuProgram_t465836879::get_offset_of_startText_4(),
	menuProgram_t465836879::get_offset_of_doggoHuntBTN_5(),
	menuProgram_t465836879::get_offset_of_str1_6(),
	menuProgram_t465836879::get_offset_of_str2_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (U3CLoadingU3Ec__Iterator0_t2464126437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2232[4] = 
{
	U3CLoadingU3Ec__Iterator0_t2464126437::get_offset_of_U24this_0(),
	U3CLoadingU3Ec__Iterator0_t2464126437::get_offset_of_U24current_1(),
	U3CLoadingU3Ec__Iterator0_t2464126437::get_offset_of_U24disposing_2(),
	U3CLoadingU3Ec__Iterator0_t2464126437::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (Program_t1866495201), -1, sizeof(Program_t1866495201_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2233[7] = 
{
	Program_t1866495201::get_offset_of_bootSFX_4(),
	Program_t1866495201::get_offset_of_messageSFX_5(),
	Program_t1866495201::get_offset_of_leetSFX_6(),
	Program_t1866495201_StaticFields::get_offset_of_normTypeSpeed_7(),
	Program_t1866495201::get_offset_of_inCoroutine_8(),
	Program_t1866495201::get_offset_of_startText_9(),
	Program_t1866495201::get_offset_of_i_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (U3CLoadingU3Ec__Iterator0_t3071600265), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2234[4] = 
{
	U3CLoadingU3Ec__Iterator0_t3071600265::get_offset_of_U24this_0(),
	U3CLoadingU3Ec__Iterator0_t3071600265::get_offset_of_U24current_1(),
	U3CLoadingU3Ec__Iterator0_t3071600265::get_offset_of_U24disposing_2(),
	U3CLoadingU3Ec__Iterator0_t3071600265::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (U3CPlayBootSFXU3Ec__Iterator1_t3195710289), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2235[4] = 
{
	U3CPlayBootSFXU3Ec__Iterator1_t3195710289::get_offset_of_U24this_0(),
	U3CPlayBootSFXU3Ec__Iterator1_t3195710289::get_offset_of_U24current_1(),
	U3CPlayBootSFXU3Ec__Iterator1_t3195710289::get_offset_of_U24disposing_2(),
	U3CPlayBootSFXU3Ec__Iterator1_t3195710289::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (Program2_t3485103329), -1, sizeof(Program2_t3485103329_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2236[6] = 
{
	Program2_t3485103329::get_offset_of_messageSFX_4(),
	Program2_t3485103329::get_offset_of_leetSFX_5(),
	Program2_t3485103329_StaticFields::get_offset_of_normTypeSpeed_6(),
	Program2_t3485103329::get_offset_of_inCoroutine_7(),
	Program2_t3485103329::get_offset_of_startText_8(),
	Program2_t3485103329::get_offset_of_i_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (U3CLoadingU3Ec__Iterator0_t2822561722), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2237[4] = 
{
	U3CLoadingU3Ec__Iterator0_t2822561722::get_offset_of_U24this_0(),
	U3CLoadingU3Ec__Iterator0_t2822561722::get_offset_of_U24current_1(),
	U3CLoadingU3Ec__Iterator0_t2822561722::get_offset_of_U24disposing_2(),
	U3CLoadingU3Ec__Iterator0_t2822561722::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (Program3_t1528788193), -1, sizeof(Program3_t1528788193_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2238[6] = 
{
	Program3_t1528788193::get_offset_of_messageSFX_4(),
	Program3_t1528788193::get_offset_of_doggoSFX_5(),
	Program3_t1528788193_StaticFields::get_offset_of_normTypeSpeed_6(),
	Program3_t1528788193::get_offset_of_inCoroutine_7(),
	Program3_t1528788193::get_offset_of_startText_8(),
	Program3_t1528788193::get_offset_of_i_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (U3CLoadingU3Ec__Iterator0_t2437702791), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2239[4] = 
{
	U3CLoadingU3Ec__Iterator0_t2437702791::get_offset_of_U24this_0(),
	U3CLoadingU3Ec__Iterator0_t2437702791::get_offset_of_U24current_1(),
	U3CLoadingU3Ec__Iterator0_t2437702791::get_offset_of_U24disposing_2(),
	U3CLoadingU3Ec__Iterator0_t2437702791::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (BootSeq_t2506222928), -1, sizeof(BootSeq_t2506222928_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2240[1] = 
{
	BootSeq_t2506222928_StaticFields::get_offset_of_BootSequence_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (Difficulty_t3960432064), -1, sizeof(Difficulty_t3960432064_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2241[2] = 
{
	Difficulty_t3960432064_StaticFields::get_offset_of_a_4(),
	Difficulty_t3960432064_StaticFields::get_offset_of_b_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (Doggo_t4070438711), -1, sizeof(Doggo_t4070438711_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2242[2] = 
{
	Doggo_t4070438711_StaticFields::get_offset_of_a_4(),
	Doggo_t4070438711_StaticFields::get_offset_of_b_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (Input_t3716682282), -1, sizeof(Input_t3716682282_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2243[3] = 
{
	Input_t3716682282_StaticFields::get_offset_of_a_4(),
	Input_t3716682282_StaticFields::get_offset_of_b_5(),
	Input_t3716682282_StaticFields::get_offset_of_c_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (Type_t4058249690), -1, sizeof(Type_t4058249690_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2244[3] = 
{
	Type_t4058249690_StaticFields::get_offset_of_instance_4(),
	Type_t4058249690_StaticFields::get_offset_of_program_5(),
	Type_t4058249690_StaticFields::get_offset_of_coroutine_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (U3CAnimateTextU3Ec__Iterator0_t429204900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2245[7] = 
{
	U3CAnimateTextU3Ec__Iterator0_t429204900::get_offset_of_U3CiU3E__1_0(),
	U3CAnimateTextU3Ec__Iterator0_t429204900::get_offset_of_message_1(),
	U3CAnimateTextU3Ec__Iterator0_t429204900::get_offset_of_textDes_2(),
	U3CAnimateTextU3Ec__Iterator0_t429204900::get_offset_of_speed_3(),
	U3CAnimateTextU3Ec__Iterator0_t429204900::get_offset_of_U24current_4(),
	U3CAnimateTextU3Ec__Iterator0_t429204900::get_offset_of_U24disposing_5(),
	U3CAnimateTextU3Ec__Iterator0_t429204900::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (Type2_t2499874421), -1, sizeof(Type2_t2499874421_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2246[3] = 
{
	Type2_t2499874421_StaticFields::get_offset_of_instance_4(),
	Type2_t2499874421_StaticFields::get_offset_of_program2_5(),
	Type2_t2499874421_StaticFields::get_offset_of_coroutine_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (U3CAnimateTextU3Ec__Iterator0_t589720723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2247[7] = 
{
	U3CAnimateTextU3Ec__Iterator0_t589720723::get_offset_of_U3CiU3E__1_0(),
	U3CAnimateTextU3Ec__Iterator0_t589720723::get_offset_of_message_1(),
	U3CAnimateTextU3Ec__Iterator0_t589720723::get_offset_of_textDes_2(),
	U3CAnimateTextU3Ec__Iterator0_t589720723::get_offset_of_speed_3(),
	U3CAnimateTextU3Ec__Iterator0_t589720723::get_offset_of_U24current_4(),
	U3CAnimateTextU3Ec__Iterator0_t589720723::get_offset_of_U24disposing_5(),
	U3CAnimateTextU3Ec__Iterator0_t589720723::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (Type3_t2499874422), -1, sizeof(Type3_t2499874422_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2248[3] = 
{
	Type3_t2499874422_StaticFields::get_offset_of_instance_4(),
	Type3_t2499874422_StaticFields::get_offset_of_program3_5(),
	Type3_t2499874422_StaticFields::get_offset_of_coroutine_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (U3CAnimateTextU3Ec__Iterator0_t772139121), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2249[7] = 
{
	U3CAnimateTextU3Ec__Iterator0_t772139121::get_offset_of_U3CiU3E__1_0(),
	U3CAnimateTextU3Ec__Iterator0_t772139121::get_offset_of_message_1(),
	U3CAnimateTextU3Ec__Iterator0_t772139121::get_offset_of_textDes_2(),
	U3CAnimateTextU3Ec__Iterator0_t772139121::get_offset_of_speed_3(),
	U3CAnimateTextU3Ec__Iterator0_t772139121::get_offset_of_U24current_4(),
	U3CAnimateTextU3Ec__Iterator0_t772139121::get_offset_of_U24disposing_5(),
	U3CAnimateTextU3Ec__Iterator0_t772139121::get_offset_of_U24PC_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
