﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct VirtFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// Anim
struct Anim_t271495542;
// Anim/<TimeTracker>c__Iterator0
struct U3CTimeTrackerU3Ec__Iterator0_t1744683265;
// Animimp
struct Animimp_t3408691587;
// Animimp/<TimeTracker>c__Iterator0
struct U3CTimeTrackerU3Ec__Iterator0_t3157006910;
// Animinf
struct Animinf_t989049961;
// Animinf/<TimeTracker>c__Iterator0
struct U3CTimeTrackerU3Ec__Iterator0_t1213291940;
// Battle
struct Battle_t2191861408;
// Battle/<Continue>c__Iterator1
struct U3CContinueU3Ec__Iterator1_t642798636;
// Battle/<DIe>c__Iterator2
struct U3CDIeU3Ec__Iterator2_t558276709;
// Battle/<EnemyAttack>c__Iterator0
struct U3CEnemyAttackU3Ec__Iterator0_t1682970516;
// Battleimp
struct Battleimp_t158148094;
// Battleimp/<Continue>c__Iterator1
struct U3CContinueU3Ec__Iterator1_t2046304316;
// Battleimp/<EnemyAttack>c__Iterator0
struct U3CEnemyAttackU3Ec__Iterator0_t889231986;
// Battleinf
struct Battleinf_t1732126188;
// Battleinf/<Continue>c__Iterator1
struct U3CContinueU3Ec__Iterator1_t1090546628;
// Battleinf/<DIe>c__Iterator2
struct U3CDIeU3Ec__Iterator2_t3004093797;
// Battleinf/<EnemyAttack>c__Iterator0
struct U3CEnemyAttackU3Ec__Iterator0_t2884124416;
// Color
struct Color_t2591083401;
// Colorimp
struct Colorimp_t1969227374;
// Colorinf
struct Colorinf_t4284197021;
// Harmony.BootSeq
struct BootSeq_t2506222928;
// Harmony.Difficulty
struct Difficulty_t3960432064;
// Harmony.Doggo
struct Doggo_t4070438711;
// Harmony.Input
struct Input_t3716682282;
// Harmony.Program
struct Program_t1866495201;
// Harmony.Program/<Loading>c__Iterator0
struct U3CLoadingU3Ec__Iterator0_t3071600265;
// Harmony.Program/<PlayBootSFX>c__Iterator1
struct U3CPlayBootSFXU3Ec__Iterator1_t3195710289;
// Harmony.Program2
struct Program2_t3485103329;
// Harmony.Program2/<Loading>c__Iterator0
struct U3CLoadingU3Ec__Iterator0_t2822561722;
// Harmony.Program3
struct Program3_t1528788193;
// Harmony.Program3/<Loading>c__Iterator0
struct U3CLoadingU3Ec__Iterator0_t2437702791;
// Harmony.Type
struct Type_t4058249690;
// Harmony.Type/<AnimateText>c__Iterator0
struct U3CAnimateTextU3Ec__Iterator0_t429204900;
// Harmony.Type2
struct Type2_t2499874421;
// Harmony.Type2/<AnimateText>c__Iterator0
struct U3CAnimateTextU3Ec__Iterator0_t589720723;
// Harmony.Type3
struct Type3_t2499874422;
// Harmony.Type3/<AnimateText>c__Iterator0
struct U3CAnimateTextU3Ec__Iterator0_t772139121;
// Harmony.menuProgram
struct menuProgram_t465836879;
// Harmony.menuProgram/<Loading>c__Iterator0
struct U3CLoadingU3Ec__Iterator0_t2464126437;
// Sprite
struct Sprite_t2249211761;
// Spriteimp
struct Spriteimp_t3952321968;
// Spriteinf
struct Spriteinf_t2378343838;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1839659084;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t1260619206;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t427135887;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Func`1<System.Boolean>
struct Func_1_t3822001908;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.NotSupportedException
struct NotSupportedException_t1314879016;
// System.Random
struct Random_t108471755;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.String
struct String_t;
// System.Void
struct Void_t1185182177;
// TMPro.MaterialReference[]
struct MaterialReferenceU5BU5D_t648826345;
// TMPro.TMP_CharacterInfo[]
struct TMP_CharacterInfoU5BU5D_t1930184704;
// TMPro.TMP_ColorGradient
struct TMP_ColorGradient_t3678055768;
// TMPro.TMP_ColorGradient[]
struct TMP_ColorGradientU5BU5D_t2496920137;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t364381626;
// TMPro.TMP_Glyph
struct TMP_Glyph_t581847833;
// TMPro.TMP_SpriteAnimator
struct TMP_SpriteAnimator_t2836635477;
// TMPro.TMP_SpriteAsset
struct TMP_SpriteAsset_t484820633;
// TMPro.TMP_Text
struct TMP_Text_t2599618874;
// TMPro.TMP_TextElement
struct TMP_TextElement_t129727469;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_t3598145122;
// TMPro.TextAlignmentOptions[]
struct TextAlignmentOptionsU5BU5D_t3552942253;
// TMPro.XML_TagAttribute[]
struct XML_TagAttributeU5BU5D_t284240280;
// UnityEngine.Animator
struct Animator_t434523843;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.AudioSourceExtension
struct AudioSourceExtension_t3064908834;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Color32[]
struct Color32U5BU5D_t3850468773;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Material[]
struct MaterialU5BU5D_t561872642;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t2532145056;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.LayoutElement
struct LayoutElement_t1785403678;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.Selectable
struct Selectable_t3250028441;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// UnityEngine.UI.Slider/SliderEvent
struct SliderEvent_t3180273144;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251;
// UnityEngine.WaitUntil
struct WaitUntil_t3373419216;

extern RuntimeClass* BootSeq_t2506222928_il2cpp_TypeInfo_var;
extern RuntimeClass* Difficulty_t3960432064_il2cpp_TypeInfo_var;
extern RuntimeClass* Doggo_t4070438711_il2cpp_TypeInfo_var;
extern RuntimeClass* Func_1_t3822001908_il2cpp_TypeInfo_var;
extern RuntimeClass* Input_t1431474628_il2cpp_TypeInfo_var;
extern RuntimeClass* Input_t3716682282_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern RuntimeClass* NotSupportedException_t1314879016_il2cpp_TypeInfo_var;
extern RuntimeClass* Program2_t3485103329_il2cpp_TypeInfo_var;
extern RuntimeClass* Program3_t1528788193_il2cpp_TypeInfo_var;
extern RuntimeClass* Program_t1866495201_il2cpp_TypeInfo_var;
extern RuntimeClass* Random_t108471755_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* Type2_t2499874421_il2cpp_TypeInfo_var;
extern RuntimeClass* Type3_t2499874422_il2cpp_TypeInfo_var;
extern RuntimeClass* Type_t4058249690_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CAnimateTextU3Ec__Iterator0_t429204900_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CAnimateTextU3Ec__Iterator0_t589720723_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CAnimateTextU3Ec__Iterator0_t772139121_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CContinueU3Ec__Iterator1_t1090546628_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CContinueU3Ec__Iterator1_t2046304316_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CContinueU3Ec__Iterator1_t642798636_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CDIeU3Ec__Iterator2_t3004093797_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CDIeU3Ec__Iterator2_t558276709_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CEnemyAttackU3Ec__Iterator0_t1682970516_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CEnemyAttackU3Ec__Iterator0_t2884124416_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CEnemyAttackU3Ec__Iterator0_t889231986_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CLoadingU3Ec__Iterator0_t2437702791_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CLoadingU3Ec__Iterator0_t2464126437_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CLoadingU3Ec__Iterator0_t2822561722_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CLoadingU3Ec__Iterator0_t3071600265_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CPlayBootSFXU3Ec__Iterator1_t3195710289_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CTimeTrackerU3Ec__Iterator0_t1213291940_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CTimeTrackerU3Ec__Iterator0_t1744683265_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CTimeTrackerU3Ec__Iterator0_t3157006910_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3_t3722313464_il2cpp_TypeInfo_var;
extern RuntimeClass* WaitForSeconds_t1699091251_il2cpp_TypeInfo_var;
extern RuntimeClass* WaitUntil_t3373419216_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1089347067;
extern String_t* _stringLiteral1125101099;
extern String_t* _stringLiteral1168549832;
extern String_t* _stringLiteral1301065091;
extern String_t* _stringLiteral1320398211;
extern String_t* _stringLiteral1378388746;
extern String_t* _stringLiteral1415876783;
extern String_t* _stringLiteral1462988390;
extern String_t* _stringLiteral160657250;
extern String_t* _stringLiteral1635465982;
extern String_t* _stringLiteral1700381005;
extern String_t* _stringLiteral1830413752;
extern String_t* _stringLiteral1985039568;
extern String_t* _stringLiteral2085072259;
extern String_t* _stringLiteral2277005576;
extern String_t* _stringLiteral231011766;
extern String_t* _stringLiteral2522622552;
extern String_t* _stringLiteral2527013464;
extern String_t* _stringLiteral2710407981;
extern String_t* _stringLiteral2807073954;
extern String_t* _stringLiteral2831055546;
extern String_t* _stringLiteral3005979166;
extern String_t* _stringLiteral3015749341;
extern String_t* _stringLiteral3017060061;
extern String_t* _stringLiteral3021844189;
extern String_t* _stringLiteral3063217984;
extern String_t* _stringLiteral3225350139;
extern String_t* _stringLiteral3452614546;
extern String_t* _stringLiteral3457922962;
extern String_t* _stringLiteral348006446;
extern String_t* _stringLiteral3602717400;
extern String_t* _stringLiteral4054995049;
extern String_t* _stringLiteral4060106857;
extern String_t* _stringLiteral4201971368;
extern String_t* _stringLiteral4242887721;
extern String_t* _stringLiteral432624090;
extern String_t* _stringLiteral436365799;
extern String_t* _stringLiteral524701805;
extern String_t* _stringLiteral531699260;
extern String_t* _stringLiteral576020035;
extern String_t* _stringLiteral626144933;
extern String_t* _stringLiteral640182759;
extern String_t* _stringLiteral681334598;
extern String_t* _stringLiteral758536563;
extern String_t* _stringLiteral877777334;
extern const RuntimeMethod* Func_1__ctor_m1399073142_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisAnim_t271495542_m3559005964_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisAnimimp_t3408691587_m1937797106_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisAniminf_t989049961_m1535312041_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisBattle_t2191861408_m3384264100_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisBattleimp_t158148094_m3358208344_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisBattleinf_t1732126188_m3397136827_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisProgram2_t3485103329_m3599024581_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisProgram3_t1528788193_m3599024580_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisProgram_t1866495201_m3502787894_RuntimeMethod_var;
extern const RuntimeMethod* U3CAnimateTextU3Ec__Iterator0_Reset_m2745469755_RuntimeMethod_var;
extern const RuntimeMethod* U3CAnimateTextU3Ec__Iterator0_Reset_m3302327437_RuntimeMethod_var;
extern const RuntimeMethod* U3CAnimateTextU3Ec__Iterator0_Reset_m3627900720_RuntimeMethod_var;
extern const RuntimeMethod* U3CContinueU3Ec__Iterator1_Reset_m1261977967_RuntimeMethod_var;
extern const RuntimeMethod* U3CContinueU3Ec__Iterator1_Reset_m3351168776_RuntimeMethod_var;
extern const RuntimeMethod* U3CContinueU3Ec__Iterator1_Reset_m784469664_RuntimeMethod_var;
extern const RuntimeMethod* U3CContinueU3Ec__Iterator1_U3CU3Em__0_m193092823_RuntimeMethod_var;
extern const RuntimeMethod* U3CContinueU3Ec__Iterator1_U3CU3Em__0_m3660005984_RuntimeMethod_var;
extern const RuntimeMethod* U3CContinueU3Ec__Iterator1_U3CU3Em__0_m934966357_RuntimeMethod_var;
extern const RuntimeMethod* U3CDIeU3Ec__Iterator2_Reset_m2654355123_RuntimeMethod_var;
extern const RuntimeMethod* U3CDIeU3Ec__Iterator2_Reset_m615284703_RuntimeMethod_var;
extern const RuntimeMethod* U3CDIeU3Ec__Iterator2_U3CU3Em__0_m4217354996_RuntimeMethod_var;
extern const RuntimeMethod* U3CDIeU3Ec__Iterator2_U3CU3Em__0_m911504022_RuntimeMethod_var;
extern const RuntimeMethod* U3CEnemyAttackU3Ec__Iterator0_Reset_m1767328419_RuntimeMethod_var;
extern const RuntimeMethod* U3CEnemyAttackU3Ec__Iterator0_Reset_m196973293_RuntimeMethod_var;
extern const RuntimeMethod* U3CEnemyAttackU3Ec__Iterator0_Reset_m2815185348_RuntimeMethod_var;
extern const RuntimeMethod* U3CLoadingU3Ec__Iterator0_Reset_m3353854054_RuntimeMethod_var;
extern const RuntimeMethod* U3CLoadingU3Ec__Iterator0_Reset_m3445375883_RuntimeMethod_var;
extern const RuntimeMethod* U3CLoadingU3Ec__Iterator0_Reset_m3655277728_RuntimeMethod_var;
extern const RuntimeMethod* U3CLoadingU3Ec__Iterator0_Reset_m423204432_RuntimeMethod_var;
extern const RuntimeMethod* U3CPlayBootSFXU3Ec__Iterator1_Reset_m3273131816_RuntimeMethod_var;
extern const RuntimeMethod* U3CTimeTrackerU3Ec__Iterator0_Reset_m1833367939_RuntimeMethod_var;
extern const RuntimeMethod* U3CTimeTrackerU3Ec__Iterator0_Reset_m1842370124_RuntimeMethod_var;
extern const RuntimeMethod* U3CTimeTrackerU3Ec__Iterator0_Reset_m885536348_RuntimeMethod_var;
extern const uint32_t Anim_Start_m349140766_MetadataUsageId;
extern const uint32_t Anim_TimeTracker_m1581750217_MetadataUsageId;
extern const uint32_t Anim_Update_m4207990122_MetadataUsageId;
extern const uint32_t Animimp_Start_m1277239060_MetadataUsageId;
extern const uint32_t Animimp_TimeTracker_m2170675636_MetadataUsageId;
extern const uint32_t Animimp_Update_m2181736325_MetadataUsageId;
extern const uint32_t Animinf_Start_m3911812142_MetadataUsageId;
extern const uint32_t Animinf_TimeTracker_m1620902351_MetadataUsageId;
extern const uint32_t Animinf_Update_m1260342309_MetadataUsageId;
extern const uint32_t Battle_Attack_m1873061951_MetadataUsageId;
extern const uint32_t Battle_Continue_m1384187413_MetadataUsageId;
extern const uint32_t Battle_DIe_m2481910125_MetadataUsageId;
extern const uint32_t Battle_DealDamage_m3118024972_MetadataUsageId;
extern const uint32_t Battle_DieGuy_m536536778_MetadataUsageId;
extern const uint32_t Battle_Die_m2800284142_MetadataUsageId;
extern const uint32_t Battle_EnemyAttack_m1219160329_MetadataUsageId;
extern const uint32_t Battle_EnemyDealDamage_m2866959850_MetadataUsageId;
extern const uint32_t Battle_Start_m3292020095_MetadataUsageId;
extern const uint32_t Battle_Update_m683330005_MetadataUsageId;
extern const uint32_t Battle__ctor_m3420311181_MetadataUsageId;
extern const uint32_t Battleimp_Attack_m1242176998_MetadataUsageId;
extern const uint32_t Battleimp_Continue_m2989872615_MetadataUsageId;
extern const uint32_t Battleimp_DealDamage_m2481885304_MetadataUsageId;
extern const uint32_t Battleimp_DieGuy_m2108545423_MetadataUsageId;
extern const uint32_t Battleimp_EnemyAttack_m1443215274_MetadataUsageId;
extern const uint32_t Battleimp_EnemyDealDamage_m1731020462_MetadataUsageId;
extern const uint32_t Battleimp_Start_m2224704123_MetadataUsageId;
extern const uint32_t Battleimp_Update_m361311231_MetadataUsageId;
extern const uint32_t Battleimp__ctor_m1554581115_MetadataUsageId;
extern const uint32_t Battleinf_Attack_m1533273237_MetadataUsageId;
extern const uint32_t Battleinf_Continue_m985971343_MetadataUsageId;
extern const uint32_t Battleinf_DIe_m2804138209_MetadataUsageId;
extern const uint32_t Battleinf_DealDamage_m3678906129_MetadataUsageId;
extern const uint32_t Battleinf_DieGuy_m3765556335_MetadataUsageId;
extern const uint32_t Battleinf_Die_m3352291207_MetadataUsageId;
extern const uint32_t Battleinf_EnemyAttack_m3332119496_MetadataUsageId;
extern const uint32_t Battleinf_EnemyDealDamage_m3350442186_MetadataUsageId;
extern const uint32_t Battleinf_Start_m1339897305_MetadataUsageId;
extern const uint32_t Battleinf_Update_m3915901730_MetadataUsageId;
extern const uint32_t Battleinf__ctor_m1987701741_MetadataUsageId;
extern const uint32_t BootSeq__cctor_m79868158_MetadataUsageId;
extern const uint32_t Difficulty__cctor_m919047636_MetadataUsageId;
extern const uint32_t Doggo__cctor_m183777946_MetadataUsageId;
extern const uint32_t Input__cctor_m615191921_MetadataUsageId;
extern const uint32_t Program2_Loading_m3138989863_MetadataUsageId;
extern const uint32_t Program2_Update_m3502782264_MetadataUsageId;
extern const uint32_t Program2__cctor_m2483671483_MetadataUsageId;
extern const uint32_t Program3_Loading_m1854714262_MetadataUsageId;
extern const uint32_t Program3_Update_m3264821048_MetadataUsageId;
extern const uint32_t Program3__cctor_m295358907_MetadataUsageId;
extern const uint32_t Program_Loading_m47061019_MetadataUsageId;
extern const uint32_t Program_PlayBootSFX_m781471345_MetadataUsageId;
extern const uint32_t Program_Update_m4123468737_MetadataUsageId;
extern const uint32_t Program__cctor_m4132216779_MetadataUsageId;
extern const uint32_t Sprite_PlayAnim_m2275555825_MetadataUsageId;
extern const uint32_t Sprite_Start_m1984966497_MetadataUsageId;
extern const uint32_t Spriteimp_PlayAnim_m1100634416_MetadataUsageId;
extern const uint32_t Spriteimp_Start_m27523747_MetadataUsageId;
extern const uint32_t Spriteinf_PlayAnim_m139970992_MetadataUsageId;
extern const uint32_t Spriteinf_Start_m996226461_MetadataUsageId;
extern const uint32_t Type2_AnimateText_m909537142_MetadataUsageId;
extern const uint32_t Type2_Awake_m2046811758_MetadataUsageId;
extern const uint32_t Type2_TypeText_m1698800569_MetadataUsageId;
extern const uint32_t Type3_AnimateText_m1584460279_MetadataUsageId;
extern const uint32_t Type3_Awake_m2046810157_MetadataUsageId;
extern const uint32_t Type3_TypeText_m2630213857_MetadataUsageId;
extern const uint32_t Type_AnimateText_m2819578080_MetadataUsageId;
extern const uint32_t Type_Awake_m1861083554_MetadataUsageId;
extern const uint32_t Type_TypeText_m4150723822_MetadataUsageId;
extern const uint32_t U3CAnimateTextU3Ec__Iterator0_MoveNext_m1115575760_MetadataUsageId;
extern const uint32_t U3CAnimateTextU3Ec__Iterator0_MoveNext_m1796649191_MetadataUsageId;
extern const uint32_t U3CAnimateTextU3Ec__Iterator0_MoveNext_m2416659315_MetadataUsageId;
extern const uint32_t U3CAnimateTextU3Ec__Iterator0_Reset_m2745469755_MetadataUsageId;
extern const uint32_t U3CAnimateTextU3Ec__Iterator0_Reset_m3302327437_MetadataUsageId;
extern const uint32_t U3CAnimateTextU3Ec__Iterator0_Reset_m3627900720_MetadataUsageId;
extern const uint32_t U3CContinueU3Ec__Iterator1_MoveNext_m1211867273_MetadataUsageId;
extern const uint32_t U3CContinueU3Ec__Iterator1_MoveNext_m3745180152_MetadataUsageId;
extern const uint32_t U3CContinueU3Ec__Iterator1_MoveNext_m505267573_MetadataUsageId;
extern const uint32_t U3CContinueU3Ec__Iterator1_Reset_m1261977967_MetadataUsageId;
extern const uint32_t U3CContinueU3Ec__Iterator1_Reset_m3351168776_MetadataUsageId;
extern const uint32_t U3CContinueU3Ec__Iterator1_Reset_m784469664_MetadataUsageId;
extern const uint32_t U3CDIeU3Ec__Iterator2_MoveNext_m3837063140_MetadataUsageId;
extern const uint32_t U3CDIeU3Ec__Iterator2_MoveNext_m803567402_MetadataUsageId;
extern const uint32_t U3CDIeU3Ec__Iterator2_Reset_m2654355123_MetadataUsageId;
extern const uint32_t U3CDIeU3Ec__Iterator2_Reset_m615284703_MetadataUsageId;
extern const uint32_t U3CEnemyAttackU3Ec__Iterator0_MoveNext_m1362218421_MetadataUsageId;
extern const uint32_t U3CEnemyAttackU3Ec__Iterator0_MoveNext_m25122958_MetadataUsageId;
extern const uint32_t U3CEnemyAttackU3Ec__Iterator0_MoveNext_m3082423899_MetadataUsageId;
extern const uint32_t U3CEnemyAttackU3Ec__Iterator0_Reset_m1767328419_MetadataUsageId;
extern const uint32_t U3CEnemyAttackU3Ec__Iterator0_Reset_m196973293_MetadataUsageId;
extern const uint32_t U3CEnemyAttackU3Ec__Iterator0_Reset_m2815185348_MetadataUsageId;
extern const uint32_t U3CLoadingU3Ec__Iterator0_MoveNext_m1813361301_MetadataUsageId;
extern const uint32_t U3CLoadingU3Ec__Iterator0_MoveNext_m2655573207_MetadataUsageId;
extern const uint32_t U3CLoadingU3Ec__Iterator0_MoveNext_m3161337628_MetadataUsageId;
extern const uint32_t U3CLoadingU3Ec__Iterator0_MoveNext_m4187657732_MetadataUsageId;
extern const uint32_t U3CLoadingU3Ec__Iterator0_Reset_m3353854054_MetadataUsageId;
extern const uint32_t U3CLoadingU3Ec__Iterator0_Reset_m3445375883_MetadataUsageId;
extern const uint32_t U3CLoadingU3Ec__Iterator0_Reset_m3655277728_MetadataUsageId;
extern const uint32_t U3CLoadingU3Ec__Iterator0_Reset_m423204432_MetadataUsageId;
extern const uint32_t U3CPlayBootSFXU3Ec__Iterator1_MoveNext_m2963412815_MetadataUsageId;
extern const uint32_t U3CPlayBootSFXU3Ec__Iterator1_Reset_m3273131816_MetadataUsageId;
extern const uint32_t U3CTimeTrackerU3Ec__Iterator0_MoveNext_m2444601745_MetadataUsageId;
extern const uint32_t U3CTimeTrackerU3Ec__Iterator0_MoveNext_m3002021655_MetadataUsageId;
extern const uint32_t U3CTimeTrackerU3Ec__Iterator0_MoveNext_m993837992_MetadataUsageId;
extern const uint32_t U3CTimeTrackerU3Ec__Iterator0_Reset_m1833367939_MetadataUsageId;
extern const uint32_t U3CTimeTrackerU3Ec__Iterator0_Reset_m1842370124_MetadataUsageId;
extern const uint32_t U3CTimeTrackerU3Ec__Iterator0_Reset_m885536348_MetadataUsageId;
extern const uint32_t menuProgram_Loading_m616370398_MetadataUsageId;
extern const uint32_t menuProgram_Start_m202465803_MetadataUsageId;
extern const uint32_t menuProgram_Update_m85793366_MetadataUsageId;



#ifndef U3CMODULEU3E_T692745545_H
#define U3CMODULEU3E_T692745545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745545 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745545_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CTIMETRACKERU3EC__ITERATOR0_T1744683265_H
#define U3CTIMETRACKERU3EC__ITERATOR0_T1744683265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Anim/<TimeTracker>c__Iterator0
struct  U3CTimeTrackerU3Ec__Iterator0_t1744683265  : public RuntimeObject
{
public:
	// Anim Anim/<TimeTracker>c__Iterator0::$this
	Anim_t271495542 * ___U24this_0;
	// System.Object Anim/<TimeTracker>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Anim/<TimeTracker>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Anim/<TimeTracker>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CTimeTrackerU3Ec__Iterator0_t1744683265, ___U24this_0)); }
	inline Anim_t271495542 * get_U24this_0() const { return ___U24this_0; }
	inline Anim_t271495542 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Anim_t271495542 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CTimeTrackerU3Ec__Iterator0_t1744683265, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CTimeTrackerU3Ec__Iterator0_t1744683265, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CTimeTrackerU3Ec__Iterator0_t1744683265, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTIMETRACKERU3EC__ITERATOR0_T1744683265_H
#ifndef U3CTIMETRACKERU3EC__ITERATOR0_T3157006910_H
#define U3CTIMETRACKERU3EC__ITERATOR0_T3157006910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Animimp/<TimeTracker>c__Iterator0
struct  U3CTimeTrackerU3Ec__Iterator0_t3157006910  : public RuntimeObject
{
public:
	// Animimp Animimp/<TimeTracker>c__Iterator0::$this
	Animimp_t3408691587 * ___U24this_0;
	// System.Object Animimp/<TimeTracker>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Animimp/<TimeTracker>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Animimp/<TimeTracker>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CTimeTrackerU3Ec__Iterator0_t3157006910, ___U24this_0)); }
	inline Animimp_t3408691587 * get_U24this_0() const { return ___U24this_0; }
	inline Animimp_t3408691587 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Animimp_t3408691587 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CTimeTrackerU3Ec__Iterator0_t3157006910, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CTimeTrackerU3Ec__Iterator0_t3157006910, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CTimeTrackerU3Ec__Iterator0_t3157006910, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTIMETRACKERU3EC__ITERATOR0_T3157006910_H
#ifndef U3CTIMETRACKERU3EC__ITERATOR0_T1213291940_H
#define U3CTIMETRACKERU3EC__ITERATOR0_T1213291940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Animinf/<TimeTracker>c__Iterator0
struct  U3CTimeTrackerU3Ec__Iterator0_t1213291940  : public RuntimeObject
{
public:
	// Animinf Animinf/<TimeTracker>c__Iterator0::$this
	Animinf_t989049961 * ___U24this_0;
	// System.Object Animinf/<TimeTracker>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Animinf/<TimeTracker>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Animinf/<TimeTracker>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CTimeTrackerU3Ec__Iterator0_t1213291940, ___U24this_0)); }
	inline Animinf_t989049961 * get_U24this_0() const { return ___U24this_0; }
	inline Animinf_t989049961 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Animinf_t989049961 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CTimeTrackerU3Ec__Iterator0_t1213291940, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CTimeTrackerU3Ec__Iterator0_t1213291940, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CTimeTrackerU3Ec__Iterator0_t1213291940, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTIMETRACKERU3EC__ITERATOR0_T1213291940_H
#ifndef U3CCONTINUEU3EC__ITERATOR1_T642798636_H
#define U3CCONTINUEU3EC__ITERATOR1_T642798636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Battle/<Continue>c__Iterator1
struct  U3CContinueU3Ec__Iterator1_t642798636  : public RuntimeObject
{
public:
	// Battle Battle/<Continue>c__Iterator1::$this
	Battle_t2191861408 * ___U24this_0;
	// System.Object Battle/<Continue>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Battle/<Continue>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 Battle/<Continue>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CContinueU3Ec__Iterator1_t642798636, ___U24this_0)); }
	inline Battle_t2191861408 * get_U24this_0() const { return ___U24this_0; }
	inline Battle_t2191861408 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Battle_t2191861408 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CContinueU3Ec__Iterator1_t642798636, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CContinueU3Ec__Iterator1_t642798636, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CContinueU3Ec__Iterator1_t642798636, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONTINUEU3EC__ITERATOR1_T642798636_H
#ifndef U3CDIEU3EC__ITERATOR2_T558276709_H
#define U3CDIEU3EC__ITERATOR2_T558276709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Battle/<DIe>c__Iterator2
struct  U3CDIeU3Ec__Iterator2_t558276709  : public RuntimeObject
{
public:
	// Battle Battle/<DIe>c__Iterator2::$this
	Battle_t2191861408 * ___U24this_0;
	// System.Object Battle/<DIe>c__Iterator2::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Battle/<DIe>c__Iterator2::$disposing
	bool ___U24disposing_2;
	// System.Int32 Battle/<DIe>c__Iterator2::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CDIeU3Ec__Iterator2_t558276709, ___U24this_0)); }
	inline Battle_t2191861408 * get_U24this_0() const { return ___U24this_0; }
	inline Battle_t2191861408 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Battle_t2191861408 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDIeU3Ec__Iterator2_t558276709, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDIeU3Ec__Iterator2_t558276709, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDIeU3Ec__Iterator2_t558276709, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDIEU3EC__ITERATOR2_T558276709_H
#ifndef U3CENEMYATTACKU3EC__ITERATOR0_T1682970516_H
#define U3CENEMYATTACKU3EC__ITERATOR0_T1682970516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Battle/<EnemyAttack>c__Iterator0
struct  U3CEnemyAttackU3Ec__Iterator0_t1682970516  : public RuntimeObject
{
public:
	// System.Single Battle/<EnemyAttack>c__Iterator0::<rndDMG>__1
	float ___U3CrndDMGU3E__1_0;
	// Battle Battle/<EnemyAttack>c__Iterator0::$this
	Battle_t2191861408 * ___U24this_1;
	// System.Object Battle/<EnemyAttack>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Battle/<EnemyAttack>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Battle/<EnemyAttack>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrndDMGU3E__1_0() { return static_cast<int32_t>(offsetof(U3CEnemyAttackU3Ec__Iterator0_t1682970516, ___U3CrndDMGU3E__1_0)); }
	inline float get_U3CrndDMGU3E__1_0() const { return ___U3CrndDMGU3E__1_0; }
	inline float* get_address_of_U3CrndDMGU3E__1_0() { return &___U3CrndDMGU3E__1_0; }
	inline void set_U3CrndDMGU3E__1_0(float value)
	{
		___U3CrndDMGU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CEnemyAttackU3Ec__Iterator0_t1682970516, ___U24this_1)); }
	inline Battle_t2191861408 * get_U24this_1() const { return ___U24this_1; }
	inline Battle_t2191861408 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Battle_t2191861408 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CEnemyAttackU3Ec__Iterator0_t1682970516, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CEnemyAttackU3Ec__Iterator0_t1682970516, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CEnemyAttackU3Ec__Iterator0_t1682970516, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CENEMYATTACKU3EC__ITERATOR0_T1682970516_H
#ifndef U3CCONTINUEU3EC__ITERATOR1_T2046304316_H
#define U3CCONTINUEU3EC__ITERATOR1_T2046304316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Battleimp/<Continue>c__Iterator1
struct  U3CContinueU3Ec__Iterator1_t2046304316  : public RuntimeObject
{
public:
	// Battleimp Battleimp/<Continue>c__Iterator1::$this
	Battleimp_t158148094 * ___U24this_0;
	// System.Object Battleimp/<Continue>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Battleimp/<Continue>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 Battleimp/<Continue>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CContinueU3Ec__Iterator1_t2046304316, ___U24this_0)); }
	inline Battleimp_t158148094 * get_U24this_0() const { return ___U24this_0; }
	inline Battleimp_t158148094 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Battleimp_t158148094 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CContinueU3Ec__Iterator1_t2046304316, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CContinueU3Ec__Iterator1_t2046304316, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CContinueU3Ec__Iterator1_t2046304316, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONTINUEU3EC__ITERATOR1_T2046304316_H
#ifndef U3CENEMYATTACKU3EC__ITERATOR0_T889231986_H
#define U3CENEMYATTACKU3EC__ITERATOR0_T889231986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Battleimp/<EnemyAttack>c__Iterator0
struct  U3CEnemyAttackU3Ec__Iterator0_t889231986  : public RuntimeObject
{
public:
	// System.Single Battleimp/<EnemyAttack>c__Iterator0::<rndDMG>__1
	float ___U3CrndDMGU3E__1_0;
	// Battleimp Battleimp/<EnemyAttack>c__Iterator0::$this
	Battleimp_t158148094 * ___U24this_1;
	// System.Object Battleimp/<EnemyAttack>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Battleimp/<EnemyAttack>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Battleimp/<EnemyAttack>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrndDMGU3E__1_0() { return static_cast<int32_t>(offsetof(U3CEnemyAttackU3Ec__Iterator0_t889231986, ___U3CrndDMGU3E__1_0)); }
	inline float get_U3CrndDMGU3E__1_0() const { return ___U3CrndDMGU3E__1_0; }
	inline float* get_address_of_U3CrndDMGU3E__1_0() { return &___U3CrndDMGU3E__1_0; }
	inline void set_U3CrndDMGU3E__1_0(float value)
	{
		___U3CrndDMGU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CEnemyAttackU3Ec__Iterator0_t889231986, ___U24this_1)); }
	inline Battleimp_t158148094 * get_U24this_1() const { return ___U24this_1; }
	inline Battleimp_t158148094 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Battleimp_t158148094 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CEnemyAttackU3Ec__Iterator0_t889231986, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CEnemyAttackU3Ec__Iterator0_t889231986, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CEnemyAttackU3Ec__Iterator0_t889231986, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CENEMYATTACKU3EC__ITERATOR0_T889231986_H
#ifndef U3CCONTINUEU3EC__ITERATOR1_T1090546628_H
#define U3CCONTINUEU3EC__ITERATOR1_T1090546628_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Battleinf/<Continue>c__Iterator1
struct  U3CContinueU3Ec__Iterator1_t1090546628  : public RuntimeObject
{
public:
	// Battleinf Battleinf/<Continue>c__Iterator1::$this
	Battleinf_t1732126188 * ___U24this_0;
	// System.Object Battleinf/<Continue>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Battleinf/<Continue>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 Battleinf/<Continue>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CContinueU3Ec__Iterator1_t1090546628, ___U24this_0)); }
	inline Battleinf_t1732126188 * get_U24this_0() const { return ___U24this_0; }
	inline Battleinf_t1732126188 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Battleinf_t1732126188 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CContinueU3Ec__Iterator1_t1090546628, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CContinueU3Ec__Iterator1_t1090546628, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CContinueU3Ec__Iterator1_t1090546628, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONTINUEU3EC__ITERATOR1_T1090546628_H
#ifndef U3CDIEU3EC__ITERATOR2_T3004093797_H
#define U3CDIEU3EC__ITERATOR2_T3004093797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Battleinf/<DIe>c__Iterator2
struct  U3CDIeU3Ec__Iterator2_t3004093797  : public RuntimeObject
{
public:
	// Battleinf Battleinf/<DIe>c__Iterator2::$this
	Battleinf_t1732126188 * ___U24this_0;
	// System.Object Battleinf/<DIe>c__Iterator2::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Battleinf/<DIe>c__Iterator2::$disposing
	bool ___U24disposing_2;
	// System.Int32 Battleinf/<DIe>c__Iterator2::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CDIeU3Ec__Iterator2_t3004093797, ___U24this_0)); }
	inline Battleinf_t1732126188 * get_U24this_0() const { return ___U24this_0; }
	inline Battleinf_t1732126188 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Battleinf_t1732126188 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDIeU3Ec__Iterator2_t3004093797, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDIeU3Ec__Iterator2_t3004093797, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDIeU3Ec__Iterator2_t3004093797, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDIEU3EC__ITERATOR2_T3004093797_H
#ifndef U3CENEMYATTACKU3EC__ITERATOR0_T2884124416_H
#define U3CENEMYATTACKU3EC__ITERATOR0_T2884124416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Battleinf/<EnemyAttack>c__Iterator0
struct  U3CEnemyAttackU3Ec__Iterator0_t2884124416  : public RuntimeObject
{
public:
	// System.Single Battleinf/<EnemyAttack>c__Iterator0::<rndDMG>__1
	float ___U3CrndDMGU3E__1_0;
	// Battleinf Battleinf/<EnemyAttack>c__Iterator0::$this
	Battleinf_t1732126188 * ___U24this_1;
	// System.Object Battleinf/<EnemyAttack>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Battleinf/<EnemyAttack>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Battleinf/<EnemyAttack>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrndDMGU3E__1_0() { return static_cast<int32_t>(offsetof(U3CEnemyAttackU3Ec__Iterator0_t2884124416, ___U3CrndDMGU3E__1_0)); }
	inline float get_U3CrndDMGU3E__1_0() const { return ___U3CrndDMGU3E__1_0; }
	inline float* get_address_of_U3CrndDMGU3E__1_0() { return &___U3CrndDMGU3E__1_0; }
	inline void set_U3CrndDMGU3E__1_0(float value)
	{
		___U3CrndDMGU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CEnemyAttackU3Ec__Iterator0_t2884124416, ___U24this_1)); }
	inline Battleinf_t1732126188 * get_U24this_1() const { return ___U24this_1; }
	inline Battleinf_t1732126188 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Battleinf_t1732126188 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CEnemyAttackU3Ec__Iterator0_t2884124416, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CEnemyAttackU3Ec__Iterator0_t2884124416, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CEnemyAttackU3Ec__Iterator0_t2884124416, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CENEMYATTACKU3EC__ITERATOR0_T2884124416_H
#ifndef U3CLOADINGU3EC__ITERATOR0_T3071600265_H
#define U3CLOADINGU3EC__ITERATOR0_T3071600265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Program/<Loading>c__Iterator0
struct  U3CLoadingU3Ec__Iterator0_t3071600265  : public RuntimeObject
{
public:
	// Harmony.Program Harmony.Program/<Loading>c__Iterator0::$this
	Program_t1866495201 * ___U24this_0;
	// System.Object Harmony.Program/<Loading>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Harmony.Program/<Loading>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Harmony.Program/<Loading>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t3071600265, ___U24this_0)); }
	inline Program_t1866495201 * get_U24this_0() const { return ___U24this_0; }
	inline Program_t1866495201 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Program_t1866495201 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t3071600265, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t3071600265, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t3071600265, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADINGU3EC__ITERATOR0_T3071600265_H
#ifndef U3CPLAYBOOTSFXU3EC__ITERATOR1_T3195710289_H
#define U3CPLAYBOOTSFXU3EC__ITERATOR1_T3195710289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Program/<PlayBootSFX>c__Iterator1
struct  U3CPlayBootSFXU3Ec__Iterator1_t3195710289  : public RuntimeObject
{
public:
	// Harmony.Program Harmony.Program/<PlayBootSFX>c__Iterator1::$this
	Program_t1866495201 * ___U24this_0;
	// System.Object Harmony.Program/<PlayBootSFX>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Harmony.Program/<PlayBootSFX>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 Harmony.Program/<PlayBootSFX>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CPlayBootSFXU3Ec__Iterator1_t3195710289, ___U24this_0)); }
	inline Program_t1866495201 * get_U24this_0() const { return ___U24this_0; }
	inline Program_t1866495201 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Program_t1866495201 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CPlayBootSFXU3Ec__Iterator1_t3195710289, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CPlayBootSFXU3Ec__Iterator1_t3195710289, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CPlayBootSFXU3Ec__Iterator1_t3195710289, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPLAYBOOTSFXU3EC__ITERATOR1_T3195710289_H
#ifndef U3CLOADINGU3EC__ITERATOR0_T2822561722_H
#define U3CLOADINGU3EC__ITERATOR0_T2822561722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Program2/<Loading>c__Iterator0
struct  U3CLoadingU3Ec__Iterator0_t2822561722  : public RuntimeObject
{
public:
	// Harmony.Program2 Harmony.Program2/<Loading>c__Iterator0::$this
	Program2_t3485103329 * ___U24this_0;
	// System.Object Harmony.Program2/<Loading>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Harmony.Program2/<Loading>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Harmony.Program2/<Loading>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t2822561722, ___U24this_0)); }
	inline Program2_t3485103329 * get_U24this_0() const { return ___U24this_0; }
	inline Program2_t3485103329 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Program2_t3485103329 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t2822561722, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t2822561722, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t2822561722, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADINGU3EC__ITERATOR0_T2822561722_H
#ifndef U3CLOADINGU3EC__ITERATOR0_T2437702791_H
#define U3CLOADINGU3EC__ITERATOR0_T2437702791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Program3/<Loading>c__Iterator0
struct  U3CLoadingU3Ec__Iterator0_t2437702791  : public RuntimeObject
{
public:
	// Harmony.Program3 Harmony.Program3/<Loading>c__Iterator0::$this
	Program3_t1528788193 * ___U24this_0;
	// System.Object Harmony.Program3/<Loading>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Harmony.Program3/<Loading>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Harmony.Program3/<Loading>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t2437702791, ___U24this_0)); }
	inline Program3_t1528788193 * get_U24this_0() const { return ___U24this_0; }
	inline Program3_t1528788193 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Program3_t1528788193 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t2437702791, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t2437702791, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t2437702791, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADINGU3EC__ITERATOR0_T2437702791_H
#ifndef U3CANIMATETEXTU3EC__ITERATOR0_T429204900_H
#define U3CANIMATETEXTU3EC__ITERATOR0_T429204900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Type/<AnimateText>c__Iterator0
struct  U3CAnimateTextU3Ec__Iterator0_t429204900  : public RuntimeObject
{
public:
	// System.Int32 Harmony.Type/<AnimateText>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// System.String Harmony.Type/<AnimateText>c__Iterator0::message
	String_t* ___message_1;
	// TMPro.TMP_Text Harmony.Type/<AnimateText>c__Iterator0::textDes
	TMP_Text_t2599618874 * ___textDes_2;
	// System.Single Harmony.Type/<AnimateText>c__Iterator0::speed
	float ___speed_3;
	// System.Object Harmony.Type/<AnimateText>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Harmony.Type/<AnimateText>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Harmony.Type/<AnimateText>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t429204900, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t429204900, ___message_1)); }
	inline String_t* get_message_1() const { return ___message_1; }
	inline String_t** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(String_t* value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier((&___message_1), value);
	}

	inline static int32_t get_offset_of_textDes_2() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t429204900, ___textDes_2)); }
	inline TMP_Text_t2599618874 * get_textDes_2() const { return ___textDes_2; }
	inline TMP_Text_t2599618874 ** get_address_of_textDes_2() { return &___textDes_2; }
	inline void set_textDes_2(TMP_Text_t2599618874 * value)
	{
		___textDes_2 = value;
		Il2CppCodeGenWriteBarrier((&___textDes_2), value);
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t429204900, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t429204900, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t429204900, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t429204900, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATETEXTU3EC__ITERATOR0_T429204900_H
#ifndef U3CANIMATETEXTU3EC__ITERATOR0_T589720723_H
#define U3CANIMATETEXTU3EC__ITERATOR0_T589720723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Type2/<AnimateText>c__Iterator0
struct  U3CAnimateTextU3Ec__Iterator0_t589720723  : public RuntimeObject
{
public:
	// System.Int32 Harmony.Type2/<AnimateText>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// System.String Harmony.Type2/<AnimateText>c__Iterator0::message
	String_t* ___message_1;
	// TMPro.TMP_Text Harmony.Type2/<AnimateText>c__Iterator0::textDes
	TMP_Text_t2599618874 * ___textDes_2;
	// System.Single Harmony.Type2/<AnimateText>c__Iterator0::speed
	float ___speed_3;
	// System.Object Harmony.Type2/<AnimateText>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Harmony.Type2/<AnimateText>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Harmony.Type2/<AnimateText>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t589720723, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t589720723, ___message_1)); }
	inline String_t* get_message_1() const { return ___message_1; }
	inline String_t** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(String_t* value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier((&___message_1), value);
	}

	inline static int32_t get_offset_of_textDes_2() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t589720723, ___textDes_2)); }
	inline TMP_Text_t2599618874 * get_textDes_2() const { return ___textDes_2; }
	inline TMP_Text_t2599618874 ** get_address_of_textDes_2() { return &___textDes_2; }
	inline void set_textDes_2(TMP_Text_t2599618874 * value)
	{
		___textDes_2 = value;
		Il2CppCodeGenWriteBarrier((&___textDes_2), value);
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t589720723, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t589720723, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t589720723, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t589720723, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATETEXTU3EC__ITERATOR0_T589720723_H
#ifndef U3CANIMATETEXTU3EC__ITERATOR0_T772139121_H
#define U3CANIMATETEXTU3EC__ITERATOR0_T772139121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Type3/<AnimateText>c__Iterator0
struct  U3CAnimateTextU3Ec__Iterator0_t772139121  : public RuntimeObject
{
public:
	// System.Int32 Harmony.Type3/<AnimateText>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// System.String Harmony.Type3/<AnimateText>c__Iterator0::message
	String_t* ___message_1;
	// TMPro.TMP_Text Harmony.Type3/<AnimateText>c__Iterator0::textDes
	TMP_Text_t2599618874 * ___textDes_2;
	// System.Single Harmony.Type3/<AnimateText>c__Iterator0::speed
	float ___speed_3;
	// System.Object Harmony.Type3/<AnimateText>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Harmony.Type3/<AnimateText>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Harmony.Type3/<AnimateText>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t772139121, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t772139121, ___message_1)); }
	inline String_t* get_message_1() const { return ___message_1; }
	inline String_t** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(String_t* value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier((&___message_1), value);
	}

	inline static int32_t get_offset_of_textDes_2() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t772139121, ___textDes_2)); }
	inline TMP_Text_t2599618874 * get_textDes_2() const { return ___textDes_2; }
	inline TMP_Text_t2599618874 ** get_address_of_textDes_2() { return &___textDes_2; }
	inline void set_textDes_2(TMP_Text_t2599618874 * value)
	{
		___textDes_2 = value;
		Il2CppCodeGenWriteBarrier((&___textDes_2), value);
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t772139121, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t772139121, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t772139121, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t772139121, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATETEXTU3EC__ITERATOR0_T772139121_H
#ifndef U3CLOADINGU3EC__ITERATOR0_T2464126437_H
#define U3CLOADINGU3EC__ITERATOR0_T2464126437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.menuProgram/<Loading>c__Iterator0
struct  U3CLoadingU3Ec__Iterator0_t2464126437  : public RuntimeObject
{
public:
	// Harmony.menuProgram Harmony.menuProgram/<Loading>c__Iterator0::$this
	menuProgram_t465836879 * ___U24this_0;
	// System.Object Harmony.menuProgram/<Loading>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Harmony.menuProgram/<Loading>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Harmony.menuProgram/<Loading>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t2464126437, ___U24this_0)); }
	inline menuProgram_t465836879 * get_U24this_0() const { return ___U24this_0; }
	inline menuProgram_t465836879 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(menuProgram_t465836879 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t2464126437, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t2464126437, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator0_t2464126437, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADINGU3EC__ITERATOR0_T2464126437_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef RANDOM_T108471755_H
#define RANDOM_T108471755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Random
struct  Random_t108471755  : public RuntimeObject
{
public:
	// System.Int32 System.Random::inext
	int32_t ___inext_3;
	// System.Int32 System.Random::inextp
	int32_t ___inextp_4;
	// System.Int32[] System.Random::SeedArray
	Int32U5BU5D_t385246372* ___SeedArray_5;

public:
	inline static int32_t get_offset_of_inext_3() { return static_cast<int32_t>(offsetof(Random_t108471755, ___inext_3)); }
	inline int32_t get_inext_3() const { return ___inext_3; }
	inline int32_t* get_address_of_inext_3() { return &___inext_3; }
	inline void set_inext_3(int32_t value)
	{
		___inext_3 = value;
	}

	inline static int32_t get_offset_of_inextp_4() { return static_cast<int32_t>(offsetof(Random_t108471755, ___inextp_4)); }
	inline int32_t get_inextp_4() const { return ___inextp_4; }
	inline int32_t* get_address_of_inextp_4() { return &___inextp_4; }
	inline void set_inextp_4(int32_t value)
	{
		___inextp_4 = value;
	}

	inline static int32_t get_offset_of_SeedArray_5() { return static_cast<int32_t>(offsetof(Random_t108471755, ___SeedArray_5)); }
	inline Int32U5BU5D_t385246372* get_SeedArray_5() const { return ___SeedArray_5; }
	inline Int32U5BU5D_t385246372** get_address_of_SeedArray_5() { return &___SeedArray_5; }
	inline void set_SeedArray_5(Int32U5BU5D_t385246372* value)
	{
		___SeedArray_5 = value;
		Il2CppCodeGenWriteBarrier((&___SeedArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOM_T108471755_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef CUSTOMYIELDINSTRUCTION_T1895667560_H
#define CUSTOMYIELDINSTRUCTION_T1895667560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CustomYieldInstruction
struct  CustomYieldInstruction_t1895667560  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMYIELDINSTRUCTION_T1895667560_H
#ifndef YIELDINSTRUCTION_T403091072_H
#define YIELDINSTRUCTION_T403091072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t403091072  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T403091072_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef BYTE_T1134296376_H
#define BYTE_T1134296376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t1134296376 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Byte_t1134296376, ___m_value_2)); }
	inline uint8_t get_m_value_2() const { return ___m_value_2; }
	inline uint8_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint8_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T1134296376_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_2)); }
	inline uint32_t get_m_value_2() const { return ___m_value_2; }
	inline uint32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef MATERIALREFERENCE_T1952344632_H
#define MATERIALREFERENCE_T1952344632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaterialReference
struct  MaterialReference_t1952344632 
{
public:
	// System.Int32 TMPro.MaterialReference::index
	int32_t ___index_0;
	// TMPro.TMP_FontAsset TMPro.MaterialReference::fontAsset
	TMP_FontAsset_t364381626 * ___fontAsset_1;
	// TMPro.TMP_SpriteAsset TMPro.MaterialReference::spriteAsset
	TMP_SpriteAsset_t484820633 * ___spriteAsset_2;
	// UnityEngine.Material TMPro.MaterialReference::material
	Material_t340375123 * ___material_3;
	// System.Boolean TMPro.MaterialReference::isDefaultMaterial
	bool ___isDefaultMaterial_4;
	// System.Boolean TMPro.MaterialReference::isFallbackMaterial
	bool ___isFallbackMaterial_5;
	// UnityEngine.Material TMPro.MaterialReference::fallbackMaterial
	Material_t340375123 * ___fallbackMaterial_6;
	// System.Single TMPro.MaterialReference::padding
	float ___padding_7;
	// System.Int32 TMPro.MaterialReference::referenceCount
	int32_t ___referenceCount_8;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_fontAsset_1() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___fontAsset_1)); }
	inline TMP_FontAsset_t364381626 * get_fontAsset_1() const { return ___fontAsset_1; }
	inline TMP_FontAsset_t364381626 ** get_address_of_fontAsset_1() { return &___fontAsset_1; }
	inline void set_fontAsset_1(TMP_FontAsset_t364381626 * value)
	{
		___fontAsset_1 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_1), value);
	}

	inline static int32_t get_offset_of_spriteAsset_2() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___spriteAsset_2)); }
	inline TMP_SpriteAsset_t484820633 * get_spriteAsset_2() const { return ___spriteAsset_2; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_spriteAsset_2() { return &___spriteAsset_2; }
	inline void set_spriteAsset_2(TMP_SpriteAsset_t484820633 * value)
	{
		___spriteAsset_2 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_2), value);
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___material_3)); }
	inline Material_t340375123 * get_material_3() const { return ___material_3; }
	inline Material_t340375123 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_t340375123 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((&___material_3), value);
	}

	inline static int32_t get_offset_of_isDefaultMaterial_4() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___isDefaultMaterial_4)); }
	inline bool get_isDefaultMaterial_4() const { return ___isDefaultMaterial_4; }
	inline bool* get_address_of_isDefaultMaterial_4() { return &___isDefaultMaterial_4; }
	inline void set_isDefaultMaterial_4(bool value)
	{
		___isDefaultMaterial_4 = value;
	}

	inline static int32_t get_offset_of_isFallbackMaterial_5() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___isFallbackMaterial_5)); }
	inline bool get_isFallbackMaterial_5() const { return ___isFallbackMaterial_5; }
	inline bool* get_address_of_isFallbackMaterial_5() { return &___isFallbackMaterial_5; }
	inline void set_isFallbackMaterial_5(bool value)
	{
		___isFallbackMaterial_5 = value;
	}

	inline static int32_t get_offset_of_fallbackMaterial_6() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___fallbackMaterial_6)); }
	inline Material_t340375123 * get_fallbackMaterial_6() const { return ___fallbackMaterial_6; }
	inline Material_t340375123 ** get_address_of_fallbackMaterial_6() { return &___fallbackMaterial_6; }
	inline void set_fallbackMaterial_6(Material_t340375123 * value)
	{
		___fallbackMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackMaterial_6), value);
	}

	inline static int32_t get_offset_of_padding_7() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___padding_7)); }
	inline float get_padding_7() const { return ___padding_7; }
	inline float* get_address_of_padding_7() { return &___padding_7; }
	inline void set_padding_7(float value)
	{
		___padding_7 = value;
	}

	inline static int32_t get_offset_of_referenceCount_8() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___referenceCount_8)); }
	inline int32_t get_referenceCount_8() const { return ___referenceCount_8; }
	inline int32_t* get_address_of_referenceCount_8() { return &___referenceCount_8; }
	inline void set_referenceCount_8(int32_t value)
	{
		___referenceCount_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.MaterialReference
struct MaterialReference_t1952344632_marshaled_pinvoke
{
	int32_t ___index_0;
	TMP_FontAsset_t364381626 * ___fontAsset_1;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_2;
	Material_t340375123 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t340375123 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
// Native definition for COM marshalling of TMPro.MaterialReference
struct MaterialReference_t1952344632_marshaled_com
{
	int32_t ___index_0;
	TMP_FontAsset_t364381626 * ___fontAsset_1;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_2;
	Material_t340375123 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t340375123 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
#endif // MATERIALREFERENCE_T1952344632_H
#ifndef TMP_BASICXMLTAGSTACK_T2962628096_H
#define TMP_BASICXMLTAGSTACK_T2962628096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_BasicXmlTagStack
struct  TMP_BasicXmlTagStack_t2962628096 
{
public:
	// System.Byte TMPro.TMP_BasicXmlTagStack::bold
	uint8_t ___bold_0;
	// System.Byte TMPro.TMP_BasicXmlTagStack::italic
	uint8_t ___italic_1;
	// System.Byte TMPro.TMP_BasicXmlTagStack::underline
	uint8_t ___underline_2;
	// System.Byte TMPro.TMP_BasicXmlTagStack::strikethrough
	uint8_t ___strikethrough_3;
	// System.Byte TMPro.TMP_BasicXmlTagStack::highlight
	uint8_t ___highlight_4;
	// System.Byte TMPro.TMP_BasicXmlTagStack::superscript
	uint8_t ___superscript_5;
	// System.Byte TMPro.TMP_BasicXmlTagStack::subscript
	uint8_t ___subscript_6;
	// System.Byte TMPro.TMP_BasicXmlTagStack::uppercase
	uint8_t ___uppercase_7;
	// System.Byte TMPro.TMP_BasicXmlTagStack::lowercase
	uint8_t ___lowercase_8;
	// System.Byte TMPro.TMP_BasicXmlTagStack::smallcaps
	uint8_t ___smallcaps_9;

public:
	inline static int32_t get_offset_of_bold_0() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___bold_0)); }
	inline uint8_t get_bold_0() const { return ___bold_0; }
	inline uint8_t* get_address_of_bold_0() { return &___bold_0; }
	inline void set_bold_0(uint8_t value)
	{
		___bold_0 = value;
	}

	inline static int32_t get_offset_of_italic_1() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___italic_1)); }
	inline uint8_t get_italic_1() const { return ___italic_1; }
	inline uint8_t* get_address_of_italic_1() { return &___italic_1; }
	inline void set_italic_1(uint8_t value)
	{
		___italic_1 = value;
	}

	inline static int32_t get_offset_of_underline_2() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___underline_2)); }
	inline uint8_t get_underline_2() const { return ___underline_2; }
	inline uint8_t* get_address_of_underline_2() { return &___underline_2; }
	inline void set_underline_2(uint8_t value)
	{
		___underline_2 = value;
	}

	inline static int32_t get_offset_of_strikethrough_3() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___strikethrough_3)); }
	inline uint8_t get_strikethrough_3() const { return ___strikethrough_3; }
	inline uint8_t* get_address_of_strikethrough_3() { return &___strikethrough_3; }
	inline void set_strikethrough_3(uint8_t value)
	{
		___strikethrough_3 = value;
	}

	inline static int32_t get_offset_of_highlight_4() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___highlight_4)); }
	inline uint8_t get_highlight_4() const { return ___highlight_4; }
	inline uint8_t* get_address_of_highlight_4() { return &___highlight_4; }
	inline void set_highlight_4(uint8_t value)
	{
		___highlight_4 = value;
	}

	inline static int32_t get_offset_of_superscript_5() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___superscript_5)); }
	inline uint8_t get_superscript_5() const { return ___superscript_5; }
	inline uint8_t* get_address_of_superscript_5() { return &___superscript_5; }
	inline void set_superscript_5(uint8_t value)
	{
		___superscript_5 = value;
	}

	inline static int32_t get_offset_of_subscript_6() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___subscript_6)); }
	inline uint8_t get_subscript_6() const { return ___subscript_6; }
	inline uint8_t* get_address_of_subscript_6() { return &___subscript_6; }
	inline void set_subscript_6(uint8_t value)
	{
		___subscript_6 = value;
	}

	inline static int32_t get_offset_of_uppercase_7() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___uppercase_7)); }
	inline uint8_t get_uppercase_7() const { return ___uppercase_7; }
	inline uint8_t* get_address_of_uppercase_7() { return &___uppercase_7; }
	inline void set_uppercase_7(uint8_t value)
	{
		___uppercase_7 = value;
	}

	inline static int32_t get_offset_of_lowercase_8() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___lowercase_8)); }
	inline uint8_t get_lowercase_8() const { return ___lowercase_8; }
	inline uint8_t* get_address_of_lowercase_8() { return &___lowercase_8; }
	inline void set_lowercase_8(uint8_t value)
	{
		___lowercase_8 = value;
	}

	inline static int32_t get_offset_of_smallcaps_9() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___smallcaps_9)); }
	inline uint8_t get_smallcaps_9() const { return ___smallcaps_9; }
	inline uint8_t* get_address_of_smallcaps_9() { return &___smallcaps_9; }
	inline void set_smallcaps_9(uint8_t value)
	{
		___smallcaps_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_BASICXMLTAGSTACK_T2962628096_H
#ifndef TMP_XMLTAGSTACK_1_T2514600297_H
#define TMP_XMLTAGSTACK_1_T2514600297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<System.Int32>
struct  TMP_XmlTagStack_1_t2514600297 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	Int32U5BU5D_t385246372* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	int32_t ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___itemStack_0)); }
	inline Int32U5BU5D_t385246372* get_itemStack_0() const { return ___itemStack_0; }
	inline Int32U5BU5D_t385246372** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(Int32U5BU5D_t385246372* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___m_defaultItem_3)); }
	inline int32_t get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline int32_t* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(int32_t value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T2514600297_H
#ifndef TMP_XMLTAGSTACK_1_T960921318_H
#define TMP_XMLTAGSTACK_1_T960921318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<System.Single>
struct  TMP_XmlTagStack_1_t960921318 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	SingleU5BU5D_t1444911251* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	float ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___itemStack_0)); }
	inline SingleU5BU5D_t1444911251* get_itemStack_0() const { return ___itemStack_0; }
	inline SingleU5BU5D_t1444911251** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(SingleU5BU5D_t1444911251* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___m_defaultItem_3)); }
	inline float get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline float* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(float value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T960921318_H
#ifndef TMP_XMLTAGSTACK_1_T3241710312_H
#define TMP_XMLTAGSTACK_1_T3241710312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.TMP_ColorGradient>
struct  TMP_XmlTagStack_1_t3241710312 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	TMP_ColorGradientU5BU5D_t2496920137* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	TMP_ColorGradient_t3678055768 * ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3241710312, ___itemStack_0)); }
	inline TMP_ColorGradientU5BU5D_t2496920137* get_itemStack_0() const { return ___itemStack_0; }
	inline TMP_ColorGradientU5BU5D_t2496920137** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(TMP_ColorGradientU5BU5D_t2496920137* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3241710312, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3241710312, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3241710312, ___m_defaultItem_3)); }
	inline TMP_ColorGradient_t3678055768 * get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline TMP_ColorGradient_t3678055768 ** get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(TMP_ColorGradient_t3678055768 * value)
	{
		___m_defaultItem_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultItem_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T3241710312_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef COLOR32_T2600501292_H
#define COLOR32_T2600501292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t2600501292 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T2600501292_H
#ifndef DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#define DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t2562230146 
{
public:
	union
	{
		struct
		{
		};
		uint8_t DrivenRectTransformTracker_t2562230146__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef SPRITESTATE_T1362986479_H
#define SPRITESTATE_T1362986479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t1362986479 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t280657092 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t280657092 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_HighlightedSprite_0)); }
	inline Sprite_t280657092 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t280657092 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t280657092 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_PressedSprite_1)); }
	inline Sprite_t280657092 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t280657092 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t280657092 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_DisabledSprite_2)); }
	inline Sprite_t280657092 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t280657092 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t280657092 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_pinvoke
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_com
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T1362986479_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef WAITFORSECONDS_T1699091251_H
#define WAITFORSECONDS_T1699091251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WaitForSeconds
struct  WaitForSeconds_t1699091251  : public YieldInstruction_t403091072
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t1699091251, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	float ___m_Seconds_0;
};
#endif // WAITFORSECONDS_T1699091251_H
#ifndef WAITUNTIL_T3373419216_H
#define WAITUNTIL_T3373419216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WaitUntil
struct  WaitUntil_t3373419216  : public CustomYieldInstruction_t1895667560
{
public:
	// System.Func`1<System.Boolean> UnityEngine.WaitUntil::m_Predicate
	Func_1_t3822001908 * ___m_Predicate_0;

public:
	inline static int32_t get_offset_of_m_Predicate_0() { return static_cast<int32_t>(offsetof(WaitUntil_t3373419216, ___m_Predicate_0)); }
	inline Func_1_t3822001908 * get_m_Predicate_0() const { return ___m_Predicate_0; }
	inline Func_1_t3822001908 ** get_address_of_m_Predicate_0() { return &___m_Predicate_0; }
	inline void set_m_Predicate_0(Func_1_t3822001908 * value)
	{
		___m_Predicate_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Predicate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITUNTIL_T3373419216_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef NOTSUPPORTEDEXCEPTION_T1314879016_H
#define NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_t1314879016  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifndef EXTENTS_T3837212874_H
#define EXTENTS_T3837212874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Extents
struct  Extents_t3837212874 
{
public:
	// UnityEngine.Vector2 TMPro.Extents::min
	Vector2_t2156229523  ___min_0;
	// UnityEngine.Vector2 TMPro.Extents::max
	Vector2_t2156229523  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Extents_t3837212874, ___min_0)); }
	inline Vector2_t2156229523  get_min_0() const { return ___min_0; }
	inline Vector2_t2156229523 * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_t2156229523  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Extents_t3837212874, ___max_1)); }
	inline Vector2_t2156229523  get_max_1() const { return ___max_1; }
	inline Vector2_t2156229523 * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_t2156229523  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENTS_T3837212874_H
#ifndef FONTSTYLES_T3828945032_H
#define FONTSTYLES_T3828945032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontStyles
struct  FontStyles_t3828945032 
{
public:
	// System.Int32 TMPro.FontStyles::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FontStyles_t3828945032, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSTYLES_T3828945032_H
#ifndef TEXTINPUTSOURCES_T1522115805_H
#define TEXTINPUTSOURCES_T1522115805_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Text/TextInputSources
struct  TextInputSources_t1522115805 
{
public:
	// System.Int32 TMPro.TMP_Text/TextInputSources::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextInputSources_t1522115805, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTINPUTSOURCES_T1522115805_H
#ifndef TMP_TEXTELEMENTTYPE_T1276645592_H
#define TMP_TEXTELEMENTTYPE_T1276645592_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElementType
struct  TMP_TextElementType_t1276645592 
{
public:
	// System.Int32 TMPro.TMP_TextElementType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TMP_TextElementType_t1276645592, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENTTYPE_T1276645592_H
#ifndef TMP_XMLTAGSTACK_1_T1515999176_H
#define TMP_XMLTAGSTACK_1_T1515999176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference>
struct  TMP_XmlTagStack_1_t1515999176 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	MaterialReferenceU5BU5D_t648826345* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	MaterialReference_t1952344632  ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___itemStack_0)); }
	inline MaterialReferenceU5BU5D_t648826345* get_itemStack_0() const { return ___itemStack_0; }
	inline MaterialReferenceU5BU5D_t648826345** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(MaterialReferenceU5BU5D_t648826345* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___m_defaultItem_3)); }
	inline MaterialReference_t1952344632  get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline MaterialReference_t1952344632 * get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(MaterialReference_t1952344632  value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T1515999176_H
#ifndef TMP_XMLTAGSTACK_1_T2164155836_H
#define TMP_XMLTAGSTACK_1_T2164155836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32>
struct  TMP_XmlTagStack_1_t2164155836 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	Color32U5BU5D_t3850468773* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	Color32_t2600501292  ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___itemStack_0)); }
	inline Color32U5BU5D_t3850468773* get_itemStack_0() const { return ___itemStack_0; }
	inline Color32U5BU5D_t3850468773** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(Color32U5BU5D_t3850468773* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___m_defaultItem_3)); }
	inline Color32_t2600501292  get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline Color32_t2600501292 * get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(Color32_t2600501292  value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T2164155836_H
#ifndef TEXTALIGNMENTOPTIONS_T4036791236_H
#define TEXTALIGNMENTOPTIONS_T4036791236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextAlignmentOptions
struct  TextAlignmentOptions_t4036791236 
{
public:
	// System.Int32 TMPro.TextAlignmentOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextAlignmentOptions_t4036791236, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTALIGNMENTOPTIONS_T4036791236_H
#ifndef TEXTOVERFLOWMODES_T1430035314_H
#define TEXTOVERFLOWMODES_T1430035314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextOverflowModes
struct  TextOverflowModes_t1430035314 
{
public:
	// System.Int32 TMPro.TextOverflowModes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextOverflowModes_t1430035314, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTOVERFLOWMODES_T1430035314_H
#ifndef TEXTRENDERFLAGS_T2418684345_H
#define TEXTRENDERFLAGS_T2418684345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextRenderFlags
struct  TextRenderFlags_t2418684345 
{
public:
	// System.Int32 TMPro.TextRenderFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextRenderFlags_t2418684345, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTRENDERFLAGS_T2418684345_H
#ifndef TEXTUREMAPPINGOPTIONS_T270963663_H
#define TEXTUREMAPPINGOPTIONS_T270963663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextureMappingOptions
struct  TextureMappingOptions_t270963663 
{
public:
	// System.Int32 TMPro.TextureMappingOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureMappingOptions_t270963663, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREMAPPINGOPTIONS_T270963663_H
#ifndef VERTEXGRADIENT_T345148380_H
#define VERTEXGRADIENT_T345148380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexGradient
struct  VertexGradient_t345148380 
{
public:
	// UnityEngine.Color TMPro.VertexGradient::topLeft
	Color_t2555686324  ___topLeft_0;
	// UnityEngine.Color TMPro.VertexGradient::topRight
	Color_t2555686324  ___topRight_1;
	// UnityEngine.Color TMPro.VertexGradient::bottomLeft
	Color_t2555686324  ___bottomLeft_2;
	// UnityEngine.Color TMPro.VertexGradient::bottomRight
	Color_t2555686324  ___bottomRight_3;

public:
	inline static int32_t get_offset_of_topLeft_0() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___topLeft_0)); }
	inline Color_t2555686324  get_topLeft_0() const { return ___topLeft_0; }
	inline Color_t2555686324 * get_address_of_topLeft_0() { return &___topLeft_0; }
	inline void set_topLeft_0(Color_t2555686324  value)
	{
		___topLeft_0 = value;
	}

	inline static int32_t get_offset_of_topRight_1() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___topRight_1)); }
	inline Color_t2555686324  get_topRight_1() const { return ___topRight_1; }
	inline Color_t2555686324 * get_address_of_topRight_1() { return &___topRight_1; }
	inline void set_topRight_1(Color_t2555686324  value)
	{
		___topRight_1 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_2() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___bottomLeft_2)); }
	inline Color_t2555686324  get_bottomLeft_2() const { return ___bottomLeft_2; }
	inline Color_t2555686324 * get_address_of_bottomLeft_2() { return &___bottomLeft_2; }
	inline void set_bottomLeft_2(Color_t2555686324  value)
	{
		___bottomLeft_2 = value;
	}

	inline static int32_t get_offset_of_bottomRight_3() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___bottomRight_3)); }
	inline Color_t2555686324  get_bottomRight_3() const { return ___bottomRight_3; }
	inline Color_t2555686324 * get_address_of_bottomRight_3() { return &___bottomRight_3; }
	inline void set_bottomRight_3(Color_t2555686324  value)
	{
		___bottomRight_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXGRADIENT_T345148380_H
#ifndef VERTEXSORTINGORDER_T2659893934_H
#define VERTEXSORTINGORDER_T2659893934_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexSortingOrder
struct  VertexSortingOrder_t2659893934 
{
public:
	// System.Int32 TMPro.VertexSortingOrder::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VertexSortingOrder_t2659893934, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXSORTINGORDER_T2659893934_H
#ifndef COROUTINE_T3829159415_H
#define COROUTINE_T3829159415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Coroutine
struct  Coroutine_t3829159415  : public YieldInstruction_t403091072
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t3829159415, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // COROUTINE_T3829159415_H
#ifndef KEYCODE_T2599294277_H
#define KEYCODE_T2599294277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t2599294277 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyCode_t2599294277, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T2599294277_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef COLORBLOCK_T2139031574_H
#define COLORBLOCK_T2139031574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2139031574 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2555686324  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2555686324  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2555686324  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2555686324  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_NormalColor_0)); }
	inline Color_t2555686324  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2555686324 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2555686324  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_HighlightedColor_1)); }
	inline Color_t2555686324  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2555686324 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2555686324  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_PressedColor_2)); }
	inline Color_t2555686324  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2555686324 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2555686324  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_DisabledColor_3)); }
	inline Color_t2555686324  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2555686324 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2555686324  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2139031574_H
#ifndef FILLMETHOD_T1167457570_H
#define FILLMETHOD_T1167457570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/FillMethod
struct  FillMethod_t1167457570 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FillMethod_t1167457570, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLMETHOD_T1167457570_H
#ifndef TYPE_T1152881528_H
#define TYPE_T1152881528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/Type
struct  Type_t1152881528 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t1152881528, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T1152881528_H
#ifndef MODE_T1066900953_H
#define MODE_T1066900953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1066900953 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t1066900953, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1066900953_H
#ifndef SELECTIONSTATE_T2656606514_H
#define SELECTIONSTATE_T2656606514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t2656606514 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SelectionState_t2656606514, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T2656606514_H
#ifndef TRANSITION_T1769908631_H
#define TRANSITION_T1769908631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t1769908631 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Transition_t1769908631, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T1769908631_H
#ifndef DIRECTION_T337909235_H
#define DIRECTION_T337909235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Slider/Direction
struct  Direction_t337909235 
{
public:
	// System.Int32 UnityEngine.UI.Slider/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t337909235, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T337909235_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef TMP_LINEINFO_T1079631636_H
#define TMP_LINEINFO_T1079631636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_LineInfo
struct  TMP_LineInfo_t1079631636 
{
public:
	// System.Int32 TMPro.TMP_LineInfo::characterCount
	int32_t ___characterCount_0;
	// System.Int32 TMPro.TMP_LineInfo::visibleCharacterCount
	int32_t ___visibleCharacterCount_1;
	// System.Int32 TMPro.TMP_LineInfo::spaceCount
	int32_t ___spaceCount_2;
	// System.Int32 TMPro.TMP_LineInfo::wordCount
	int32_t ___wordCount_3;
	// System.Int32 TMPro.TMP_LineInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_4;
	// System.Int32 TMPro.TMP_LineInfo::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_5;
	// System.Int32 TMPro.TMP_LineInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_6;
	// System.Int32 TMPro.TMP_LineInfo::lastVisibleCharacterIndex
	int32_t ___lastVisibleCharacterIndex_7;
	// System.Single TMPro.TMP_LineInfo::length
	float ___length_8;
	// System.Single TMPro.TMP_LineInfo::lineHeight
	float ___lineHeight_9;
	// System.Single TMPro.TMP_LineInfo::ascender
	float ___ascender_10;
	// System.Single TMPro.TMP_LineInfo::baseline
	float ___baseline_11;
	// System.Single TMPro.TMP_LineInfo::descender
	float ___descender_12;
	// System.Single TMPro.TMP_LineInfo::maxAdvance
	float ___maxAdvance_13;
	// System.Single TMPro.TMP_LineInfo::width
	float ___width_14;
	// System.Single TMPro.TMP_LineInfo::marginLeft
	float ___marginLeft_15;
	// System.Single TMPro.TMP_LineInfo::marginRight
	float ___marginRight_16;
	// TMPro.TextAlignmentOptions TMPro.TMP_LineInfo::alignment
	int32_t ___alignment_17;
	// TMPro.Extents TMPro.TMP_LineInfo::lineExtents
	Extents_t3837212874  ___lineExtents_18;

public:
	inline static int32_t get_offset_of_characterCount_0() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___characterCount_0)); }
	inline int32_t get_characterCount_0() const { return ___characterCount_0; }
	inline int32_t* get_address_of_characterCount_0() { return &___characterCount_0; }
	inline void set_characterCount_0(int32_t value)
	{
		___characterCount_0 = value;
	}

	inline static int32_t get_offset_of_visibleCharacterCount_1() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___visibleCharacterCount_1)); }
	inline int32_t get_visibleCharacterCount_1() const { return ___visibleCharacterCount_1; }
	inline int32_t* get_address_of_visibleCharacterCount_1() { return &___visibleCharacterCount_1; }
	inline void set_visibleCharacterCount_1(int32_t value)
	{
		___visibleCharacterCount_1 = value;
	}

	inline static int32_t get_offset_of_spaceCount_2() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___spaceCount_2)); }
	inline int32_t get_spaceCount_2() const { return ___spaceCount_2; }
	inline int32_t* get_address_of_spaceCount_2() { return &___spaceCount_2; }
	inline void set_spaceCount_2(int32_t value)
	{
		___spaceCount_2 = value;
	}

	inline static int32_t get_offset_of_wordCount_3() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___wordCount_3)); }
	inline int32_t get_wordCount_3() const { return ___wordCount_3; }
	inline int32_t* get_address_of_wordCount_3() { return &___wordCount_3; }
	inline void set_wordCount_3(int32_t value)
	{
		___wordCount_3 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_4() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___firstCharacterIndex_4)); }
	inline int32_t get_firstCharacterIndex_4() const { return ___firstCharacterIndex_4; }
	inline int32_t* get_address_of_firstCharacterIndex_4() { return &___firstCharacterIndex_4; }
	inline void set_firstCharacterIndex_4(int32_t value)
	{
		___firstCharacterIndex_4 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_5() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___firstVisibleCharacterIndex_5)); }
	inline int32_t get_firstVisibleCharacterIndex_5() const { return ___firstVisibleCharacterIndex_5; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_5() { return &___firstVisibleCharacterIndex_5; }
	inline void set_firstVisibleCharacterIndex_5(int32_t value)
	{
		___firstVisibleCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_6() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lastCharacterIndex_6)); }
	inline int32_t get_lastCharacterIndex_6() const { return ___lastCharacterIndex_6; }
	inline int32_t* get_address_of_lastCharacterIndex_6() { return &___lastCharacterIndex_6; }
	inline void set_lastCharacterIndex_6(int32_t value)
	{
		___lastCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharacterIndex_7() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lastVisibleCharacterIndex_7)); }
	inline int32_t get_lastVisibleCharacterIndex_7() const { return ___lastVisibleCharacterIndex_7; }
	inline int32_t* get_address_of_lastVisibleCharacterIndex_7() { return &___lastVisibleCharacterIndex_7; }
	inline void set_lastVisibleCharacterIndex_7(int32_t value)
	{
		___lastVisibleCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_length_8() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___length_8)); }
	inline float get_length_8() const { return ___length_8; }
	inline float* get_address_of_length_8() { return &___length_8; }
	inline void set_length_8(float value)
	{
		___length_8 = value;
	}

	inline static int32_t get_offset_of_lineHeight_9() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lineHeight_9)); }
	inline float get_lineHeight_9() const { return ___lineHeight_9; }
	inline float* get_address_of_lineHeight_9() { return &___lineHeight_9; }
	inline void set_lineHeight_9(float value)
	{
		___lineHeight_9 = value;
	}

	inline static int32_t get_offset_of_ascender_10() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___ascender_10)); }
	inline float get_ascender_10() const { return ___ascender_10; }
	inline float* get_address_of_ascender_10() { return &___ascender_10; }
	inline void set_ascender_10(float value)
	{
		___ascender_10 = value;
	}

	inline static int32_t get_offset_of_baseline_11() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___baseline_11)); }
	inline float get_baseline_11() const { return ___baseline_11; }
	inline float* get_address_of_baseline_11() { return &___baseline_11; }
	inline void set_baseline_11(float value)
	{
		___baseline_11 = value;
	}

	inline static int32_t get_offset_of_descender_12() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___descender_12)); }
	inline float get_descender_12() const { return ___descender_12; }
	inline float* get_address_of_descender_12() { return &___descender_12; }
	inline void set_descender_12(float value)
	{
		___descender_12 = value;
	}

	inline static int32_t get_offset_of_maxAdvance_13() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___maxAdvance_13)); }
	inline float get_maxAdvance_13() const { return ___maxAdvance_13; }
	inline float* get_address_of_maxAdvance_13() { return &___maxAdvance_13; }
	inline void set_maxAdvance_13(float value)
	{
		___maxAdvance_13 = value;
	}

	inline static int32_t get_offset_of_width_14() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___width_14)); }
	inline float get_width_14() const { return ___width_14; }
	inline float* get_address_of_width_14() { return &___width_14; }
	inline void set_width_14(float value)
	{
		___width_14 = value;
	}

	inline static int32_t get_offset_of_marginLeft_15() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___marginLeft_15)); }
	inline float get_marginLeft_15() const { return ___marginLeft_15; }
	inline float* get_address_of_marginLeft_15() { return &___marginLeft_15; }
	inline void set_marginLeft_15(float value)
	{
		___marginLeft_15 = value;
	}

	inline static int32_t get_offset_of_marginRight_16() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___marginRight_16)); }
	inline float get_marginRight_16() const { return ___marginRight_16; }
	inline float* get_address_of_marginRight_16() { return &___marginRight_16; }
	inline void set_marginRight_16(float value)
	{
		___marginRight_16 = value;
	}

	inline static int32_t get_offset_of_alignment_17() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___alignment_17)); }
	inline int32_t get_alignment_17() const { return ___alignment_17; }
	inline int32_t* get_address_of_alignment_17() { return &___alignment_17; }
	inline void set_alignment_17(int32_t value)
	{
		___alignment_17 = value;
	}

	inline static int32_t get_offset_of_lineExtents_18() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lineExtents_18)); }
	inline Extents_t3837212874  get_lineExtents_18() const { return ___lineExtents_18; }
	inline Extents_t3837212874 * get_address_of_lineExtents_18() { return &___lineExtents_18; }
	inline void set_lineExtents_18(Extents_t3837212874  value)
	{
		___lineExtents_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_LINEINFO_T1079631636_H
#ifndef TMP_XMLTAGSTACK_1_T3600445780_H
#define TMP_XMLTAGSTACK_1_T3600445780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions>
struct  TMP_XmlTagStack_1_t3600445780 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	TextAlignmentOptionsU5BU5D_t3552942253* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	int32_t ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___itemStack_0)); }
	inline TextAlignmentOptionsU5BU5D_t3552942253* get_itemStack_0() const { return ___itemStack_0; }
	inline TextAlignmentOptionsU5BU5D_t3552942253** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(TextAlignmentOptionsU5BU5D_t3552942253* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___m_defaultItem_3)); }
	inline int32_t get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline int32_t* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(int32_t value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T3600445780_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef NAVIGATION_T3049316579_H
#define NAVIGATION_T3049316579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t3049316579 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3250028441 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnUp_1)); }
	inline Selectable_t3250028441 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t3250028441 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnDown_2)); }
	inline Selectable_t3250028441 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t3250028441 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnLeft_3)); }
	inline Selectable_t3250028441 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t3250028441 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnRight_4)); }
	inline Selectable_t3250028441 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t3250028441 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T3049316579_H
#ifndef FUNC_1_T3822001908_H
#define FUNC_1_T3822001908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`1<System.Boolean>
struct  Func_1_t3822001908  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_1_T3822001908_H
#ifndef WORDWRAPSTATE_T341939652_H
#define WORDWRAPSTATE_T341939652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.WordWrapState
struct  WordWrapState_t341939652 
{
public:
	// System.Int32 TMPro.WordWrapState::previous_WordBreak
	int32_t ___previous_WordBreak_0;
	// System.Int32 TMPro.WordWrapState::total_CharacterCount
	int32_t ___total_CharacterCount_1;
	// System.Int32 TMPro.WordWrapState::visible_CharacterCount
	int32_t ___visible_CharacterCount_2;
	// System.Int32 TMPro.WordWrapState::visible_SpriteCount
	int32_t ___visible_SpriteCount_3;
	// System.Int32 TMPro.WordWrapState::visible_LinkCount
	int32_t ___visible_LinkCount_4;
	// System.Int32 TMPro.WordWrapState::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.WordWrapState::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.WordWrapState::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.WordWrapState::lastVisibleCharIndex
	int32_t ___lastVisibleCharIndex_8;
	// System.Int32 TMPro.WordWrapState::lineNumber
	int32_t ___lineNumber_9;
	// System.Single TMPro.WordWrapState::maxCapHeight
	float ___maxCapHeight_10;
	// System.Single TMPro.WordWrapState::maxAscender
	float ___maxAscender_11;
	// System.Single TMPro.WordWrapState::maxDescender
	float ___maxDescender_12;
	// System.Single TMPro.WordWrapState::maxLineAscender
	float ___maxLineAscender_13;
	// System.Single TMPro.WordWrapState::maxLineDescender
	float ___maxLineDescender_14;
	// System.Single TMPro.WordWrapState::previousLineAscender
	float ___previousLineAscender_15;
	// System.Single TMPro.WordWrapState::xAdvance
	float ___xAdvance_16;
	// System.Single TMPro.WordWrapState::preferredWidth
	float ___preferredWidth_17;
	// System.Single TMPro.WordWrapState::preferredHeight
	float ___preferredHeight_18;
	// System.Single TMPro.WordWrapState::previousLineScale
	float ___previousLineScale_19;
	// System.Int32 TMPro.WordWrapState::wordCount
	int32_t ___wordCount_20;
	// TMPro.FontStyles TMPro.WordWrapState::fontStyle
	int32_t ___fontStyle_21;
	// System.Single TMPro.WordWrapState::fontScale
	float ___fontScale_22;
	// System.Single TMPro.WordWrapState::fontScaleMultiplier
	float ___fontScaleMultiplier_23;
	// System.Single TMPro.WordWrapState::currentFontSize
	float ___currentFontSize_24;
	// System.Single TMPro.WordWrapState::baselineOffset
	float ___baselineOffset_25;
	// System.Single TMPro.WordWrapState::lineOffset
	float ___lineOffset_26;
	// TMPro.TMP_TextInfo TMPro.WordWrapState::textInfo
	TMP_TextInfo_t3598145122 * ___textInfo_27;
	// TMPro.TMP_LineInfo TMPro.WordWrapState::lineInfo
	TMP_LineInfo_t1079631636  ___lineInfo_28;
	// UnityEngine.Color32 TMPro.WordWrapState::vertexColor
	Color32_t2600501292  ___vertexColor_29;
	// UnityEngine.Color32 TMPro.WordWrapState::underlineColor
	Color32_t2600501292  ___underlineColor_30;
	// UnityEngine.Color32 TMPro.WordWrapState::strikethroughColor
	Color32_t2600501292  ___strikethroughColor_31;
	// UnityEngine.Color32 TMPro.WordWrapState::highlightColor
	Color32_t2600501292  ___highlightColor_32;
	// TMPro.TMP_BasicXmlTagStack TMPro.WordWrapState::basicStyleStack
	TMP_BasicXmlTagStack_t2962628096  ___basicStyleStack_33;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::colorStack
	TMP_XmlTagStack_1_t2164155836  ___colorStack_34;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::underlineColorStack
	TMP_XmlTagStack_1_t2164155836  ___underlineColorStack_35;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::strikethroughColorStack
	TMP_XmlTagStack_1_t2164155836  ___strikethroughColorStack_36;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::highlightColorStack
	TMP_XmlTagStack_1_t2164155836  ___highlightColorStack_37;
	// TMPro.TMP_XmlTagStack`1<TMPro.TMP_ColorGradient> TMPro.WordWrapState::colorGradientStack
	TMP_XmlTagStack_1_t3241710312  ___colorGradientStack_38;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::sizeStack
	TMP_XmlTagStack_1_t960921318  ___sizeStack_39;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::indentStack
	TMP_XmlTagStack_1_t960921318  ___indentStack_40;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::fontWeightStack
	TMP_XmlTagStack_1_t2514600297  ___fontWeightStack_41;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::styleStack
	TMP_XmlTagStack_1_t2514600297  ___styleStack_42;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::baselineStack
	TMP_XmlTagStack_1_t960921318  ___baselineStack_43;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::actionStack
	TMP_XmlTagStack_1_t2514600297  ___actionStack_44;
	// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference> TMPro.WordWrapState::materialReferenceStack
	TMP_XmlTagStack_1_t1515999176  ___materialReferenceStack_45;
	// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions> TMPro.WordWrapState::lineJustificationStack
	TMP_XmlTagStack_1_t3600445780  ___lineJustificationStack_46;
	// System.Int32 TMPro.WordWrapState::spriteAnimationID
	int32_t ___spriteAnimationID_47;
	// TMPro.TMP_FontAsset TMPro.WordWrapState::currentFontAsset
	TMP_FontAsset_t364381626 * ___currentFontAsset_48;
	// TMPro.TMP_SpriteAsset TMPro.WordWrapState::currentSpriteAsset
	TMP_SpriteAsset_t484820633 * ___currentSpriteAsset_49;
	// UnityEngine.Material TMPro.WordWrapState::currentMaterial
	Material_t340375123 * ___currentMaterial_50;
	// System.Int32 TMPro.WordWrapState::currentMaterialIndex
	int32_t ___currentMaterialIndex_51;
	// TMPro.Extents TMPro.WordWrapState::meshExtents
	Extents_t3837212874  ___meshExtents_52;
	// System.Boolean TMPro.WordWrapState::tagNoParsing
	bool ___tagNoParsing_53;
	// System.Boolean TMPro.WordWrapState::isNonBreakingSpace
	bool ___isNonBreakingSpace_54;

public:
	inline static int32_t get_offset_of_previous_WordBreak_0() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___previous_WordBreak_0)); }
	inline int32_t get_previous_WordBreak_0() const { return ___previous_WordBreak_0; }
	inline int32_t* get_address_of_previous_WordBreak_0() { return &___previous_WordBreak_0; }
	inline void set_previous_WordBreak_0(int32_t value)
	{
		___previous_WordBreak_0 = value;
	}

	inline static int32_t get_offset_of_total_CharacterCount_1() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___total_CharacterCount_1)); }
	inline int32_t get_total_CharacterCount_1() const { return ___total_CharacterCount_1; }
	inline int32_t* get_address_of_total_CharacterCount_1() { return &___total_CharacterCount_1; }
	inline void set_total_CharacterCount_1(int32_t value)
	{
		___total_CharacterCount_1 = value;
	}

	inline static int32_t get_offset_of_visible_CharacterCount_2() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___visible_CharacterCount_2)); }
	inline int32_t get_visible_CharacterCount_2() const { return ___visible_CharacterCount_2; }
	inline int32_t* get_address_of_visible_CharacterCount_2() { return &___visible_CharacterCount_2; }
	inline void set_visible_CharacterCount_2(int32_t value)
	{
		___visible_CharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_visible_SpriteCount_3() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___visible_SpriteCount_3)); }
	inline int32_t get_visible_SpriteCount_3() const { return ___visible_SpriteCount_3; }
	inline int32_t* get_address_of_visible_SpriteCount_3() { return &___visible_SpriteCount_3; }
	inline void set_visible_SpriteCount_3(int32_t value)
	{
		___visible_SpriteCount_3 = value;
	}

	inline static int32_t get_offset_of_visible_LinkCount_4() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___visible_LinkCount_4)); }
	inline int32_t get_visible_LinkCount_4() const { return ___visible_LinkCount_4; }
	inline int32_t* get_address_of_visible_LinkCount_4() { return &___visible_LinkCount_4; }
	inline void set_visible_LinkCount_4(int32_t value)
	{
		___visible_LinkCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharIndex_8() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lastVisibleCharIndex_8)); }
	inline int32_t get_lastVisibleCharIndex_8() const { return ___lastVisibleCharIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharIndex_8() { return &___lastVisibleCharIndex_8; }
	inline void set_lastVisibleCharIndex_8(int32_t value)
	{
		___lastVisibleCharIndex_8 = value;
	}

	inline static int32_t get_offset_of_lineNumber_9() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineNumber_9)); }
	inline int32_t get_lineNumber_9() const { return ___lineNumber_9; }
	inline int32_t* get_address_of_lineNumber_9() { return &___lineNumber_9; }
	inline void set_lineNumber_9(int32_t value)
	{
		___lineNumber_9 = value;
	}

	inline static int32_t get_offset_of_maxCapHeight_10() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxCapHeight_10)); }
	inline float get_maxCapHeight_10() const { return ___maxCapHeight_10; }
	inline float* get_address_of_maxCapHeight_10() { return &___maxCapHeight_10; }
	inline void set_maxCapHeight_10(float value)
	{
		___maxCapHeight_10 = value;
	}

	inline static int32_t get_offset_of_maxAscender_11() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxAscender_11)); }
	inline float get_maxAscender_11() const { return ___maxAscender_11; }
	inline float* get_address_of_maxAscender_11() { return &___maxAscender_11; }
	inline void set_maxAscender_11(float value)
	{
		___maxAscender_11 = value;
	}

	inline static int32_t get_offset_of_maxDescender_12() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxDescender_12)); }
	inline float get_maxDescender_12() const { return ___maxDescender_12; }
	inline float* get_address_of_maxDescender_12() { return &___maxDescender_12; }
	inline void set_maxDescender_12(float value)
	{
		___maxDescender_12 = value;
	}

	inline static int32_t get_offset_of_maxLineAscender_13() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxLineAscender_13)); }
	inline float get_maxLineAscender_13() const { return ___maxLineAscender_13; }
	inline float* get_address_of_maxLineAscender_13() { return &___maxLineAscender_13; }
	inline void set_maxLineAscender_13(float value)
	{
		___maxLineAscender_13 = value;
	}

	inline static int32_t get_offset_of_maxLineDescender_14() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxLineDescender_14)); }
	inline float get_maxLineDescender_14() const { return ___maxLineDescender_14; }
	inline float* get_address_of_maxLineDescender_14() { return &___maxLineDescender_14; }
	inline void set_maxLineDescender_14(float value)
	{
		___maxLineDescender_14 = value;
	}

	inline static int32_t get_offset_of_previousLineAscender_15() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___previousLineAscender_15)); }
	inline float get_previousLineAscender_15() const { return ___previousLineAscender_15; }
	inline float* get_address_of_previousLineAscender_15() { return &___previousLineAscender_15; }
	inline void set_previousLineAscender_15(float value)
	{
		___previousLineAscender_15 = value;
	}

	inline static int32_t get_offset_of_xAdvance_16() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___xAdvance_16)); }
	inline float get_xAdvance_16() const { return ___xAdvance_16; }
	inline float* get_address_of_xAdvance_16() { return &___xAdvance_16; }
	inline void set_xAdvance_16(float value)
	{
		___xAdvance_16 = value;
	}

	inline static int32_t get_offset_of_preferredWidth_17() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___preferredWidth_17)); }
	inline float get_preferredWidth_17() const { return ___preferredWidth_17; }
	inline float* get_address_of_preferredWidth_17() { return &___preferredWidth_17; }
	inline void set_preferredWidth_17(float value)
	{
		___preferredWidth_17 = value;
	}

	inline static int32_t get_offset_of_preferredHeight_18() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___preferredHeight_18)); }
	inline float get_preferredHeight_18() const { return ___preferredHeight_18; }
	inline float* get_address_of_preferredHeight_18() { return &___preferredHeight_18; }
	inline void set_preferredHeight_18(float value)
	{
		___preferredHeight_18 = value;
	}

	inline static int32_t get_offset_of_previousLineScale_19() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___previousLineScale_19)); }
	inline float get_previousLineScale_19() const { return ___previousLineScale_19; }
	inline float* get_address_of_previousLineScale_19() { return &___previousLineScale_19; }
	inline void set_previousLineScale_19(float value)
	{
		___previousLineScale_19 = value;
	}

	inline static int32_t get_offset_of_wordCount_20() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___wordCount_20)); }
	inline int32_t get_wordCount_20() const { return ___wordCount_20; }
	inline int32_t* get_address_of_wordCount_20() { return &___wordCount_20; }
	inline void set_wordCount_20(int32_t value)
	{
		___wordCount_20 = value;
	}

	inline static int32_t get_offset_of_fontStyle_21() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontStyle_21)); }
	inline int32_t get_fontStyle_21() const { return ___fontStyle_21; }
	inline int32_t* get_address_of_fontStyle_21() { return &___fontStyle_21; }
	inline void set_fontStyle_21(int32_t value)
	{
		___fontStyle_21 = value;
	}

	inline static int32_t get_offset_of_fontScale_22() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontScale_22)); }
	inline float get_fontScale_22() const { return ___fontScale_22; }
	inline float* get_address_of_fontScale_22() { return &___fontScale_22; }
	inline void set_fontScale_22(float value)
	{
		___fontScale_22 = value;
	}

	inline static int32_t get_offset_of_fontScaleMultiplier_23() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontScaleMultiplier_23)); }
	inline float get_fontScaleMultiplier_23() const { return ___fontScaleMultiplier_23; }
	inline float* get_address_of_fontScaleMultiplier_23() { return &___fontScaleMultiplier_23; }
	inline void set_fontScaleMultiplier_23(float value)
	{
		___fontScaleMultiplier_23 = value;
	}

	inline static int32_t get_offset_of_currentFontSize_24() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentFontSize_24)); }
	inline float get_currentFontSize_24() const { return ___currentFontSize_24; }
	inline float* get_address_of_currentFontSize_24() { return &___currentFontSize_24; }
	inline void set_currentFontSize_24(float value)
	{
		___currentFontSize_24 = value;
	}

	inline static int32_t get_offset_of_baselineOffset_25() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___baselineOffset_25)); }
	inline float get_baselineOffset_25() const { return ___baselineOffset_25; }
	inline float* get_address_of_baselineOffset_25() { return &___baselineOffset_25; }
	inline void set_baselineOffset_25(float value)
	{
		___baselineOffset_25 = value;
	}

	inline static int32_t get_offset_of_lineOffset_26() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineOffset_26)); }
	inline float get_lineOffset_26() const { return ___lineOffset_26; }
	inline float* get_address_of_lineOffset_26() { return &___lineOffset_26; }
	inline void set_lineOffset_26(float value)
	{
		___lineOffset_26 = value;
	}

	inline static int32_t get_offset_of_textInfo_27() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___textInfo_27)); }
	inline TMP_TextInfo_t3598145122 * get_textInfo_27() const { return ___textInfo_27; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_textInfo_27() { return &___textInfo_27; }
	inline void set_textInfo_27(TMP_TextInfo_t3598145122 * value)
	{
		___textInfo_27 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_27), value);
	}

	inline static int32_t get_offset_of_lineInfo_28() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineInfo_28)); }
	inline TMP_LineInfo_t1079631636  get_lineInfo_28() const { return ___lineInfo_28; }
	inline TMP_LineInfo_t1079631636 * get_address_of_lineInfo_28() { return &___lineInfo_28; }
	inline void set_lineInfo_28(TMP_LineInfo_t1079631636  value)
	{
		___lineInfo_28 = value;
	}

	inline static int32_t get_offset_of_vertexColor_29() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___vertexColor_29)); }
	inline Color32_t2600501292  get_vertexColor_29() const { return ___vertexColor_29; }
	inline Color32_t2600501292 * get_address_of_vertexColor_29() { return &___vertexColor_29; }
	inline void set_vertexColor_29(Color32_t2600501292  value)
	{
		___vertexColor_29 = value;
	}

	inline static int32_t get_offset_of_underlineColor_30() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___underlineColor_30)); }
	inline Color32_t2600501292  get_underlineColor_30() const { return ___underlineColor_30; }
	inline Color32_t2600501292 * get_address_of_underlineColor_30() { return &___underlineColor_30; }
	inline void set_underlineColor_30(Color32_t2600501292  value)
	{
		___underlineColor_30 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_31() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___strikethroughColor_31)); }
	inline Color32_t2600501292  get_strikethroughColor_31() const { return ___strikethroughColor_31; }
	inline Color32_t2600501292 * get_address_of_strikethroughColor_31() { return &___strikethroughColor_31; }
	inline void set_strikethroughColor_31(Color32_t2600501292  value)
	{
		___strikethroughColor_31 = value;
	}

	inline static int32_t get_offset_of_highlightColor_32() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___highlightColor_32)); }
	inline Color32_t2600501292  get_highlightColor_32() const { return ___highlightColor_32; }
	inline Color32_t2600501292 * get_address_of_highlightColor_32() { return &___highlightColor_32; }
	inline void set_highlightColor_32(Color32_t2600501292  value)
	{
		___highlightColor_32 = value;
	}

	inline static int32_t get_offset_of_basicStyleStack_33() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___basicStyleStack_33)); }
	inline TMP_BasicXmlTagStack_t2962628096  get_basicStyleStack_33() const { return ___basicStyleStack_33; }
	inline TMP_BasicXmlTagStack_t2962628096 * get_address_of_basicStyleStack_33() { return &___basicStyleStack_33; }
	inline void set_basicStyleStack_33(TMP_BasicXmlTagStack_t2962628096  value)
	{
		___basicStyleStack_33 = value;
	}

	inline static int32_t get_offset_of_colorStack_34() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___colorStack_34)); }
	inline TMP_XmlTagStack_1_t2164155836  get_colorStack_34() const { return ___colorStack_34; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_colorStack_34() { return &___colorStack_34; }
	inline void set_colorStack_34(TMP_XmlTagStack_1_t2164155836  value)
	{
		___colorStack_34 = value;
	}

	inline static int32_t get_offset_of_underlineColorStack_35() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___underlineColorStack_35)); }
	inline TMP_XmlTagStack_1_t2164155836  get_underlineColorStack_35() const { return ___underlineColorStack_35; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_underlineColorStack_35() { return &___underlineColorStack_35; }
	inline void set_underlineColorStack_35(TMP_XmlTagStack_1_t2164155836  value)
	{
		___underlineColorStack_35 = value;
	}

	inline static int32_t get_offset_of_strikethroughColorStack_36() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___strikethroughColorStack_36)); }
	inline TMP_XmlTagStack_1_t2164155836  get_strikethroughColorStack_36() const { return ___strikethroughColorStack_36; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_strikethroughColorStack_36() { return &___strikethroughColorStack_36; }
	inline void set_strikethroughColorStack_36(TMP_XmlTagStack_1_t2164155836  value)
	{
		___strikethroughColorStack_36 = value;
	}

	inline static int32_t get_offset_of_highlightColorStack_37() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___highlightColorStack_37)); }
	inline TMP_XmlTagStack_1_t2164155836  get_highlightColorStack_37() const { return ___highlightColorStack_37; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_highlightColorStack_37() { return &___highlightColorStack_37; }
	inline void set_highlightColorStack_37(TMP_XmlTagStack_1_t2164155836  value)
	{
		___highlightColorStack_37 = value;
	}

	inline static int32_t get_offset_of_colorGradientStack_38() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___colorGradientStack_38)); }
	inline TMP_XmlTagStack_1_t3241710312  get_colorGradientStack_38() const { return ___colorGradientStack_38; }
	inline TMP_XmlTagStack_1_t3241710312 * get_address_of_colorGradientStack_38() { return &___colorGradientStack_38; }
	inline void set_colorGradientStack_38(TMP_XmlTagStack_1_t3241710312  value)
	{
		___colorGradientStack_38 = value;
	}

	inline static int32_t get_offset_of_sizeStack_39() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___sizeStack_39)); }
	inline TMP_XmlTagStack_1_t960921318  get_sizeStack_39() const { return ___sizeStack_39; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_sizeStack_39() { return &___sizeStack_39; }
	inline void set_sizeStack_39(TMP_XmlTagStack_1_t960921318  value)
	{
		___sizeStack_39 = value;
	}

	inline static int32_t get_offset_of_indentStack_40() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___indentStack_40)); }
	inline TMP_XmlTagStack_1_t960921318  get_indentStack_40() const { return ___indentStack_40; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_indentStack_40() { return &___indentStack_40; }
	inline void set_indentStack_40(TMP_XmlTagStack_1_t960921318  value)
	{
		___indentStack_40 = value;
	}

	inline static int32_t get_offset_of_fontWeightStack_41() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontWeightStack_41)); }
	inline TMP_XmlTagStack_1_t2514600297  get_fontWeightStack_41() const { return ___fontWeightStack_41; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_fontWeightStack_41() { return &___fontWeightStack_41; }
	inline void set_fontWeightStack_41(TMP_XmlTagStack_1_t2514600297  value)
	{
		___fontWeightStack_41 = value;
	}

	inline static int32_t get_offset_of_styleStack_42() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___styleStack_42)); }
	inline TMP_XmlTagStack_1_t2514600297  get_styleStack_42() const { return ___styleStack_42; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_styleStack_42() { return &___styleStack_42; }
	inline void set_styleStack_42(TMP_XmlTagStack_1_t2514600297  value)
	{
		___styleStack_42 = value;
	}

	inline static int32_t get_offset_of_baselineStack_43() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___baselineStack_43)); }
	inline TMP_XmlTagStack_1_t960921318  get_baselineStack_43() const { return ___baselineStack_43; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_baselineStack_43() { return &___baselineStack_43; }
	inline void set_baselineStack_43(TMP_XmlTagStack_1_t960921318  value)
	{
		___baselineStack_43 = value;
	}

	inline static int32_t get_offset_of_actionStack_44() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___actionStack_44)); }
	inline TMP_XmlTagStack_1_t2514600297  get_actionStack_44() const { return ___actionStack_44; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_actionStack_44() { return &___actionStack_44; }
	inline void set_actionStack_44(TMP_XmlTagStack_1_t2514600297  value)
	{
		___actionStack_44 = value;
	}

	inline static int32_t get_offset_of_materialReferenceStack_45() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___materialReferenceStack_45)); }
	inline TMP_XmlTagStack_1_t1515999176  get_materialReferenceStack_45() const { return ___materialReferenceStack_45; }
	inline TMP_XmlTagStack_1_t1515999176 * get_address_of_materialReferenceStack_45() { return &___materialReferenceStack_45; }
	inline void set_materialReferenceStack_45(TMP_XmlTagStack_1_t1515999176  value)
	{
		___materialReferenceStack_45 = value;
	}

	inline static int32_t get_offset_of_lineJustificationStack_46() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineJustificationStack_46)); }
	inline TMP_XmlTagStack_1_t3600445780  get_lineJustificationStack_46() const { return ___lineJustificationStack_46; }
	inline TMP_XmlTagStack_1_t3600445780 * get_address_of_lineJustificationStack_46() { return &___lineJustificationStack_46; }
	inline void set_lineJustificationStack_46(TMP_XmlTagStack_1_t3600445780  value)
	{
		___lineJustificationStack_46 = value;
	}

	inline static int32_t get_offset_of_spriteAnimationID_47() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___spriteAnimationID_47)); }
	inline int32_t get_spriteAnimationID_47() const { return ___spriteAnimationID_47; }
	inline int32_t* get_address_of_spriteAnimationID_47() { return &___spriteAnimationID_47; }
	inline void set_spriteAnimationID_47(int32_t value)
	{
		___spriteAnimationID_47 = value;
	}

	inline static int32_t get_offset_of_currentFontAsset_48() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentFontAsset_48)); }
	inline TMP_FontAsset_t364381626 * get_currentFontAsset_48() const { return ___currentFontAsset_48; }
	inline TMP_FontAsset_t364381626 ** get_address_of_currentFontAsset_48() { return &___currentFontAsset_48; }
	inline void set_currentFontAsset_48(TMP_FontAsset_t364381626 * value)
	{
		___currentFontAsset_48 = value;
		Il2CppCodeGenWriteBarrier((&___currentFontAsset_48), value);
	}

	inline static int32_t get_offset_of_currentSpriteAsset_49() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentSpriteAsset_49)); }
	inline TMP_SpriteAsset_t484820633 * get_currentSpriteAsset_49() const { return ___currentSpriteAsset_49; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_currentSpriteAsset_49() { return &___currentSpriteAsset_49; }
	inline void set_currentSpriteAsset_49(TMP_SpriteAsset_t484820633 * value)
	{
		___currentSpriteAsset_49 = value;
		Il2CppCodeGenWriteBarrier((&___currentSpriteAsset_49), value);
	}

	inline static int32_t get_offset_of_currentMaterial_50() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentMaterial_50)); }
	inline Material_t340375123 * get_currentMaterial_50() const { return ___currentMaterial_50; }
	inline Material_t340375123 ** get_address_of_currentMaterial_50() { return &___currentMaterial_50; }
	inline void set_currentMaterial_50(Material_t340375123 * value)
	{
		___currentMaterial_50 = value;
		Il2CppCodeGenWriteBarrier((&___currentMaterial_50), value);
	}

	inline static int32_t get_offset_of_currentMaterialIndex_51() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentMaterialIndex_51)); }
	inline int32_t get_currentMaterialIndex_51() const { return ___currentMaterialIndex_51; }
	inline int32_t* get_address_of_currentMaterialIndex_51() { return &___currentMaterialIndex_51; }
	inline void set_currentMaterialIndex_51(int32_t value)
	{
		___currentMaterialIndex_51 = value;
	}

	inline static int32_t get_offset_of_meshExtents_52() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___meshExtents_52)); }
	inline Extents_t3837212874  get_meshExtents_52() const { return ___meshExtents_52; }
	inline Extents_t3837212874 * get_address_of_meshExtents_52() { return &___meshExtents_52; }
	inline void set_meshExtents_52(Extents_t3837212874  value)
	{
		___meshExtents_52 = value;
	}

	inline static int32_t get_offset_of_tagNoParsing_53() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___tagNoParsing_53)); }
	inline bool get_tagNoParsing_53() const { return ___tagNoParsing_53; }
	inline bool* get_address_of_tagNoParsing_53() { return &___tagNoParsing_53; }
	inline void set_tagNoParsing_53(bool value)
	{
		___tagNoParsing_53 = value;
	}

	inline static int32_t get_offset_of_isNonBreakingSpace_54() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___isNonBreakingSpace_54)); }
	inline bool get_isNonBreakingSpace_54() const { return ___isNonBreakingSpace_54; }
	inline bool* get_address_of_isNonBreakingSpace_54() { return &___isNonBreakingSpace_54; }
	inline void set_isNonBreakingSpace_54(bool value)
	{
		___isNonBreakingSpace_54 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.WordWrapState
struct WordWrapState_t341939652_marshaled_pinvoke
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_t3598145122 * ___textInfo_27;
	TMP_LineInfo_t1079631636  ___lineInfo_28;
	Color32_t2600501292  ___vertexColor_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	TMP_BasicXmlTagStack_t2962628096  ___basicStyleStack_33;
	TMP_XmlTagStack_1_t2164155836  ___colorStack_34;
	TMP_XmlTagStack_1_t2164155836  ___underlineColorStack_35;
	TMP_XmlTagStack_1_t2164155836  ___strikethroughColorStack_36;
	TMP_XmlTagStack_1_t2164155836  ___highlightColorStack_37;
	TMP_XmlTagStack_1_t3241710312  ___colorGradientStack_38;
	TMP_XmlTagStack_1_t960921318  ___sizeStack_39;
	TMP_XmlTagStack_1_t960921318  ___indentStack_40;
	TMP_XmlTagStack_1_t2514600297  ___fontWeightStack_41;
	TMP_XmlTagStack_1_t2514600297  ___styleStack_42;
	TMP_XmlTagStack_1_t960921318  ___baselineStack_43;
	TMP_XmlTagStack_1_t2514600297  ___actionStack_44;
	TMP_XmlTagStack_1_t1515999176  ___materialReferenceStack_45;
	TMP_XmlTagStack_1_t3600445780  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t364381626 * ___currentFontAsset_48;
	TMP_SpriteAsset_t484820633 * ___currentSpriteAsset_49;
	Material_t340375123 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_t3837212874  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
// Native definition for COM marshalling of TMPro.WordWrapState
struct WordWrapState_t341939652_marshaled_com
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_t3598145122 * ___textInfo_27;
	TMP_LineInfo_t1079631636  ___lineInfo_28;
	Color32_t2600501292  ___vertexColor_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	TMP_BasicXmlTagStack_t2962628096  ___basicStyleStack_33;
	TMP_XmlTagStack_1_t2164155836  ___colorStack_34;
	TMP_XmlTagStack_1_t2164155836  ___underlineColorStack_35;
	TMP_XmlTagStack_1_t2164155836  ___strikethroughColorStack_36;
	TMP_XmlTagStack_1_t2164155836  ___highlightColorStack_37;
	TMP_XmlTagStack_1_t3241710312  ___colorGradientStack_38;
	TMP_XmlTagStack_1_t960921318  ___sizeStack_39;
	TMP_XmlTagStack_1_t960921318  ___indentStack_40;
	TMP_XmlTagStack_1_t2514600297  ___fontWeightStack_41;
	TMP_XmlTagStack_1_t2514600297  ___styleStack_42;
	TMP_XmlTagStack_1_t960921318  ___baselineStack_43;
	TMP_XmlTagStack_1_t2514600297  ___actionStack_44;
	TMP_XmlTagStack_1_t1515999176  ___materialReferenceStack_45;
	TMP_XmlTagStack_1_t3600445780  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t364381626 * ___currentFontAsset_48;
	TMP_SpriteAsset_t484820633 * ___currentSpriteAsset_49;
	Material_t340375123 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_t3837212874  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
#endif // WORDWRAPSTATE_T341939652_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef RENDERER_T2627027031_H
#define RENDERER_T2627027031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t2627027031  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T2627027031_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:
	// System.Int32 UnityEngine.Transform::<hierarchyCount>k__BackingField
	int32_t ___U3ChierarchyCountU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3ChierarchyCountU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Transform_t3600365921, ___U3ChierarchyCountU3Ek__BackingField_4)); }
	inline int32_t get_U3ChierarchyCountU3Ek__BackingField_4() const { return ___U3ChierarchyCountU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3ChierarchyCountU3Ek__BackingField_4() { return &___U3ChierarchyCountU3Ek__BackingField_4; }
	inline void set_U3ChierarchyCountU3Ek__BackingField_4(int32_t value)
	{
		___U3ChierarchyCountU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef ANIMATOR_T434523843_H
#define ANIMATOR_T434523843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animator
struct  Animator_t434523843  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATOR_T434523843_H
#ifndef AUDIOBEHAVIOUR_T2879336574_H
#define AUDIOBEHAVIOUR_T2879336574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioBehaviour
struct  AudioBehaviour_t2879336574  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOBEHAVIOUR_T2879336574_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef SPRITERENDERER_T3235626157_H
#define SPRITERENDERER_T3235626157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SpriteRenderer
struct  SpriteRenderer_t3235626157  : public Renderer_t2627027031
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITERENDERER_T3235626157_H
#ifndef ANIM_T271495542_H
#define ANIM_T271495542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Anim
struct  Anim_t271495542  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject Anim::quad1
	GameObject_t1113636619 * ___quad1_4;
	// UnityEngine.GameObject Anim::quad2
	GameObject_t1113636619 * ___quad2_5;
	// UnityEngine.GameObject Anim::dog
	GameObject_t1113636619 * ___dog_6;
	// UnityEngine.GameObject Anim::guy
	GameObject_t1113636619 * ___guy_7;
	// System.Boolean Anim::isDone
	bool ___isDone_8;
	// System.Int32 Anim::i
	int32_t ___i_9;
	// System.Single Anim::speed
	float ___speed_10;
	// System.Single Anim::startTime
	float ___startTime_11;
	// System.Single Anim::journeyLength
	float ___journeyLength_12;
	// System.Single Anim::journeyLength2
	float ___journeyLength2_13;
	// System.Single Anim::journeyLength3
	float ___journeyLength3_14;
	// System.Single Anim::journeyLength4
	float ___journeyLength4_15;

public:
	inline static int32_t get_offset_of_quad1_4() { return static_cast<int32_t>(offsetof(Anim_t271495542, ___quad1_4)); }
	inline GameObject_t1113636619 * get_quad1_4() const { return ___quad1_4; }
	inline GameObject_t1113636619 ** get_address_of_quad1_4() { return &___quad1_4; }
	inline void set_quad1_4(GameObject_t1113636619 * value)
	{
		___quad1_4 = value;
		Il2CppCodeGenWriteBarrier((&___quad1_4), value);
	}

	inline static int32_t get_offset_of_quad2_5() { return static_cast<int32_t>(offsetof(Anim_t271495542, ___quad2_5)); }
	inline GameObject_t1113636619 * get_quad2_5() const { return ___quad2_5; }
	inline GameObject_t1113636619 ** get_address_of_quad2_5() { return &___quad2_5; }
	inline void set_quad2_5(GameObject_t1113636619 * value)
	{
		___quad2_5 = value;
		Il2CppCodeGenWriteBarrier((&___quad2_5), value);
	}

	inline static int32_t get_offset_of_dog_6() { return static_cast<int32_t>(offsetof(Anim_t271495542, ___dog_6)); }
	inline GameObject_t1113636619 * get_dog_6() const { return ___dog_6; }
	inline GameObject_t1113636619 ** get_address_of_dog_6() { return &___dog_6; }
	inline void set_dog_6(GameObject_t1113636619 * value)
	{
		___dog_6 = value;
		Il2CppCodeGenWriteBarrier((&___dog_6), value);
	}

	inline static int32_t get_offset_of_guy_7() { return static_cast<int32_t>(offsetof(Anim_t271495542, ___guy_7)); }
	inline GameObject_t1113636619 * get_guy_7() const { return ___guy_7; }
	inline GameObject_t1113636619 ** get_address_of_guy_7() { return &___guy_7; }
	inline void set_guy_7(GameObject_t1113636619 * value)
	{
		___guy_7 = value;
		Il2CppCodeGenWriteBarrier((&___guy_7), value);
	}

	inline static int32_t get_offset_of_isDone_8() { return static_cast<int32_t>(offsetof(Anim_t271495542, ___isDone_8)); }
	inline bool get_isDone_8() const { return ___isDone_8; }
	inline bool* get_address_of_isDone_8() { return &___isDone_8; }
	inline void set_isDone_8(bool value)
	{
		___isDone_8 = value;
	}

	inline static int32_t get_offset_of_i_9() { return static_cast<int32_t>(offsetof(Anim_t271495542, ___i_9)); }
	inline int32_t get_i_9() const { return ___i_9; }
	inline int32_t* get_address_of_i_9() { return &___i_9; }
	inline void set_i_9(int32_t value)
	{
		___i_9 = value;
	}

	inline static int32_t get_offset_of_speed_10() { return static_cast<int32_t>(offsetof(Anim_t271495542, ___speed_10)); }
	inline float get_speed_10() const { return ___speed_10; }
	inline float* get_address_of_speed_10() { return &___speed_10; }
	inline void set_speed_10(float value)
	{
		___speed_10 = value;
	}

	inline static int32_t get_offset_of_startTime_11() { return static_cast<int32_t>(offsetof(Anim_t271495542, ___startTime_11)); }
	inline float get_startTime_11() const { return ___startTime_11; }
	inline float* get_address_of_startTime_11() { return &___startTime_11; }
	inline void set_startTime_11(float value)
	{
		___startTime_11 = value;
	}

	inline static int32_t get_offset_of_journeyLength_12() { return static_cast<int32_t>(offsetof(Anim_t271495542, ___journeyLength_12)); }
	inline float get_journeyLength_12() const { return ___journeyLength_12; }
	inline float* get_address_of_journeyLength_12() { return &___journeyLength_12; }
	inline void set_journeyLength_12(float value)
	{
		___journeyLength_12 = value;
	}

	inline static int32_t get_offset_of_journeyLength2_13() { return static_cast<int32_t>(offsetof(Anim_t271495542, ___journeyLength2_13)); }
	inline float get_journeyLength2_13() const { return ___journeyLength2_13; }
	inline float* get_address_of_journeyLength2_13() { return &___journeyLength2_13; }
	inline void set_journeyLength2_13(float value)
	{
		___journeyLength2_13 = value;
	}

	inline static int32_t get_offset_of_journeyLength3_14() { return static_cast<int32_t>(offsetof(Anim_t271495542, ___journeyLength3_14)); }
	inline float get_journeyLength3_14() const { return ___journeyLength3_14; }
	inline float* get_address_of_journeyLength3_14() { return &___journeyLength3_14; }
	inline void set_journeyLength3_14(float value)
	{
		___journeyLength3_14 = value;
	}

	inline static int32_t get_offset_of_journeyLength4_15() { return static_cast<int32_t>(offsetof(Anim_t271495542, ___journeyLength4_15)); }
	inline float get_journeyLength4_15() const { return ___journeyLength4_15; }
	inline float* get_address_of_journeyLength4_15() { return &___journeyLength4_15; }
	inline void set_journeyLength4_15(float value)
	{
		___journeyLength4_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIM_T271495542_H
#ifndef ANIMIMP_T3408691587_H
#define ANIMIMP_T3408691587_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Animimp
struct  Animimp_t3408691587  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject Animimp::quad1
	GameObject_t1113636619 * ___quad1_4;
	// UnityEngine.GameObject Animimp::quad2
	GameObject_t1113636619 * ___quad2_5;
	// UnityEngine.GameObject Animimp::dog
	GameObject_t1113636619 * ___dog_6;
	// UnityEngine.GameObject Animimp::guy
	GameObject_t1113636619 * ___guy_7;
	// System.Boolean Animimp::isDone
	bool ___isDone_8;
	// System.Int32 Animimp::i
	int32_t ___i_9;
	// System.Single Animimp::speed
	float ___speed_10;
	// System.Single Animimp::startTime
	float ___startTime_11;
	// System.Single Animimp::journeyLength
	float ___journeyLength_12;
	// System.Single Animimp::journeyLength2
	float ___journeyLength2_13;
	// System.Single Animimp::journeyLength3
	float ___journeyLength3_14;
	// System.Single Animimp::journeyLength4
	float ___journeyLength4_15;

public:
	inline static int32_t get_offset_of_quad1_4() { return static_cast<int32_t>(offsetof(Animimp_t3408691587, ___quad1_4)); }
	inline GameObject_t1113636619 * get_quad1_4() const { return ___quad1_4; }
	inline GameObject_t1113636619 ** get_address_of_quad1_4() { return &___quad1_4; }
	inline void set_quad1_4(GameObject_t1113636619 * value)
	{
		___quad1_4 = value;
		Il2CppCodeGenWriteBarrier((&___quad1_4), value);
	}

	inline static int32_t get_offset_of_quad2_5() { return static_cast<int32_t>(offsetof(Animimp_t3408691587, ___quad2_5)); }
	inline GameObject_t1113636619 * get_quad2_5() const { return ___quad2_5; }
	inline GameObject_t1113636619 ** get_address_of_quad2_5() { return &___quad2_5; }
	inline void set_quad2_5(GameObject_t1113636619 * value)
	{
		___quad2_5 = value;
		Il2CppCodeGenWriteBarrier((&___quad2_5), value);
	}

	inline static int32_t get_offset_of_dog_6() { return static_cast<int32_t>(offsetof(Animimp_t3408691587, ___dog_6)); }
	inline GameObject_t1113636619 * get_dog_6() const { return ___dog_6; }
	inline GameObject_t1113636619 ** get_address_of_dog_6() { return &___dog_6; }
	inline void set_dog_6(GameObject_t1113636619 * value)
	{
		___dog_6 = value;
		Il2CppCodeGenWriteBarrier((&___dog_6), value);
	}

	inline static int32_t get_offset_of_guy_7() { return static_cast<int32_t>(offsetof(Animimp_t3408691587, ___guy_7)); }
	inline GameObject_t1113636619 * get_guy_7() const { return ___guy_7; }
	inline GameObject_t1113636619 ** get_address_of_guy_7() { return &___guy_7; }
	inline void set_guy_7(GameObject_t1113636619 * value)
	{
		___guy_7 = value;
		Il2CppCodeGenWriteBarrier((&___guy_7), value);
	}

	inline static int32_t get_offset_of_isDone_8() { return static_cast<int32_t>(offsetof(Animimp_t3408691587, ___isDone_8)); }
	inline bool get_isDone_8() const { return ___isDone_8; }
	inline bool* get_address_of_isDone_8() { return &___isDone_8; }
	inline void set_isDone_8(bool value)
	{
		___isDone_8 = value;
	}

	inline static int32_t get_offset_of_i_9() { return static_cast<int32_t>(offsetof(Animimp_t3408691587, ___i_9)); }
	inline int32_t get_i_9() const { return ___i_9; }
	inline int32_t* get_address_of_i_9() { return &___i_9; }
	inline void set_i_9(int32_t value)
	{
		___i_9 = value;
	}

	inline static int32_t get_offset_of_speed_10() { return static_cast<int32_t>(offsetof(Animimp_t3408691587, ___speed_10)); }
	inline float get_speed_10() const { return ___speed_10; }
	inline float* get_address_of_speed_10() { return &___speed_10; }
	inline void set_speed_10(float value)
	{
		___speed_10 = value;
	}

	inline static int32_t get_offset_of_startTime_11() { return static_cast<int32_t>(offsetof(Animimp_t3408691587, ___startTime_11)); }
	inline float get_startTime_11() const { return ___startTime_11; }
	inline float* get_address_of_startTime_11() { return &___startTime_11; }
	inline void set_startTime_11(float value)
	{
		___startTime_11 = value;
	}

	inline static int32_t get_offset_of_journeyLength_12() { return static_cast<int32_t>(offsetof(Animimp_t3408691587, ___journeyLength_12)); }
	inline float get_journeyLength_12() const { return ___journeyLength_12; }
	inline float* get_address_of_journeyLength_12() { return &___journeyLength_12; }
	inline void set_journeyLength_12(float value)
	{
		___journeyLength_12 = value;
	}

	inline static int32_t get_offset_of_journeyLength2_13() { return static_cast<int32_t>(offsetof(Animimp_t3408691587, ___journeyLength2_13)); }
	inline float get_journeyLength2_13() const { return ___journeyLength2_13; }
	inline float* get_address_of_journeyLength2_13() { return &___journeyLength2_13; }
	inline void set_journeyLength2_13(float value)
	{
		___journeyLength2_13 = value;
	}

	inline static int32_t get_offset_of_journeyLength3_14() { return static_cast<int32_t>(offsetof(Animimp_t3408691587, ___journeyLength3_14)); }
	inline float get_journeyLength3_14() const { return ___journeyLength3_14; }
	inline float* get_address_of_journeyLength3_14() { return &___journeyLength3_14; }
	inline void set_journeyLength3_14(float value)
	{
		___journeyLength3_14 = value;
	}

	inline static int32_t get_offset_of_journeyLength4_15() { return static_cast<int32_t>(offsetof(Animimp_t3408691587, ___journeyLength4_15)); }
	inline float get_journeyLength4_15() const { return ___journeyLength4_15; }
	inline float* get_address_of_journeyLength4_15() { return &___journeyLength4_15; }
	inline void set_journeyLength4_15(float value)
	{
		___journeyLength4_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMIMP_T3408691587_H
#ifndef ANIMINF_T989049961_H
#define ANIMINF_T989049961_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Animinf
struct  Animinf_t989049961  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject Animinf::quad1
	GameObject_t1113636619 * ___quad1_4;
	// UnityEngine.GameObject Animinf::quad2
	GameObject_t1113636619 * ___quad2_5;
	// UnityEngine.GameObject Animinf::dog
	GameObject_t1113636619 * ___dog_6;
	// UnityEngine.GameObject Animinf::guy
	GameObject_t1113636619 * ___guy_7;
	// System.Boolean Animinf::isDone
	bool ___isDone_8;
	// System.Int32 Animinf::i
	int32_t ___i_9;
	// System.Single Animinf::speed
	float ___speed_10;
	// System.Single Animinf::startTime
	float ___startTime_11;
	// System.Single Animinf::journeyLength
	float ___journeyLength_12;
	// System.Single Animinf::journeyLength2
	float ___journeyLength2_13;
	// System.Single Animinf::journeyLength3
	float ___journeyLength3_14;
	// System.Single Animinf::journeyLength4
	float ___journeyLength4_15;

public:
	inline static int32_t get_offset_of_quad1_4() { return static_cast<int32_t>(offsetof(Animinf_t989049961, ___quad1_4)); }
	inline GameObject_t1113636619 * get_quad1_4() const { return ___quad1_4; }
	inline GameObject_t1113636619 ** get_address_of_quad1_4() { return &___quad1_4; }
	inline void set_quad1_4(GameObject_t1113636619 * value)
	{
		___quad1_4 = value;
		Il2CppCodeGenWriteBarrier((&___quad1_4), value);
	}

	inline static int32_t get_offset_of_quad2_5() { return static_cast<int32_t>(offsetof(Animinf_t989049961, ___quad2_5)); }
	inline GameObject_t1113636619 * get_quad2_5() const { return ___quad2_5; }
	inline GameObject_t1113636619 ** get_address_of_quad2_5() { return &___quad2_5; }
	inline void set_quad2_5(GameObject_t1113636619 * value)
	{
		___quad2_5 = value;
		Il2CppCodeGenWriteBarrier((&___quad2_5), value);
	}

	inline static int32_t get_offset_of_dog_6() { return static_cast<int32_t>(offsetof(Animinf_t989049961, ___dog_6)); }
	inline GameObject_t1113636619 * get_dog_6() const { return ___dog_6; }
	inline GameObject_t1113636619 ** get_address_of_dog_6() { return &___dog_6; }
	inline void set_dog_6(GameObject_t1113636619 * value)
	{
		___dog_6 = value;
		Il2CppCodeGenWriteBarrier((&___dog_6), value);
	}

	inline static int32_t get_offset_of_guy_7() { return static_cast<int32_t>(offsetof(Animinf_t989049961, ___guy_7)); }
	inline GameObject_t1113636619 * get_guy_7() const { return ___guy_7; }
	inline GameObject_t1113636619 ** get_address_of_guy_7() { return &___guy_7; }
	inline void set_guy_7(GameObject_t1113636619 * value)
	{
		___guy_7 = value;
		Il2CppCodeGenWriteBarrier((&___guy_7), value);
	}

	inline static int32_t get_offset_of_isDone_8() { return static_cast<int32_t>(offsetof(Animinf_t989049961, ___isDone_8)); }
	inline bool get_isDone_8() const { return ___isDone_8; }
	inline bool* get_address_of_isDone_8() { return &___isDone_8; }
	inline void set_isDone_8(bool value)
	{
		___isDone_8 = value;
	}

	inline static int32_t get_offset_of_i_9() { return static_cast<int32_t>(offsetof(Animinf_t989049961, ___i_9)); }
	inline int32_t get_i_9() const { return ___i_9; }
	inline int32_t* get_address_of_i_9() { return &___i_9; }
	inline void set_i_9(int32_t value)
	{
		___i_9 = value;
	}

	inline static int32_t get_offset_of_speed_10() { return static_cast<int32_t>(offsetof(Animinf_t989049961, ___speed_10)); }
	inline float get_speed_10() const { return ___speed_10; }
	inline float* get_address_of_speed_10() { return &___speed_10; }
	inline void set_speed_10(float value)
	{
		___speed_10 = value;
	}

	inline static int32_t get_offset_of_startTime_11() { return static_cast<int32_t>(offsetof(Animinf_t989049961, ___startTime_11)); }
	inline float get_startTime_11() const { return ___startTime_11; }
	inline float* get_address_of_startTime_11() { return &___startTime_11; }
	inline void set_startTime_11(float value)
	{
		___startTime_11 = value;
	}

	inline static int32_t get_offset_of_journeyLength_12() { return static_cast<int32_t>(offsetof(Animinf_t989049961, ___journeyLength_12)); }
	inline float get_journeyLength_12() const { return ___journeyLength_12; }
	inline float* get_address_of_journeyLength_12() { return &___journeyLength_12; }
	inline void set_journeyLength_12(float value)
	{
		___journeyLength_12 = value;
	}

	inline static int32_t get_offset_of_journeyLength2_13() { return static_cast<int32_t>(offsetof(Animinf_t989049961, ___journeyLength2_13)); }
	inline float get_journeyLength2_13() const { return ___journeyLength2_13; }
	inline float* get_address_of_journeyLength2_13() { return &___journeyLength2_13; }
	inline void set_journeyLength2_13(float value)
	{
		___journeyLength2_13 = value;
	}

	inline static int32_t get_offset_of_journeyLength3_14() { return static_cast<int32_t>(offsetof(Animinf_t989049961, ___journeyLength3_14)); }
	inline float get_journeyLength3_14() const { return ___journeyLength3_14; }
	inline float* get_address_of_journeyLength3_14() { return &___journeyLength3_14; }
	inline void set_journeyLength3_14(float value)
	{
		___journeyLength3_14 = value;
	}

	inline static int32_t get_offset_of_journeyLength4_15() { return static_cast<int32_t>(offsetof(Animinf_t989049961, ___journeyLength4_15)); }
	inline float get_journeyLength4_15() const { return ___journeyLength4_15; }
	inline float* get_address_of_journeyLength4_15() { return &___journeyLength4_15; }
	inline void set_journeyLength4_15(float value)
	{
		___journeyLength4_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMINF_T989049961_H
#ifndef BATTLE_T2191861408_H
#define BATTLE_T2191861408_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Battle
struct  Battle_t2191861408  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Slider Battle::slider
	Slider_t3903728902 * ___slider_4;
	// UnityEngine.UI.Slider Battle::guySlider
	Slider_t3903728902 * ___guySlider_5;
	// TMPro.TMP_Text Battle::attackText
	TMP_Text_t2599618874 * ___attackText_6;
	// TMPro.TMP_Text Battle::dogHealth
	TMP_Text_t2599618874 * ___dogHealth_7;
	// TMPro.TMP_Text Battle::guyHealth
	TMP_Text_t2599618874 * ___guyHealth_8;
	// UnityEngine.UI.Image Battle::black
	Image_t2670269651 * ___black_9;
	// UnityEngine.Animator Battle::animator
	Animator_t434523843 * ___animator_10;
	// UnityEngine.AudioSource Battle::DogDieSFX
	AudioSource_t3935305588 * ___DogDieSFX_11;
	// UnityEngine.AudioSource Battle::GuydDieSFX
	AudioSource_t3935305588 * ___GuydDieSFX_12;
	// UnityEngine.GameObject Battle::GUI
	GameObject_t1113636619 * ___GUI_13;
	// UnityEngine.GameObject Battle::GameOver
	GameObject_t1113636619 * ___GameOver_14;
	// Anim Battle::anim
	Anim_t271495542 * ___anim_15;
	// System.Random Battle::rnd
	Random_t108471755 * ___rnd_16;
	// System.Boolean Battle::myTurn
	bool ___myTurn_17;
	// System.Boolean Battle::ableToAttack
	bool ___ableToAttack_18;
	// System.Boolean Battle::enemyAbleToAttack
	bool ___enemyAbleToAttack_19;
	// System.Boolean Battle::isReady
	bool ___isReady_20;
	// System.Boolean Battle::isDone
	bool ___isDone_21;
	// System.Boolean Battle::isDead
	bool ___isDead_22;
	// System.Single Battle::<CurrentHealth>k__BackingField
	float ___U3CCurrentHealthU3Ek__BackingField_23;
	// System.Single Battle::<MaxHealth>k__BackingField
	float ___U3CMaxHealthU3Ek__BackingField_24;
	// System.Single Battle::<CurrentHealthGuy>k__BackingField
	float ___U3CCurrentHealthGuyU3Ek__BackingField_25;
	// System.Single Battle::<MaxHealthGuy>k__BackingField
	float ___U3CMaxHealthGuyU3Ek__BackingField_26;

public:
	inline static int32_t get_offset_of_slider_4() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___slider_4)); }
	inline Slider_t3903728902 * get_slider_4() const { return ___slider_4; }
	inline Slider_t3903728902 ** get_address_of_slider_4() { return &___slider_4; }
	inline void set_slider_4(Slider_t3903728902 * value)
	{
		___slider_4 = value;
		Il2CppCodeGenWriteBarrier((&___slider_4), value);
	}

	inline static int32_t get_offset_of_guySlider_5() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___guySlider_5)); }
	inline Slider_t3903728902 * get_guySlider_5() const { return ___guySlider_5; }
	inline Slider_t3903728902 ** get_address_of_guySlider_5() { return &___guySlider_5; }
	inline void set_guySlider_5(Slider_t3903728902 * value)
	{
		___guySlider_5 = value;
		Il2CppCodeGenWriteBarrier((&___guySlider_5), value);
	}

	inline static int32_t get_offset_of_attackText_6() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___attackText_6)); }
	inline TMP_Text_t2599618874 * get_attackText_6() const { return ___attackText_6; }
	inline TMP_Text_t2599618874 ** get_address_of_attackText_6() { return &___attackText_6; }
	inline void set_attackText_6(TMP_Text_t2599618874 * value)
	{
		___attackText_6 = value;
		Il2CppCodeGenWriteBarrier((&___attackText_6), value);
	}

	inline static int32_t get_offset_of_dogHealth_7() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___dogHealth_7)); }
	inline TMP_Text_t2599618874 * get_dogHealth_7() const { return ___dogHealth_7; }
	inline TMP_Text_t2599618874 ** get_address_of_dogHealth_7() { return &___dogHealth_7; }
	inline void set_dogHealth_7(TMP_Text_t2599618874 * value)
	{
		___dogHealth_7 = value;
		Il2CppCodeGenWriteBarrier((&___dogHealth_7), value);
	}

	inline static int32_t get_offset_of_guyHealth_8() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___guyHealth_8)); }
	inline TMP_Text_t2599618874 * get_guyHealth_8() const { return ___guyHealth_8; }
	inline TMP_Text_t2599618874 ** get_address_of_guyHealth_8() { return &___guyHealth_8; }
	inline void set_guyHealth_8(TMP_Text_t2599618874 * value)
	{
		___guyHealth_8 = value;
		Il2CppCodeGenWriteBarrier((&___guyHealth_8), value);
	}

	inline static int32_t get_offset_of_black_9() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___black_9)); }
	inline Image_t2670269651 * get_black_9() const { return ___black_9; }
	inline Image_t2670269651 ** get_address_of_black_9() { return &___black_9; }
	inline void set_black_9(Image_t2670269651 * value)
	{
		___black_9 = value;
		Il2CppCodeGenWriteBarrier((&___black_9), value);
	}

	inline static int32_t get_offset_of_animator_10() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___animator_10)); }
	inline Animator_t434523843 * get_animator_10() const { return ___animator_10; }
	inline Animator_t434523843 ** get_address_of_animator_10() { return &___animator_10; }
	inline void set_animator_10(Animator_t434523843 * value)
	{
		___animator_10 = value;
		Il2CppCodeGenWriteBarrier((&___animator_10), value);
	}

	inline static int32_t get_offset_of_DogDieSFX_11() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___DogDieSFX_11)); }
	inline AudioSource_t3935305588 * get_DogDieSFX_11() const { return ___DogDieSFX_11; }
	inline AudioSource_t3935305588 ** get_address_of_DogDieSFX_11() { return &___DogDieSFX_11; }
	inline void set_DogDieSFX_11(AudioSource_t3935305588 * value)
	{
		___DogDieSFX_11 = value;
		Il2CppCodeGenWriteBarrier((&___DogDieSFX_11), value);
	}

	inline static int32_t get_offset_of_GuydDieSFX_12() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___GuydDieSFX_12)); }
	inline AudioSource_t3935305588 * get_GuydDieSFX_12() const { return ___GuydDieSFX_12; }
	inline AudioSource_t3935305588 ** get_address_of_GuydDieSFX_12() { return &___GuydDieSFX_12; }
	inline void set_GuydDieSFX_12(AudioSource_t3935305588 * value)
	{
		___GuydDieSFX_12 = value;
		Il2CppCodeGenWriteBarrier((&___GuydDieSFX_12), value);
	}

	inline static int32_t get_offset_of_GUI_13() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___GUI_13)); }
	inline GameObject_t1113636619 * get_GUI_13() const { return ___GUI_13; }
	inline GameObject_t1113636619 ** get_address_of_GUI_13() { return &___GUI_13; }
	inline void set_GUI_13(GameObject_t1113636619 * value)
	{
		___GUI_13 = value;
		Il2CppCodeGenWriteBarrier((&___GUI_13), value);
	}

	inline static int32_t get_offset_of_GameOver_14() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___GameOver_14)); }
	inline GameObject_t1113636619 * get_GameOver_14() const { return ___GameOver_14; }
	inline GameObject_t1113636619 ** get_address_of_GameOver_14() { return &___GameOver_14; }
	inline void set_GameOver_14(GameObject_t1113636619 * value)
	{
		___GameOver_14 = value;
		Il2CppCodeGenWriteBarrier((&___GameOver_14), value);
	}

	inline static int32_t get_offset_of_anim_15() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___anim_15)); }
	inline Anim_t271495542 * get_anim_15() const { return ___anim_15; }
	inline Anim_t271495542 ** get_address_of_anim_15() { return &___anim_15; }
	inline void set_anim_15(Anim_t271495542 * value)
	{
		___anim_15 = value;
		Il2CppCodeGenWriteBarrier((&___anim_15), value);
	}

	inline static int32_t get_offset_of_rnd_16() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___rnd_16)); }
	inline Random_t108471755 * get_rnd_16() const { return ___rnd_16; }
	inline Random_t108471755 ** get_address_of_rnd_16() { return &___rnd_16; }
	inline void set_rnd_16(Random_t108471755 * value)
	{
		___rnd_16 = value;
		Il2CppCodeGenWriteBarrier((&___rnd_16), value);
	}

	inline static int32_t get_offset_of_myTurn_17() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___myTurn_17)); }
	inline bool get_myTurn_17() const { return ___myTurn_17; }
	inline bool* get_address_of_myTurn_17() { return &___myTurn_17; }
	inline void set_myTurn_17(bool value)
	{
		___myTurn_17 = value;
	}

	inline static int32_t get_offset_of_ableToAttack_18() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___ableToAttack_18)); }
	inline bool get_ableToAttack_18() const { return ___ableToAttack_18; }
	inline bool* get_address_of_ableToAttack_18() { return &___ableToAttack_18; }
	inline void set_ableToAttack_18(bool value)
	{
		___ableToAttack_18 = value;
	}

	inline static int32_t get_offset_of_enemyAbleToAttack_19() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___enemyAbleToAttack_19)); }
	inline bool get_enemyAbleToAttack_19() const { return ___enemyAbleToAttack_19; }
	inline bool* get_address_of_enemyAbleToAttack_19() { return &___enemyAbleToAttack_19; }
	inline void set_enemyAbleToAttack_19(bool value)
	{
		___enemyAbleToAttack_19 = value;
	}

	inline static int32_t get_offset_of_isReady_20() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___isReady_20)); }
	inline bool get_isReady_20() const { return ___isReady_20; }
	inline bool* get_address_of_isReady_20() { return &___isReady_20; }
	inline void set_isReady_20(bool value)
	{
		___isReady_20 = value;
	}

	inline static int32_t get_offset_of_isDone_21() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___isDone_21)); }
	inline bool get_isDone_21() const { return ___isDone_21; }
	inline bool* get_address_of_isDone_21() { return &___isDone_21; }
	inline void set_isDone_21(bool value)
	{
		___isDone_21 = value;
	}

	inline static int32_t get_offset_of_isDead_22() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___isDead_22)); }
	inline bool get_isDead_22() const { return ___isDead_22; }
	inline bool* get_address_of_isDead_22() { return &___isDead_22; }
	inline void set_isDead_22(bool value)
	{
		___isDead_22 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentHealthU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___U3CCurrentHealthU3Ek__BackingField_23)); }
	inline float get_U3CCurrentHealthU3Ek__BackingField_23() const { return ___U3CCurrentHealthU3Ek__BackingField_23; }
	inline float* get_address_of_U3CCurrentHealthU3Ek__BackingField_23() { return &___U3CCurrentHealthU3Ek__BackingField_23; }
	inline void set_U3CCurrentHealthU3Ek__BackingField_23(float value)
	{
		___U3CCurrentHealthU3Ek__BackingField_23 = value;
	}

	inline static int32_t get_offset_of_U3CMaxHealthU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___U3CMaxHealthU3Ek__BackingField_24)); }
	inline float get_U3CMaxHealthU3Ek__BackingField_24() const { return ___U3CMaxHealthU3Ek__BackingField_24; }
	inline float* get_address_of_U3CMaxHealthU3Ek__BackingField_24() { return &___U3CMaxHealthU3Ek__BackingField_24; }
	inline void set_U3CMaxHealthU3Ek__BackingField_24(float value)
	{
		___U3CMaxHealthU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentHealthGuyU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___U3CCurrentHealthGuyU3Ek__BackingField_25)); }
	inline float get_U3CCurrentHealthGuyU3Ek__BackingField_25() const { return ___U3CCurrentHealthGuyU3Ek__BackingField_25; }
	inline float* get_address_of_U3CCurrentHealthGuyU3Ek__BackingField_25() { return &___U3CCurrentHealthGuyU3Ek__BackingField_25; }
	inline void set_U3CCurrentHealthGuyU3Ek__BackingField_25(float value)
	{
		___U3CCurrentHealthGuyU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_U3CMaxHealthGuyU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(Battle_t2191861408, ___U3CMaxHealthGuyU3Ek__BackingField_26)); }
	inline float get_U3CMaxHealthGuyU3Ek__BackingField_26() const { return ___U3CMaxHealthGuyU3Ek__BackingField_26; }
	inline float* get_address_of_U3CMaxHealthGuyU3Ek__BackingField_26() { return &___U3CMaxHealthGuyU3Ek__BackingField_26; }
	inline void set_U3CMaxHealthGuyU3Ek__BackingField_26(float value)
	{
		___U3CMaxHealthGuyU3Ek__BackingField_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLE_T2191861408_H
#ifndef BATTLEIMP_T158148094_H
#define BATTLEIMP_T158148094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Battleimp
struct  Battleimp_t158148094  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Slider Battleimp::slider
	Slider_t3903728902 * ___slider_4;
	// UnityEngine.UI.Slider Battleimp::guySlider
	Slider_t3903728902 * ___guySlider_5;
	// TMPro.TMP_Text Battleimp::attackText
	TMP_Text_t2599618874 * ___attackText_6;
	// TMPro.TMP_Text Battleimp::dogHealth
	TMP_Text_t2599618874 * ___dogHealth_7;
	// TMPro.TMP_Text Battleimp::guyHealth
	TMP_Text_t2599618874 * ___guyHealth_8;
	// UnityEngine.UI.Image Battleimp::black
	Image_t2670269651 * ___black_9;
	// UnityEngine.Animator Battleimp::animator
	Animator_t434523843 * ___animator_10;
	// UnityEngine.AudioSource Battleimp::DogDieSFX
	AudioSource_t3935305588 * ___DogDieSFX_11;
	// UnityEngine.AudioSource Battleimp::GuydDieSFX
	AudioSource_t3935305588 * ___GuydDieSFX_12;
	// UnityEngine.GameObject Battleimp::GUI
	GameObject_t1113636619 * ___GUI_13;
	// UnityEngine.GameObject Battleimp::GameOver
	GameObject_t1113636619 * ___GameOver_14;
	// Animimp Battleimp::anim
	Animimp_t3408691587 * ___anim_15;
	// System.Random Battleimp::rnd
	Random_t108471755 * ___rnd_16;
	// System.Boolean Battleimp::myTurn
	bool ___myTurn_17;
	// System.Boolean Battleimp::ableToAttack
	bool ___ableToAttack_18;
	// System.Boolean Battleimp::enemyAbleToAttack
	bool ___enemyAbleToAttack_19;
	// System.Boolean Battleimp::isReady
	bool ___isReady_20;
	// System.Boolean Battleimp::isDone
	bool ___isDone_21;
	// System.Boolean Battleimp::isDead
	bool ___isDead_22;
	// System.Single Battleimp::<CurrentHealth>k__BackingField
	float ___U3CCurrentHealthU3Ek__BackingField_23;
	// System.Single Battleimp::<MaxHealth>k__BackingField
	float ___U3CMaxHealthU3Ek__BackingField_24;
	// System.Single Battleimp::<CurrentHealthGuy>k__BackingField
	float ___U3CCurrentHealthGuyU3Ek__BackingField_25;
	// System.Single Battleimp::<MaxHealthGuy>k__BackingField
	float ___U3CMaxHealthGuyU3Ek__BackingField_26;

public:
	inline static int32_t get_offset_of_slider_4() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___slider_4)); }
	inline Slider_t3903728902 * get_slider_4() const { return ___slider_4; }
	inline Slider_t3903728902 ** get_address_of_slider_4() { return &___slider_4; }
	inline void set_slider_4(Slider_t3903728902 * value)
	{
		___slider_4 = value;
		Il2CppCodeGenWriteBarrier((&___slider_4), value);
	}

	inline static int32_t get_offset_of_guySlider_5() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___guySlider_5)); }
	inline Slider_t3903728902 * get_guySlider_5() const { return ___guySlider_5; }
	inline Slider_t3903728902 ** get_address_of_guySlider_5() { return &___guySlider_5; }
	inline void set_guySlider_5(Slider_t3903728902 * value)
	{
		___guySlider_5 = value;
		Il2CppCodeGenWriteBarrier((&___guySlider_5), value);
	}

	inline static int32_t get_offset_of_attackText_6() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___attackText_6)); }
	inline TMP_Text_t2599618874 * get_attackText_6() const { return ___attackText_6; }
	inline TMP_Text_t2599618874 ** get_address_of_attackText_6() { return &___attackText_6; }
	inline void set_attackText_6(TMP_Text_t2599618874 * value)
	{
		___attackText_6 = value;
		Il2CppCodeGenWriteBarrier((&___attackText_6), value);
	}

	inline static int32_t get_offset_of_dogHealth_7() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___dogHealth_7)); }
	inline TMP_Text_t2599618874 * get_dogHealth_7() const { return ___dogHealth_7; }
	inline TMP_Text_t2599618874 ** get_address_of_dogHealth_7() { return &___dogHealth_7; }
	inline void set_dogHealth_7(TMP_Text_t2599618874 * value)
	{
		___dogHealth_7 = value;
		Il2CppCodeGenWriteBarrier((&___dogHealth_7), value);
	}

	inline static int32_t get_offset_of_guyHealth_8() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___guyHealth_8)); }
	inline TMP_Text_t2599618874 * get_guyHealth_8() const { return ___guyHealth_8; }
	inline TMP_Text_t2599618874 ** get_address_of_guyHealth_8() { return &___guyHealth_8; }
	inline void set_guyHealth_8(TMP_Text_t2599618874 * value)
	{
		___guyHealth_8 = value;
		Il2CppCodeGenWriteBarrier((&___guyHealth_8), value);
	}

	inline static int32_t get_offset_of_black_9() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___black_9)); }
	inline Image_t2670269651 * get_black_9() const { return ___black_9; }
	inline Image_t2670269651 ** get_address_of_black_9() { return &___black_9; }
	inline void set_black_9(Image_t2670269651 * value)
	{
		___black_9 = value;
		Il2CppCodeGenWriteBarrier((&___black_9), value);
	}

	inline static int32_t get_offset_of_animator_10() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___animator_10)); }
	inline Animator_t434523843 * get_animator_10() const { return ___animator_10; }
	inline Animator_t434523843 ** get_address_of_animator_10() { return &___animator_10; }
	inline void set_animator_10(Animator_t434523843 * value)
	{
		___animator_10 = value;
		Il2CppCodeGenWriteBarrier((&___animator_10), value);
	}

	inline static int32_t get_offset_of_DogDieSFX_11() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___DogDieSFX_11)); }
	inline AudioSource_t3935305588 * get_DogDieSFX_11() const { return ___DogDieSFX_11; }
	inline AudioSource_t3935305588 ** get_address_of_DogDieSFX_11() { return &___DogDieSFX_11; }
	inline void set_DogDieSFX_11(AudioSource_t3935305588 * value)
	{
		___DogDieSFX_11 = value;
		Il2CppCodeGenWriteBarrier((&___DogDieSFX_11), value);
	}

	inline static int32_t get_offset_of_GuydDieSFX_12() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___GuydDieSFX_12)); }
	inline AudioSource_t3935305588 * get_GuydDieSFX_12() const { return ___GuydDieSFX_12; }
	inline AudioSource_t3935305588 ** get_address_of_GuydDieSFX_12() { return &___GuydDieSFX_12; }
	inline void set_GuydDieSFX_12(AudioSource_t3935305588 * value)
	{
		___GuydDieSFX_12 = value;
		Il2CppCodeGenWriteBarrier((&___GuydDieSFX_12), value);
	}

	inline static int32_t get_offset_of_GUI_13() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___GUI_13)); }
	inline GameObject_t1113636619 * get_GUI_13() const { return ___GUI_13; }
	inline GameObject_t1113636619 ** get_address_of_GUI_13() { return &___GUI_13; }
	inline void set_GUI_13(GameObject_t1113636619 * value)
	{
		___GUI_13 = value;
		Il2CppCodeGenWriteBarrier((&___GUI_13), value);
	}

	inline static int32_t get_offset_of_GameOver_14() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___GameOver_14)); }
	inline GameObject_t1113636619 * get_GameOver_14() const { return ___GameOver_14; }
	inline GameObject_t1113636619 ** get_address_of_GameOver_14() { return &___GameOver_14; }
	inline void set_GameOver_14(GameObject_t1113636619 * value)
	{
		___GameOver_14 = value;
		Il2CppCodeGenWriteBarrier((&___GameOver_14), value);
	}

	inline static int32_t get_offset_of_anim_15() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___anim_15)); }
	inline Animimp_t3408691587 * get_anim_15() const { return ___anim_15; }
	inline Animimp_t3408691587 ** get_address_of_anim_15() { return &___anim_15; }
	inline void set_anim_15(Animimp_t3408691587 * value)
	{
		___anim_15 = value;
		Il2CppCodeGenWriteBarrier((&___anim_15), value);
	}

	inline static int32_t get_offset_of_rnd_16() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___rnd_16)); }
	inline Random_t108471755 * get_rnd_16() const { return ___rnd_16; }
	inline Random_t108471755 ** get_address_of_rnd_16() { return &___rnd_16; }
	inline void set_rnd_16(Random_t108471755 * value)
	{
		___rnd_16 = value;
		Il2CppCodeGenWriteBarrier((&___rnd_16), value);
	}

	inline static int32_t get_offset_of_myTurn_17() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___myTurn_17)); }
	inline bool get_myTurn_17() const { return ___myTurn_17; }
	inline bool* get_address_of_myTurn_17() { return &___myTurn_17; }
	inline void set_myTurn_17(bool value)
	{
		___myTurn_17 = value;
	}

	inline static int32_t get_offset_of_ableToAttack_18() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___ableToAttack_18)); }
	inline bool get_ableToAttack_18() const { return ___ableToAttack_18; }
	inline bool* get_address_of_ableToAttack_18() { return &___ableToAttack_18; }
	inline void set_ableToAttack_18(bool value)
	{
		___ableToAttack_18 = value;
	}

	inline static int32_t get_offset_of_enemyAbleToAttack_19() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___enemyAbleToAttack_19)); }
	inline bool get_enemyAbleToAttack_19() const { return ___enemyAbleToAttack_19; }
	inline bool* get_address_of_enemyAbleToAttack_19() { return &___enemyAbleToAttack_19; }
	inline void set_enemyAbleToAttack_19(bool value)
	{
		___enemyAbleToAttack_19 = value;
	}

	inline static int32_t get_offset_of_isReady_20() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___isReady_20)); }
	inline bool get_isReady_20() const { return ___isReady_20; }
	inline bool* get_address_of_isReady_20() { return &___isReady_20; }
	inline void set_isReady_20(bool value)
	{
		___isReady_20 = value;
	}

	inline static int32_t get_offset_of_isDone_21() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___isDone_21)); }
	inline bool get_isDone_21() const { return ___isDone_21; }
	inline bool* get_address_of_isDone_21() { return &___isDone_21; }
	inline void set_isDone_21(bool value)
	{
		___isDone_21 = value;
	}

	inline static int32_t get_offset_of_isDead_22() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___isDead_22)); }
	inline bool get_isDead_22() const { return ___isDead_22; }
	inline bool* get_address_of_isDead_22() { return &___isDead_22; }
	inline void set_isDead_22(bool value)
	{
		___isDead_22 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentHealthU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___U3CCurrentHealthU3Ek__BackingField_23)); }
	inline float get_U3CCurrentHealthU3Ek__BackingField_23() const { return ___U3CCurrentHealthU3Ek__BackingField_23; }
	inline float* get_address_of_U3CCurrentHealthU3Ek__BackingField_23() { return &___U3CCurrentHealthU3Ek__BackingField_23; }
	inline void set_U3CCurrentHealthU3Ek__BackingField_23(float value)
	{
		___U3CCurrentHealthU3Ek__BackingField_23 = value;
	}

	inline static int32_t get_offset_of_U3CMaxHealthU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___U3CMaxHealthU3Ek__BackingField_24)); }
	inline float get_U3CMaxHealthU3Ek__BackingField_24() const { return ___U3CMaxHealthU3Ek__BackingField_24; }
	inline float* get_address_of_U3CMaxHealthU3Ek__BackingField_24() { return &___U3CMaxHealthU3Ek__BackingField_24; }
	inline void set_U3CMaxHealthU3Ek__BackingField_24(float value)
	{
		___U3CMaxHealthU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentHealthGuyU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___U3CCurrentHealthGuyU3Ek__BackingField_25)); }
	inline float get_U3CCurrentHealthGuyU3Ek__BackingField_25() const { return ___U3CCurrentHealthGuyU3Ek__BackingField_25; }
	inline float* get_address_of_U3CCurrentHealthGuyU3Ek__BackingField_25() { return &___U3CCurrentHealthGuyU3Ek__BackingField_25; }
	inline void set_U3CCurrentHealthGuyU3Ek__BackingField_25(float value)
	{
		___U3CCurrentHealthGuyU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_U3CMaxHealthGuyU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(Battleimp_t158148094, ___U3CMaxHealthGuyU3Ek__BackingField_26)); }
	inline float get_U3CMaxHealthGuyU3Ek__BackingField_26() const { return ___U3CMaxHealthGuyU3Ek__BackingField_26; }
	inline float* get_address_of_U3CMaxHealthGuyU3Ek__BackingField_26() { return &___U3CMaxHealthGuyU3Ek__BackingField_26; }
	inline void set_U3CMaxHealthGuyU3Ek__BackingField_26(float value)
	{
		___U3CMaxHealthGuyU3Ek__BackingField_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLEIMP_T158148094_H
#ifndef BATTLEINF_T1732126188_H
#define BATTLEINF_T1732126188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Battleinf
struct  Battleinf_t1732126188  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Slider Battleinf::slider
	Slider_t3903728902 * ___slider_4;
	// UnityEngine.UI.Slider Battleinf::guySlider
	Slider_t3903728902 * ___guySlider_5;
	// TMPro.TMP_Text Battleinf::attackText
	TMP_Text_t2599618874 * ___attackText_6;
	// TMPro.TMP_Text Battleinf::nameText
	TMP_Text_t2599618874 * ___nameText_7;
	// TMPro.TMP_Text Battleinf::dogHealth
	TMP_Text_t2599618874 * ___dogHealth_8;
	// TMPro.TMP_Text Battleinf::guyHealth
	TMP_Text_t2599618874 * ___guyHealth_9;
	// UnityEngine.SpriteRenderer Battleinf::dogSprite
	SpriteRenderer_t3235626157 * ___dogSprite_10;
	// UnityEngine.UI.Image Battleinf::black
	Image_t2670269651 * ___black_11;
	// UnityEngine.Animator Battleinf::animator
	Animator_t434523843 * ___animator_12;
	// UnityEngine.AudioSource Battleinf::DogDieSFX
	AudioSource_t3935305588 * ___DogDieSFX_13;
	// UnityEngine.AudioSource Battleinf::GuydDieSFX
	AudioSource_t3935305588 * ___GuydDieSFX_14;
	// UnityEngine.GameObject Battleinf::GUI
	GameObject_t1113636619 * ___GUI_15;
	// UnityEngine.GameObject Battleinf::GameOver
	GameObject_t1113636619 * ___GameOver_16;
	// Animinf Battleinf::anim
	Animinf_t989049961 * ___anim_17;
	// System.Random Battleinf::rnd
	Random_t108471755 * ___rnd_18;
	// System.Boolean Battleinf::myTurn
	bool ___myTurn_19;
	// System.Boolean Battleinf::ableToAttack
	bool ___ableToAttack_20;
	// System.Boolean Battleinf::enemyAbleToAttack
	bool ___enemyAbleToAttack_21;
	// System.Boolean Battleinf::isReady
	bool ___isReady_22;
	// System.Boolean Battleinf::isDone
	bool ___isDone_23;
	// System.Boolean Battleinf::isDead
	bool ___isDead_24;
	// System.Byte Battleinf::r
	uint8_t ___r_25;
	// System.Byte Battleinf::g
	uint8_t ___g_26;
	// System.Byte Battleinf::b
	uint8_t ___b_27;
	// UnityEngine.Color32 Battleinf::col
	Color32_t2600501292  ___col_28;
	// System.Int32 Battleinf::n
	int32_t ___n_29;
	// System.Single Battleinf::<CurrentHealth>k__BackingField
	float ___U3CCurrentHealthU3Ek__BackingField_30;
	// System.Single Battleinf::<MaxHealth>k__BackingField
	float ___U3CMaxHealthU3Ek__BackingField_31;
	// System.Single Battleinf::<CurrentHealthGuy>k__BackingField
	float ___U3CCurrentHealthGuyU3Ek__BackingField_32;
	// System.Single Battleinf::<MaxHealthGuy>k__BackingField
	float ___U3CMaxHealthGuyU3Ek__BackingField_33;

public:
	inline static int32_t get_offset_of_slider_4() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___slider_4)); }
	inline Slider_t3903728902 * get_slider_4() const { return ___slider_4; }
	inline Slider_t3903728902 ** get_address_of_slider_4() { return &___slider_4; }
	inline void set_slider_4(Slider_t3903728902 * value)
	{
		___slider_4 = value;
		Il2CppCodeGenWriteBarrier((&___slider_4), value);
	}

	inline static int32_t get_offset_of_guySlider_5() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___guySlider_5)); }
	inline Slider_t3903728902 * get_guySlider_5() const { return ___guySlider_5; }
	inline Slider_t3903728902 ** get_address_of_guySlider_5() { return &___guySlider_5; }
	inline void set_guySlider_5(Slider_t3903728902 * value)
	{
		___guySlider_5 = value;
		Il2CppCodeGenWriteBarrier((&___guySlider_5), value);
	}

	inline static int32_t get_offset_of_attackText_6() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___attackText_6)); }
	inline TMP_Text_t2599618874 * get_attackText_6() const { return ___attackText_6; }
	inline TMP_Text_t2599618874 ** get_address_of_attackText_6() { return &___attackText_6; }
	inline void set_attackText_6(TMP_Text_t2599618874 * value)
	{
		___attackText_6 = value;
		Il2CppCodeGenWriteBarrier((&___attackText_6), value);
	}

	inline static int32_t get_offset_of_nameText_7() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___nameText_7)); }
	inline TMP_Text_t2599618874 * get_nameText_7() const { return ___nameText_7; }
	inline TMP_Text_t2599618874 ** get_address_of_nameText_7() { return &___nameText_7; }
	inline void set_nameText_7(TMP_Text_t2599618874 * value)
	{
		___nameText_7 = value;
		Il2CppCodeGenWriteBarrier((&___nameText_7), value);
	}

	inline static int32_t get_offset_of_dogHealth_8() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___dogHealth_8)); }
	inline TMP_Text_t2599618874 * get_dogHealth_8() const { return ___dogHealth_8; }
	inline TMP_Text_t2599618874 ** get_address_of_dogHealth_8() { return &___dogHealth_8; }
	inline void set_dogHealth_8(TMP_Text_t2599618874 * value)
	{
		___dogHealth_8 = value;
		Il2CppCodeGenWriteBarrier((&___dogHealth_8), value);
	}

	inline static int32_t get_offset_of_guyHealth_9() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___guyHealth_9)); }
	inline TMP_Text_t2599618874 * get_guyHealth_9() const { return ___guyHealth_9; }
	inline TMP_Text_t2599618874 ** get_address_of_guyHealth_9() { return &___guyHealth_9; }
	inline void set_guyHealth_9(TMP_Text_t2599618874 * value)
	{
		___guyHealth_9 = value;
		Il2CppCodeGenWriteBarrier((&___guyHealth_9), value);
	}

	inline static int32_t get_offset_of_dogSprite_10() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___dogSprite_10)); }
	inline SpriteRenderer_t3235626157 * get_dogSprite_10() const { return ___dogSprite_10; }
	inline SpriteRenderer_t3235626157 ** get_address_of_dogSprite_10() { return &___dogSprite_10; }
	inline void set_dogSprite_10(SpriteRenderer_t3235626157 * value)
	{
		___dogSprite_10 = value;
		Il2CppCodeGenWriteBarrier((&___dogSprite_10), value);
	}

	inline static int32_t get_offset_of_black_11() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___black_11)); }
	inline Image_t2670269651 * get_black_11() const { return ___black_11; }
	inline Image_t2670269651 ** get_address_of_black_11() { return &___black_11; }
	inline void set_black_11(Image_t2670269651 * value)
	{
		___black_11 = value;
		Il2CppCodeGenWriteBarrier((&___black_11), value);
	}

	inline static int32_t get_offset_of_animator_12() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___animator_12)); }
	inline Animator_t434523843 * get_animator_12() const { return ___animator_12; }
	inline Animator_t434523843 ** get_address_of_animator_12() { return &___animator_12; }
	inline void set_animator_12(Animator_t434523843 * value)
	{
		___animator_12 = value;
		Il2CppCodeGenWriteBarrier((&___animator_12), value);
	}

	inline static int32_t get_offset_of_DogDieSFX_13() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___DogDieSFX_13)); }
	inline AudioSource_t3935305588 * get_DogDieSFX_13() const { return ___DogDieSFX_13; }
	inline AudioSource_t3935305588 ** get_address_of_DogDieSFX_13() { return &___DogDieSFX_13; }
	inline void set_DogDieSFX_13(AudioSource_t3935305588 * value)
	{
		___DogDieSFX_13 = value;
		Il2CppCodeGenWriteBarrier((&___DogDieSFX_13), value);
	}

	inline static int32_t get_offset_of_GuydDieSFX_14() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___GuydDieSFX_14)); }
	inline AudioSource_t3935305588 * get_GuydDieSFX_14() const { return ___GuydDieSFX_14; }
	inline AudioSource_t3935305588 ** get_address_of_GuydDieSFX_14() { return &___GuydDieSFX_14; }
	inline void set_GuydDieSFX_14(AudioSource_t3935305588 * value)
	{
		___GuydDieSFX_14 = value;
		Il2CppCodeGenWriteBarrier((&___GuydDieSFX_14), value);
	}

	inline static int32_t get_offset_of_GUI_15() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___GUI_15)); }
	inline GameObject_t1113636619 * get_GUI_15() const { return ___GUI_15; }
	inline GameObject_t1113636619 ** get_address_of_GUI_15() { return &___GUI_15; }
	inline void set_GUI_15(GameObject_t1113636619 * value)
	{
		___GUI_15 = value;
		Il2CppCodeGenWriteBarrier((&___GUI_15), value);
	}

	inline static int32_t get_offset_of_GameOver_16() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___GameOver_16)); }
	inline GameObject_t1113636619 * get_GameOver_16() const { return ___GameOver_16; }
	inline GameObject_t1113636619 ** get_address_of_GameOver_16() { return &___GameOver_16; }
	inline void set_GameOver_16(GameObject_t1113636619 * value)
	{
		___GameOver_16 = value;
		Il2CppCodeGenWriteBarrier((&___GameOver_16), value);
	}

	inline static int32_t get_offset_of_anim_17() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___anim_17)); }
	inline Animinf_t989049961 * get_anim_17() const { return ___anim_17; }
	inline Animinf_t989049961 ** get_address_of_anim_17() { return &___anim_17; }
	inline void set_anim_17(Animinf_t989049961 * value)
	{
		___anim_17 = value;
		Il2CppCodeGenWriteBarrier((&___anim_17), value);
	}

	inline static int32_t get_offset_of_rnd_18() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___rnd_18)); }
	inline Random_t108471755 * get_rnd_18() const { return ___rnd_18; }
	inline Random_t108471755 ** get_address_of_rnd_18() { return &___rnd_18; }
	inline void set_rnd_18(Random_t108471755 * value)
	{
		___rnd_18 = value;
		Il2CppCodeGenWriteBarrier((&___rnd_18), value);
	}

	inline static int32_t get_offset_of_myTurn_19() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___myTurn_19)); }
	inline bool get_myTurn_19() const { return ___myTurn_19; }
	inline bool* get_address_of_myTurn_19() { return &___myTurn_19; }
	inline void set_myTurn_19(bool value)
	{
		___myTurn_19 = value;
	}

	inline static int32_t get_offset_of_ableToAttack_20() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___ableToAttack_20)); }
	inline bool get_ableToAttack_20() const { return ___ableToAttack_20; }
	inline bool* get_address_of_ableToAttack_20() { return &___ableToAttack_20; }
	inline void set_ableToAttack_20(bool value)
	{
		___ableToAttack_20 = value;
	}

	inline static int32_t get_offset_of_enemyAbleToAttack_21() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___enemyAbleToAttack_21)); }
	inline bool get_enemyAbleToAttack_21() const { return ___enemyAbleToAttack_21; }
	inline bool* get_address_of_enemyAbleToAttack_21() { return &___enemyAbleToAttack_21; }
	inline void set_enemyAbleToAttack_21(bool value)
	{
		___enemyAbleToAttack_21 = value;
	}

	inline static int32_t get_offset_of_isReady_22() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___isReady_22)); }
	inline bool get_isReady_22() const { return ___isReady_22; }
	inline bool* get_address_of_isReady_22() { return &___isReady_22; }
	inline void set_isReady_22(bool value)
	{
		___isReady_22 = value;
	}

	inline static int32_t get_offset_of_isDone_23() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___isDone_23)); }
	inline bool get_isDone_23() const { return ___isDone_23; }
	inline bool* get_address_of_isDone_23() { return &___isDone_23; }
	inline void set_isDone_23(bool value)
	{
		___isDone_23 = value;
	}

	inline static int32_t get_offset_of_isDead_24() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___isDead_24)); }
	inline bool get_isDead_24() const { return ___isDead_24; }
	inline bool* get_address_of_isDead_24() { return &___isDead_24; }
	inline void set_isDead_24(bool value)
	{
		___isDead_24 = value;
	}

	inline static int32_t get_offset_of_r_25() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___r_25)); }
	inline uint8_t get_r_25() const { return ___r_25; }
	inline uint8_t* get_address_of_r_25() { return &___r_25; }
	inline void set_r_25(uint8_t value)
	{
		___r_25 = value;
	}

	inline static int32_t get_offset_of_g_26() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___g_26)); }
	inline uint8_t get_g_26() const { return ___g_26; }
	inline uint8_t* get_address_of_g_26() { return &___g_26; }
	inline void set_g_26(uint8_t value)
	{
		___g_26 = value;
	}

	inline static int32_t get_offset_of_b_27() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___b_27)); }
	inline uint8_t get_b_27() const { return ___b_27; }
	inline uint8_t* get_address_of_b_27() { return &___b_27; }
	inline void set_b_27(uint8_t value)
	{
		___b_27 = value;
	}

	inline static int32_t get_offset_of_col_28() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___col_28)); }
	inline Color32_t2600501292  get_col_28() const { return ___col_28; }
	inline Color32_t2600501292 * get_address_of_col_28() { return &___col_28; }
	inline void set_col_28(Color32_t2600501292  value)
	{
		___col_28 = value;
	}

	inline static int32_t get_offset_of_n_29() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___n_29)); }
	inline int32_t get_n_29() const { return ___n_29; }
	inline int32_t* get_address_of_n_29() { return &___n_29; }
	inline void set_n_29(int32_t value)
	{
		___n_29 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentHealthU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___U3CCurrentHealthU3Ek__BackingField_30)); }
	inline float get_U3CCurrentHealthU3Ek__BackingField_30() const { return ___U3CCurrentHealthU3Ek__BackingField_30; }
	inline float* get_address_of_U3CCurrentHealthU3Ek__BackingField_30() { return &___U3CCurrentHealthU3Ek__BackingField_30; }
	inline void set_U3CCurrentHealthU3Ek__BackingField_30(float value)
	{
		___U3CCurrentHealthU3Ek__BackingField_30 = value;
	}

	inline static int32_t get_offset_of_U3CMaxHealthU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___U3CMaxHealthU3Ek__BackingField_31)); }
	inline float get_U3CMaxHealthU3Ek__BackingField_31() const { return ___U3CMaxHealthU3Ek__BackingField_31; }
	inline float* get_address_of_U3CMaxHealthU3Ek__BackingField_31() { return &___U3CMaxHealthU3Ek__BackingField_31; }
	inline void set_U3CMaxHealthU3Ek__BackingField_31(float value)
	{
		___U3CMaxHealthU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentHealthGuyU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___U3CCurrentHealthGuyU3Ek__BackingField_32)); }
	inline float get_U3CCurrentHealthGuyU3Ek__BackingField_32() const { return ___U3CCurrentHealthGuyU3Ek__BackingField_32; }
	inline float* get_address_of_U3CCurrentHealthGuyU3Ek__BackingField_32() { return &___U3CCurrentHealthGuyU3Ek__BackingField_32; }
	inline void set_U3CCurrentHealthGuyU3Ek__BackingField_32(float value)
	{
		___U3CCurrentHealthGuyU3Ek__BackingField_32 = value;
	}

	inline static int32_t get_offset_of_U3CMaxHealthGuyU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(Battleinf_t1732126188, ___U3CMaxHealthGuyU3Ek__BackingField_33)); }
	inline float get_U3CMaxHealthGuyU3Ek__BackingField_33() const { return ___U3CMaxHealthGuyU3Ek__BackingField_33; }
	inline float* get_address_of_U3CMaxHealthGuyU3Ek__BackingField_33() { return &___U3CMaxHealthGuyU3Ek__BackingField_33; }
	inline void set_U3CMaxHealthGuyU3Ek__BackingField_33(float value)
	{
		___U3CMaxHealthGuyU3Ek__BackingField_33 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLEINF_T1732126188_H
#ifndef COLOR_T2591083401_H
#define COLOR_T2591083401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Color
struct  Color_t2591083401  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Slider Color::slider
	Slider_t3903728902 * ___slider_4;
	// UnityEngine.UI.Image Color::img
	Image_t2670269651 * ___img_5;
	// UnityEngine.Color32 Color::col
	Color32_t2600501292  ___col_6;
	// System.Single Color::val
	float ___val_7;

public:
	inline static int32_t get_offset_of_slider_4() { return static_cast<int32_t>(offsetof(Color_t2591083401, ___slider_4)); }
	inline Slider_t3903728902 * get_slider_4() const { return ___slider_4; }
	inline Slider_t3903728902 ** get_address_of_slider_4() { return &___slider_4; }
	inline void set_slider_4(Slider_t3903728902 * value)
	{
		___slider_4 = value;
		Il2CppCodeGenWriteBarrier((&___slider_4), value);
	}

	inline static int32_t get_offset_of_img_5() { return static_cast<int32_t>(offsetof(Color_t2591083401, ___img_5)); }
	inline Image_t2670269651 * get_img_5() const { return ___img_5; }
	inline Image_t2670269651 ** get_address_of_img_5() { return &___img_5; }
	inline void set_img_5(Image_t2670269651 * value)
	{
		___img_5 = value;
		Il2CppCodeGenWriteBarrier((&___img_5), value);
	}

	inline static int32_t get_offset_of_col_6() { return static_cast<int32_t>(offsetof(Color_t2591083401, ___col_6)); }
	inline Color32_t2600501292  get_col_6() const { return ___col_6; }
	inline Color32_t2600501292 * get_address_of_col_6() { return &___col_6; }
	inline void set_col_6(Color32_t2600501292  value)
	{
		___col_6 = value;
	}

	inline static int32_t get_offset_of_val_7() { return static_cast<int32_t>(offsetof(Color_t2591083401, ___val_7)); }
	inline float get_val_7() const { return ___val_7; }
	inline float* get_address_of_val_7() { return &___val_7; }
	inline void set_val_7(float value)
	{
		___val_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2591083401_H
#ifndef COLORIMP_T1969227374_H
#define COLORIMP_T1969227374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorimp
struct  Colorimp_t1969227374  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Slider Colorimp::slider
	Slider_t3903728902 * ___slider_4;
	// UnityEngine.UI.Image Colorimp::img
	Image_t2670269651 * ___img_5;
	// UnityEngine.Color32 Colorimp::col
	Color32_t2600501292  ___col_6;
	// System.Single Colorimp::val
	float ___val_7;

public:
	inline static int32_t get_offset_of_slider_4() { return static_cast<int32_t>(offsetof(Colorimp_t1969227374, ___slider_4)); }
	inline Slider_t3903728902 * get_slider_4() const { return ___slider_4; }
	inline Slider_t3903728902 ** get_address_of_slider_4() { return &___slider_4; }
	inline void set_slider_4(Slider_t3903728902 * value)
	{
		___slider_4 = value;
		Il2CppCodeGenWriteBarrier((&___slider_4), value);
	}

	inline static int32_t get_offset_of_img_5() { return static_cast<int32_t>(offsetof(Colorimp_t1969227374, ___img_5)); }
	inline Image_t2670269651 * get_img_5() const { return ___img_5; }
	inline Image_t2670269651 ** get_address_of_img_5() { return &___img_5; }
	inline void set_img_5(Image_t2670269651 * value)
	{
		___img_5 = value;
		Il2CppCodeGenWriteBarrier((&___img_5), value);
	}

	inline static int32_t get_offset_of_col_6() { return static_cast<int32_t>(offsetof(Colorimp_t1969227374, ___col_6)); }
	inline Color32_t2600501292  get_col_6() const { return ___col_6; }
	inline Color32_t2600501292 * get_address_of_col_6() { return &___col_6; }
	inline void set_col_6(Color32_t2600501292  value)
	{
		___col_6 = value;
	}

	inline static int32_t get_offset_of_val_7() { return static_cast<int32_t>(offsetof(Colorimp_t1969227374, ___val_7)); }
	inline float get_val_7() const { return ___val_7; }
	inline float* get_address_of_val_7() { return &___val_7; }
	inline void set_val_7(float value)
	{
		___val_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORIMP_T1969227374_H
#ifndef COLORINF_T4284197021_H
#define COLORINF_T4284197021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorinf
struct  Colorinf_t4284197021  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Slider Colorinf::slider
	Slider_t3903728902 * ___slider_4;
	// UnityEngine.UI.Image Colorinf::img
	Image_t2670269651 * ___img_5;
	// UnityEngine.Color32 Colorinf::col
	Color32_t2600501292  ___col_6;
	// System.Single Colorinf::val
	float ___val_7;

public:
	inline static int32_t get_offset_of_slider_4() { return static_cast<int32_t>(offsetof(Colorinf_t4284197021, ___slider_4)); }
	inline Slider_t3903728902 * get_slider_4() const { return ___slider_4; }
	inline Slider_t3903728902 ** get_address_of_slider_4() { return &___slider_4; }
	inline void set_slider_4(Slider_t3903728902 * value)
	{
		___slider_4 = value;
		Il2CppCodeGenWriteBarrier((&___slider_4), value);
	}

	inline static int32_t get_offset_of_img_5() { return static_cast<int32_t>(offsetof(Colorinf_t4284197021, ___img_5)); }
	inline Image_t2670269651 * get_img_5() const { return ___img_5; }
	inline Image_t2670269651 ** get_address_of_img_5() { return &___img_5; }
	inline void set_img_5(Image_t2670269651 * value)
	{
		___img_5 = value;
		Il2CppCodeGenWriteBarrier((&___img_5), value);
	}

	inline static int32_t get_offset_of_col_6() { return static_cast<int32_t>(offsetof(Colorinf_t4284197021, ___col_6)); }
	inline Color32_t2600501292  get_col_6() const { return ___col_6; }
	inline Color32_t2600501292 * get_address_of_col_6() { return &___col_6; }
	inline void set_col_6(Color32_t2600501292  value)
	{
		___col_6 = value;
	}

	inline static int32_t get_offset_of_val_7() { return static_cast<int32_t>(offsetof(Colorinf_t4284197021, ___val_7)); }
	inline float get_val_7() const { return ___val_7; }
	inline float* get_address_of_val_7() { return &___val_7; }
	inline void set_val_7(float value)
	{
		___val_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORINF_T4284197021_H
#ifndef BOOTSEQ_T2506222928_H
#define BOOTSEQ_T2506222928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.BootSeq
struct  BootSeq_t2506222928  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct BootSeq_t2506222928_StaticFields
{
public:
	// System.String Harmony.BootSeq::BootSequence
	String_t* ___BootSequence_4;

public:
	inline static int32_t get_offset_of_BootSequence_4() { return static_cast<int32_t>(offsetof(BootSeq_t2506222928_StaticFields, ___BootSequence_4)); }
	inline String_t* get_BootSequence_4() const { return ___BootSequence_4; }
	inline String_t** get_address_of_BootSequence_4() { return &___BootSequence_4; }
	inline void set_BootSequence_4(String_t* value)
	{
		___BootSequence_4 = value;
		Il2CppCodeGenWriteBarrier((&___BootSequence_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOTSEQ_T2506222928_H
#ifndef DIFFICULTY_T3960432064_H
#define DIFFICULTY_T3960432064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Difficulty
struct  Difficulty_t3960432064  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Difficulty_t3960432064_StaticFields
{
public:
	// System.String Harmony.Difficulty::a
	String_t* ___a_4;
	// System.String Harmony.Difficulty::b
	String_t* ___b_5;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Difficulty_t3960432064_StaticFields, ___a_4)); }
	inline String_t* get_a_4() const { return ___a_4; }
	inline String_t** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(String_t* value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(Difficulty_t3960432064_StaticFields, ___b_5)); }
	inline String_t* get_b_5() const { return ___b_5; }
	inline String_t** get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(String_t* value)
	{
		___b_5 = value;
		Il2CppCodeGenWriteBarrier((&___b_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIFFICULTY_T3960432064_H
#ifndef DOGGO_T4070438711_H
#define DOGGO_T4070438711_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Doggo
struct  Doggo_t4070438711  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Doggo_t4070438711_StaticFields
{
public:
	// System.String Harmony.Doggo::a
	String_t* ___a_4;
	// System.String Harmony.Doggo::b
	String_t* ___b_5;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Doggo_t4070438711_StaticFields, ___a_4)); }
	inline String_t* get_a_4() const { return ___a_4; }
	inline String_t** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(String_t* value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(Doggo_t4070438711_StaticFields, ___b_5)); }
	inline String_t* get_b_5() const { return ___b_5; }
	inline String_t** get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(String_t* value)
	{
		___b_5 = value;
		Il2CppCodeGenWriteBarrier((&___b_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOGGO_T4070438711_H
#ifndef INPUT_T3716682282_H
#define INPUT_T3716682282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Input
struct  Input_t3716682282  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Input_t3716682282_StaticFields
{
public:
	// System.String Harmony.Input::a
	String_t* ___a_4;
	// System.String Harmony.Input::b
	String_t* ___b_5;
	// System.String Harmony.Input::c
	String_t* ___c_6;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Input_t3716682282_StaticFields, ___a_4)); }
	inline String_t* get_a_4() const { return ___a_4; }
	inline String_t** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(String_t* value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(Input_t3716682282_StaticFields, ___b_5)); }
	inline String_t* get_b_5() const { return ___b_5; }
	inline String_t** get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(String_t* value)
	{
		___b_5 = value;
		Il2CppCodeGenWriteBarrier((&___b_5), value);
	}

	inline static int32_t get_offset_of_c_6() { return static_cast<int32_t>(offsetof(Input_t3716682282_StaticFields, ___c_6)); }
	inline String_t* get_c_6() const { return ___c_6; }
	inline String_t** get_address_of_c_6() { return &___c_6; }
	inline void set_c_6(String_t* value)
	{
		___c_6 = value;
		Il2CppCodeGenWriteBarrier((&___c_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUT_T3716682282_H
#ifndef PROGRAM_T1866495201_H
#define PROGRAM_T1866495201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Program
struct  Program_t1866495201  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioSource Harmony.Program::bootSFX
	AudioSource_t3935305588 * ___bootSFX_4;
	// UnityEngine.AudioSource Harmony.Program::messageSFX
	AudioSource_t3935305588 * ___messageSFX_5;
	// UnityEngine.AudioSource Harmony.Program::leetSFX
	AudioSource_t3935305588 * ___leetSFX_6;
	// System.Boolean Harmony.Program::inCoroutine
	bool ___inCoroutine_8;
	// TMPro.TMP_Text Harmony.Program::startText
	TMP_Text_t2599618874 * ___startText_9;
	// System.Int32 Harmony.Program::i
	int32_t ___i_10;

public:
	inline static int32_t get_offset_of_bootSFX_4() { return static_cast<int32_t>(offsetof(Program_t1866495201, ___bootSFX_4)); }
	inline AudioSource_t3935305588 * get_bootSFX_4() const { return ___bootSFX_4; }
	inline AudioSource_t3935305588 ** get_address_of_bootSFX_4() { return &___bootSFX_4; }
	inline void set_bootSFX_4(AudioSource_t3935305588 * value)
	{
		___bootSFX_4 = value;
		Il2CppCodeGenWriteBarrier((&___bootSFX_4), value);
	}

	inline static int32_t get_offset_of_messageSFX_5() { return static_cast<int32_t>(offsetof(Program_t1866495201, ___messageSFX_5)); }
	inline AudioSource_t3935305588 * get_messageSFX_5() const { return ___messageSFX_5; }
	inline AudioSource_t3935305588 ** get_address_of_messageSFX_5() { return &___messageSFX_5; }
	inline void set_messageSFX_5(AudioSource_t3935305588 * value)
	{
		___messageSFX_5 = value;
		Il2CppCodeGenWriteBarrier((&___messageSFX_5), value);
	}

	inline static int32_t get_offset_of_leetSFX_6() { return static_cast<int32_t>(offsetof(Program_t1866495201, ___leetSFX_6)); }
	inline AudioSource_t3935305588 * get_leetSFX_6() const { return ___leetSFX_6; }
	inline AudioSource_t3935305588 ** get_address_of_leetSFX_6() { return &___leetSFX_6; }
	inline void set_leetSFX_6(AudioSource_t3935305588 * value)
	{
		___leetSFX_6 = value;
		Il2CppCodeGenWriteBarrier((&___leetSFX_6), value);
	}

	inline static int32_t get_offset_of_inCoroutine_8() { return static_cast<int32_t>(offsetof(Program_t1866495201, ___inCoroutine_8)); }
	inline bool get_inCoroutine_8() const { return ___inCoroutine_8; }
	inline bool* get_address_of_inCoroutine_8() { return &___inCoroutine_8; }
	inline void set_inCoroutine_8(bool value)
	{
		___inCoroutine_8 = value;
	}

	inline static int32_t get_offset_of_startText_9() { return static_cast<int32_t>(offsetof(Program_t1866495201, ___startText_9)); }
	inline TMP_Text_t2599618874 * get_startText_9() const { return ___startText_9; }
	inline TMP_Text_t2599618874 ** get_address_of_startText_9() { return &___startText_9; }
	inline void set_startText_9(TMP_Text_t2599618874 * value)
	{
		___startText_9 = value;
		Il2CppCodeGenWriteBarrier((&___startText_9), value);
	}

	inline static int32_t get_offset_of_i_10() { return static_cast<int32_t>(offsetof(Program_t1866495201, ___i_10)); }
	inline int32_t get_i_10() const { return ___i_10; }
	inline int32_t* get_address_of_i_10() { return &___i_10; }
	inline void set_i_10(int32_t value)
	{
		___i_10 = value;
	}
};

struct Program_t1866495201_StaticFields
{
public:
	// System.Single Harmony.Program::normTypeSpeed
	float ___normTypeSpeed_7;

public:
	inline static int32_t get_offset_of_normTypeSpeed_7() { return static_cast<int32_t>(offsetof(Program_t1866495201_StaticFields, ___normTypeSpeed_7)); }
	inline float get_normTypeSpeed_7() const { return ___normTypeSpeed_7; }
	inline float* get_address_of_normTypeSpeed_7() { return &___normTypeSpeed_7; }
	inline void set_normTypeSpeed_7(float value)
	{
		___normTypeSpeed_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRAM_T1866495201_H
#ifndef PROGRAM2_T3485103329_H
#define PROGRAM2_T3485103329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Program2
struct  Program2_t3485103329  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioSource Harmony.Program2::messageSFX
	AudioSource_t3935305588 * ___messageSFX_4;
	// UnityEngine.AudioSource Harmony.Program2::leetSFX
	AudioSource_t3935305588 * ___leetSFX_5;
	// System.Boolean Harmony.Program2::inCoroutine
	bool ___inCoroutine_7;
	// TMPro.TMP_Text Harmony.Program2::startText
	TMP_Text_t2599618874 * ___startText_8;
	// System.Int32 Harmony.Program2::i
	int32_t ___i_9;

public:
	inline static int32_t get_offset_of_messageSFX_4() { return static_cast<int32_t>(offsetof(Program2_t3485103329, ___messageSFX_4)); }
	inline AudioSource_t3935305588 * get_messageSFX_4() const { return ___messageSFX_4; }
	inline AudioSource_t3935305588 ** get_address_of_messageSFX_4() { return &___messageSFX_4; }
	inline void set_messageSFX_4(AudioSource_t3935305588 * value)
	{
		___messageSFX_4 = value;
		Il2CppCodeGenWriteBarrier((&___messageSFX_4), value);
	}

	inline static int32_t get_offset_of_leetSFX_5() { return static_cast<int32_t>(offsetof(Program2_t3485103329, ___leetSFX_5)); }
	inline AudioSource_t3935305588 * get_leetSFX_5() const { return ___leetSFX_5; }
	inline AudioSource_t3935305588 ** get_address_of_leetSFX_5() { return &___leetSFX_5; }
	inline void set_leetSFX_5(AudioSource_t3935305588 * value)
	{
		___leetSFX_5 = value;
		Il2CppCodeGenWriteBarrier((&___leetSFX_5), value);
	}

	inline static int32_t get_offset_of_inCoroutine_7() { return static_cast<int32_t>(offsetof(Program2_t3485103329, ___inCoroutine_7)); }
	inline bool get_inCoroutine_7() const { return ___inCoroutine_7; }
	inline bool* get_address_of_inCoroutine_7() { return &___inCoroutine_7; }
	inline void set_inCoroutine_7(bool value)
	{
		___inCoroutine_7 = value;
	}

	inline static int32_t get_offset_of_startText_8() { return static_cast<int32_t>(offsetof(Program2_t3485103329, ___startText_8)); }
	inline TMP_Text_t2599618874 * get_startText_8() const { return ___startText_8; }
	inline TMP_Text_t2599618874 ** get_address_of_startText_8() { return &___startText_8; }
	inline void set_startText_8(TMP_Text_t2599618874 * value)
	{
		___startText_8 = value;
		Il2CppCodeGenWriteBarrier((&___startText_8), value);
	}

	inline static int32_t get_offset_of_i_9() { return static_cast<int32_t>(offsetof(Program2_t3485103329, ___i_9)); }
	inline int32_t get_i_9() const { return ___i_9; }
	inline int32_t* get_address_of_i_9() { return &___i_9; }
	inline void set_i_9(int32_t value)
	{
		___i_9 = value;
	}
};

struct Program2_t3485103329_StaticFields
{
public:
	// System.Single Harmony.Program2::normTypeSpeed
	float ___normTypeSpeed_6;

public:
	inline static int32_t get_offset_of_normTypeSpeed_6() { return static_cast<int32_t>(offsetof(Program2_t3485103329_StaticFields, ___normTypeSpeed_6)); }
	inline float get_normTypeSpeed_6() const { return ___normTypeSpeed_6; }
	inline float* get_address_of_normTypeSpeed_6() { return &___normTypeSpeed_6; }
	inline void set_normTypeSpeed_6(float value)
	{
		___normTypeSpeed_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRAM2_T3485103329_H
#ifndef PROGRAM3_T1528788193_H
#define PROGRAM3_T1528788193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Program3
struct  Program3_t1528788193  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioSource Harmony.Program3::messageSFX
	AudioSource_t3935305588 * ___messageSFX_4;
	// UnityEngine.AudioSource Harmony.Program3::doggoSFX
	AudioSource_t3935305588 * ___doggoSFX_5;
	// System.Boolean Harmony.Program3::inCoroutine
	bool ___inCoroutine_7;
	// TMPro.TMP_Text Harmony.Program3::startText
	TMP_Text_t2599618874 * ___startText_8;
	// System.Int32 Harmony.Program3::i
	int32_t ___i_9;

public:
	inline static int32_t get_offset_of_messageSFX_4() { return static_cast<int32_t>(offsetof(Program3_t1528788193, ___messageSFX_4)); }
	inline AudioSource_t3935305588 * get_messageSFX_4() const { return ___messageSFX_4; }
	inline AudioSource_t3935305588 ** get_address_of_messageSFX_4() { return &___messageSFX_4; }
	inline void set_messageSFX_4(AudioSource_t3935305588 * value)
	{
		___messageSFX_4 = value;
		Il2CppCodeGenWriteBarrier((&___messageSFX_4), value);
	}

	inline static int32_t get_offset_of_doggoSFX_5() { return static_cast<int32_t>(offsetof(Program3_t1528788193, ___doggoSFX_5)); }
	inline AudioSource_t3935305588 * get_doggoSFX_5() const { return ___doggoSFX_5; }
	inline AudioSource_t3935305588 ** get_address_of_doggoSFX_5() { return &___doggoSFX_5; }
	inline void set_doggoSFX_5(AudioSource_t3935305588 * value)
	{
		___doggoSFX_5 = value;
		Il2CppCodeGenWriteBarrier((&___doggoSFX_5), value);
	}

	inline static int32_t get_offset_of_inCoroutine_7() { return static_cast<int32_t>(offsetof(Program3_t1528788193, ___inCoroutine_7)); }
	inline bool get_inCoroutine_7() const { return ___inCoroutine_7; }
	inline bool* get_address_of_inCoroutine_7() { return &___inCoroutine_7; }
	inline void set_inCoroutine_7(bool value)
	{
		___inCoroutine_7 = value;
	}

	inline static int32_t get_offset_of_startText_8() { return static_cast<int32_t>(offsetof(Program3_t1528788193, ___startText_8)); }
	inline TMP_Text_t2599618874 * get_startText_8() const { return ___startText_8; }
	inline TMP_Text_t2599618874 ** get_address_of_startText_8() { return &___startText_8; }
	inline void set_startText_8(TMP_Text_t2599618874 * value)
	{
		___startText_8 = value;
		Il2CppCodeGenWriteBarrier((&___startText_8), value);
	}

	inline static int32_t get_offset_of_i_9() { return static_cast<int32_t>(offsetof(Program3_t1528788193, ___i_9)); }
	inline int32_t get_i_9() const { return ___i_9; }
	inline int32_t* get_address_of_i_9() { return &___i_9; }
	inline void set_i_9(int32_t value)
	{
		___i_9 = value;
	}
};

struct Program3_t1528788193_StaticFields
{
public:
	// System.Single Harmony.Program3::normTypeSpeed
	float ___normTypeSpeed_6;

public:
	inline static int32_t get_offset_of_normTypeSpeed_6() { return static_cast<int32_t>(offsetof(Program3_t1528788193_StaticFields, ___normTypeSpeed_6)); }
	inline float get_normTypeSpeed_6() const { return ___normTypeSpeed_6; }
	inline float* get_address_of_normTypeSpeed_6() { return &___normTypeSpeed_6; }
	inline void set_normTypeSpeed_6(float value)
	{
		___normTypeSpeed_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRAM3_T1528788193_H
#ifndef TYPE_T4058249690_H
#define TYPE_T4058249690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Type
struct  Type_t4058249690  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Type_t4058249690_StaticFields
{
public:
	// Harmony.Type Harmony.Type::instance
	Type_t4058249690 * ___instance_4;
	// Harmony.Program Harmony.Type::program
	Program_t1866495201 * ___program_5;
	// System.Collections.IEnumerator Harmony.Type::coroutine
	RuntimeObject* ___coroutine_6;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(Type_t4058249690_StaticFields, ___instance_4)); }
	inline Type_t4058249690 * get_instance_4() const { return ___instance_4; }
	inline Type_t4058249690 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(Type_t4058249690 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_program_5() { return static_cast<int32_t>(offsetof(Type_t4058249690_StaticFields, ___program_5)); }
	inline Program_t1866495201 * get_program_5() const { return ___program_5; }
	inline Program_t1866495201 ** get_address_of_program_5() { return &___program_5; }
	inline void set_program_5(Program_t1866495201 * value)
	{
		___program_5 = value;
		Il2CppCodeGenWriteBarrier((&___program_5), value);
	}

	inline static int32_t get_offset_of_coroutine_6() { return static_cast<int32_t>(offsetof(Type_t4058249690_StaticFields, ___coroutine_6)); }
	inline RuntimeObject* get_coroutine_6() const { return ___coroutine_6; }
	inline RuntimeObject** get_address_of_coroutine_6() { return &___coroutine_6; }
	inline void set_coroutine_6(RuntimeObject* value)
	{
		___coroutine_6 = value;
		Il2CppCodeGenWriteBarrier((&___coroutine_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T4058249690_H
#ifndef TYPE2_T2499874421_H
#define TYPE2_T2499874421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Type2
struct  Type2_t2499874421  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Type2_t2499874421_StaticFields
{
public:
	// Harmony.Type2 Harmony.Type2::instance
	Type2_t2499874421 * ___instance_4;
	// Harmony.Program2 Harmony.Type2::program2
	Program2_t3485103329 * ___program2_5;
	// System.Collections.IEnumerator Harmony.Type2::coroutine
	RuntimeObject* ___coroutine_6;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(Type2_t2499874421_StaticFields, ___instance_4)); }
	inline Type2_t2499874421 * get_instance_4() const { return ___instance_4; }
	inline Type2_t2499874421 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(Type2_t2499874421 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_program2_5() { return static_cast<int32_t>(offsetof(Type2_t2499874421_StaticFields, ___program2_5)); }
	inline Program2_t3485103329 * get_program2_5() const { return ___program2_5; }
	inline Program2_t3485103329 ** get_address_of_program2_5() { return &___program2_5; }
	inline void set_program2_5(Program2_t3485103329 * value)
	{
		___program2_5 = value;
		Il2CppCodeGenWriteBarrier((&___program2_5), value);
	}

	inline static int32_t get_offset_of_coroutine_6() { return static_cast<int32_t>(offsetof(Type2_t2499874421_StaticFields, ___coroutine_6)); }
	inline RuntimeObject* get_coroutine_6() const { return ___coroutine_6; }
	inline RuntimeObject** get_address_of_coroutine_6() { return &___coroutine_6; }
	inline void set_coroutine_6(RuntimeObject* value)
	{
		___coroutine_6 = value;
		Il2CppCodeGenWriteBarrier((&___coroutine_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE2_T2499874421_H
#ifndef TYPE3_T2499874422_H
#define TYPE3_T2499874422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.Type3
struct  Type3_t2499874422  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Type3_t2499874422_StaticFields
{
public:
	// Harmony.Type3 Harmony.Type3::instance
	Type3_t2499874422 * ___instance_4;
	// Harmony.Program3 Harmony.Type3::program3
	Program3_t1528788193 * ___program3_5;
	// System.Collections.IEnumerator Harmony.Type3::coroutine
	RuntimeObject* ___coroutine_6;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(Type3_t2499874422_StaticFields, ___instance_4)); }
	inline Type3_t2499874422 * get_instance_4() const { return ___instance_4; }
	inline Type3_t2499874422 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(Type3_t2499874422 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_program3_5() { return static_cast<int32_t>(offsetof(Type3_t2499874422_StaticFields, ___program3_5)); }
	inline Program3_t1528788193 * get_program3_5() const { return ___program3_5; }
	inline Program3_t1528788193 ** get_address_of_program3_5() { return &___program3_5; }
	inline void set_program3_5(Program3_t1528788193 * value)
	{
		___program3_5 = value;
		Il2CppCodeGenWriteBarrier((&___program3_5), value);
	}

	inline static int32_t get_offset_of_coroutine_6() { return static_cast<int32_t>(offsetof(Type3_t2499874422_StaticFields, ___coroutine_6)); }
	inline RuntimeObject* get_coroutine_6() const { return ___coroutine_6; }
	inline RuntimeObject** get_address_of_coroutine_6() { return &___coroutine_6; }
	inline void set_coroutine_6(RuntimeObject* value)
	{
		___coroutine_6 = value;
		Il2CppCodeGenWriteBarrier((&___coroutine_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE3_T2499874422_H
#ifndef MENUPROGRAM_T465836879_H
#define MENUPROGRAM_T465836879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Harmony.menuProgram
struct  menuProgram_t465836879  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_Text Harmony.menuProgram::startText
	TMP_Text_t2599618874 * ___startText_4;
	// UnityEngine.GameObject Harmony.menuProgram::doggoHuntBTN
	GameObject_t1113636619 * ___doggoHuntBTN_5;
	// System.String Harmony.menuProgram::str1
	String_t* ___str1_6;
	// System.String Harmony.menuProgram::str2
	String_t* ___str2_7;

public:
	inline static int32_t get_offset_of_startText_4() { return static_cast<int32_t>(offsetof(menuProgram_t465836879, ___startText_4)); }
	inline TMP_Text_t2599618874 * get_startText_4() const { return ___startText_4; }
	inline TMP_Text_t2599618874 ** get_address_of_startText_4() { return &___startText_4; }
	inline void set_startText_4(TMP_Text_t2599618874 * value)
	{
		___startText_4 = value;
		Il2CppCodeGenWriteBarrier((&___startText_4), value);
	}

	inline static int32_t get_offset_of_doggoHuntBTN_5() { return static_cast<int32_t>(offsetof(menuProgram_t465836879, ___doggoHuntBTN_5)); }
	inline GameObject_t1113636619 * get_doggoHuntBTN_5() const { return ___doggoHuntBTN_5; }
	inline GameObject_t1113636619 ** get_address_of_doggoHuntBTN_5() { return &___doggoHuntBTN_5; }
	inline void set_doggoHuntBTN_5(GameObject_t1113636619 * value)
	{
		___doggoHuntBTN_5 = value;
		Il2CppCodeGenWriteBarrier((&___doggoHuntBTN_5), value);
	}

	inline static int32_t get_offset_of_str1_6() { return static_cast<int32_t>(offsetof(menuProgram_t465836879, ___str1_6)); }
	inline String_t* get_str1_6() const { return ___str1_6; }
	inline String_t** get_address_of_str1_6() { return &___str1_6; }
	inline void set_str1_6(String_t* value)
	{
		___str1_6 = value;
		Il2CppCodeGenWriteBarrier((&___str1_6), value);
	}

	inline static int32_t get_offset_of_str2_7() { return static_cast<int32_t>(offsetof(menuProgram_t465836879, ___str2_7)); }
	inline String_t* get_str2_7() const { return ___str2_7; }
	inline String_t** get_address_of_str2_7() { return &___str2_7; }
	inline void set_str2_7(String_t* value)
	{
		___str2_7 = value;
		Il2CppCodeGenWriteBarrier((&___str2_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUPROGRAM_T465836879_H
#ifndef SPRITE_T2249211761_H
#define SPRITE_T2249211761_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sprite
struct  Sprite_t2249211761  : public MonoBehaviour_t3962482529
{
public:
	// Battle Sprite::battle
	Battle_t2191861408 * ___battle_4;
	// UnityEngine.Animator Sprite::anim
	Animator_t434523843 * ___anim_5;
	// System.Boolean Sprite::stahp
	bool ___stahp_6;

public:
	inline static int32_t get_offset_of_battle_4() { return static_cast<int32_t>(offsetof(Sprite_t2249211761, ___battle_4)); }
	inline Battle_t2191861408 * get_battle_4() const { return ___battle_4; }
	inline Battle_t2191861408 ** get_address_of_battle_4() { return &___battle_4; }
	inline void set_battle_4(Battle_t2191861408 * value)
	{
		___battle_4 = value;
		Il2CppCodeGenWriteBarrier((&___battle_4), value);
	}

	inline static int32_t get_offset_of_anim_5() { return static_cast<int32_t>(offsetof(Sprite_t2249211761, ___anim_5)); }
	inline Animator_t434523843 * get_anim_5() const { return ___anim_5; }
	inline Animator_t434523843 ** get_address_of_anim_5() { return &___anim_5; }
	inline void set_anim_5(Animator_t434523843 * value)
	{
		___anim_5 = value;
		Il2CppCodeGenWriteBarrier((&___anim_5), value);
	}

	inline static int32_t get_offset_of_stahp_6() { return static_cast<int32_t>(offsetof(Sprite_t2249211761, ___stahp_6)); }
	inline bool get_stahp_6() const { return ___stahp_6; }
	inline bool* get_address_of_stahp_6() { return &___stahp_6; }
	inline void set_stahp_6(bool value)
	{
		___stahp_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITE_T2249211761_H
#ifndef SPRITEIMP_T3952321968_H
#define SPRITEIMP_T3952321968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spriteimp
struct  Spriteimp_t3952321968  : public MonoBehaviour_t3962482529
{
public:
	// Battleimp Spriteimp::battle
	Battleimp_t158148094 * ___battle_4;
	// UnityEngine.Animator Spriteimp::anim
	Animator_t434523843 * ___anim_5;
	// System.Boolean Spriteimp::stahp
	bool ___stahp_6;

public:
	inline static int32_t get_offset_of_battle_4() { return static_cast<int32_t>(offsetof(Spriteimp_t3952321968, ___battle_4)); }
	inline Battleimp_t158148094 * get_battle_4() const { return ___battle_4; }
	inline Battleimp_t158148094 ** get_address_of_battle_4() { return &___battle_4; }
	inline void set_battle_4(Battleimp_t158148094 * value)
	{
		___battle_4 = value;
		Il2CppCodeGenWriteBarrier((&___battle_4), value);
	}

	inline static int32_t get_offset_of_anim_5() { return static_cast<int32_t>(offsetof(Spriteimp_t3952321968, ___anim_5)); }
	inline Animator_t434523843 * get_anim_5() const { return ___anim_5; }
	inline Animator_t434523843 ** get_address_of_anim_5() { return &___anim_5; }
	inline void set_anim_5(Animator_t434523843 * value)
	{
		___anim_5 = value;
		Il2CppCodeGenWriteBarrier((&___anim_5), value);
	}

	inline static int32_t get_offset_of_stahp_6() { return static_cast<int32_t>(offsetof(Spriteimp_t3952321968, ___stahp_6)); }
	inline bool get_stahp_6() const { return ___stahp_6; }
	inline bool* get_address_of_stahp_6() { return &___stahp_6; }
	inline void set_stahp_6(bool value)
	{
		___stahp_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEIMP_T3952321968_H
#ifndef SPRITEINF_T2378343838_H
#define SPRITEINF_T2378343838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spriteinf
struct  Spriteinf_t2378343838  : public MonoBehaviour_t3962482529
{
public:
	// Battleinf Spriteinf::battle
	Battleinf_t1732126188 * ___battle_4;
	// UnityEngine.Animator Spriteinf::anim
	Animator_t434523843 * ___anim_5;
	// System.Boolean Spriteinf::stahp
	bool ___stahp_6;

public:
	inline static int32_t get_offset_of_battle_4() { return static_cast<int32_t>(offsetof(Spriteinf_t2378343838, ___battle_4)); }
	inline Battleinf_t1732126188 * get_battle_4() const { return ___battle_4; }
	inline Battleinf_t1732126188 ** get_address_of_battle_4() { return &___battle_4; }
	inline void set_battle_4(Battleinf_t1732126188 * value)
	{
		___battle_4 = value;
		Il2CppCodeGenWriteBarrier((&___battle_4), value);
	}

	inline static int32_t get_offset_of_anim_5() { return static_cast<int32_t>(offsetof(Spriteinf_t2378343838, ___anim_5)); }
	inline Animator_t434523843 * get_anim_5() const { return ___anim_5; }
	inline Animator_t434523843 ** get_address_of_anim_5() { return &___anim_5; }
	inline void set_anim_5(Animator_t434523843 * value)
	{
		___anim_5 = value;
		Il2CppCodeGenWriteBarrier((&___anim_5), value);
	}

	inline static int32_t get_offset_of_stahp_6() { return static_cast<int32_t>(offsetof(Spriteinf_t2378343838, ___stahp_6)); }
	inline bool get_stahp_6() const { return ___stahp_6; }
	inline bool* get_address_of_stahp_6() { return &___stahp_6; }
	inline void set_stahp_6(bool value)
	{
		___stahp_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEINF_T2378343838_H
#ifndef AUDIOSOURCE_T3935305588_H
#define AUDIOSOURCE_T3935305588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioSource
struct  AudioSource_t3935305588  : public AudioBehaviour_t2879336574
{
public:
	// UnityEngine.AudioSourceExtension UnityEngine.AudioSource::spatializerExtension
	AudioSourceExtension_t3064908834 * ___spatializerExtension_4;
	// UnityEngine.AudioSourceExtension UnityEngine.AudioSource::ambisonicExtension
	AudioSourceExtension_t3064908834 * ___ambisonicExtension_5;

public:
	inline static int32_t get_offset_of_spatializerExtension_4() { return static_cast<int32_t>(offsetof(AudioSource_t3935305588, ___spatializerExtension_4)); }
	inline AudioSourceExtension_t3064908834 * get_spatializerExtension_4() const { return ___spatializerExtension_4; }
	inline AudioSourceExtension_t3064908834 ** get_address_of_spatializerExtension_4() { return &___spatializerExtension_4; }
	inline void set_spatializerExtension_4(AudioSourceExtension_t3064908834 * value)
	{
		___spatializerExtension_4 = value;
		Il2CppCodeGenWriteBarrier((&___spatializerExtension_4), value);
	}

	inline static int32_t get_offset_of_ambisonicExtension_5() { return static_cast<int32_t>(offsetof(AudioSource_t3935305588, ___ambisonicExtension_5)); }
	inline AudioSourceExtension_t3064908834 * get_ambisonicExtension_5() const { return ___ambisonicExtension_5; }
	inline AudioSourceExtension_t3064908834 ** get_address_of_ambisonicExtension_5() { return &___ambisonicExtension_5; }
	inline void set_ambisonicExtension_5(AudioSourceExtension_t3064908834 * value)
	{
		___ambisonicExtension_5 = value;
		Il2CppCodeGenWriteBarrier((&___ambisonicExtension_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOSOURCE_T3935305588_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_8;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_9;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_t2598313366 * ___m_CanvasRenderer_10;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_11;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_12;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_14;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_16;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_19;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_6)); }
	inline Material_t340375123 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t340375123 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t340375123 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_6), value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_7)); }
	inline Color_t2555686324  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t2555686324 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t2555686324  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_8)); }
	inline bool get_m_RaycastTarget_8() const { return ___m_RaycastTarget_8; }
	inline bool* get_address_of_m_RaycastTarget_8() { return &___m_RaycastTarget_8; }
	inline void set_m_RaycastTarget_8(bool value)
	{
		___m_RaycastTarget_8 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_9)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_9() const { return ___m_RectTransform_9; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_9() { return &___m_RectTransform_9; }
	inline void set_m_RectTransform_9(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_9), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRenderer_10)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRenderer_10() const { return ___m_CanvasRenderer_10; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRenderer_10() { return &___m_CanvasRenderer_10; }
	inline void set_m_CanvasRenderer_10(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_10), value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_11)); }
	inline Canvas_t3310196443 * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_t3310196443 * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_11), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_12)); }
	inline bool get_m_VertsDirty_12() const { return ___m_VertsDirty_12; }
	inline bool* get_address_of_m_VertsDirty_12() { return &___m_VertsDirty_12; }
	inline void set_m_VertsDirty_12(bool value)
	{
		___m_VertsDirty_12 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_13)); }
	inline bool get_m_MaterialDirty_13() const { return ___m_MaterialDirty_13; }
	inline bool* get_address_of_m_MaterialDirty_13() { return &___m_MaterialDirty_13; }
	inline void set_m_MaterialDirty_13(bool value)
	{
		___m_MaterialDirty_13 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_14() const { return ___m_OnDirtyLayoutCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_14() { return &___m_OnDirtyLayoutCallback_14; }
	inline void set_m_OnDirtyLayoutCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_14), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_15)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_15() const { return ___m_OnDirtyVertsCallback_15; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_15() { return &___m_OnDirtyVertsCallback_15; }
	inline void set_m_OnDirtyVertsCallback_15(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_15), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_16)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_16() const { return ___m_OnDirtyMaterialCallback_16; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_16() { return &___m_OnDirtyMaterialCallback_16; }
	inline void set_m_OnDirtyMaterialCallback_16(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_16), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_19() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_19)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_19() const { return ___m_ColorTweenRunner_19; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_19() { return &___m_ColorTweenRunner_19; }
	inline void set_m_ColorTweenRunner_19(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_19), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_20(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_20 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_17;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_18;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t340375123 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t340375123 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_4), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_5), value);
	}

	inline static int32_t get_offset_of_s_Mesh_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_17)); }
	inline Mesh_t3648964284 * get_s_Mesh_17() const { return ___s_Mesh_17; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_17() { return &___s_Mesh_17; }
	inline void set_s_Mesh_17(Mesh_t3648964284 * value)
	{
		___s_Mesh_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_17), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_18)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_18() const { return ___s_VertexHelper_18; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_18() { return &___s_VertexHelper_18; }
	inline void set_s_VertexHelper_18(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef SELECTABLE_T3250028441_H
#define SELECTABLE_T3250028441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t3250028441  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t3049316579  ___m_Navigation_5;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_6;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t2139031574  ___m_Colors_7;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t1362986479  ___m_SpriteState_8;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t2532145056 * ___m_AnimationTriggers_9;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_10;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t1660335611 * ___m_TargetGraphic_11;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_12;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_13;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_14;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_15;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_16;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t1260619206 * ___m_CanvasGroupCache_17;

public:
	inline static int32_t get_offset_of_m_Navigation_5() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Navigation_5)); }
	inline Navigation_t3049316579  get_m_Navigation_5() const { return ___m_Navigation_5; }
	inline Navigation_t3049316579 * get_address_of_m_Navigation_5() { return &___m_Navigation_5; }
	inline void set_m_Navigation_5(Navigation_t3049316579  value)
	{
		___m_Navigation_5 = value;
	}

	inline static int32_t get_offset_of_m_Transition_6() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Transition_6)); }
	inline int32_t get_m_Transition_6() const { return ___m_Transition_6; }
	inline int32_t* get_address_of_m_Transition_6() { return &___m_Transition_6; }
	inline void set_m_Transition_6(int32_t value)
	{
		___m_Transition_6 = value;
	}

	inline static int32_t get_offset_of_m_Colors_7() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Colors_7)); }
	inline ColorBlock_t2139031574  get_m_Colors_7() const { return ___m_Colors_7; }
	inline ColorBlock_t2139031574 * get_address_of_m_Colors_7() { return &___m_Colors_7; }
	inline void set_m_Colors_7(ColorBlock_t2139031574  value)
	{
		___m_Colors_7 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_8() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_SpriteState_8)); }
	inline SpriteState_t1362986479  get_m_SpriteState_8() const { return ___m_SpriteState_8; }
	inline SpriteState_t1362986479 * get_address_of_m_SpriteState_8() { return &___m_SpriteState_8; }
	inline void set_m_SpriteState_8(SpriteState_t1362986479  value)
	{
		___m_SpriteState_8 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_9() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_AnimationTriggers_9)); }
	inline AnimationTriggers_t2532145056 * get_m_AnimationTriggers_9() const { return ___m_AnimationTriggers_9; }
	inline AnimationTriggers_t2532145056 ** get_address_of_m_AnimationTriggers_9() { return &___m_AnimationTriggers_9; }
	inline void set_m_AnimationTriggers_9(AnimationTriggers_t2532145056 * value)
	{
		___m_AnimationTriggers_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_9), value);
	}

	inline static int32_t get_offset_of_m_Interactable_10() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Interactable_10)); }
	inline bool get_m_Interactable_10() const { return ___m_Interactable_10; }
	inline bool* get_address_of_m_Interactable_10() { return &___m_Interactable_10; }
	inline void set_m_Interactable_10(bool value)
	{
		___m_Interactable_10 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_11() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_TargetGraphic_11)); }
	inline Graphic_t1660335611 * get_m_TargetGraphic_11() const { return ___m_TargetGraphic_11; }
	inline Graphic_t1660335611 ** get_address_of_m_TargetGraphic_11() { return &___m_TargetGraphic_11; }
	inline void set_m_TargetGraphic_11(Graphic_t1660335611 * value)
	{
		___m_TargetGraphic_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_11), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_12() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_GroupsAllowInteraction_12)); }
	inline bool get_m_GroupsAllowInteraction_12() const { return ___m_GroupsAllowInteraction_12; }
	inline bool* get_address_of_m_GroupsAllowInteraction_12() { return &___m_GroupsAllowInteraction_12; }
	inline void set_m_GroupsAllowInteraction_12(bool value)
	{
		___m_GroupsAllowInteraction_12 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_13() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CurrentSelectionState_13)); }
	inline int32_t get_m_CurrentSelectionState_13() const { return ___m_CurrentSelectionState_13; }
	inline int32_t* get_address_of_m_CurrentSelectionState_13() { return &___m_CurrentSelectionState_13; }
	inline void set_m_CurrentSelectionState_13(int32_t value)
	{
		___m_CurrentSelectionState_13 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerInsideU3Ek__BackingField_14)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_14() const { return ___U3CisPointerInsideU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_14() { return &___U3CisPointerInsideU3Ek__BackingField_14; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_14(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerDownU3Ek__BackingField_15)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_15() const { return ___U3CisPointerDownU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_15() { return &___U3CisPointerDownU3Ek__BackingField_15; }
	inline void set_U3CisPointerDownU3Ek__BackingField_15(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3ChasSelectionU3Ek__BackingField_16)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_16() const { return ___U3ChasSelectionU3Ek__BackingField_16; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_16() { return &___U3ChasSelectionU3Ek__BackingField_16; }
	inline void set_U3ChasSelectionU3Ek__BackingField_16(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_17() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CanvasGroupCache_17)); }
	inline List_1_t1260619206 * get_m_CanvasGroupCache_17() const { return ___m_CanvasGroupCache_17; }
	inline List_1_t1260619206 ** get_address_of_m_CanvasGroupCache_17() { return &___m_CanvasGroupCache_17; }
	inline void set_m_CanvasGroupCache_17(List_1_t1260619206 * value)
	{
		___m_CanvasGroupCache_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_17), value);
	}
};

struct Selectable_t3250028441_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t427135887 * ___s_List_4;

public:
	inline static int32_t get_offset_of_s_List_4() { return static_cast<int32_t>(offsetof(Selectable_t3250028441_StaticFields, ___s_List_4)); }
	inline List_1_t427135887 * get_s_List_4() const { return ___s_List_4; }
	inline List_1_t427135887 ** get_address_of_s_List_4() { return &___s_List_4; }
	inline void set_s_List_4(List_1_t427135887 * value)
	{
		___s_List_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T3250028441_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_21;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_22;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_23;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_25;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_26;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_27;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_28;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_29;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_21)); }
	inline bool get_m_ShouldRecalculateStencil_21() const { return ___m_ShouldRecalculateStencil_21; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_21() { return &___m_ShouldRecalculateStencil_21; }
	inline void set_m_ShouldRecalculateStencil_21(bool value)
	{
		___m_ShouldRecalculateStencil_21 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_22)); }
	inline Material_t340375123 * get_m_MaskMaterial_22() const { return ___m_MaskMaterial_22; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_22() { return &___m_MaskMaterial_22; }
	inline void set_m_MaskMaterial_22(Material_t340375123 * value)
	{
		___m_MaskMaterial_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_22), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_23)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_23() const { return ___m_ParentMask_23; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_23() { return &___m_ParentMask_23; }
	inline void set_m_ParentMask_23(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_23), value);
	}

	inline static int32_t get_offset_of_m_Maskable_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_24)); }
	inline bool get_m_Maskable_24() const { return ___m_Maskable_24; }
	inline bool* get_address_of_m_Maskable_24() { return &___m_Maskable_24; }
	inline void set_m_Maskable_24(bool value)
	{
		___m_Maskable_24 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_25)); }
	inline bool get_m_IncludeForMasking_25() const { return ___m_IncludeForMasking_25; }
	inline bool* get_address_of_m_IncludeForMasking_25() { return &___m_IncludeForMasking_25; }
	inline void set_m_IncludeForMasking_25(bool value)
	{
		___m_IncludeForMasking_25 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_26)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_26() const { return ___m_OnCullStateChanged_26; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_26() { return &___m_OnCullStateChanged_26; }
	inline void set_m_OnCullStateChanged_26(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_26), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_27)); }
	inline bool get_m_ShouldRecalculate_27() const { return ___m_ShouldRecalculate_27; }
	inline bool* get_address_of_m_ShouldRecalculate_27() { return &___m_ShouldRecalculate_27; }
	inline void set_m_ShouldRecalculate_27(bool value)
	{
		___m_ShouldRecalculate_27 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_28)); }
	inline int32_t get_m_StencilValue_28() const { return ___m_StencilValue_28; }
	inline int32_t* get_address_of_m_StencilValue_28() { return &___m_StencilValue_28; }
	inline void set_m_StencilValue_28(int32_t value)
	{
		___m_StencilValue_28 = value;
	}

	inline static int32_t get_offset_of_m_Corners_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_29)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_29() const { return ___m_Corners_29; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_29() { return &___m_Corners_29; }
	inline void set_m_Corners_29(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef SLIDER_T3903728902_H
#define SLIDER_T3903728902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Slider
struct  Slider_t3903728902  : public Selectable_t3250028441
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_FillRect
	RectTransform_t3704657025 * ___m_FillRect_18;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_HandleRect
	RectTransform_t3704657025 * ___m_HandleRect_19;
	// UnityEngine.UI.Slider/Direction UnityEngine.UI.Slider::m_Direction
	int32_t ___m_Direction_20;
	// System.Single UnityEngine.UI.Slider::m_MinValue
	float ___m_MinValue_21;
	// System.Single UnityEngine.UI.Slider::m_MaxValue
	float ___m_MaxValue_22;
	// System.Boolean UnityEngine.UI.Slider::m_WholeNumbers
	bool ___m_WholeNumbers_23;
	// System.Single UnityEngine.UI.Slider::m_Value
	float ___m_Value_24;
	// UnityEngine.UI.Slider/SliderEvent UnityEngine.UI.Slider::m_OnValueChanged
	SliderEvent_t3180273144 * ___m_OnValueChanged_25;
	// UnityEngine.UI.Image UnityEngine.UI.Slider::m_FillImage
	Image_t2670269651 * ___m_FillImage_26;
	// UnityEngine.Transform UnityEngine.UI.Slider::m_FillTransform
	Transform_t3600365921 * ___m_FillTransform_27;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_FillContainerRect
	RectTransform_t3704657025 * ___m_FillContainerRect_28;
	// UnityEngine.Transform UnityEngine.UI.Slider::m_HandleTransform
	Transform_t3600365921 * ___m_HandleTransform_29;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_HandleContainerRect
	RectTransform_t3704657025 * ___m_HandleContainerRect_30;
	// UnityEngine.Vector2 UnityEngine.UI.Slider::m_Offset
	Vector2_t2156229523  ___m_Offset_31;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.Slider::m_Tracker
	DrivenRectTransformTracker_t2562230146  ___m_Tracker_32;

public:
	inline static int32_t get_offset_of_m_FillRect_18() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_FillRect_18)); }
	inline RectTransform_t3704657025 * get_m_FillRect_18() const { return ___m_FillRect_18; }
	inline RectTransform_t3704657025 ** get_address_of_m_FillRect_18() { return &___m_FillRect_18; }
	inline void set_m_FillRect_18(RectTransform_t3704657025 * value)
	{
		___m_FillRect_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_FillRect_18), value);
	}

	inline static int32_t get_offset_of_m_HandleRect_19() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_HandleRect_19)); }
	inline RectTransform_t3704657025 * get_m_HandleRect_19() const { return ___m_HandleRect_19; }
	inline RectTransform_t3704657025 ** get_address_of_m_HandleRect_19() { return &___m_HandleRect_19; }
	inline void set_m_HandleRect_19(RectTransform_t3704657025 * value)
	{
		___m_HandleRect_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleRect_19), value);
	}

	inline static int32_t get_offset_of_m_Direction_20() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_Direction_20)); }
	inline int32_t get_m_Direction_20() const { return ___m_Direction_20; }
	inline int32_t* get_address_of_m_Direction_20() { return &___m_Direction_20; }
	inline void set_m_Direction_20(int32_t value)
	{
		___m_Direction_20 = value;
	}

	inline static int32_t get_offset_of_m_MinValue_21() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_MinValue_21)); }
	inline float get_m_MinValue_21() const { return ___m_MinValue_21; }
	inline float* get_address_of_m_MinValue_21() { return &___m_MinValue_21; }
	inline void set_m_MinValue_21(float value)
	{
		___m_MinValue_21 = value;
	}

	inline static int32_t get_offset_of_m_MaxValue_22() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_MaxValue_22)); }
	inline float get_m_MaxValue_22() const { return ___m_MaxValue_22; }
	inline float* get_address_of_m_MaxValue_22() { return &___m_MaxValue_22; }
	inline void set_m_MaxValue_22(float value)
	{
		___m_MaxValue_22 = value;
	}

	inline static int32_t get_offset_of_m_WholeNumbers_23() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_WholeNumbers_23)); }
	inline bool get_m_WholeNumbers_23() const { return ___m_WholeNumbers_23; }
	inline bool* get_address_of_m_WholeNumbers_23() { return &___m_WholeNumbers_23; }
	inline void set_m_WholeNumbers_23(bool value)
	{
		___m_WholeNumbers_23 = value;
	}

	inline static int32_t get_offset_of_m_Value_24() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_Value_24)); }
	inline float get_m_Value_24() const { return ___m_Value_24; }
	inline float* get_address_of_m_Value_24() { return &___m_Value_24; }
	inline void set_m_Value_24(float value)
	{
		___m_Value_24 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_25() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_OnValueChanged_25)); }
	inline SliderEvent_t3180273144 * get_m_OnValueChanged_25() const { return ___m_OnValueChanged_25; }
	inline SliderEvent_t3180273144 ** get_address_of_m_OnValueChanged_25() { return &___m_OnValueChanged_25; }
	inline void set_m_OnValueChanged_25(SliderEvent_t3180273144 * value)
	{
		___m_OnValueChanged_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_25), value);
	}

	inline static int32_t get_offset_of_m_FillImage_26() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_FillImage_26)); }
	inline Image_t2670269651 * get_m_FillImage_26() const { return ___m_FillImage_26; }
	inline Image_t2670269651 ** get_address_of_m_FillImage_26() { return &___m_FillImage_26; }
	inline void set_m_FillImage_26(Image_t2670269651 * value)
	{
		___m_FillImage_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_FillImage_26), value);
	}

	inline static int32_t get_offset_of_m_FillTransform_27() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_FillTransform_27)); }
	inline Transform_t3600365921 * get_m_FillTransform_27() const { return ___m_FillTransform_27; }
	inline Transform_t3600365921 ** get_address_of_m_FillTransform_27() { return &___m_FillTransform_27; }
	inline void set_m_FillTransform_27(Transform_t3600365921 * value)
	{
		___m_FillTransform_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_FillTransform_27), value);
	}

	inline static int32_t get_offset_of_m_FillContainerRect_28() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_FillContainerRect_28)); }
	inline RectTransform_t3704657025 * get_m_FillContainerRect_28() const { return ___m_FillContainerRect_28; }
	inline RectTransform_t3704657025 ** get_address_of_m_FillContainerRect_28() { return &___m_FillContainerRect_28; }
	inline void set_m_FillContainerRect_28(RectTransform_t3704657025 * value)
	{
		___m_FillContainerRect_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_FillContainerRect_28), value);
	}

	inline static int32_t get_offset_of_m_HandleTransform_29() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_HandleTransform_29)); }
	inline Transform_t3600365921 * get_m_HandleTransform_29() const { return ___m_HandleTransform_29; }
	inline Transform_t3600365921 ** get_address_of_m_HandleTransform_29() { return &___m_HandleTransform_29; }
	inline void set_m_HandleTransform_29(Transform_t3600365921 * value)
	{
		___m_HandleTransform_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleTransform_29), value);
	}

	inline static int32_t get_offset_of_m_HandleContainerRect_30() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_HandleContainerRect_30)); }
	inline RectTransform_t3704657025 * get_m_HandleContainerRect_30() const { return ___m_HandleContainerRect_30; }
	inline RectTransform_t3704657025 ** get_address_of_m_HandleContainerRect_30() { return &___m_HandleContainerRect_30; }
	inline void set_m_HandleContainerRect_30(RectTransform_t3704657025 * value)
	{
		___m_HandleContainerRect_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleContainerRect_30), value);
	}

	inline static int32_t get_offset_of_m_Offset_31() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_Offset_31)); }
	inline Vector2_t2156229523  get_m_Offset_31() const { return ___m_Offset_31; }
	inline Vector2_t2156229523 * get_address_of_m_Offset_31() { return &___m_Offset_31; }
	inline void set_m_Offset_31(Vector2_t2156229523  value)
	{
		___m_Offset_31 = value;
	}

	inline static int32_t get_offset_of_m_Tracker_32() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_Tracker_32)); }
	inline DrivenRectTransformTracker_t2562230146  get_m_Tracker_32() const { return ___m_Tracker_32; }
	inline DrivenRectTransformTracker_t2562230146 * get_address_of_m_Tracker_32() { return &___m_Tracker_32; }
	inline void set_m_Tracker_32(DrivenRectTransformTracker_t2562230146  value)
	{
		___m_Tracker_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLIDER_T3903728902_H
#ifndef TMP_TEXT_T2599618874_H
#define TMP_TEXT_T2599618874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Text
struct  TMP_Text_t2599618874  : public MaskableGraphic_t3839221559
{
public:
	// System.String TMPro.TMP_Text::m_text
	String_t* ___m_text_30;
	// System.Boolean TMPro.TMP_Text::m_isRightToLeft
	bool ___m_isRightToLeft_31;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_fontAsset
	TMP_FontAsset_t364381626 * ___m_fontAsset_32;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_currentFontAsset
	TMP_FontAsset_t364381626 * ___m_currentFontAsset_33;
	// System.Boolean TMPro.TMP_Text::m_isSDFShader
	bool ___m_isSDFShader_34;
	// UnityEngine.Material TMPro.TMP_Text::m_sharedMaterial
	Material_t340375123 * ___m_sharedMaterial_35;
	// UnityEngine.Material TMPro.TMP_Text::m_currentMaterial
	Material_t340375123 * ___m_currentMaterial_36;
	// TMPro.MaterialReference[] TMPro.TMP_Text::m_materialReferences
	MaterialReferenceU5BU5D_t648826345* ___m_materialReferences_37;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_Text::m_materialReferenceIndexLookup
	Dictionary_2_t1839659084 * ___m_materialReferenceIndexLookup_38;
	// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference> TMPro.TMP_Text::m_materialReferenceStack
	TMP_XmlTagStack_1_t1515999176  ___m_materialReferenceStack_39;
	// System.Int32 TMPro.TMP_Text::m_currentMaterialIndex
	int32_t ___m_currentMaterialIndex_40;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontSharedMaterials
	MaterialU5BU5D_t561872642* ___m_fontSharedMaterials_41;
	// UnityEngine.Material TMPro.TMP_Text::m_fontMaterial
	Material_t340375123 * ___m_fontMaterial_42;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontMaterials
	MaterialU5BU5D_t561872642* ___m_fontMaterials_43;
	// System.Boolean TMPro.TMP_Text::m_isMaterialDirty
	bool ___m_isMaterialDirty_44;
	// UnityEngine.Color32 TMPro.TMP_Text::m_fontColor32
	Color32_t2600501292  ___m_fontColor32_45;
	// UnityEngine.Color TMPro.TMP_Text::m_fontColor
	Color_t2555686324  ___m_fontColor_46;
	// UnityEngine.Color32 TMPro.TMP_Text::m_underlineColor
	Color32_t2600501292  ___m_underlineColor_48;
	// UnityEngine.Color32 TMPro.TMP_Text::m_strikethroughColor
	Color32_t2600501292  ___m_strikethroughColor_49;
	// UnityEngine.Color32 TMPro.TMP_Text::m_highlightColor
	Color32_t2600501292  ___m_highlightColor_50;
	// System.Boolean TMPro.TMP_Text::m_enableVertexGradient
	bool ___m_enableVertexGradient_51;
	// TMPro.VertexGradient TMPro.TMP_Text::m_fontColorGradient
	VertexGradient_t345148380  ___m_fontColorGradient_52;
	// TMPro.TMP_ColorGradient TMPro.TMP_Text::m_fontColorGradientPreset
	TMP_ColorGradient_t3678055768 * ___m_fontColorGradientPreset_53;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_spriteAsset
	TMP_SpriteAsset_t484820633 * ___m_spriteAsset_54;
	// System.Boolean TMPro.TMP_Text::m_tintAllSprites
	bool ___m_tintAllSprites_55;
	// System.Boolean TMPro.TMP_Text::m_tintSprite
	bool ___m_tintSprite_56;
	// UnityEngine.Color32 TMPro.TMP_Text::m_spriteColor
	Color32_t2600501292  ___m_spriteColor_57;
	// System.Boolean TMPro.TMP_Text::m_overrideHtmlColors
	bool ___m_overrideHtmlColors_58;
	// UnityEngine.Color32 TMPro.TMP_Text::m_faceColor
	Color32_t2600501292  ___m_faceColor_59;
	// UnityEngine.Color32 TMPro.TMP_Text::m_outlineColor
	Color32_t2600501292  ___m_outlineColor_60;
	// System.Single TMPro.TMP_Text::m_outlineWidth
	float ___m_outlineWidth_61;
	// System.Single TMPro.TMP_Text::m_fontSize
	float ___m_fontSize_62;
	// System.Single TMPro.TMP_Text::m_currentFontSize
	float ___m_currentFontSize_63;
	// System.Single TMPro.TMP_Text::m_fontSizeBase
	float ___m_fontSizeBase_64;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.TMP_Text::m_sizeStack
	TMP_XmlTagStack_1_t960921318  ___m_sizeStack_65;
	// System.Int32 TMPro.TMP_Text::m_fontWeight
	int32_t ___m_fontWeight_66;
	// System.Int32 TMPro.TMP_Text::m_fontWeightInternal
	int32_t ___m_fontWeightInternal_67;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.TMP_Text::m_fontWeightStack
	TMP_XmlTagStack_1_t2514600297  ___m_fontWeightStack_68;
	// System.Boolean TMPro.TMP_Text::m_enableAutoSizing
	bool ___m_enableAutoSizing_69;
	// System.Single TMPro.TMP_Text::m_maxFontSize
	float ___m_maxFontSize_70;
	// System.Single TMPro.TMP_Text::m_minFontSize
	float ___m_minFontSize_71;
	// System.Single TMPro.TMP_Text::m_fontSizeMin
	float ___m_fontSizeMin_72;
	// System.Single TMPro.TMP_Text::m_fontSizeMax
	float ___m_fontSizeMax_73;
	// TMPro.FontStyles TMPro.TMP_Text::m_fontStyle
	int32_t ___m_fontStyle_74;
	// TMPro.FontStyles TMPro.TMP_Text::m_style
	int32_t ___m_style_75;
	// TMPro.TMP_BasicXmlTagStack TMPro.TMP_Text::m_fontStyleStack
	TMP_BasicXmlTagStack_t2962628096  ___m_fontStyleStack_76;
	// System.Boolean TMPro.TMP_Text::m_isUsingBold
	bool ___m_isUsingBold_77;
	// TMPro.TextAlignmentOptions TMPro.TMP_Text::m_textAlignment
	int32_t ___m_textAlignment_78;
	// TMPro.TextAlignmentOptions TMPro.TMP_Text::m_lineJustification
	int32_t ___m_lineJustification_79;
	// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions> TMPro.TMP_Text::m_lineJustificationStack
	TMP_XmlTagStack_1_t3600445780  ___m_lineJustificationStack_80;
	// UnityEngine.Vector3[] TMPro.TMP_Text::m_textContainerLocalCorners
	Vector3U5BU5D_t1718750761* ___m_textContainerLocalCorners_81;
	// System.Boolean TMPro.TMP_Text::m_isAlignmentEnumConverted
	bool ___m_isAlignmentEnumConverted_82;
	// System.Single TMPro.TMP_Text::m_characterSpacing
	float ___m_characterSpacing_83;
	// System.Single TMPro.TMP_Text::m_cSpacing
	float ___m_cSpacing_84;
	// System.Single TMPro.TMP_Text::m_monoSpacing
	float ___m_monoSpacing_85;
	// System.Single TMPro.TMP_Text::m_wordSpacing
	float ___m_wordSpacing_86;
	// System.Single TMPro.TMP_Text::m_lineSpacing
	float ___m_lineSpacing_87;
	// System.Single TMPro.TMP_Text::m_lineSpacingDelta
	float ___m_lineSpacingDelta_88;
	// System.Single TMPro.TMP_Text::m_lineHeight
	float ___m_lineHeight_89;
	// System.Single TMPro.TMP_Text::m_lineSpacingMax
	float ___m_lineSpacingMax_90;
	// System.Single TMPro.TMP_Text::m_paragraphSpacing
	float ___m_paragraphSpacing_91;
	// System.Single TMPro.TMP_Text::m_charWidthMaxAdj
	float ___m_charWidthMaxAdj_92;
	// System.Single TMPro.TMP_Text::m_charWidthAdjDelta
	float ___m_charWidthAdjDelta_93;
	// System.Boolean TMPro.TMP_Text::m_enableWordWrapping
	bool ___m_enableWordWrapping_94;
	// System.Boolean TMPro.TMP_Text::m_isCharacterWrappingEnabled
	bool ___m_isCharacterWrappingEnabled_95;
	// System.Boolean TMPro.TMP_Text::m_isNonBreakingSpace
	bool ___m_isNonBreakingSpace_96;
	// System.Boolean TMPro.TMP_Text::m_isIgnoringAlignment
	bool ___m_isIgnoringAlignment_97;
	// System.Single TMPro.TMP_Text::m_wordWrappingRatios
	float ___m_wordWrappingRatios_98;
	// TMPro.TextOverflowModes TMPro.TMP_Text::m_overflowMode
	int32_t ___m_overflowMode_99;
	// System.Int32 TMPro.TMP_Text::m_firstOverflowCharacterIndex
	int32_t ___m_firstOverflowCharacterIndex_100;
	// TMPro.TMP_Text TMPro.TMP_Text::m_linkedTextComponent
	TMP_Text_t2599618874 * ___m_linkedTextComponent_101;
	// System.Boolean TMPro.TMP_Text::m_isLinkedTextComponent
	bool ___m_isLinkedTextComponent_102;
	// System.Boolean TMPro.TMP_Text::m_isTextTruncated
	bool ___m_isTextTruncated_103;
	// System.Boolean TMPro.TMP_Text::m_enableKerning
	bool ___m_enableKerning_104;
	// System.Boolean TMPro.TMP_Text::m_enableExtraPadding
	bool ___m_enableExtraPadding_105;
	// System.Boolean TMPro.TMP_Text::checkPaddingRequired
	bool ___checkPaddingRequired_106;
	// System.Boolean TMPro.TMP_Text::m_isRichText
	bool ___m_isRichText_107;
	// System.Boolean TMPro.TMP_Text::m_parseCtrlCharacters
	bool ___m_parseCtrlCharacters_108;
	// System.Boolean TMPro.TMP_Text::m_isOverlay
	bool ___m_isOverlay_109;
	// System.Boolean TMPro.TMP_Text::m_isOrthographic
	bool ___m_isOrthographic_110;
	// System.Boolean TMPro.TMP_Text::m_isCullingEnabled
	bool ___m_isCullingEnabled_111;
	// System.Boolean TMPro.TMP_Text::m_ignoreRectMaskCulling
	bool ___m_ignoreRectMaskCulling_112;
	// System.Boolean TMPro.TMP_Text::m_ignoreCulling
	bool ___m_ignoreCulling_113;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_horizontalMapping
	int32_t ___m_horizontalMapping_114;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_verticalMapping
	int32_t ___m_verticalMapping_115;
	// System.Single TMPro.TMP_Text::m_uvLineOffset
	float ___m_uvLineOffset_116;
	// TMPro.TextRenderFlags TMPro.TMP_Text::m_renderMode
	int32_t ___m_renderMode_117;
	// TMPro.VertexSortingOrder TMPro.TMP_Text::m_geometrySortingOrder
	int32_t ___m_geometrySortingOrder_118;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacter
	int32_t ___m_firstVisibleCharacter_119;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleCharacters
	int32_t ___m_maxVisibleCharacters_120;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleWords
	int32_t ___m_maxVisibleWords_121;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleLines
	int32_t ___m_maxVisibleLines_122;
	// System.Boolean TMPro.TMP_Text::m_useMaxVisibleDescender
	bool ___m_useMaxVisibleDescender_123;
	// System.Int32 TMPro.TMP_Text::m_pageToDisplay
	int32_t ___m_pageToDisplay_124;
	// System.Boolean TMPro.TMP_Text::m_isNewPage
	bool ___m_isNewPage_125;
	// UnityEngine.Vector4 TMPro.TMP_Text::m_margin
	Vector4_t3319028937  ___m_margin_126;
	// System.Single TMPro.TMP_Text::m_marginLeft
	float ___m_marginLeft_127;
	// System.Single TMPro.TMP_Text::m_marginRight
	float ___m_marginRight_128;
	// System.Single TMPro.TMP_Text::m_marginWidth
	float ___m_marginWidth_129;
	// System.Single TMPro.TMP_Text::m_marginHeight
	float ___m_marginHeight_130;
	// System.Single TMPro.TMP_Text::m_width
	float ___m_width_131;
	// TMPro.TMP_TextInfo TMPro.TMP_Text::m_textInfo
	TMP_TextInfo_t3598145122 * ___m_textInfo_132;
	// System.Boolean TMPro.TMP_Text::m_havePropertiesChanged
	bool ___m_havePropertiesChanged_133;
	// System.Boolean TMPro.TMP_Text::m_isUsingLegacyAnimationComponent
	bool ___m_isUsingLegacyAnimationComponent_134;
	// UnityEngine.Transform TMPro.TMP_Text::m_transform
	Transform_t3600365921 * ___m_transform_135;
	// UnityEngine.RectTransform TMPro.TMP_Text::m_rectTransform
	RectTransform_t3704657025 * ___m_rectTransform_136;
	// System.Boolean TMPro.TMP_Text::<autoSizeTextContainer>k__BackingField
	bool ___U3CautoSizeTextContainerU3Ek__BackingField_137;
	// System.Boolean TMPro.TMP_Text::m_autoSizeTextContainer
	bool ___m_autoSizeTextContainer_138;
	// UnityEngine.Mesh TMPro.TMP_Text::m_mesh
	Mesh_t3648964284 * ___m_mesh_139;
	// System.Boolean TMPro.TMP_Text::m_isVolumetricText
	bool ___m_isVolumetricText_140;
	// TMPro.TMP_SpriteAnimator TMPro.TMP_Text::m_spriteAnimator
	TMP_SpriteAnimator_t2836635477 * ___m_spriteAnimator_141;
	// System.Single TMPro.TMP_Text::m_flexibleHeight
	float ___m_flexibleHeight_142;
	// System.Single TMPro.TMP_Text::m_flexibleWidth
	float ___m_flexibleWidth_143;
	// System.Single TMPro.TMP_Text::m_minWidth
	float ___m_minWidth_144;
	// System.Single TMPro.TMP_Text::m_minHeight
	float ___m_minHeight_145;
	// System.Single TMPro.TMP_Text::m_maxWidth
	float ___m_maxWidth_146;
	// System.Single TMPro.TMP_Text::m_maxHeight
	float ___m_maxHeight_147;
	// UnityEngine.UI.LayoutElement TMPro.TMP_Text::m_LayoutElement
	LayoutElement_t1785403678 * ___m_LayoutElement_148;
	// System.Single TMPro.TMP_Text::m_preferredWidth
	float ___m_preferredWidth_149;
	// System.Single TMPro.TMP_Text::m_renderedWidth
	float ___m_renderedWidth_150;
	// System.Boolean TMPro.TMP_Text::m_isPreferredWidthDirty
	bool ___m_isPreferredWidthDirty_151;
	// System.Single TMPro.TMP_Text::m_preferredHeight
	float ___m_preferredHeight_152;
	// System.Single TMPro.TMP_Text::m_renderedHeight
	float ___m_renderedHeight_153;
	// System.Boolean TMPro.TMP_Text::m_isPreferredHeightDirty
	bool ___m_isPreferredHeightDirty_154;
	// System.Boolean TMPro.TMP_Text::m_isCalculatingPreferredValues
	bool ___m_isCalculatingPreferredValues_155;
	// System.Int32 TMPro.TMP_Text::m_recursiveCount
	int32_t ___m_recursiveCount_156;
	// System.Int32 TMPro.TMP_Text::m_layoutPriority
	int32_t ___m_layoutPriority_157;
	// System.Boolean TMPro.TMP_Text::m_isCalculateSizeRequired
	bool ___m_isCalculateSizeRequired_158;
	// System.Boolean TMPro.TMP_Text::m_isLayoutDirty
	bool ___m_isLayoutDirty_159;
	// System.Boolean TMPro.TMP_Text::m_verticesAlreadyDirty
	bool ___m_verticesAlreadyDirty_160;
	// System.Boolean TMPro.TMP_Text::m_layoutAlreadyDirty
	bool ___m_layoutAlreadyDirty_161;
	// System.Boolean TMPro.TMP_Text::m_isAwake
	bool ___m_isAwake_162;
	// System.Boolean TMPro.TMP_Text::m_isWaitingOnResourceLoad
	bool ___m_isWaitingOnResourceLoad_163;
	// System.Boolean TMPro.TMP_Text::m_isInputParsingRequired
	bool ___m_isInputParsingRequired_164;
	// TMPro.TMP_Text/TextInputSources TMPro.TMP_Text::m_inputSource
	int32_t ___m_inputSource_165;
	// System.String TMPro.TMP_Text::old_text
	String_t* ___old_text_166;
	// System.Single TMPro.TMP_Text::m_fontScale
	float ___m_fontScale_167;
	// System.Single TMPro.TMP_Text::m_fontScaleMultiplier
	float ___m_fontScaleMultiplier_168;
	// System.Char[] TMPro.TMP_Text::m_htmlTag
	CharU5BU5D_t3528271667* ___m_htmlTag_169;
	// TMPro.XML_TagAttribute[] TMPro.TMP_Text::m_xmlAttribute
	XML_TagAttributeU5BU5D_t284240280* ___m_xmlAttribute_170;
	// System.Single[] TMPro.TMP_Text::m_attributeParameterValues
	SingleU5BU5D_t1444911251* ___m_attributeParameterValues_171;
	// System.Single TMPro.TMP_Text::tag_LineIndent
	float ___tag_LineIndent_172;
	// System.Single TMPro.TMP_Text::tag_Indent
	float ___tag_Indent_173;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.TMP_Text::m_indentStack
	TMP_XmlTagStack_1_t960921318  ___m_indentStack_174;
	// System.Boolean TMPro.TMP_Text::tag_NoParsing
	bool ___tag_NoParsing_175;
	// System.Boolean TMPro.TMP_Text::m_isParsingText
	bool ___m_isParsingText_176;
	// UnityEngine.Matrix4x4 TMPro.TMP_Text::m_FXMatrix
	Matrix4x4_t1817901843  ___m_FXMatrix_177;
	// System.Boolean TMPro.TMP_Text::m_isFXMatrixSet
	bool ___m_isFXMatrixSet_178;
	// System.Int32[] TMPro.TMP_Text::m_char_buffer
	Int32U5BU5D_t385246372* ___m_char_buffer_179;
	// TMPro.TMP_CharacterInfo[] TMPro.TMP_Text::m_internalCharacterInfo
	TMP_CharacterInfoU5BU5D_t1930184704* ___m_internalCharacterInfo_180;
	// System.Char[] TMPro.TMP_Text::m_input_CharArray
	CharU5BU5D_t3528271667* ___m_input_CharArray_181;
	// System.Int32 TMPro.TMP_Text::m_charArray_Length
	int32_t ___m_charArray_Length_182;
	// System.Int32 TMPro.TMP_Text::m_totalCharacterCount
	int32_t ___m_totalCharacterCount_183;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedWordWrapState
	WordWrapState_t341939652  ___m_SavedWordWrapState_184;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedLineState
	WordWrapState_t341939652  ___m_SavedLineState_185;
	// System.Int32 TMPro.TMP_Text::m_characterCount
	int32_t ___m_characterCount_186;
	// System.Int32 TMPro.TMP_Text::m_firstCharacterOfLine
	int32_t ___m_firstCharacterOfLine_187;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacterOfLine
	int32_t ___m_firstVisibleCharacterOfLine_188;
	// System.Int32 TMPro.TMP_Text::m_lastCharacterOfLine
	int32_t ___m_lastCharacterOfLine_189;
	// System.Int32 TMPro.TMP_Text::m_lastVisibleCharacterOfLine
	int32_t ___m_lastVisibleCharacterOfLine_190;
	// System.Int32 TMPro.TMP_Text::m_lineNumber
	int32_t ___m_lineNumber_191;
	// System.Int32 TMPro.TMP_Text::m_lineVisibleCharacterCount
	int32_t ___m_lineVisibleCharacterCount_192;
	// System.Int32 TMPro.TMP_Text::m_pageNumber
	int32_t ___m_pageNumber_193;
	// System.Single TMPro.TMP_Text::m_maxAscender
	float ___m_maxAscender_194;
	// System.Single TMPro.TMP_Text::m_maxCapHeight
	float ___m_maxCapHeight_195;
	// System.Single TMPro.TMP_Text::m_maxDescender
	float ___m_maxDescender_196;
	// System.Single TMPro.TMP_Text::m_maxLineAscender
	float ___m_maxLineAscender_197;
	// System.Single TMPro.TMP_Text::m_maxLineDescender
	float ___m_maxLineDescender_198;
	// System.Single TMPro.TMP_Text::m_startOfLineAscender
	float ___m_startOfLineAscender_199;
	// System.Single TMPro.TMP_Text::m_lineOffset
	float ___m_lineOffset_200;
	// TMPro.Extents TMPro.TMP_Text::m_meshExtents
	Extents_t3837212874  ___m_meshExtents_201;
	// UnityEngine.Color32 TMPro.TMP_Text::m_htmlColor
	Color32_t2600501292  ___m_htmlColor_202;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_colorStack
	TMP_XmlTagStack_1_t2164155836  ___m_colorStack_203;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_underlineColorStack
	TMP_XmlTagStack_1_t2164155836  ___m_underlineColorStack_204;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_strikethroughColorStack
	TMP_XmlTagStack_1_t2164155836  ___m_strikethroughColorStack_205;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_highlightColorStack
	TMP_XmlTagStack_1_t2164155836  ___m_highlightColorStack_206;
	// TMPro.TMP_ColorGradient TMPro.TMP_Text::m_colorGradientPreset
	TMP_ColorGradient_t3678055768 * ___m_colorGradientPreset_207;
	// TMPro.TMP_XmlTagStack`1<TMPro.TMP_ColorGradient> TMPro.TMP_Text::m_colorGradientStack
	TMP_XmlTagStack_1_t3241710312  ___m_colorGradientStack_208;
	// System.Single TMPro.TMP_Text::m_tabSpacing
	float ___m_tabSpacing_209;
	// System.Single TMPro.TMP_Text::m_spacing
	float ___m_spacing_210;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.TMP_Text::m_styleStack
	TMP_XmlTagStack_1_t2514600297  ___m_styleStack_211;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.TMP_Text::m_actionStack
	TMP_XmlTagStack_1_t2514600297  ___m_actionStack_212;
	// System.Single TMPro.TMP_Text::m_padding
	float ___m_padding_213;
	// System.Single TMPro.TMP_Text::m_baselineOffset
	float ___m_baselineOffset_214;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.TMP_Text::m_baselineOffsetStack
	TMP_XmlTagStack_1_t960921318  ___m_baselineOffsetStack_215;
	// System.Single TMPro.TMP_Text::m_xAdvance
	float ___m_xAdvance_216;
	// TMPro.TMP_TextElementType TMPro.TMP_Text::m_textElementType
	int32_t ___m_textElementType_217;
	// TMPro.TMP_TextElement TMPro.TMP_Text::m_cached_TextElement
	TMP_TextElement_t129727469 * ___m_cached_TextElement_218;
	// TMPro.TMP_Glyph TMPro.TMP_Text::m_cached_Underline_GlyphInfo
	TMP_Glyph_t581847833 * ___m_cached_Underline_GlyphInfo_219;
	// TMPro.TMP_Glyph TMPro.TMP_Text::m_cached_Ellipsis_GlyphInfo
	TMP_Glyph_t581847833 * ___m_cached_Ellipsis_GlyphInfo_220;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_defaultSpriteAsset
	TMP_SpriteAsset_t484820633 * ___m_defaultSpriteAsset_221;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_currentSpriteAsset
	TMP_SpriteAsset_t484820633 * ___m_currentSpriteAsset_222;
	// System.Int32 TMPro.TMP_Text::m_spriteCount
	int32_t ___m_spriteCount_223;
	// System.Int32 TMPro.TMP_Text::m_spriteIndex
	int32_t ___m_spriteIndex_224;
	// System.Int32 TMPro.TMP_Text::m_spriteAnimationID
	int32_t ___m_spriteAnimationID_225;
	// System.Boolean TMPro.TMP_Text::m_ignoreActiveState
	bool ___m_ignoreActiveState_226;
	// System.Single[] TMPro.TMP_Text::k_Power
	SingleU5BU5D_t1444911251* ___k_Power_227;

public:
	inline static int32_t get_offset_of_m_text_30() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_text_30)); }
	inline String_t* get_m_text_30() const { return ___m_text_30; }
	inline String_t** get_address_of_m_text_30() { return &___m_text_30; }
	inline void set_m_text_30(String_t* value)
	{
		___m_text_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_text_30), value);
	}

	inline static int32_t get_offset_of_m_isRightToLeft_31() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isRightToLeft_31)); }
	inline bool get_m_isRightToLeft_31() const { return ___m_isRightToLeft_31; }
	inline bool* get_address_of_m_isRightToLeft_31() { return &___m_isRightToLeft_31; }
	inline void set_m_isRightToLeft_31(bool value)
	{
		___m_isRightToLeft_31 = value;
	}

	inline static int32_t get_offset_of_m_fontAsset_32() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontAsset_32)); }
	inline TMP_FontAsset_t364381626 * get_m_fontAsset_32() const { return ___m_fontAsset_32; }
	inline TMP_FontAsset_t364381626 ** get_address_of_m_fontAsset_32() { return &___m_fontAsset_32; }
	inline void set_m_fontAsset_32(TMP_FontAsset_t364381626 * value)
	{
		___m_fontAsset_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontAsset_32), value);
	}

	inline static int32_t get_offset_of_m_currentFontAsset_33() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_currentFontAsset_33)); }
	inline TMP_FontAsset_t364381626 * get_m_currentFontAsset_33() const { return ___m_currentFontAsset_33; }
	inline TMP_FontAsset_t364381626 ** get_address_of_m_currentFontAsset_33() { return &___m_currentFontAsset_33; }
	inline void set_m_currentFontAsset_33(TMP_FontAsset_t364381626 * value)
	{
		___m_currentFontAsset_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentFontAsset_33), value);
	}

	inline static int32_t get_offset_of_m_isSDFShader_34() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isSDFShader_34)); }
	inline bool get_m_isSDFShader_34() const { return ___m_isSDFShader_34; }
	inline bool* get_address_of_m_isSDFShader_34() { return &___m_isSDFShader_34; }
	inline void set_m_isSDFShader_34(bool value)
	{
		___m_isSDFShader_34 = value;
	}

	inline static int32_t get_offset_of_m_sharedMaterial_35() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_sharedMaterial_35)); }
	inline Material_t340375123 * get_m_sharedMaterial_35() const { return ___m_sharedMaterial_35; }
	inline Material_t340375123 ** get_address_of_m_sharedMaterial_35() { return &___m_sharedMaterial_35; }
	inline void set_m_sharedMaterial_35(Material_t340375123 * value)
	{
		___m_sharedMaterial_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_sharedMaterial_35), value);
	}

	inline static int32_t get_offset_of_m_currentMaterial_36() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_currentMaterial_36)); }
	inline Material_t340375123 * get_m_currentMaterial_36() const { return ___m_currentMaterial_36; }
	inline Material_t340375123 ** get_address_of_m_currentMaterial_36() { return &___m_currentMaterial_36; }
	inline void set_m_currentMaterial_36(Material_t340375123 * value)
	{
		___m_currentMaterial_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentMaterial_36), value);
	}

	inline static int32_t get_offset_of_m_materialReferences_37() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_materialReferences_37)); }
	inline MaterialReferenceU5BU5D_t648826345* get_m_materialReferences_37() const { return ___m_materialReferences_37; }
	inline MaterialReferenceU5BU5D_t648826345** get_address_of_m_materialReferences_37() { return &___m_materialReferences_37; }
	inline void set_m_materialReferences_37(MaterialReferenceU5BU5D_t648826345* value)
	{
		___m_materialReferences_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialReferences_37), value);
	}

	inline static int32_t get_offset_of_m_materialReferenceIndexLookup_38() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_materialReferenceIndexLookup_38)); }
	inline Dictionary_2_t1839659084 * get_m_materialReferenceIndexLookup_38() const { return ___m_materialReferenceIndexLookup_38; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_materialReferenceIndexLookup_38() { return &___m_materialReferenceIndexLookup_38; }
	inline void set_m_materialReferenceIndexLookup_38(Dictionary_2_t1839659084 * value)
	{
		___m_materialReferenceIndexLookup_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialReferenceIndexLookup_38), value);
	}

	inline static int32_t get_offset_of_m_materialReferenceStack_39() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_materialReferenceStack_39)); }
	inline TMP_XmlTagStack_1_t1515999176  get_m_materialReferenceStack_39() const { return ___m_materialReferenceStack_39; }
	inline TMP_XmlTagStack_1_t1515999176 * get_address_of_m_materialReferenceStack_39() { return &___m_materialReferenceStack_39; }
	inline void set_m_materialReferenceStack_39(TMP_XmlTagStack_1_t1515999176  value)
	{
		___m_materialReferenceStack_39 = value;
	}

	inline static int32_t get_offset_of_m_currentMaterialIndex_40() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_currentMaterialIndex_40)); }
	inline int32_t get_m_currentMaterialIndex_40() const { return ___m_currentMaterialIndex_40; }
	inline int32_t* get_address_of_m_currentMaterialIndex_40() { return &___m_currentMaterialIndex_40; }
	inline void set_m_currentMaterialIndex_40(int32_t value)
	{
		___m_currentMaterialIndex_40 = value;
	}

	inline static int32_t get_offset_of_m_fontSharedMaterials_41() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontSharedMaterials_41)); }
	inline MaterialU5BU5D_t561872642* get_m_fontSharedMaterials_41() const { return ___m_fontSharedMaterials_41; }
	inline MaterialU5BU5D_t561872642** get_address_of_m_fontSharedMaterials_41() { return &___m_fontSharedMaterials_41; }
	inline void set_m_fontSharedMaterials_41(MaterialU5BU5D_t561872642* value)
	{
		___m_fontSharedMaterials_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontSharedMaterials_41), value);
	}

	inline static int32_t get_offset_of_m_fontMaterial_42() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontMaterial_42)); }
	inline Material_t340375123 * get_m_fontMaterial_42() const { return ___m_fontMaterial_42; }
	inline Material_t340375123 ** get_address_of_m_fontMaterial_42() { return &___m_fontMaterial_42; }
	inline void set_m_fontMaterial_42(Material_t340375123 * value)
	{
		___m_fontMaterial_42 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontMaterial_42), value);
	}

	inline static int32_t get_offset_of_m_fontMaterials_43() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontMaterials_43)); }
	inline MaterialU5BU5D_t561872642* get_m_fontMaterials_43() const { return ___m_fontMaterials_43; }
	inline MaterialU5BU5D_t561872642** get_address_of_m_fontMaterials_43() { return &___m_fontMaterials_43; }
	inline void set_m_fontMaterials_43(MaterialU5BU5D_t561872642* value)
	{
		___m_fontMaterials_43 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontMaterials_43), value);
	}

	inline static int32_t get_offset_of_m_isMaterialDirty_44() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isMaterialDirty_44)); }
	inline bool get_m_isMaterialDirty_44() const { return ___m_isMaterialDirty_44; }
	inline bool* get_address_of_m_isMaterialDirty_44() { return &___m_isMaterialDirty_44; }
	inline void set_m_isMaterialDirty_44(bool value)
	{
		___m_isMaterialDirty_44 = value;
	}

	inline static int32_t get_offset_of_m_fontColor32_45() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontColor32_45)); }
	inline Color32_t2600501292  get_m_fontColor32_45() const { return ___m_fontColor32_45; }
	inline Color32_t2600501292 * get_address_of_m_fontColor32_45() { return &___m_fontColor32_45; }
	inline void set_m_fontColor32_45(Color32_t2600501292  value)
	{
		___m_fontColor32_45 = value;
	}

	inline static int32_t get_offset_of_m_fontColor_46() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontColor_46)); }
	inline Color_t2555686324  get_m_fontColor_46() const { return ___m_fontColor_46; }
	inline Color_t2555686324 * get_address_of_m_fontColor_46() { return &___m_fontColor_46; }
	inline void set_m_fontColor_46(Color_t2555686324  value)
	{
		___m_fontColor_46 = value;
	}

	inline static int32_t get_offset_of_m_underlineColor_48() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_underlineColor_48)); }
	inline Color32_t2600501292  get_m_underlineColor_48() const { return ___m_underlineColor_48; }
	inline Color32_t2600501292 * get_address_of_m_underlineColor_48() { return &___m_underlineColor_48; }
	inline void set_m_underlineColor_48(Color32_t2600501292  value)
	{
		___m_underlineColor_48 = value;
	}

	inline static int32_t get_offset_of_m_strikethroughColor_49() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_strikethroughColor_49)); }
	inline Color32_t2600501292  get_m_strikethroughColor_49() const { return ___m_strikethroughColor_49; }
	inline Color32_t2600501292 * get_address_of_m_strikethroughColor_49() { return &___m_strikethroughColor_49; }
	inline void set_m_strikethroughColor_49(Color32_t2600501292  value)
	{
		___m_strikethroughColor_49 = value;
	}

	inline static int32_t get_offset_of_m_highlightColor_50() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_highlightColor_50)); }
	inline Color32_t2600501292  get_m_highlightColor_50() const { return ___m_highlightColor_50; }
	inline Color32_t2600501292 * get_address_of_m_highlightColor_50() { return &___m_highlightColor_50; }
	inline void set_m_highlightColor_50(Color32_t2600501292  value)
	{
		___m_highlightColor_50 = value;
	}

	inline static int32_t get_offset_of_m_enableVertexGradient_51() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_enableVertexGradient_51)); }
	inline bool get_m_enableVertexGradient_51() const { return ___m_enableVertexGradient_51; }
	inline bool* get_address_of_m_enableVertexGradient_51() { return &___m_enableVertexGradient_51; }
	inline void set_m_enableVertexGradient_51(bool value)
	{
		___m_enableVertexGradient_51 = value;
	}

	inline static int32_t get_offset_of_m_fontColorGradient_52() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontColorGradient_52)); }
	inline VertexGradient_t345148380  get_m_fontColorGradient_52() const { return ___m_fontColorGradient_52; }
	inline VertexGradient_t345148380 * get_address_of_m_fontColorGradient_52() { return &___m_fontColorGradient_52; }
	inline void set_m_fontColorGradient_52(VertexGradient_t345148380  value)
	{
		___m_fontColorGradient_52 = value;
	}

	inline static int32_t get_offset_of_m_fontColorGradientPreset_53() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontColorGradientPreset_53)); }
	inline TMP_ColorGradient_t3678055768 * get_m_fontColorGradientPreset_53() const { return ___m_fontColorGradientPreset_53; }
	inline TMP_ColorGradient_t3678055768 ** get_address_of_m_fontColorGradientPreset_53() { return &___m_fontColorGradientPreset_53; }
	inline void set_m_fontColorGradientPreset_53(TMP_ColorGradient_t3678055768 * value)
	{
		___m_fontColorGradientPreset_53 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontColorGradientPreset_53), value);
	}

	inline static int32_t get_offset_of_m_spriteAsset_54() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteAsset_54)); }
	inline TMP_SpriteAsset_t484820633 * get_m_spriteAsset_54() const { return ___m_spriteAsset_54; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_spriteAsset_54() { return &___m_spriteAsset_54; }
	inline void set_m_spriteAsset_54(TMP_SpriteAsset_t484820633 * value)
	{
		___m_spriteAsset_54 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAsset_54), value);
	}

	inline static int32_t get_offset_of_m_tintAllSprites_55() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_tintAllSprites_55)); }
	inline bool get_m_tintAllSprites_55() const { return ___m_tintAllSprites_55; }
	inline bool* get_address_of_m_tintAllSprites_55() { return &___m_tintAllSprites_55; }
	inline void set_m_tintAllSprites_55(bool value)
	{
		___m_tintAllSprites_55 = value;
	}

	inline static int32_t get_offset_of_m_tintSprite_56() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_tintSprite_56)); }
	inline bool get_m_tintSprite_56() const { return ___m_tintSprite_56; }
	inline bool* get_address_of_m_tintSprite_56() { return &___m_tintSprite_56; }
	inline void set_m_tintSprite_56(bool value)
	{
		___m_tintSprite_56 = value;
	}

	inline static int32_t get_offset_of_m_spriteColor_57() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteColor_57)); }
	inline Color32_t2600501292  get_m_spriteColor_57() const { return ___m_spriteColor_57; }
	inline Color32_t2600501292 * get_address_of_m_spriteColor_57() { return &___m_spriteColor_57; }
	inline void set_m_spriteColor_57(Color32_t2600501292  value)
	{
		___m_spriteColor_57 = value;
	}

	inline static int32_t get_offset_of_m_overrideHtmlColors_58() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_overrideHtmlColors_58)); }
	inline bool get_m_overrideHtmlColors_58() const { return ___m_overrideHtmlColors_58; }
	inline bool* get_address_of_m_overrideHtmlColors_58() { return &___m_overrideHtmlColors_58; }
	inline void set_m_overrideHtmlColors_58(bool value)
	{
		___m_overrideHtmlColors_58 = value;
	}

	inline static int32_t get_offset_of_m_faceColor_59() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_faceColor_59)); }
	inline Color32_t2600501292  get_m_faceColor_59() const { return ___m_faceColor_59; }
	inline Color32_t2600501292 * get_address_of_m_faceColor_59() { return &___m_faceColor_59; }
	inline void set_m_faceColor_59(Color32_t2600501292  value)
	{
		___m_faceColor_59 = value;
	}

	inline static int32_t get_offset_of_m_outlineColor_60() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_outlineColor_60)); }
	inline Color32_t2600501292  get_m_outlineColor_60() const { return ___m_outlineColor_60; }
	inline Color32_t2600501292 * get_address_of_m_outlineColor_60() { return &___m_outlineColor_60; }
	inline void set_m_outlineColor_60(Color32_t2600501292  value)
	{
		___m_outlineColor_60 = value;
	}

	inline static int32_t get_offset_of_m_outlineWidth_61() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_outlineWidth_61)); }
	inline float get_m_outlineWidth_61() const { return ___m_outlineWidth_61; }
	inline float* get_address_of_m_outlineWidth_61() { return &___m_outlineWidth_61; }
	inline void set_m_outlineWidth_61(float value)
	{
		___m_outlineWidth_61 = value;
	}

	inline static int32_t get_offset_of_m_fontSize_62() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontSize_62)); }
	inline float get_m_fontSize_62() const { return ___m_fontSize_62; }
	inline float* get_address_of_m_fontSize_62() { return &___m_fontSize_62; }
	inline void set_m_fontSize_62(float value)
	{
		___m_fontSize_62 = value;
	}

	inline static int32_t get_offset_of_m_currentFontSize_63() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_currentFontSize_63)); }
	inline float get_m_currentFontSize_63() const { return ___m_currentFontSize_63; }
	inline float* get_address_of_m_currentFontSize_63() { return &___m_currentFontSize_63; }
	inline void set_m_currentFontSize_63(float value)
	{
		___m_currentFontSize_63 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeBase_64() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontSizeBase_64)); }
	inline float get_m_fontSizeBase_64() const { return ___m_fontSizeBase_64; }
	inline float* get_address_of_m_fontSizeBase_64() { return &___m_fontSizeBase_64; }
	inline void set_m_fontSizeBase_64(float value)
	{
		___m_fontSizeBase_64 = value;
	}

	inline static int32_t get_offset_of_m_sizeStack_65() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_sizeStack_65)); }
	inline TMP_XmlTagStack_1_t960921318  get_m_sizeStack_65() const { return ___m_sizeStack_65; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_m_sizeStack_65() { return &___m_sizeStack_65; }
	inline void set_m_sizeStack_65(TMP_XmlTagStack_1_t960921318  value)
	{
		___m_sizeStack_65 = value;
	}

	inline static int32_t get_offset_of_m_fontWeight_66() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontWeight_66)); }
	inline int32_t get_m_fontWeight_66() const { return ___m_fontWeight_66; }
	inline int32_t* get_address_of_m_fontWeight_66() { return &___m_fontWeight_66; }
	inline void set_m_fontWeight_66(int32_t value)
	{
		___m_fontWeight_66 = value;
	}

	inline static int32_t get_offset_of_m_fontWeightInternal_67() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontWeightInternal_67)); }
	inline int32_t get_m_fontWeightInternal_67() const { return ___m_fontWeightInternal_67; }
	inline int32_t* get_address_of_m_fontWeightInternal_67() { return &___m_fontWeightInternal_67; }
	inline void set_m_fontWeightInternal_67(int32_t value)
	{
		___m_fontWeightInternal_67 = value;
	}

	inline static int32_t get_offset_of_m_fontWeightStack_68() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontWeightStack_68)); }
	inline TMP_XmlTagStack_1_t2514600297  get_m_fontWeightStack_68() const { return ___m_fontWeightStack_68; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_m_fontWeightStack_68() { return &___m_fontWeightStack_68; }
	inline void set_m_fontWeightStack_68(TMP_XmlTagStack_1_t2514600297  value)
	{
		___m_fontWeightStack_68 = value;
	}

	inline static int32_t get_offset_of_m_enableAutoSizing_69() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_enableAutoSizing_69)); }
	inline bool get_m_enableAutoSizing_69() const { return ___m_enableAutoSizing_69; }
	inline bool* get_address_of_m_enableAutoSizing_69() { return &___m_enableAutoSizing_69; }
	inline void set_m_enableAutoSizing_69(bool value)
	{
		___m_enableAutoSizing_69 = value;
	}

	inline static int32_t get_offset_of_m_maxFontSize_70() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxFontSize_70)); }
	inline float get_m_maxFontSize_70() const { return ___m_maxFontSize_70; }
	inline float* get_address_of_m_maxFontSize_70() { return &___m_maxFontSize_70; }
	inline void set_m_maxFontSize_70(float value)
	{
		___m_maxFontSize_70 = value;
	}

	inline static int32_t get_offset_of_m_minFontSize_71() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_minFontSize_71)); }
	inline float get_m_minFontSize_71() const { return ___m_minFontSize_71; }
	inline float* get_address_of_m_minFontSize_71() { return &___m_minFontSize_71; }
	inline void set_m_minFontSize_71(float value)
	{
		___m_minFontSize_71 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeMin_72() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontSizeMin_72)); }
	inline float get_m_fontSizeMin_72() const { return ___m_fontSizeMin_72; }
	inline float* get_address_of_m_fontSizeMin_72() { return &___m_fontSizeMin_72; }
	inline void set_m_fontSizeMin_72(float value)
	{
		___m_fontSizeMin_72 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeMax_73() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontSizeMax_73)); }
	inline float get_m_fontSizeMax_73() const { return ___m_fontSizeMax_73; }
	inline float* get_address_of_m_fontSizeMax_73() { return &___m_fontSizeMax_73; }
	inline void set_m_fontSizeMax_73(float value)
	{
		___m_fontSizeMax_73 = value;
	}

	inline static int32_t get_offset_of_m_fontStyle_74() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontStyle_74)); }
	inline int32_t get_m_fontStyle_74() const { return ___m_fontStyle_74; }
	inline int32_t* get_address_of_m_fontStyle_74() { return &___m_fontStyle_74; }
	inline void set_m_fontStyle_74(int32_t value)
	{
		___m_fontStyle_74 = value;
	}

	inline static int32_t get_offset_of_m_style_75() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_style_75)); }
	inline int32_t get_m_style_75() const { return ___m_style_75; }
	inline int32_t* get_address_of_m_style_75() { return &___m_style_75; }
	inline void set_m_style_75(int32_t value)
	{
		___m_style_75 = value;
	}

	inline static int32_t get_offset_of_m_fontStyleStack_76() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontStyleStack_76)); }
	inline TMP_BasicXmlTagStack_t2962628096  get_m_fontStyleStack_76() const { return ___m_fontStyleStack_76; }
	inline TMP_BasicXmlTagStack_t2962628096 * get_address_of_m_fontStyleStack_76() { return &___m_fontStyleStack_76; }
	inline void set_m_fontStyleStack_76(TMP_BasicXmlTagStack_t2962628096  value)
	{
		___m_fontStyleStack_76 = value;
	}

	inline static int32_t get_offset_of_m_isUsingBold_77() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isUsingBold_77)); }
	inline bool get_m_isUsingBold_77() const { return ___m_isUsingBold_77; }
	inline bool* get_address_of_m_isUsingBold_77() { return &___m_isUsingBold_77; }
	inline void set_m_isUsingBold_77(bool value)
	{
		___m_isUsingBold_77 = value;
	}

	inline static int32_t get_offset_of_m_textAlignment_78() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_textAlignment_78)); }
	inline int32_t get_m_textAlignment_78() const { return ___m_textAlignment_78; }
	inline int32_t* get_address_of_m_textAlignment_78() { return &___m_textAlignment_78; }
	inline void set_m_textAlignment_78(int32_t value)
	{
		___m_textAlignment_78 = value;
	}

	inline static int32_t get_offset_of_m_lineJustification_79() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineJustification_79)); }
	inline int32_t get_m_lineJustification_79() const { return ___m_lineJustification_79; }
	inline int32_t* get_address_of_m_lineJustification_79() { return &___m_lineJustification_79; }
	inline void set_m_lineJustification_79(int32_t value)
	{
		___m_lineJustification_79 = value;
	}

	inline static int32_t get_offset_of_m_lineJustificationStack_80() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineJustificationStack_80)); }
	inline TMP_XmlTagStack_1_t3600445780  get_m_lineJustificationStack_80() const { return ___m_lineJustificationStack_80; }
	inline TMP_XmlTagStack_1_t3600445780 * get_address_of_m_lineJustificationStack_80() { return &___m_lineJustificationStack_80; }
	inline void set_m_lineJustificationStack_80(TMP_XmlTagStack_1_t3600445780  value)
	{
		___m_lineJustificationStack_80 = value;
	}

	inline static int32_t get_offset_of_m_textContainerLocalCorners_81() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_textContainerLocalCorners_81)); }
	inline Vector3U5BU5D_t1718750761* get_m_textContainerLocalCorners_81() const { return ___m_textContainerLocalCorners_81; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_textContainerLocalCorners_81() { return &___m_textContainerLocalCorners_81; }
	inline void set_m_textContainerLocalCorners_81(Vector3U5BU5D_t1718750761* value)
	{
		___m_textContainerLocalCorners_81 = value;
		Il2CppCodeGenWriteBarrier((&___m_textContainerLocalCorners_81), value);
	}

	inline static int32_t get_offset_of_m_isAlignmentEnumConverted_82() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isAlignmentEnumConverted_82)); }
	inline bool get_m_isAlignmentEnumConverted_82() const { return ___m_isAlignmentEnumConverted_82; }
	inline bool* get_address_of_m_isAlignmentEnumConverted_82() { return &___m_isAlignmentEnumConverted_82; }
	inline void set_m_isAlignmentEnumConverted_82(bool value)
	{
		___m_isAlignmentEnumConverted_82 = value;
	}

	inline static int32_t get_offset_of_m_characterSpacing_83() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_characterSpacing_83)); }
	inline float get_m_characterSpacing_83() const { return ___m_characterSpacing_83; }
	inline float* get_address_of_m_characterSpacing_83() { return &___m_characterSpacing_83; }
	inline void set_m_characterSpacing_83(float value)
	{
		___m_characterSpacing_83 = value;
	}

	inline static int32_t get_offset_of_m_cSpacing_84() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_cSpacing_84)); }
	inline float get_m_cSpacing_84() const { return ___m_cSpacing_84; }
	inline float* get_address_of_m_cSpacing_84() { return &___m_cSpacing_84; }
	inline void set_m_cSpacing_84(float value)
	{
		___m_cSpacing_84 = value;
	}

	inline static int32_t get_offset_of_m_monoSpacing_85() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_monoSpacing_85)); }
	inline float get_m_monoSpacing_85() const { return ___m_monoSpacing_85; }
	inline float* get_address_of_m_monoSpacing_85() { return &___m_monoSpacing_85; }
	inline void set_m_monoSpacing_85(float value)
	{
		___m_monoSpacing_85 = value;
	}

	inline static int32_t get_offset_of_m_wordSpacing_86() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_wordSpacing_86)); }
	inline float get_m_wordSpacing_86() const { return ___m_wordSpacing_86; }
	inline float* get_address_of_m_wordSpacing_86() { return &___m_wordSpacing_86; }
	inline void set_m_wordSpacing_86(float value)
	{
		___m_wordSpacing_86 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacing_87() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineSpacing_87)); }
	inline float get_m_lineSpacing_87() const { return ___m_lineSpacing_87; }
	inline float* get_address_of_m_lineSpacing_87() { return &___m_lineSpacing_87; }
	inline void set_m_lineSpacing_87(float value)
	{
		___m_lineSpacing_87 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacingDelta_88() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineSpacingDelta_88)); }
	inline float get_m_lineSpacingDelta_88() const { return ___m_lineSpacingDelta_88; }
	inline float* get_address_of_m_lineSpacingDelta_88() { return &___m_lineSpacingDelta_88; }
	inline void set_m_lineSpacingDelta_88(float value)
	{
		___m_lineSpacingDelta_88 = value;
	}

	inline static int32_t get_offset_of_m_lineHeight_89() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineHeight_89)); }
	inline float get_m_lineHeight_89() const { return ___m_lineHeight_89; }
	inline float* get_address_of_m_lineHeight_89() { return &___m_lineHeight_89; }
	inline void set_m_lineHeight_89(float value)
	{
		___m_lineHeight_89 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacingMax_90() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineSpacingMax_90)); }
	inline float get_m_lineSpacingMax_90() const { return ___m_lineSpacingMax_90; }
	inline float* get_address_of_m_lineSpacingMax_90() { return &___m_lineSpacingMax_90; }
	inline void set_m_lineSpacingMax_90(float value)
	{
		___m_lineSpacingMax_90 = value;
	}

	inline static int32_t get_offset_of_m_paragraphSpacing_91() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_paragraphSpacing_91)); }
	inline float get_m_paragraphSpacing_91() const { return ___m_paragraphSpacing_91; }
	inline float* get_address_of_m_paragraphSpacing_91() { return &___m_paragraphSpacing_91; }
	inline void set_m_paragraphSpacing_91(float value)
	{
		___m_paragraphSpacing_91 = value;
	}

	inline static int32_t get_offset_of_m_charWidthMaxAdj_92() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_charWidthMaxAdj_92)); }
	inline float get_m_charWidthMaxAdj_92() const { return ___m_charWidthMaxAdj_92; }
	inline float* get_address_of_m_charWidthMaxAdj_92() { return &___m_charWidthMaxAdj_92; }
	inline void set_m_charWidthMaxAdj_92(float value)
	{
		___m_charWidthMaxAdj_92 = value;
	}

	inline static int32_t get_offset_of_m_charWidthAdjDelta_93() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_charWidthAdjDelta_93)); }
	inline float get_m_charWidthAdjDelta_93() const { return ___m_charWidthAdjDelta_93; }
	inline float* get_address_of_m_charWidthAdjDelta_93() { return &___m_charWidthAdjDelta_93; }
	inline void set_m_charWidthAdjDelta_93(float value)
	{
		___m_charWidthAdjDelta_93 = value;
	}

	inline static int32_t get_offset_of_m_enableWordWrapping_94() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_enableWordWrapping_94)); }
	inline bool get_m_enableWordWrapping_94() const { return ___m_enableWordWrapping_94; }
	inline bool* get_address_of_m_enableWordWrapping_94() { return &___m_enableWordWrapping_94; }
	inline void set_m_enableWordWrapping_94(bool value)
	{
		___m_enableWordWrapping_94 = value;
	}

	inline static int32_t get_offset_of_m_isCharacterWrappingEnabled_95() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isCharacterWrappingEnabled_95)); }
	inline bool get_m_isCharacterWrappingEnabled_95() const { return ___m_isCharacterWrappingEnabled_95; }
	inline bool* get_address_of_m_isCharacterWrappingEnabled_95() { return &___m_isCharacterWrappingEnabled_95; }
	inline void set_m_isCharacterWrappingEnabled_95(bool value)
	{
		___m_isCharacterWrappingEnabled_95 = value;
	}

	inline static int32_t get_offset_of_m_isNonBreakingSpace_96() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isNonBreakingSpace_96)); }
	inline bool get_m_isNonBreakingSpace_96() const { return ___m_isNonBreakingSpace_96; }
	inline bool* get_address_of_m_isNonBreakingSpace_96() { return &___m_isNonBreakingSpace_96; }
	inline void set_m_isNonBreakingSpace_96(bool value)
	{
		___m_isNonBreakingSpace_96 = value;
	}

	inline static int32_t get_offset_of_m_isIgnoringAlignment_97() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isIgnoringAlignment_97)); }
	inline bool get_m_isIgnoringAlignment_97() const { return ___m_isIgnoringAlignment_97; }
	inline bool* get_address_of_m_isIgnoringAlignment_97() { return &___m_isIgnoringAlignment_97; }
	inline void set_m_isIgnoringAlignment_97(bool value)
	{
		___m_isIgnoringAlignment_97 = value;
	}

	inline static int32_t get_offset_of_m_wordWrappingRatios_98() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_wordWrappingRatios_98)); }
	inline float get_m_wordWrappingRatios_98() const { return ___m_wordWrappingRatios_98; }
	inline float* get_address_of_m_wordWrappingRatios_98() { return &___m_wordWrappingRatios_98; }
	inline void set_m_wordWrappingRatios_98(float value)
	{
		___m_wordWrappingRatios_98 = value;
	}

	inline static int32_t get_offset_of_m_overflowMode_99() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_overflowMode_99)); }
	inline int32_t get_m_overflowMode_99() const { return ___m_overflowMode_99; }
	inline int32_t* get_address_of_m_overflowMode_99() { return &___m_overflowMode_99; }
	inline void set_m_overflowMode_99(int32_t value)
	{
		___m_overflowMode_99 = value;
	}

	inline static int32_t get_offset_of_m_firstOverflowCharacterIndex_100() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_firstOverflowCharacterIndex_100)); }
	inline int32_t get_m_firstOverflowCharacterIndex_100() const { return ___m_firstOverflowCharacterIndex_100; }
	inline int32_t* get_address_of_m_firstOverflowCharacterIndex_100() { return &___m_firstOverflowCharacterIndex_100; }
	inline void set_m_firstOverflowCharacterIndex_100(int32_t value)
	{
		___m_firstOverflowCharacterIndex_100 = value;
	}

	inline static int32_t get_offset_of_m_linkedTextComponent_101() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_linkedTextComponent_101)); }
	inline TMP_Text_t2599618874 * get_m_linkedTextComponent_101() const { return ___m_linkedTextComponent_101; }
	inline TMP_Text_t2599618874 ** get_address_of_m_linkedTextComponent_101() { return &___m_linkedTextComponent_101; }
	inline void set_m_linkedTextComponent_101(TMP_Text_t2599618874 * value)
	{
		___m_linkedTextComponent_101 = value;
		Il2CppCodeGenWriteBarrier((&___m_linkedTextComponent_101), value);
	}

	inline static int32_t get_offset_of_m_isLinkedTextComponent_102() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isLinkedTextComponent_102)); }
	inline bool get_m_isLinkedTextComponent_102() const { return ___m_isLinkedTextComponent_102; }
	inline bool* get_address_of_m_isLinkedTextComponent_102() { return &___m_isLinkedTextComponent_102; }
	inline void set_m_isLinkedTextComponent_102(bool value)
	{
		___m_isLinkedTextComponent_102 = value;
	}

	inline static int32_t get_offset_of_m_isTextTruncated_103() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isTextTruncated_103)); }
	inline bool get_m_isTextTruncated_103() const { return ___m_isTextTruncated_103; }
	inline bool* get_address_of_m_isTextTruncated_103() { return &___m_isTextTruncated_103; }
	inline void set_m_isTextTruncated_103(bool value)
	{
		___m_isTextTruncated_103 = value;
	}

	inline static int32_t get_offset_of_m_enableKerning_104() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_enableKerning_104)); }
	inline bool get_m_enableKerning_104() const { return ___m_enableKerning_104; }
	inline bool* get_address_of_m_enableKerning_104() { return &___m_enableKerning_104; }
	inline void set_m_enableKerning_104(bool value)
	{
		___m_enableKerning_104 = value;
	}

	inline static int32_t get_offset_of_m_enableExtraPadding_105() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_enableExtraPadding_105)); }
	inline bool get_m_enableExtraPadding_105() const { return ___m_enableExtraPadding_105; }
	inline bool* get_address_of_m_enableExtraPadding_105() { return &___m_enableExtraPadding_105; }
	inline void set_m_enableExtraPadding_105(bool value)
	{
		___m_enableExtraPadding_105 = value;
	}

	inline static int32_t get_offset_of_checkPaddingRequired_106() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___checkPaddingRequired_106)); }
	inline bool get_checkPaddingRequired_106() const { return ___checkPaddingRequired_106; }
	inline bool* get_address_of_checkPaddingRequired_106() { return &___checkPaddingRequired_106; }
	inline void set_checkPaddingRequired_106(bool value)
	{
		___checkPaddingRequired_106 = value;
	}

	inline static int32_t get_offset_of_m_isRichText_107() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isRichText_107)); }
	inline bool get_m_isRichText_107() const { return ___m_isRichText_107; }
	inline bool* get_address_of_m_isRichText_107() { return &___m_isRichText_107; }
	inline void set_m_isRichText_107(bool value)
	{
		___m_isRichText_107 = value;
	}

	inline static int32_t get_offset_of_m_parseCtrlCharacters_108() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_parseCtrlCharacters_108)); }
	inline bool get_m_parseCtrlCharacters_108() const { return ___m_parseCtrlCharacters_108; }
	inline bool* get_address_of_m_parseCtrlCharacters_108() { return &___m_parseCtrlCharacters_108; }
	inline void set_m_parseCtrlCharacters_108(bool value)
	{
		___m_parseCtrlCharacters_108 = value;
	}

	inline static int32_t get_offset_of_m_isOverlay_109() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isOverlay_109)); }
	inline bool get_m_isOverlay_109() const { return ___m_isOverlay_109; }
	inline bool* get_address_of_m_isOverlay_109() { return &___m_isOverlay_109; }
	inline void set_m_isOverlay_109(bool value)
	{
		___m_isOverlay_109 = value;
	}

	inline static int32_t get_offset_of_m_isOrthographic_110() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isOrthographic_110)); }
	inline bool get_m_isOrthographic_110() const { return ___m_isOrthographic_110; }
	inline bool* get_address_of_m_isOrthographic_110() { return &___m_isOrthographic_110; }
	inline void set_m_isOrthographic_110(bool value)
	{
		___m_isOrthographic_110 = value;
	}

	inline static int32_t get_offset_of_m_isCullingEnabled_111() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isCullingEnabled_111)); }
	inline bool get_m_isCullingEnabled_111() const { return ___m_isCullingEnabled_111; }
	inline bool* get_address_of_m_isCullingEnabled_111() { return &___m_isCullingEnabled_111; }
	inline void set_m_isCullingEnabled_111(bool value)
	{
		___m_isCullingEnabled_111 = value;
	}

	inline static int32_t get_offset_of_m_ignoreRectMaskCulling_112() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_ignoreRectMaskCulling_112)); }
	inline bool get_m_ignoreRectMaskCulling_112() const { return ___m_ignoreRectMaskCulling_112; }
	inline bool* get_address_of_m_ignoreRectMaskCulling_112() { return &___m_ignoreRectMaskCulling_112; }
	inline void set_m_ignoreRectMaskCulling_112(bool value)
	{
		___m_ignoreRectMaskCulling_112 = value;
	}

	inline static int32_t get_offset_of_m_ignoreCulling_113() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_ignoreCulling_113)); }
	inline bool get_m_ignoreCulling_113() const { return ___m_ignoreCulling_113; }
	inline bool* get_address_of_m_ignoreCulling_113() { return &___m_ignoreCulling_113; }
	inline void set_m_ignoreCulling_113(bool value)
	{
		___m_ignoreCulling_113 = value;
	}

	inline static int32_t get_offset_of_m_horizontalMapping_114() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_horizontalMapping_114)); }
	inline int32_t get_m_horizontalMapping_114() const { return ___m_horizontalMapping_114; }
	inline int32_t* get_address_of_m_horizontalMapping_114() { return &___m_horizontalMapping_114; }
	inline void set_m_horizontalMapping_114(int32_t value)
	{
		___m_horizontalMapping_114 = value;
	}

	inline static int32_t get_offset_of_m_verticalMapping_115() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_verticalMapping_115)); }
	inline int32_t get_m_verticalMapping_115() const { return ___m_verticalMapping_115; }
	inline int32_t* get_address_of_m_verticalMapping_115() { return &___m_verticalMapping_115; }
	inline void set_m_verticalMapping_115(int32_t value)
	{
		___m_verticalMapping_115 = value;
	}

	inline static int32_t get_offset_of_m_uvLineOffset_116() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_uvLineOffset_116)); }
	inline float get_m_uvLineOffset_116() const { return ___m_uvLineOffset_116; }
	inline float* get_address_of_m_uvLineOffset_116() { return &___m_uvLineOffset_116; }
	inline void set_m_uvLineOffset_116(float value)
	{
		___m_uvLineOffset_116 = value;
	}

	inline static int32_t get_offset_of_m_renderMode_117() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_renderMode_117)); }
	inline int32_t get_m_renderMode_117() const { return ___m_renderMode_117; }
	inline int32_t* get_address_of_m_renderMode_117() { return &___m_renderMode_117; }
	inline void set_m_renderMode_117(int32_t value)
	{
		___m_renderMode_117 = value;
	}

	inline static int32_t get_offset_of_m_geometrySortingOrder_118() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_geometrySortingOrder_118)); }
	inline int32_t get_m_geometrySortingOrder_118() const { return ___m_geometrySortingOrder_118; }
	inline int32_t* get_address_of_m_geometrySortingOrder_118() { return &___m_geometrySortingOrder_118; }
	inline void set_m_geometrySortingOrder_118(int32_t value)
	{
		___m_geometrySortingOrder_118 = value;
	}

	inline static int32_t get_offset_of_m_firstVisibleCharacter_119() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_firstVisibleCharacter_119)); }
	inline int32_t get_m_firstVisibleCharacter_119() const { return ___m_firstVisibleCharacter_119; }
	inline int32_t* get_address_of_m_firstVisibleCharacter_119() { return &___m_firstVisibleCharacter_119; }
	inline void set_m_firstVisibleCharacter_119(int32_t value)
	{
		___m_firstVisibleCharacter_119 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleCharacters_120() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxVisibleCharacters_120)); }
	inline int32_t get_m_maxVisibleCharacters_120() const { return ___m_maxVisibleCharacters_120; }
	inline int32_t* get_address_of_m_maxVisibleCharacters_120() { return &___m_maxVisibleCharacters_120; }
	inline void set_m_maxVisibleCharacters_120(int32_t value)
	{
		___m_maxVisibleCharacters_120 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleWords_121() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxVisibleWords_121)); }
	inline int32_t get_m_maxVisibleWords_121() const { return ___m_maxVisibleWords_121; }
	inline int32_t* get_address_of_m_maxVisibleWords_121() { return &___m_maxVisibleWords_121; }
	inline void set_m_maxVisibleWords_121(int32_t value)
	{
		___m_maxVisibleWords_121 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleLines_122() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxVisibleLines_122)); }
	inline int32_t get_m_maxVisibleLines_122() const { return ___m_maxVisibleLines_122; }
	inline int32_t* get_address_of_m_maxVisibleLines_122() { return &___m_maxVisibleLines_122; }
	inline void set_m_maxVisibleLines_122(int32_t value)
	{
		___m_maxVisibleLines_122 = value;
	}

	inline static int32_t get_offset_of_m_useMaxVisibleDescender_123() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_useMaxVisibleDescender_123)); }
	inline bool get_m_useMaxVisibleDescender_123() const { return ___m_useMaxVisibleDescender_123; }
	inline bool* get_address_of_m_useMaxVisibleDescender_123() { return &___m_useMaxVisibleDescender_123; }
	inline void set_m_useMaxVisibleDescender_123(bool value)
	{
		___m_useMaxVisibleDescender_123 = value;
	}

	inline static int32_t get_offset_of_m_pageToDisplay_124() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_pageToDisplay_124)); }
	inline int32_t get_m_pageToDisplay_124() const { return ___m_pageToDisplay_124; }
	inline int32_t* get_address_of_m_pageToDisplay_124() { return &___m_pageToDisplay_124; }
	inline void set_m_pageToDisplay_124(int32_t value)
	{
		___m_pageToDisplay_124 = value;
	}

	inline static int32_t get_offset_of_m_isNewPage_125() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isNewPage_125)); }
	inline bool get_m_isNewPage_125() const { return ___m_isNewPage_125; }
	inline bool* get_address_of_m_isNewPage_125() { return &___m_isNewPage_125; }
	inline void set_m_isNewPage_125(bool value)
	{
		___m_isNewPage_125 = value;
	}

	inline static int32_t get_offset_of_m_margin_126() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_margin_126)); }
	inline Vector4_t3319028937  get_m_margin_126() const { return ___m_margin_126; }
	inline Vector4_t3319028937 * get_address_of_m_margin_126() { return &___m_margin_126; }
	inline void set_m_margin_126(Vector4_t3319028937  value)
	{
		___m_margin_126 = value;
	}

	inline static int32_t get_offset_of_m_marginLeft_127() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_marginLeft_127)); }
	inline float get_m_marginLeft_127() const { return ___m_marginLeft_127; }
	inline float* get_address_of_m_marginLeft_127() { return &___m_marginLeft_127; }
	inline void set_m_marginLeft_127(float value)
	{
		___m_marginLeft_127 = value;
	}

	inline static int32_t get_offset_of_m_marginRight_128() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_marginRight_128)); }
	inline float get_m_marginRight_128() const { return ___m_marginRight_128; }
	inline float* get_address_of_m_marginRight_128() { return &___m_marginRight_128; }
	inline void set_m_marginRight_128(float value)
	{
		___m_marginRight_128 = value;
	}

	inline static int32_t get_offset_of_m_marginWidth_129() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_marginWidth_129)); }
	inline float get_m_marginWidth_129() const { return ___m_marginWidth_129; }
	inline float* get_address_of_m_marginWidth_129() { return &___m_marginWidth_129; }
	inline void set_m_marginWidth_129(float value)
	{
		___m_marginWidth_129 = value;
	}

	inline static int32_t get_offset_of_m_marginHeight_130() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_marginHeight_130)); }
	inline float get_m_marginHeight_130() const { return ___m_marginHeight_130; }
	inline float* get_address_of_m_marginHeight_130() { return &___m_marginHeight_130; }
	inline void set_m_marginHeight_130(float value)
	{
		___m_marginHeight_130 = value;
	}

	inline static int32_t get_offset_of_m_width_131() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_width_131)); }
	inline float get_m_width_131() const { return ___m_width_131; }
	inline float* get_address_of_m_width_131() { return &___m_width_131; }
	inline void set_m_width_131(float value)
	{
		___m_width_131 = value;
	}

	inline static int32_t get_offset_of_m_textInfo_132() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_textInfo_132)); }
	inline TMP_TextInfo_t3598145122 * get_m_textInfo_132() const { return ___m_textInfo_132; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_m_textInfo_132() { return &___m_textInfo_132; }
	inline void set_m_textInfo_132(TMP_TextInfo_t3598145122 * value)
	{
		___m_textInfo_132 = value;
		Il2CppCodeGenWriteBarrier((&___m_textInfo_132), value);
	}

	inline static int32_t get_offset_of_m_havePropertiesChanged_133() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_havePropertiesChanged_133)); }
	inline bool get_m_havePropertiesChanged_133() const { return ___m_havePropertiesChanged_133; }
	inline bool* get_address_of_m_havePropertiesChanged_133() { return &___m_havePropertiesChanged_133; }
	inline void set_m_havePropertiesChanged_133(bool value)
	{
		___m_havePropertiesChanged_133 = value;
	}

	inline static int32_t get_offset_of_m_isUsingLegacyAnimationComponent_134() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isUsingLegacyAnimationComponent_134)); }
	inline bool get_m_isUsingLegacyAnimationComponent_134() const { return ___m_isUsingLegacyAnimationComponent_134; }
	inline bool* get_address_of_m_isUsingLegacyAnimationComponent_134() { return &___m_isUsingLegacyAnimationComponent_134; }
	inline void set_m_isUsingLegacyAnimationComponent_134(bool value)
	{
		___m_isUsingLegacyAnimationComponent_134 = value;
	}

	inline static int32_t get_offset_of_m_transform_135() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_transform_135)); }
	inline Transform_t3600365921 * get_m_transform_135() const { return ___m_transform_135; }
	inline Transform_t3600365921 ** get_address_of_m_transform_135() { return &___m_transform_135; }
	inline void set_m_transform_135(Transform_t3600365921 * value)
	{
		___m_transform_135 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_135), value);
	}

	inline static int32_t get_offset_of_m_rectTransform_136() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_rectTransform_136)); }
	inline RectTransform_t3704657025 * get_m_rectTransform_136() const { return ___m_rectTransform_136; }
	inline RectTransform_t3704657025 ** get_address_of_m_rectTransform_136() { return &___m_rectTransform_136; }
	inline void set_m_rectTransform_136(RectTransform_t3704657025 * value)
	{
		___m_rectTransform_136 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_136), value);
	}

	inline static int32_t get_offset_of_U3CautoSizeTextContainerU3Ek__BackingField_137() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___U3CautoSizeTextContainerU3Ek__BackingField_137)); }
	inline bool get_U3CautoSizeTextContainerU3Ek__BackingField_137() const { return ___U3CautoSizeTextContainerU3Ek__BackingField_137; }
	inline bool* get_address_of_U3CautoSizeTextContainerU3Ek__BackingField_137() { return &___U3CautoSizeTextContainerU3Ek__BackingField_137; }
	inline void set_U3CautoSizeTextContainerU3Ek__BackingField_137(bool value)
	{
		___U3CautoSizeTextContainerU3Ek__BackingField_137 = value;
	}

	inline static int32_t get_offset_of_m_autoSizeTextContainer_138() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_autoSizeTextContainer_138)); }
	inline bool get_m_autoSizeTextContainer_138() const { return ___m_autoSizeTextContainer_138; }
	inline bool* get_address_of_m_autoSizeTextContainer_138() { return &___m_autoSizeTextContainer_138; }
	inline void set_m_autoSizeTextContainer_138(bool value)
	{
		___m_autoSizeTextContainer_138 = value;
	}

	inline static int32_t get_offset_of_m_mesh_139() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_mesh_139)); }
	inline Mesh_t3648964284 * get_m_mesh_139() const { return ___m_mesh_139; }
	inline Mesh_t3648964284 ** get_address_of_m_mesh_139() { return &___m_mesh_139; }
	inline void set_m_mesh_139(Mesh_t3648964284 * value)
	{
		___m_mesh_139 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_139), value);
	}

	inline static int32_t get_offset_of_m_isVolumetricText_140() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isVolumetricText_140)); }
	inline bool get_m_isVolumetricText_140() const { return ___m_isVolumetricText_140; }
	inline bool* get_address_of_m_isVolumetricText_140() { return &___m_isVolumetricText_140; }
	inline void set_m_isVolumetricText_140(bool value)
	{
		___m_isVolumetricText_140 = value;
	}

	inline static int32_t get_offset_of_m_spriteAnimator_141() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteAnimator_141)); }
	inline TMP_SpriteAnimator_t2836635477 * get_m_spriteAnimator_141() const { return ___m_spriteAnimator_141; }
	inline TMP_SpriteAnimator_t2836635477 ** get_address_of_m_spriteAnimator_141() { return &___m_spriteAnimator_141; }
	inline void set_m_spriteAnimator_141(TMP_SpriteAnimator_t2836635477 * value)
	{
		___m_spriteAnimator_141 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAnimator_141), value);
	}

	inline static int32_t get_offset_of_m_flexibleHeight_142() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_flexibleHeight_142)); }
	inline float get_m_flexibleHeight_142() const { return ___m_flexibleHeight_142; }
	inline float* get_address_of_m_flexibleHeight_142() { return &___m_flexibleHeight_142; }
	inline void set_m_flexibleHeight_142(float value)
	{
		___m_flexibleHeight_142 = value;
	}

	inline static int32_t get_offset_of_m_flexibleWidth_143() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_flexibleWidth_143)); }
	inline float get_m_flexibleWidth_143() const { return ___m_flexibleWidth_143; }
	inline float* get_address_of_m_flexibleWidth_143() { return &___m_flexibleWidth_143; }
	inline void set_m_flexibleWidth_143(float value)
	{
		___m_flexibleWidth_143 = value;
	}

	inline static int32_t get_offset_of_m_minWidth_144() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_minWidth_144)); }
	inline float get_m_minWidth_144() const { return ___m_minWidth_144; }
	inline float* get_address_of_m_minWidth_144() { return &___m_minWidth_144; }
	inline void set_m_minWidth_144(float value)
	{
		___m_minWidth_144 = value;
	}

	inline static int32_t get_offset_of_m_minHeight_145() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_minHeight_145)); }
	inline float get_m_minHeight_145() const { return ___m_minHeight_145; }
	inline float* get_address_of_m_minHeight_145() { return &___m_minHeight_145; }
	inline void set_m_minHeight_145(float value)
	{
		___m_minHeight_145 = value;
	}

	inline static int32_t get_offset_of_m_maxWidth_146() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxWidth_146)); }
	inline float get_m_maxWidth_146() const { return ___m_maxWidth_146; }
	inline float* get_address_of_m_maxWidth_146() { return &___m_maxWidth_146; }
	inline void set_m_maxWidth_146(float value)
	{
		___m_maxWidth_146 = value;
	}

	inline static int32_t get_offset_of_m_maxHeight_147() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxHeight_147)); }
	inline float get_m_maxHeight_147() const { return ___m_maxHeight_147; }
	inline float* get_address_of_m_maxHeight_147() { return &___m_maxHeight_147; }
	inline void set_m_maxHeight_147(float value)
	{
		___m_maxHeight_147 = value;
	}

	inline static int32_t get_offset_of_m_LayoutElement_148() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_LayoutElement_148)); }
	inline LayoutElement_t1785403678 * get_m_LayoutElement_148() const { return ___m_LayoutElement_148; }
	inline LayoutElement_t1785403678 ** get_address_of_m_LayoutElement_148() { return &___m_LayoutElement_148; }
	inline void set_m_LayoutElement_148(LayoutElement_t1785403678 * value)
	{
		___m_LayoutElement_148 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutElement_148), value);
	}

	inline static int32_t get_offset_of_m_preferredWidth_149() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_preferredWidth_149)); }
	inline float get_m_preferredWidth_149() const { return ___m_preferredWidth_149; }
	inline float* get_address_of_m_preferredWidth_149() { return &___m_preferredWidth_149; }
	inline void set_m_preferredWidth_149(float value)
	{
		___m_preferredWidth_149 = value;
	}

	inline static int32_t get_offset_of_m_renderedWidth_150() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_renderedWidth_150)); }
	inline float get_m_renderedWidth_150() const { return ___m_renderedWidth_150; }
	inline float* get_address_of_m_renderedWidth_150() { return &___m_renderedWidth_150; }
	inline void set_m_renderedWidth_150(float value)
	{
		___m_renderedWidth_150 = value;
	}

	inline static int32_t get_offset_of_m_isPreferredWidthDirty_151() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isPreferredWidthDirty_151)); }
	inline bool get_m_isPreferredWidthDirty_151() const { return ___m_isPreferredWidthDirty_151; }
	inline bool* get_address_of_m_isPreferredWidthDirty_151() { return &___m_isPreferredWidthDirty_151; }
	inline void set_m_isPreferredWidthDirty_151(bool value)
	{
		___m_isPreferredWidthDirty_151 = value;
	}

	inline static int32_t get_offset_of_m_preferredHeight_152() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_preferredHeight_152)); }
	inline float get_m_preferredHeight_152() const { return ___m_preferredHeight_152; }
	inline float* get_address_of_m_preferredHeight_152() { return &___m_preferredHeight_152; }
	inline void set_m_preferredHeight_152(float value)
	{
		___m_preferredHeight_152 = value;
	}

	inline static int32_t get_offset_of_m_renderedHeight_153() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_renderedHeight_153)); }
	inline float get_m_renderedHeight_153() const { return ___m_renderedHeight_153; }
	inline float* get_address_of_m_renderedHeight_153() { return &___m_renderedHeight_153; }
	inline void set_m_renderedHeight_153(float value)
	{
		___m_renderedHeight_153 = value;
	}

	inline static int32_t get_offset_of_m_isPreferredHeightDirty_154() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isPreferredHeightDirty_154)); }
	inline bool get_m_isPreferredHeightDirty_154() const { return ___m_isPreferredHeightDirty_154; }
	inline bool* get_address_of_m_isPreferredHeightDirty_154() { return &___m_isPreferredHeightDirty_154; }
	inline void set_m_isPreferredHeightDirty_154(bool value)
	{
		___m_isPreferredHeightDirty_154 = value;
	}

	inline static int32_t get_offset_of_m_isCalculatingPreferredValues_155() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isCalculatingPreferredValues_155)); }
	inline bool get_m_isCalculatingPreferredValues_155() const { return ___m_isCalculatingPreferredValues_155; }
	inline bool* get_address_of_m_isCalculatingPreferredValues_155() { return &___m_isCalculatingPreferredValues_155; }
	inline void set_m_isCalculatingPreferredValues_155(bool value)
	{
		___m_isCalculatingPreferredValues_155 = value;
	}

	inline static int32_t get_offset_of_m_recursiveCount_156() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_recursiveCount_156)); }
	inline int32_t get_m_recursiveCount_156() const { return ___m_recursiveCount_156; }
	inline int32_t* get_address_of_m_recursiveCount_156() { return &___m_recursiveCount_156; }
	inline void set_m_recursiveCount_156(int32_t value)
	{
		___m_recursiveCount_156 = value;
	}

	inline static int32_t get_offset_of_m_layoutPriority_157() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_layoutPriority_157)); }
	inline int32_t get_m_layoutPriority_157() const { return ___m_layoutPriority_157; }
	inline int32_t* get_address_of_m_layoutPriority_157() { return &___m_layoutPriority_157; }
	inline void set_m_layoutPriority_157(int32_t value)
	{
		___m_layoutPriority_157 = value;
	}

	inline static int32_t get_offset_of_m_isCalculateSizeRequired_158() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isCalculateSizeRequired_158)); }
	inline bool get_m_isCalculateSizeRequired_158() const { return ___m_isCalculateSizeRequired_158; }
	inline bool* get_address_of_m_isCalculateSizeRequired_158() { return &___m_isCalculateSizeRequired_158; }
	inline void set_m_isCalculateSizeRequired_158(bool value)
	{
		___m_isCalculateSizeRequired_158 = value;
	}

	inline static int32_t get_offset_of_m_isLayoutDirty_159() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isLayoutDirty_159)); }
	inline bool get_m_isLayoutDirty_159() const { return ___m_isLayoutDirty_159; }
	inline bool* get_address_of_m_isLayoutDirty_159() { return &___m_isLayoutDirty_159; }
	inline void set_m_isLayoutDirty_159(bool value)
	{
		___m_isLayoutDirty_159 = value;
	}

	inline static int32_t get_offset_of_m_verticesAlreadyDirty_160() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_verticesAlreadyDirty_160)); }
	inline bool get_m_verticesAlreadyDirty_160() const { return ___m_verticesAlreadyDirty_160; }
	inline bool* get_address_of_m_verticesAlreadyDirty_160() { return &___m_verticesAlreadyDirty_160; }
	inline void set_m_verticesAlreadyDirty_160(bool value)
	{
		___m_verticesAlreadyDirty_160 = value;
	}

	inline static int32_t get_offset_of_m_layoutAlreadyDirty_161() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_layoutAlreadyDirty_161)); }
	inline bool get_m_layoutAlreadyDirty_161() const { return ___m_layoutAlreadyDirty_161; }
	inline bool* get_address_of_m_layoutAlreadyDirty_161() { return &___m_layoutAlreadyDirty_161; }
	inline void set_m_layoutAlreadyDirty_161(bool value)
	{
		___m_layoutAlreadyDirty_161 = value;
	}

	inline static int32_t get_offset_of_m_isAwake_162() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isAwake_162)); }
	inline bool get_m_isAwake_162() const { return ___m_isAwake_162; }
	inline bool* get_address_of_m_isAwake_162() { return &___m_isAwake_162; }
	inline void set_m_isAwake_162(bool value)
	{
		___m_isAwake_162 = value;
	}

	inline static int32_t get_offset_of_m_isWaitingOnResourceLoad_163() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isWaitingOnResourceLoad_163)); }
	inline bool get_m_isWaitingOnResourceLoad_163() const { return ___m_isWaitingOnResourceLoad_163; }
	inline bool* get_address_of_m_isWaitingOnResourceLoad_163() { return &___m_isWaitingOnResourceLoad_163; }
	inline void set_m_isWaitingOnResourceLoad_163(bool value)
	{
		___m_isWaitingOnResourceLoad_163 = value;
	}

	inline static int32_t get_offset_of_m_isInputParsingRequired_164() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isInputParsingRequired_164)); }
	inline bool get_m_isInputParsingRequired_164() const { return ___m_isInputParsingRequired_164; }
	inline bool* get_address_of_m_isInputParsingRequired_164() { return &___m_isInputParsingRequired_164; }
	inline void set_m_isInputParsingRequired_164(bool value)
	{
		___m_isInputParsingRequired_164 = value;
	}

	inline static int32_t get_offset_of_m_inputSource_165() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_inputSource_165)); }
	inline int32_t get_m_inputSource_165() const { return ___m_inputSource_165; }
	inline int32_t* get_address_of_m_inputSource_165() { return &___m_inputSource_165; }
	inline void set_m_inputSource_165(int32_t value)
	{
		___m_inputSource_165 = value;
	}

	inline static int32_t get_offset_of_old_text_166() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___old_text_166)); }
	inline String_t* get_old_text_166() const { return ___old_text_166; }
	inline String_t** get_address_of_old_text_166() { return &___old_text_166; }
	inline void set_old_text_166(String_t* value)
	{
		___old_text_166 = value;
		Il2CppCodeGenWriteBarrier((&___old_text_166), value);
	}

	inline static int32_t get_offset_of_m_fontScale_167() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontScale_167)); }
	inline float get_m_fontScale_167() const { return ___m_fontScale_167; }
	inline float* get_address_of_m_fontScale_167() { return &___m_fontScale_167; }
	inline void set_m_fontScale_167(float value)
	{
		___m_fontScale_167 = value;
	}

	inline static int32_t get_offset_of_m_fontScaleMultiplier_168() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontScaleMultiplier_168)); }
	inline float get_m_fontScaleMultiplier_168() const { return ___m_fontScaleMultiplier_168; }
	inline float* get_address_of_m_fontScaleMultiplier_168() { return &___m_fontScaleMultiplier_168; }
	inline void set_m_fontScaleMultiplier_168(float value)
	{
		___m_fontScaleMultiplier_168 = value;
	}

	inline static int32_t get_offset_of_m_htmlTag_169() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_htmlTag_169)); }
	inline CharU5BU5D_t3528271667* get_m_htmlTag_169() const { return ___m_htmlTag_169; }
	inline CharU5BU5D_t3528271667** get_address_of_m_htmlTag_169() { return &___m_htmlTag_169; }
	inline void set_m_htmlTag_169(CharU5BU5D_t3528271667* value)
	{
		___m_htmlTag_169 = value;
		Il2CppCodeGenWriteBarrier((&___m_htmlTag_169), value);
	}

	inline static int32_t get_offset_of_m_xmlAttribute_170() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_xmlAttribute_170)); }
	inline XML_TagAttributeU5BU5D_t284240280* get_m_xmlAttribute_170() const { return ___m_xmlAttribute_170; }
	inline XML_TagAttributeU5BU5D_t284240280** get_address_of_m_xmlAttribute_170() { return &___m_xmlAttribute_170; }
	inline void set_m_xmlAttribute_170(XML_TagAttributeU5BU5D_t284240280* value)
	{
		___m_xmlAttribute_170 = value;
		Il2CppCodeGenWriteBarrier((&___m_xmlAttribute_170), value);
	}

	inline static int32_t get_offset_of_m_attributeParameterValues_171() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_attributeParameterValues_171)); }
	inline SingleU5BU5D_t1444911251* get_m_attributeParameterValues_171() const { return ___m_attributeParameterValues_171; }
	inline SingleU5BU5D_t1444911251** get_address_of_m_attributeParameterValues_171() { return &___m_attributeParameterValues_171; }
	inline void set_m_attributeParameterValues_171(SingleU5BU5D_t1444911251* value)
	{
		___m_attributeParameterValues_171 = value;
		Il2CppCodeGenWriteBarrier((&___m_attributeParameterValues_171), value);
	}

	inline static int32_t get_offset_of_tag_LineIndent_172() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___tag_LineIndent_172)); }
	inline float get_tag_LineIndent_172() const { return ___tag_LineIndent_172; }
	inline float* get_address_of_tag_LineIndent_172() { return &___tag_LineIndent_172; }
	inline void set_tag_LineIndent_172(float value)
	{
		___tag_LineIndent_172 = value;
	}

	inline static int32_t get_offset_of_tag_Indent_173() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___tag_Indent_173)); }
	inline float get_tag_Indent_173() const { return ___tag_Indent_173; }
	inline float* get_address_of_tag_Indent_173() { return &___tag_Indent_173; }
	inline void set_tag_Indent_173(float value)
	{
		___tag_Indent_173 = value;
	}

	inline static int32_t get_offset_of_m_indentStack_174() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_indentStack_174)); }
	inline TMP_XmlTagStack_1_t960921318  get_m_indentStack_174() const { return ___m_indentStack_174; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_m_indentStack_174() { return &___m_indentStack_174; }
	inline void set_m_indentStack_174(TMP_XmlTagStack_1_t960921318  value)
	{
		___m_indentStack_174 = value;
	}

	inline static int32_t get_offset_of_tag_NoParsing_175() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___tag_NoParsing_175)); }
	inline bool get_tag_NoParsing_175() const { return ___tag_NoParsing_175; }
	inline bool* get_address_of_tag_NoParsing_175() { return &___tag_NoParsing_175; }
	inline void set_tag_NoParsing_175(bool value)
	{
		___tag_NoParsing_175 = value;
	}

	inline static int32_t get_offset_of_m_isParsingText_176() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isParsingText_176)); }
	inline bool get_m_isParsingText_176() const { return ___m_isParsingText_176; }
	inline bool* get_address_of_m_isParsingText_176() { return &___m_isParsingText_176; }
	inline void set_m_isParsingText_176(bool value)
	{
		___m_isParsingText_176 = value;
	}

	inline static int32_t get_offset_of_m_FXMatrix_177() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_FXMatrix_177)); }
	inline Matrix4x4_t1817901843  get_m_FXMatrix_177() const { return ___m_FXMatrix_177; }
	inline Matrix4x4_t1817901843 * get_address_of_m_FXMatrix_177() { return &___m_FXMatrix_177; }
	inline void set_m_FXMatrix_177(Matrix4x4_t1817901843  value)
	{
		___m_FXMatrix_177 = value;
	}

	inline static int32_t get_offset_of_m_isFXMatrixSet_178() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isFXMatrixSet_178)); }
	inline bool get_m_isFXMatrixSet_178() const { return ___m_isFXMatrixSet_178; }
	inline bool* get_address_of_m_isFXMatrixSet_178() { return &___m_isFXMatrixSet_178; }
	inline void set_m_isFXMatrixSet_178(bool value)
	{
		___m_isFXMatrixSet_178 = value;
	}

	inline static int32_t get_offset_of_m_char_buffer_179() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_char_buffer_179)); }
	inline Int32U5BU5D_t385246372* get_m_char_buffer_179() const { return ___m_char_buffer_179; }
	inline Int32U5BU5D_t385246372** get_address_of_m_char_buffer_179() { return &___m_char_buffer_179; }
	inline void set_m_char_buffer_179(Int32U5BU5D_t385246372* value)
	{
		___m_char_buffer_179 = value;
		Il2CppCodeGenWriteBarrier((&___m_char_buffer_179), value);
	}

	inline static int32_t get_offset_of_m_internalCharacterInfo_180() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_internalCharacterInfo_180)); }
	inline TMP_CharacterInfoU5BU5D_t1930184704* get_m_internalCharacterInfo_180() const { return ___m_internalCharacterInfo_180; }
	inline TMP_CharacterInfoU5BU5D_t1930184704** get_address_of_m_internalCharacterInfo_180() { return &___m_internalCharacterInfo_180; }
	inline void set_m_internalCharacterInfo_180(TMP_CharacterInfoU5BU5D_t1930184704* value)
	{
		___m_internalCharacterInfo_180 = value;
		Il2CppCodeGenWriteBarrier((&___m_internalCharacterInfo_180), value);
	}

	inline static int32_t get_offset_of_m_input_CharArray_181() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_input_CharArray_181)); }
	inline CharU5BU5D_t3528271667* get_m_input_CharArray_181() const { return ___m_input_CharArray_181; }
	inline CharU5BU5D_t3528271667** get_address_of_m_input_CharArray_181() { return &___m_input_CharArray_181; }
	inline void set_m_input_CharArray_181(CharU5BU5D_t3528271667* value)
	{
		___m_input_CharArray_181 = value;
		Il2CppCodeGenWriteBarrier((&___m_input_CharArray_181), value);
	}

	inline static int32_t get_offset_of_m_charArray_Length_182() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_charArray_Length_182)); }
	inline int32_t get_m_charArray_Length_182() const { return ___m_charArray_Length_182; }
	inline int32_t* get_address_of_m_charArray_Length_182() { return &___m_charArray_Length_182; }
	inline void set_m_charArray_Length_182(int32_t value)
	{
		___m_charArray_Length_182 = value;
	}

	inline static int32_t get_offset_of_m_totalCharacterCount_183() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_totalCharacterCount_183)); }
	inline int32_t get_m_totalCharacterCount_183() const { return ___m_totalCharacterCount_183; }
	inline int32_t* get_address_of_m_totalCharacterCount_183() { return &___m_totalCharacterCount_183; }
	inline void set_m_totalCharacterCount_183(int32_t value)
	{
		___m_totalCharacterCount_183 = value;
	}

	inline static int32_t get_offset_of_m_SavedWordWrapState_184() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_SavedWordWrapState_184)); }
	inline WordWrapState_t341939652  get_m_SavedWordWrapState_184() const { return ___m_SavedWordWrapState_184; }
	inline WordWrapState_t341939652 * get_address_of_m_SavedWordWrapState_184() { return &___m_SavedWordWrapState_184; }
	inline void set_m_SavedWordWrapState_184(WordWrapState_t341939652  value)
	{
		___m_SavedWordWrapState_184 = value;
	}

	inline static int32_t get_offset_of_m_SavedLineState_185() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_SavedLineState_185)); }
	inline WordWrapState_t341939652  get_m_SavedLineState_185() const { return ___m_SavedLineState_185; }
	inline WordWrapState_t341939652 * get_address_of_m_SavedLineState_185() { return &___m_SavedLineState_185; }
	inline void set_m_SavedLineState_185(WordWrapState_t341939652  value)
	{
		___m_SavedLineState_185 = value;
	}

	inline static int32_t get_offset_of_m_characterCount_186() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_characterCount_186)); }
	inline int32_t get_m_characterCount_186() const { return ___m_characterCount_186; }
	inline int32_t* get_address_of_m_characterCount_186() { return &___m_characterCount_186; }
	inline void set_m_characterCount_186(int32_t value)
	{
		___m_characterCount_186 = value;
	}

	inline static int32_t get_offset_of_m_firstCharacterOfLine_187() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_firstCharacterOfLine_187)); }
	inline int32_t get_m_firstCharacterOfLine_187() const { return ___m_firstCharacterOfLine_187; }
	inline int32_t* get_address_of_m_firstCharacterOfLine_187() { return &___m_firstCharacterOfLine_187; }
	inline void set_m_firstCharacterOfLine_187(int32_t value)
	{
		___m_firstCharacterOfLine_187 = value;
	}

	inline static int32_t get_offset_of_m_firstVisibleCharacterOfLine_188() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_firstVisibleCharacterOfLine_188)); }
	inline int32_t get_m_firstVisibleCharacterOfLine_188() const { return ___m_firstVisibleCharacterOfLine_188; }
	inline int32_t* get_address_of_m_firstVisibleCharacterOfLine_188() { return &___m_firstVisibleCharacterOfLine_188; }
	inline void set_m_firstVisibleCharacterOfLine_188(int32_t value)
	{
		___m_firstVisibleCharacterOfLine_188 = value;
	}

	inline static int32_t get_offset_of_m_lastCharacterOfLine_189() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lastCharacterOfLine_189)); }
	inline int32_t get_m_lastCharacterOfLine_189() const { return ___m_lastCharacterOfLine_189; }
	inline int32_t* get_address_of_m_lastCharacterOfLine_189() { return &___m_lastCharacterOfLine_189; }
	inline void set_m_lastCharacterOfLine_189(int32_t value)
	{
		___m_lastCharacterOfLine_189 = value;
	}

	inline static int32_t get_offset_of_m_lastVisibleCharacterOfLine_190() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lastVisibleCharacterOfLine_190)); }
	inline int32_t get_m_lastVisibleCharacterOfLine_190() const { return ___m_lastVisibleCharacterOfLine_190; }
	inline int32_t* get_address_of_m_lastVisibleCharacterOfLine_190() { return &___m_lastVisibleCharacterOfLine_190; }
	inline void set_m_lastVisibleCharacterOfLine_190(int32_t value)
	{
		___m_lastVisibleCharacterOfLine_190 = value;
	}

	inline static int32_t get_offset_of_m_lineNumber_191() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineNumber_191)); }
	inline int32_t get_m_lineNumber_191() const { return ___m_lineNumber_191; }
	inline int32_t* get_address_of_m_lineNumber_191() { return &___m_lineNumber_191; }
	inline void set_m_lineNumber_191(int32_t value)
	{
		___m_lineNumber_191 = value;
	}

	inline static int32_t get_offset_of_m_lineVisibleCharacterCount_192() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineVisibleCharacterCount_192)); }
	inline int32_t get_m_lineVisibleCharacterCount_192() const { return ___m_lineVisibleCharacterCount_192; }
	inline int32_t* get_address_of_m_lineVisibleCharacterCount_192() { return &___m_lineVisibleCharacterCount_192; }
	inline void set_m_lineVisibleCharacterCount_192(int32_t value)
	{
		___m_lineVisibleCharacterCount_192 = value;
	}

	inline static int32_t get_offset_of_m_pageNumber_193() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_pageNumber_193)); }
	inline int32_t get_m_pageNumber_193() const { return ___m_pageNumber_193; }
	inline int32_t* get_address_of_m_pageNumber_193() { return &___m_pageNumber_193; }
	inline void set_m_pageNumber_193(int32_t value)
	{
		___m_pageNumber_193 = value;
	}

	inline static int32_t get_offset_of_m_maxAscender_194() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxAscender_194)); }
	inline float get_m_maxAscender_194() const { return ___m_maxAscender_194; }
	inline float* get_address_of_m_maxAscender_194() { return &___m_maxAscender_194; }
	inline void set_m_maxAscender_194(float value)
	{
		___m_maxAscender_194 = value;
	}

	inline static int32_t get_offset_of_m_maxCapHeight_195() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxCapHeight_195)); }
	inline float get_m_maxCapHeight_195() const { return ___m_maxCapHeight_195; }
	inline float* get_address_of_m_maxCapHeight_195() { return &___m_maxCapHeight_195; }
	inline void set_m_maxCapHeight_195(float value)
	{
		___m_maxCapHeight_195 = value;
	}

	inline static int32_t get_offset_of_m_maxDescender_196() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxDescender_196)); }
	inline float get_m_maxDescender_196() const { return ___m_maxDescender_196; }
	inline float* get_address_of_m_maxDescender_196() { return &___m_maxDescender_196; }
	inline void set_m_maxDescender_196(float value)
	{
		___m_maxDescender_196 = value;
	}

	inline static int32_t get_offset_of_m_maxLineAscender_197() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxLineAscender_197)); }
	inline float get_m_maxLineAscender_197() const { return ___m_maxLineAscender_197; }
	inline float* get_address_of_m_maxLineAscender_197() { return &___m_maxLineAscender_197; }
	inline void set_m_maxLineAscender_197(float value)
	{
		___m_maxLineAscender_197 = value;
	}

	inline static int32_t get_offset_of_m_maxLineDescender_198() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxLineDescender_198)); }
	inline float get_m_maxLineDescender_198() const { return ___m_maxLineDescender_198; }
	inline float* get_address_of_m_maxLineDescender_198() { return &___m_maxLineDescender_198; }
	inline void set_m_maxLineDescender_198(float value)
	{
		___m_maxLineDescender_198 = value;
	}

	inline static int32_t get_offset_of_m_startOfLineAscender_199() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_startOfLineAscender_199)); }
	inline float get_m_startOfLineAscender_199() const { return ___m_startOfLineAscender_199; }
	inline float* get_address_of_m_startOfLineAscender_199() { return &___m_startOfLineAscender_199; }
	inline void set_m_startOfLineAscender_199(float value)
	{
		___m_startOfLineAscender_199 = value;
	}

	inline static int32_t get_offset_of_m_lineOffset_200() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineOffset_200)); }
	inline float get_m_lineOffset_200() const { return ___m_lineOffset_200; }
	inline float* get_address_of_m_lineOffset_200() { return &___m_lineOffset_200; }
	inline void set_m_lineOffset_200(float value)
	{
		___m_lineOffset_200 = value;
	}

	inline static int32_t get_offset_of_m_meshExtents_201() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_meshExtents_201)); }
	inline Extents_t3837212874  get_m_meshExtents_201() const { return ___m_meshExtents_201; }
	inline Extents_t3837212874 * get_address_of_m_meshExtents_201() { return &___m_meshExtents_201; }
	inline void set_m_meshExtents_201(Extents_t3837212874  value)
	{
		___m_meshExtents_201 = value;
	}

	inline static int32_t get_offset_of_m_htmlColor_202() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_htmlColor_202)); }
	inline Color32_t2600501292  get_m_htmlColor_202() const { return ___m_htmlColor_202; }
	inline Color32_t2600501292 * get_address_of_m_htmlColor_202() { return &___m_htmlColor_202; }
	inline void set_m_htmlColor_202(Color32_t2600501292  value)
	{
		___m_htmlColor_202 = value;
	}

	inline static int32_t get_offset_of_m_colorStack_203() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_colorStack_203)); }
	inline TMP_XmlTagStack_1_t2164155836  get_m_colorStack_203() const { return ___m_colorStack_203; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_m_colorStack_203() { return &___m_colorStack_203; }
	inline void set_m_colorStack_203(TMP_XmlTagStack_1_t2164155836  value)
	{
		___m_colorStack_203 = value;
	}

	inline static int32_t get_offset_of_m_underlineColorStack_204() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_underlineColorStack_204)); }
	inline TMP_XmlTagStack_1_t2164155836  get_m_underlineColorStack_204() const { return ___m_underlineColorStack_204; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_m_underlineColorStack_204() { return &___m_underlineColorStack_204; }
	inline void set_m_underlineColorStack_204(TMP_XmlTagStack_1_t2164155836  value)
	{
		___m_underlineColorStack_204 = value;
	}

	inline static int32_t get_offset_of_m_strikethroughColorStack_205() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_strikethroughColorStack_205)); }
	inline TMP_XmlTagStack_1_t2164155836  get_m_strikethroughColorStack_205() const { return ___m_strikethroughColorStack_205; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_m_strikethroughColorStack_205() { return &___m_strikethroughColorStack_205; }
	inline void set_m_strikethroughColorStack_205(TMP_XmlTagStack_1_t2164155836  value)
	{
		___m_strikethroughColorStack_205 = value;
	}

	inline static int32_t get_offset_of_m_highlightColorStack_206() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_highlightColorStack_206)); }
	inline TMP_XmlTagStack_1_t2164155836  get_m_highlightColorStack_206() const { return ___m_highlightColorStack_206; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_m_highlightColorStack_206() { return &___m_highlightColorStack_206; }
	inline void set_m_highlightColorStack_206(TMP_XmlTagStack_1_t2164155836  value)
	{
		___m_highlightColorStack_206 = value;
	}

	inline static int32_t get_offset_of_m_colorGradientPreset_207() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_colorGradientPreset_207)); }
	inline TMP_ColorGradient_t3678055768 * get_m_colorGradientPreset_207() const { return ___m_colorGradientPreset_207; }
	inline TMP_ColorGradient_t3678055768 ** get_address_of_m_colorGradientPreset_207() { return &___m_colorGradientPreset_207; }
	inline void set_m_colorGradientPreset_207(TMP_ColorGradient_t3678055768 * value)
	{
		___m_colorGradientPreset_207 = value;
		Il2CppCodeGenWriteBarrier((&___m_colorGradientPreset_207), value);
	}

	inline static int32_t get_offset_of_m_colorGradientStack_208() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_colorGradientStack_208)); }
	inline TMP_XmlTagStack_1_t3241710312  get_m_colorGradientStack_208() const { return ___m_colorGradientStack_208; }
	inline TMP_XmlTagStack_1_t3241710312 * get_address_of_m_colorGradientStack_208() { return &___m_colorGradientStack_208; }
	inline void set_m_colorGradientStack_208(TMP_XmlTagStack_1_t3241710312  value)
	{
		___m_colorGradientStack_208 = value;
	}

	inline static int32_t get_offset_of_m_tabSpacing_209() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_tabSpacing_209)); }
	inline float get_m_tabSpacing_209() const { return ___m_tabSpacing_209; }
	inline float* get_address_of_m_tabSpacing_209() { return &___m_tabSpacing_209; }
	inline void set_m_tabSpacing_209(float value)
	{
		___m_tabSpacing_209 = value;
	}

	inline static int32_t get_offset_of_m_spacing_210() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spacing_210)); }
	inline float get_m_spacing_210() const { return ___m_spacing_210; }
	inline float* get_address_of_m_spacing_210() { return &___m_spacing_210; }
	inline void set_m_spacing_210(float value)
	{
		___m_spacing_210 = value;
	}

	inline static int32_t get_offset_of_m_styleStack_211() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_styleStack_211)); }
	inline TMP_XmlTagStack_1_t2514600297  get_m_styleStack_211() const { return ___m_styleStack_211; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_m_styleStack_211() { return &___m_styleStack_211; }
	inline void set_m_styleStack_211(TMP_XmlTagStack_1_t2514600297  value)
	{
		___m_styleStack_211 = value;
	}

	inline static int32_t get_offset_of_m_actionStack_212() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_actionStack_212)); }
	inline TMP_XmlTagStack_1_t2514600297  get_m_actionStack_212() const { return ___m_actionStack_212; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_m_actionStack_212() { return &___m_actionStack_212; }
	inline void set_m_actionStack_212(TMP_XmlTagStack_1_t2514600297  value)
	{
		___m_actionStack_212 = value;
	}

	inline static int32_t get_offset_of_m_padding_213() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_padding_213)); }
	inline float get_m_padding_213() const { return ___m_padding_213; }
	inline float* get_address_of_m_padding_213() { return &___m_padding_213; }
	inline void set_m_padding_213(float value)
	{
		___m_padding_213 = value;
	}

	inline static int32_t get_offset_of_m_baselineOffset_214() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_baselineOffset_214)); }
	inline float get_m_baselineOffset_214() const { return ___m_baselineOffset_214; }
	inline float* get_address_of_m_baselineOffset_214() { return &___m_baselineOffset_214; }
	inline void set_m_baselineOffset_214(float value)
	{
		___m_baselineOffset_214 = value;
	}

	inline static int32_t get_offset_of_m_baselineOffsetStack_215() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_baselineOffsetStack_215)); }
	inline TMP_XmlTagStack_1_t960921318  get_m_baselineOffsetStack_215() const { return ___m_baselineOffsetStack_215; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_m_baselineOffsetStack_215() { return &___m_baselineOffsetStack_215; }
	inline void set_m_baselineOffsetStack_215(TMP_XmlTagStack_1_t960921318  value)
	{
		___m_baselineOffsetStack_215 = value;
	}

	inline static int32_t get_offset_of_m_xAdvance_216() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_xAdvance_216)); }
	inline float get_m_xAdvance_216() const { return ___m_xAdvance_216; }
	inline float* get_address_of_m_xAdvance_216() { return &___m_xAdvance_216; }
	inline void set_m_xAdvance_216(float value)
	{
		___m_xAdvance_216 = value;
	}

	inline static int32_t get_offset_of_m_textElementType_217() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_textElementType_217)); }
	inline int32_t get_m_textElementType_217() const { return ___m_textElementType_217; }
	inline int32_t* get_address_of_m_textElementType_217() { return &___m_textElementType_217; }
	inline void set_m_textElementType_217(int32_t value)
	{
		___m_textElementType_217 = value;
	}

	inline static int32_t get_offset_of_m_cached_TextElement_218() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_cached_TextElement_218)); }
	inline TMP_TextElement_t129727469 * get_m_cached_TextElement_218() const { return ___m_cached_TextElement_218; }
	inline TMP_TextElement_t129727469 ** get_address_of_m_cached_TextElement_218() { return &___m_cached_TextElement_218; }
	inline void set_m_cached_TextElement_218(TMP_TextElement_t129727469 * value)
	{
		___m_cached_TextElement_218 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_TextElement_218), value);
	}

	inline static int32_t get_offset_of_m_cached_Underline_GlyphInfo_219() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_cached_Underline_GlyphInfo_219)); }
	inline TMP_Glyph_t581847833 * get_m_cached_Underline_GlyphInfo_219() const { return ___m_cached_Underline_GlyphInfo_219; }
	inline TMP_Glyph_t581847833 ** get_address_of_m_cached_Underline_GlyphInfo_219() { return &___m_cached_Underline_GlyphInfo_219; }
	inline void set_m_cached_Underline_GlyphInfo_219(TMP_Glyph_t581847833 * value)
	{
		___m_cached_Underline_GlyphInfo_219 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_Underline_GlyphInfo_219), value);
	}

	inline static int32_t get_offset_of_m_cached_Ellipsis_GlyphInfo_220() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_cached_Ellipsis_GlyphInfo_220)); }
	inline TMP_Glyph_t581847833 * get_m_cached_Ellipsis_GlyphInfo_220() const { return ___m_cached_Ellipsis_GlyphInfo_220; }
	inline TMP_Glyph_t581847833 ** get_address_of_m_cached_Ellipsis_GlyphInfo_220() { return &___m_cached_Ellipsis_GlyphInfo_220; }
	inline void set_m_cached_Ellipsis_GlyphInfo_220(TMP_Glyph_t581847833 * value)
	{
		___m_cached_Ellipsis_GlyphInfo_220 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_Ellipsis_GlyphInfo_220), value);
	}

	inline static int32_t get_offset_of_m_defaultSpriteAsset_221() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_defaultSpriteAsset_221)); }
	inline TMP_SpriteAsset_t484820633 * get_m_defaultSpriteAsset_221() const { return ___m_defaultSpriteAsset_221; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_defaultSpriteAsset_221() { return &___m_defaultSpriteAsset_221; }
	inline void set_m_defaultSpriteAsset_221(TMP_SpriteAsset_t484820633 * value)
	{
		___m_defaultSpriteAsset_221 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultSpriteAsset_221), value);
	}

	inline static int32_t get_offset_of_m_currentSpriteAsset_222() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_currentSpriteAsset_222)); }
	inline TMP_SpriteAsset_t484820633 * get_m_currentSpriteAsset_222() const { return ___m_currentSpriteAsset_222; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_currentSpriteAsset_222() { return &___m_currentSpriteAsset_222; }
	inline void set_m_currentSpriteAsset_222(TMP_SpriteAsset_t484820633 * value)
	{
		___m_currentSpriteAsset_222 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentSpriteAsset_222), value);
	}

	inline static int32_t get_offset_of_m_spriteCount_223() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteCount_223)); }
	inline int32_t get_m_spriteCount_223() const { return ___m_spriteCount_223; }
	inline int32_t* get_address_of_m_spriteCount_223() { return &___m_spriteCount_223; }
	inline void set_m_spriteCount_223(int32_t value)
	{
		___m_spriteCount_223 = value;
	}

	inline static int32_t get_offset_of_m_spriteIndex_224() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteIndex_224)); }
	inline int32_t get_m_spriteIndex_224() const { return ___m_spriteIndex_224; }
	inline int32_t* get_address_of_m_spriteIndex_224() { return &___m_spriteIndex_224; }
	inline void set_m_spriteIndex_224(int32_t value)
	{
		___m_spriteIndex_224 = value;
	}

	inline static int32_t get_offset_of_m_spriteAnimationID_225() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteAnimationID_225)); }
	inline int32_t get_m_spriteAnimationID_225() const { return ___m_spriteAnimationID_225; }
	inline int32_t* get_address_of_m_spriteAnimationID_225() { return &___m_spriteAnimationID_225; }
	inline void set_m_spriteAnimationID_225(int32_t value)
	{
		___m_spriteAnimationID_225 = value;
	}

	inline static int32_t get_offset_of_m_ignoreActiveState_226() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_ignoreActiveState_226)); }
	inline bool get_m_ignoreActiveState_226() const { return ___m_ignoreActiveState_226; }
	inline bool* get_address_of_m_ignoreActiveState_226() { return &___m_ignoreActiveState_226; }
	inline void set_m_ignoreActiveState_226(bool value)
	{
		___m_ignoreActiveState_226 = value;
	}

	inline static int32_t get_offset_of_k_Power_227() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___k_Power_227)); }
	inline SingleU5BU5D_t1444911251* get_k_Power_227() const { return ___k_Power_227; }
	inline SingleU5BU5D_t1444911251** get_address_of_k_Power_227() { return &___k_Power_227; }
	inline void set_k_Power_227(SingleU5BU5D_t1444911251* value)
	{
		___k_Power_227 = value;
		Il2CppCodeGenWriteBarrier((&___k_Power_227), value);
	}
};

struct TMP_Text_t2599618874_StaticFields
{
public:
	// UnityEngine.Color32 TMPro.TMP_Text::s_colorWhite
	Color32_t2600501292  ___s_colorWhite_47;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargePositiveVector2
	Vector2_t2156229523  ___k_LargePositiveVector2_228;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargeNegativeVector2
	Vector2_t2156229523  ___k_LargeNegativeVector2_229;
	// System.Single TMPro.TMP_Text::k_LargePositiveFloat
	float ___k_LargePositiveFloat_230;
	// System.Single TMPro.TMP_Text::k_LargeNegativeFloat
	float ___k_LargeNegativeFloat_231;
	// System.Int32 TMPro.TMP_Text::k_LargePositiveInt
	int32_t ___k_LargePositiveInt_232;
	// System.Int32 TMPro.TMP_Text::k_LargeNegativeInt
	int32_t ___k_LargeNegativeInt_233;

public:
	inline static int32_t get_offset_of_s_colorWhite_47() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___s_colorWhite_47)); }
	inline Color32_t2600501292  get_s_colorWhite_47() const { return ___s_colorWhite_47; }
	inline Color32_t2600501292 * get_address_of_s_colorWhite_47() { return &___s_colorWhite_47; }
	inline void set_s_colorWhite_47(Color32_t2600501292  value)
	{
		___s_colorWhite_47 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveVector2_228() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargePositiveVector2_228)); }
	inline Vector2_t2156229523  get_k_LargePositiveVector2_228() const { return ___k_LargePositiveVector2_228; }
	inline Vector2_t2156229523 * get_address_of_k_LargePositiveVector2_228() { return &___k_LargePositiveVector2_228; }
	inline void set_k_LargePositiveVector2_228(Vector2_t2156229523  value)
	{
		___k_LargePositiveVector2_228 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeVector2_229() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargeNegativeVector2_229)); }
	inline Vector2_t2156229523  get_k_LargeNegativeVector2_229() const { return ___k_LargeNegativeVector2_229; }
	inline Vector2_t2156229523 * get_address_of_k_LargeNegativeVector2_229() { return &___k_LargeNegativeVector2_229; }
	inline void set_k_LargeNegativeVector2_229(Vector2_t2156229523  value)
	{
		___k_LargeNegativeVector2_229 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveFloat_230() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargePositiveFloat_230)); }
	inline float get_k_LargePositiveFloat_230() const { return ___k_LargePositiveFloat_230; }
	inline float* get_address_of_k_LargePositiveFloat_230() { return &___k_LargePositiveFloat_230; }
	inline void set_k_LargePositiveFloat_230(float value)
	{
		___k_LargePositiveFloat_230 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeFloat_231() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargeNegativeFloat_231)); }
	inline float get_k_LargeNegativeFloat_231() const { return ___k_LargeNegativeFloat_231; }
	inline float* get_address_of_k_LargeNegativeFloat_231() { return &___k_LargeNegativeFloat_231; }
	inline void set_k_LargeNegativeFloat_231(float value)
	{
		___k_LargeNegativeFloat_231 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveInt_232() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargePositiveInt_232)); }
	inline int32_t get_k_LargePositiveInt_232() const { return ___k_LargePositiveInt_232; }
	inline int32_t* get_address_of_k_LargePositiveInt_232() { return &___k_LargePositiveInt_232; }
	inline void set_k_LargePositiveInt_232(int32_t value)
	{
		___k_LargePositiveInt_232 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeInt_233() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargeNegativeInt_233)); }
	inline int32_t get_k_LargeNegativeInt_233() const { return ___k_LargeNegativeInt_233; }
	inline int32_t* get_address_of_k_LargeNegativeInt_233() { return &___k_LargeNegativeInt_233; }
	inline void set_k_LargeNegativeInt_233(int32_t value)
	{
		___k_LargeNegativeInt_233 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXT_T2599618874_H
#ifndef IMAGE_T2670269651_H
#define IMAGE_T2670269651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image
struct  Image_t2670269651  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t280657092 * ___m_Sprite_31;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t280657092 * ___m_OverrideSprite_32;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_33;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_34;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_35;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_36;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_37;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_38;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_39;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_40;

public:
	inline static int32_t get_offset_of_m_Sprite_31() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Sprite_31)); }
	inline Sprite_t280657092 * get_m_Sprite_31() const { return ___m_Sprite_31; }
	inline Sprite_t280657092 ** get_address_of_m_Sprite_31() { return &___m_Sprite_31; }
	inline void set_m_Sprite_31(Sprite_t280657092 * value)
	{
		___m_Sprite_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_Sprite_31), value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_32() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_OverrideSprite_32)); }
	inline Sprite_t280657092 * get_m_OverrideSprite_32() const { return ___m_OverrideSprite_32; }
	inline Sprite_t280657092 ** get_address_of_m_OverrideSprite_32() { return &___m_OverrideSprite_32; }
	inline void set_m_OverrideSprite_32(Sprite_t280657092 * value)
	{
		___m_OverrideSprite_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_OverrideSprite_32), value);
	}

	inline static int32_t get_offset_of_m_Type_33() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Type_33)); }
	inline int32_t get_m_Type_33() const { return ___m_Type_33; }
	inline int32_t* get_address_of_m_Type_33() { return &___m_Type_33; }
	inline void set_m_Type_33(int32_t value)
	{
		___m_Type_33 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_34() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_PreserveAspect_34)); }
	inline bool get_m_PreserveAspect_34() const { return ___m_PreserveAspect_34; }
	inline bool* get_address_of_m_PreserveAspect_34() { return &___m_PreserveAspect_34; }
	inline void set_m_PreserveAspect_34(bool value)
	{
		___m_PreserveAspect_34 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_35() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillCenter_35)); }
	inline bool get_m_FillCenter_35() const { return ___m_FillCenter_35; }
	inline bool* get_address_of_m_FillCenter_35() { return &___m_FillCenter_35; }
	inline void set_m_FillCenter_35(bool value)
	{
		___m_FillCenter_35 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_36() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillMethod_36)); }
	inline int32_t get_m_FillMethod_36() const { return ___m_FillMethod_36; }
	inline int32_t* get_address_of_m_FillMethod_36() { return &___m_FillMethod_36; }
	inline void set_m_FillMethod_36(int32_t value)
	{
		___m_FillMethod_36 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_37() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillAmount_37)); }
	inline float get_m_FillAmount_37() const { return ___m_FillAmount_37; }
	inline float* get_address_of_m_FillAmount_37() { return &___m_FillAmount_37; }
	inline void set_m_FillAmount_37(float value)
	{
		___m_FillAmount_37 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_38() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillClockwise_38)); }
	inline bool get_m_FillClockwise_38() const { return ___m_FillClockwise_38; }
	inline bool* get_address_of_m_FillClockwise_38() { return &___m_FillClockwise_38; }
	inline void set_m_FillClockwise_38(bool value)
	{
		___m_FillClockwise_38 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_39() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillOrigin_39)); }
	inline int32_t get_m_FillOrigin_39() const { return ___m_FillOrigin_39; }
	inline int32_t* get_address_of_m_FillOrigin_39() { return &___m_FillOrigin_39; }
	inline void set_m_FillOrigin_39(int32_t value)
	{
		___m_FillOrigin_39 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_40() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_AlphaHitTestMinimumThreshold_40)); }
	inline float get_m_AlphaHitTestMinimumThreshold_40() const { return ___m_AlphaHitTestMinimumThreshold_40; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_40() { return &___m_AlphaHitTestMinimumThreshold_40; }
	inline void set_m_AlphaHitTestMinimumThreshold_40(float value)
	{
		___m_AlphaHitTestMinimumThreshold_40 = value;
	}
};

struct Image_t2670269651_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t340375123 * ___s_ETC1DefaultUI_30;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_t1457185986* ___s_VertScratch_41;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_t1457185986* ___s_UVScratch_42;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t1718750761* ___s_Xy_43;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t1718750761* ___s_Uv_44;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_30() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_ETC1DefaultUI_30)); }
	inline Material_t340375123 * get_s_ETC1DefaultUI_30() const { return ___s_ETC1DefaultUI_30; }
	inline Material_t340375123 ** get_address_of_s_ETC1DefaultUI_30() { return &___s_ETC1DefaultUI_30; }
	inline void set_s_ETC1DefaultUI_30(Material_t340375123 * value)
	{
		___s_ETC1DefaultUI_30 = value;
		Il2CppCodeGenWriteBarrier((&___s_ETC1DefaultUI_30), value);
	}

	inline static int32_t get_offset_of_s_VertScratch_41() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_VertScratch_41)); }
	inline Vector2U5BU5D_t1457185986* get_s_VertScratch_41() const { return ___s_VertScratch_41; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_VertScratch_41() { return &___s_VertScratch_41; }
	inline void set_s_VertScratch_41(Vector2U5BU5D_t1457185986* value)
	{
		___s_VertScratch_41 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertScratch_41), value);
	}

	inline static int32_t get_offset_of_s_UVScratch_42() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_UVScratch_42)); }
	inline Vector2U5BU5D_t1457185986* get_s_UVScratch_42() const { return ___s_UVScratch_42; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_UVScratch_42() { return &___s_UVScratch_42; }
	inline void set_s_UVScratch_42(Vector2U5BU5D_t1457185986* value)
	{
		___s_UVScratch_42 = value;
		Il2CppCodeGenWriteBarrier((&___s_UVScratch_42), value);
	}

	inline static int32_t get_offset_of_s_Xy_43() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Xy_43)); }
	inline Vector3U5BU5D_t1718750761* get_s_Xy_43() const { return ___s_Xy_43; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Xy_43() { return &___s_Xy_43; }
	inline void set_s_Xy_43(Vector3U5BU5D_t1718750761* value)
	{
		___s_Xy_43 = value;
		Il2CppCodeGenWriteBarrier((&___s_Xy_43), value);
	}

	inline static int32_t get_offset_of_s_Uv_44() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Uv_44)); }
	inline Vector3U5BU5D_t1718750761* get_s_Uv_44() const { return ___s_Uv_44; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Uv_44() { return &___s_Uv_44; }
	inline void set_s_Uv_44(Vector3U5BU5D_t1718750761* value)
	{
		___s_Uv_44 = value;
		Il2CppCodeGenWriteBarrier((&___s_Uv_44), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGE_T2670269651_H


// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// System.Void System.Func`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_1__ctor_m1399073142_gshared (Func_1_t3822001908 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_time()
extern "C"  float Time_get_time_m2907476221 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3600365921 * GameObject_get_transform_m1369836730 (GameObject_t1113636619 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t3722313464  Transform_get_position_m36019626 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m3353183577 (Vector3_t3722313464 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Distance_m886789632 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Anim::TimeTracker()
extern "C"  RuntimeObject* Anim_TimeTracker_m1581750217 (Anim_t271495542 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t3829159415 * MonoBehaviour_StartCoroutine_m3411253000 (MonoBehaviour_t3962482529 * __this, RuntimeObject* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Vector3_Lerp_m407887542 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m3387557959 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Anim/<TimeTracker>c__Iterator0::.ctor()
extern "C"  void U3CTimeTrackerU3Ec__Iterator0__ctor_m1471372418 (U3CTimeTrackerU3Ec__Iterator0_t1744683265 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C"  void WaitForSeconds__ctor_m2199082655 (WaitForSeconds_t1699091251 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m2730133172 (NotSupportedException_t1314879016 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Animimp::TimeTracker()
extern "C"  RuntimeObject* Animimp_TimeTracker_m2170675636 (Animimp_t3408691587 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Animimp/<TimeTracker>c__Iterator0::.ctor()
extern "C"  void U3CTimeTrackerU3Ec__Iterator0__ctor_m2697886460 (U3CTimeTrackerU3Ec__Iterator0_t3157006910 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Animinf::TimeTracker()
extern "C"  RuntimeObject* Animinf_TimeTracker_m1620902351 (Animinf_t989049961 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Animinf/<TimeTracker>c__Iterator0::.ctor()
extern "C"  void U3CTimeTrackerU3Ec__Iterator0__ctor_m184673188 (U3CTimeTrackerU3Ec__Iterator0_t1213291940 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Random::.ctor()
extern "C"  void Random__ctor_m4122933043 (Random_t108471755 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::FindGameObjectWithTag(System.String)
extern "C"  GameObject_t1113636619 * GameObject_FindGameObjectWithTag_m2129039296 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<Anim>()
#define GameObject_GetComponent_TisAnim_t271495542_m3559005964(__this, method) ((  Anim_t271495542 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C"  void GameObject_SetActive_m796801857 (GameObject_t1113636619 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Battle::set_MaxHealth(System.Single)
extern "C"  void Battle_set_MaxHealth_m4182892573 (Battle_t2191861408 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Battle::get_MaxHealth()
extern "C"  float Battle_get_MaxHealth_m2763667083 (Battle_t2191861408 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Battle::set_CurrentHealth(System.Single)
extern "C"  void Battle_set_CurrentHealth_m4268259891 (Battle_t2191861408 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Battle::get_CurrentHealth()
extern "C"  float Battle_get_CurrentHealth_m2136656731 (Battle_t2191861408 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Single::ToString()
extern "C"  String_t* Single_ToString_m3947131094 (float* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m3937257545 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TMPro.TMP_Text::set_text(System.String)
extern "C"  void TMP_Text_set_text_m1216996582 (TMP_Text_t2599618874 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Battle::CalcHealth()
extern "C"  float Battle_CalcHealth_m4040825151 (Battle_t2191861408 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Battle::set_MaxHealthGuy(System.Single)
extern "C"  void Battle_set_MaxHealthGuy_m2194838754 (Battle_t2191861408 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Battle::get_MaxHealthGuy()
extern "C"  float Battle_get_MaxHealthGuy_m876340381 (Battle_t2191861408 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Battle::set_CurrentHealthGuy(System.Single)
extern "C"  void Battle_set_CurrentHealthGuy_m2682337132 (Battle_t2191861408 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Battle::get_CurrentHealthGuy()
extern "C"  float Battle_get_CurrentHealthGuy_m1411648811 (Battle_t2191861408 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Battle::CalcHealthGuy()
extern "C"  float Battle_CalcHealthGuy_m3105725521 (Battle_t2191861408 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Battle::EnemyAttack()
extern "C"  RuntimeObject* Battle_EnemyAttack_m1219160329 (Battle_t2191861408 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
extern "C"  bool Input_GetKeyDown_m17791917 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32)
extern "C"  void SceneManager_LoadScene_m3463216446 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m3755062657 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Battle::DealDamage(System.Single)
extern "C"  void Battle_DealDamage_m3118024972 (Battle_t2191861408 * __this, float ___damageValue0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Battle::Die()
extern "C"  void Battle_Die_m2800284142 (Battle_t2191861408 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Battle/<EnemyAttack>c__Iterator0::.ctor()
extern "C"  void U3CEnemyAttackU3Ec__Iterator0__ctor_m3060783474 (U3CEnemyAttackU3Ec__Iterator0_t1682970516 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Battle::DieGuy()
extern "C"  void Battle_DieGuy_m536536778 (Battle_t2191861408 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::Play()
extern "C"  void AudioSource_Play_m48294159 (AudioSource_t3935305588 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Battle::Continue()
extern "C"  RuntimeObject* Battle_Continue_m1384187413 (Battle_t2191861408 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Battle/<Continue>c__Iterator1::.ctor()
extern "C"  void U3CContinueU3Ec__Iterator1__ctor_m3300558351 (U3CContinueU3Ec__Iterator1_t642798636 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Battle::DIe()
extern "C"  RuntimeObject* Battle_DIe_m2481910125 (Battle_t2191861408 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Battle/<DIe>c__Iterator2::.ctor()
extern "C"  void U3CDIeU3Ec__Iterator2__ctor_m2870459626 (U3CDIeU3Ec__Iterator2_t558276709 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetBool(System.String,System.Boolean)
extern "C"  void Animator_SetBool_m234840832 (Animator_t434523843 * __this, String_t* p0, bool p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Func`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Func_1__ctor_m1399073142(__this, p0, p1, method) ((  void (*) (Func_1_t3822001908 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_1__ctor_m1399073142_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.WaitUntil::.ctor(System.Func`1<System.Boolean>)
extern "C"  void WaitUntil__ctor_m4227046299 (WaitUntil_t3373419216 * __this, Func_1_t3822001908 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Battle::EnemyDealDamage(System.Single)
extern "C"  void Battle_EnemyDealDamage_m2866959850 (Battle_t2191861408 * __this, float ___damageValue0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<Animimp>()
#define GameObject_GetComponent_TisAnimimp_t3408691587_m1937797106(__this, method) ((  Animimp_t3408691587 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void Battleimp::set_MaxHealth(System.Single)
extern "C"  void Battleimp_set_MaxHealth_m603327789 (Battleimp_t158148094 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Battleimp::get_MaxHealth()
extern "C"  float Battleimp_get_MaxHealth_m961871286 (Battleimp_t158148094 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Battleimp::set_CurrentHealth(System.Single)
extern "C"  void Battleimp_set_CurrentHealth_m826649395 (Battleimp_t158148094 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Battleimp::get_CurrentHealth()
extern "C"  float Battleimp_get_CurrentHealth_m1013476409 (Battleimp_t158148094 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Battleimp::CalcHealth()
extern "C"  float Battleimp_CalcHealth_m3863819909 (Battleimp_t158148094 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Battleimp::set_MaxHealthGuy(System.Single)
extern "C"  void Battleimp_set_MaxHealthGuy_m322227139 (Battleimp_t158148094 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Battleimp::get_MaxHealthGuy()
extern "C"  float Battleimp_get_MaxHealthGuy_m3578614347 (Battleimp_t158148094 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Battleimp::set_CurrentHealthGuy(System.Single)
extern "C"  void Battleimp_set_CurrentHealthGuy_m2081096126 (Battleimp_t158148094 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Battleimp::get_CurrentHealthGuy()
extern "C"  float Battleimp_get_CurrentHealthGuy_m437652102 (Battleimp_t158148094 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Battleimp::CalcHealthGuy()
extern "C"  float Battleimp_CalcHealthGuy_m1665544502 (Battleimp_t158148094 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Battleimp::EnemyAttack()
extern "C"  RuntimeObject* Battleimp_EnemyAttack_m1443215274 (Battleimp_t158148094 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Battleimp::DealDamage(System.Single)
extern "C"  void Battleimp_DealDamage_m2481885304 (Battleimp_t158148094 * __this, float ___damageValue0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Battleimp/<EnemyAttack>c__Iterator0::.ctor()
extern "C"  void U3CEnemyAttackU3Ec__Iterator0__ctor_m1449397385 (U3CEnemyAttackU3Ec__Iterator0_t889231986 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Battleimp::DieGuy()
extern "C"  void Battleimp_DieGuy_m2108545423 (Battleimp_t158148094 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Battleimp/<Continue>c__Iterator1::.ctor()
extern "C"  void U3CContinueU3Ec__Iterator1__ctor_m131153455 (U3CContinueU3Ec__Iterator1_t2046304316 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Battleimp::Continue()
extern "C"  RuntimeObject* Battleimp_Continue_m2989872615 (Battleimp_t158148094 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Battleimp::EnemyDealDamage(System.Single)
extern "C"  void Battleimp_EnemyDealDamage_m1731020462 (Battleimp_t158148094 * __this, float ___damageValue0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<Animinf>()
#define GameObject_GetComponent_TisAniminf_t989049961_m1535312041(__this, method) ((  Animinf_t989049961 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void Battleinf::set_MaxHealth(System.Single)
extern "C"  void Battleinf_set_MaxHealth_m500050573 (Battleinf_t1732126188 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Battleinf::get_MaxHealth()
extern "C"  float Battleinf_get_MaxHealth_m3113673643 (Battleinf_t1732126188 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Battleinf::set_CurrentHealth(System.Single)
extern "C"  void Battleinf_set_CurrentHealth_m3599131987 (Battleinf_t1732126188 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Battleinf::get_CurrentHealth()
extern "C"  float Battleinf_get_CurrentHealth_m3568673109 (Battleinf_t1732126188 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Battleinf::CalcHealth()
extern "C"  float Battleinf_CalcHealth_m705597414 (Battleinf_t1732126188 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Battleinf::set_MaxHealthGuy(System.Single)
extern "C"  void Battleinf_set_MaxHealthGuy_m3424096180 (Battleinf_t1732126188 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Battleinf::get_MaxHealthGuy()
extern "C"  float Battleinf_get_MaxHealthGuy_m2575423585 (Battleinf_t1732126188 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Battleinf::set_CurrentHealthGuy(System.Single)
extern "C"  void Battleinf_set_CurrentHealthGuy_m2983613390 (Battleinf_t1732126188 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Battleinf::get_CurrentHealthGuy()
extern "C"  float Battleinf_get_CurrentHealthGuy_m560545868 (Battleinf_t1732126188 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Battleinf::CalcHealthGuy()
extern "C"  float Battleinf_CalcHealthGuy_m1174535655 (Battleinf_t1732126188 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m904156431 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color32::op_Implicit(UnityEngine.Color32)
extern "C"  Color_t2555686324  Color32_op_Implicit_m213813866 (RuntimeObject * __this /* static, unused */, Color32_t2600501292  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpriteRenderer::set_color(UnityEngine.Color)
extern "C"  void SpriteRenderer_set_color_m3056777566 (SpriteRenderer_t3235626157 * __this, Color_t2555686324  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Battleinf::EnemyAttack()
extern "C"  RuntimeObject* Battleinf_EnemyAttack_m3332119496 (Battleinf_t1732126188 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Battleinf::DealDamage(System.Single)
extern "C"  void Battleinf_DealDamage_m3678906129 (Battleinf_t1732126188 * __this, float ___damageValue0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Battleinf::Die()
extern "C"  void Battleinf_Die_m3352291207 (Battleinf_t1732126188 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Battleinf/<EnemyAttack>c__Iterator0::.ctor()
extern "C"  void U3CEnemyAttackU3Ec__Iterator0__ctor_m3824501192 (U3CEnemyAttackU3Ec__Iterator0_t2884124416 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Battleinf::DieGuy()
extern "C"  void Battleinf_DieGuy_m3765556335 (Battleinf_t1732126188 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Battleinf::Continue()
extern "C"  RuntimeObject* Battleinf_Continue_m985971343 (Battleinf_t1732126188 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Battleinf/<Continue>c__Iterator1::.ctor()
extern "C"  void U3CContinueU3Ec__Iterator1__ctor_m1400880857 (U3CContinueU3Ec__Iterator1_t1090546628 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Battleinf::DIe()
extern "C"  RuntimeObject* Battleinf_DIe_m2804138209 (Battleinf_t1732126188 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Battleinf/<DIe>c__Iterator2::.ctor()
extern "C"  void U3CDIeU3Ec__Iterator2__ctor_m4284698291 (U3CDIeU3Ec__Iterator2_t3004093797 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Battleinf::EnemyDealDamage(System.Single)
extern "C"  void Battleinf_EnemyDealDamage_m3350442186 (Battleinf_t1732126188 * __this, float ___damageValue0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color32::.ctor(System.Byte,System.Byte,System.Byte,System.Byte)
extern "C"  void Color32__ctor_m4150508762 (Color32_t2600501292 * __this, uint8_t p0, uint8_t p1, uint8_t p2, uint8_t p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Harmony.Program::Loading()
extern "C"  RuntimeObject* Program_Loading_m47061019 (Program_t1866495201 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Harmony.Program/<Loading>c__Iterator0::.ctor()
extern "C"  void U3CLoadingU3Ec__Iterator0__ctor_m2551927464 (U3CLoadingU3Ec__Iterator0_t3071600265 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Harmony.Program/<PlayBootSFX>c__Iterator1::.ctor()
extern "C"  void U3CPlayBootSFXU3Ec__Iterator1__ctor_m3619584200 (U3CPlayBootSFXU3Ec__Iterator1_t3195710289 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Harmony.Type::TypeText(System.String,TMPro.TMP_Text,System.Single)
extern "C"  void Type_TypeText_m4150723822 (RuntimeObject * __this /* static, unused */, String_t* ___message0, TMP_Text_t2599618874 * ___textDes1, float ___speed2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Harmony.Program::PlayBootSFX()
extern "C"  RuntimeObject* Program_PlayBootSFX_m781471345 (Program_t1866495201 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Harmony.Program2::Loading()
extern "C"  RuntimeObject* Program2_Loading_m3138989863 (Program2_t3485103329 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Harmony.Program2/<Loading>c__Iterator0::.ctor()
extern "C"  void U3CLoadingU3Ec__Iterator0__ctor_m4231034442 (U3CLoadingU3Ec__Iterator0_t2822561722 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Harmony.Type2::TypeText(System.String,TMPro.TMP_Text,System.Single)
extern "C"  void Type2_TypeText_m1698800569 (RuntimeObject * __this /* static, unused */, String_t* ___message0, TMP_Text_t2599618874 * ___textDes1, float ___speed2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Harmony.Program3::Loading()
extern "C"  RuntimeObject* Program3_Loading_m1854714262 (Program3_t1528788193 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Harmony.Program3/<Loading>c__Iterator0::.ctor()
extern "C"  void U3CLoadingU3Ec__Iterator0__ctor_m1320084545 (U3CLoadingU3Ec__Iterator0_t2437702791 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Harmony.Type3::TypeText(System.String,TMPro.TMP_Text,System.Single)
extern "C"  void Type3_TypeText_m2630213857 (RuntimeObject * __this /* static, unused */, String_t* ___message0, TMP_Text_t2599618874 * ___textDes1, float ___speed2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
extern "C"  void PlayerPrefs_SetInt_m2842000469 (RuntimeObject * __this /* static, unused */, String_t* p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PlayerPrefs::Save()
extern "C"  void PlayerPrefs_Save_m2701929462 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<Harmony.Program>()
#define GameObject_GetComponent_TisProgram_t1866495201_m3502787894(__this, method) ((  Program_t1866495201 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Collections.IEnumerator Harmony.Type::AnimateText(System.String,TMPro.TMP_Text,System.Single)
extern "C"  RuntimeObject* Type_AnimateText_m2819578080 (RuntimeObject * __this /* static, unused */, String_t* ___message0, TMP_Text_t2599618874 * ___textDes1, float ___speed2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Harmony.Type/<AnimateText>c__Iterator0::.ctor()
extern "C"  void U3CAnimateTextU3Ec__Iterator0__ctor_m4276049717 (U3CAnimateTextU3Ec__Iterator0_t429204900 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Substring(System.Int32,System.Int32)
extern "C"  String_t* String_Substring_m1610150815 (String_t* __this, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::get_Length()
extern "C"  int32_t String_get_Length_m3847582255 (String_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<Harmony.Program2>()
#define GameObject_GetComponent_TisProgram2_t3485103329_m3599024581(__this, method) ((  Program2_t3485103329 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Collections.IEnumerator Harmony.Type2::AnimateText(System.String,TMPro.TMP_Text,System.Single)
extern "C"  RuntimeObject* Type2_AnimateText_m909537142 (RuntimeObject * __this /* static, unused */, String_t* ___message0, TMP_Text_t2599618874 * ___textDes1, float ___speed2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Harmony.Type2/<AnimateText>c__Iterator0::.ctor()
extern "C"  void U3CAnimateTextU3Ec__Iterator0__ctor_m3226696527 (U3CAnimateTextU3Ec__Iterator0_t589720723 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<Harmony.Program3>()
#define GameObject_GetComponent_TisProgram3_t1528788193_m3599024580(__this, method) ((  Program3_t1528788193 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Collections.IEnumerator Harmony.Type3::AnimateText(System.String,TMPro.TMP_Text,System.Single)
extern "C"  RuntimeObject* Type3_AnimateText_m1584460279 (RuntimeObject * __this /* static, unused */, String_t* ___message0, TMP_Text_t2599618874 * ___textDes1, float ___speed2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Harmony.Type3/<AnimateText>c__Iterator0::.ctor()
extern "C"  void U3CAnimateTextU3Ec__Iterator0__ctor_m4028396701 (U3CAnimateTextU3Ec__Iterator0_t772139121 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Harmony.menuProgram::Loading()
extern "C"  RuntimeObject* menuProgram_Loading_m616370398 (menuProgram_t465836879 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String)
extern "C"  int32_t PlayerPrefs_GetInt_m3797620966 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application::Quit()
extern "C"  void Application_Quit_m470877999 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Harmony.menuProgram/<Loading>c__Iterator0::.ctor()
extern "C"  void U3CLoadingU3Ec__Iterator0__ctor_m2397844334 (U3CLoadingU3Ec__Iterator0_t2464126437 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<Battle>()
#define GameObject_GetComponent_TisBattle_t2191861408_m3384264100(__this, method) ((  Battle_t2191861408 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void Sprite::PlayAnim()
extern "C"  void Sprite_PlayAnim_m2275555825 (Sprite_t2249211761 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::Play(System.String)
extern "C"  void Animator_Play_m1697843332 (Animator_t434523843 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<Battleimp>()
#define GameObject_GetComponent_TisBattleimp_t158148094_m3358208344(__this, method) ((  Battleimp_t158148094 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void Spriteimp::PlayAnim()
extern "C"  void Spriteimp_PlayAnim_m1100634416 (Spriteimp_t3952321968 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<Battleinf>()
#define GameObject_GetComponent_TisBattleinf_t1732126188_m3397136827(__this, method) ((  Battleinf_t1732126188 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void Spriteinf::PlayAnim()
extern "C"  void Spriteinf_PlayAnim_m139970992 (Spriteinf_t2378343838 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Anim::.ctor()
extern "C"  void Anim__ctor_m4009390434 (Anim_t271495542 * __this, const RuntimeMethod* method)
{
	{
		__this->set_speed_10((1.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Anim::Start()
extern "C"  void Anim_Start_m349140766 (Anim_t271495542 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Anim_Start_m349140766_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_startTime_11(L_0);
		GameObject_t1113636619 * L_1 = __this->get_quad1_4();
		Transform_t3600365921 * L_2 = GameObject_get_transform_m1369836730(L_1, /*hidden argument*/NULL);
		Vector3_t3722313464  L_3 = Transform_get_position_m36019626(L_2, /*hidden argument*/NULL);
		Vector3_t3722313464  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector3__ctor_m3353183577((&L_4), (-16.55f), (-0.013f), (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		float L_5 = Vector3_Distance_m886789632(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		__this->set_journeyLength_12(L_5);
		GameObject_t1113636619 * L_6 = __this->get_quad2_5();
		Transform_t3600365921 * L_7 = GameObject_get_transform_m1369836730(L_6, /*hidden argument*/NULL);
		Vector3_t3722313464  L_8 = Transform_get_position_m36019626(L_7, /*hidden argument*/NULL);
		Vector3_t3722313464  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m3353183577((&L_9), (16.42f), (-0.016f), (0.0f), /*hidden argument*/NULL);
		float L_10 = Vector3_Distance_m886789632(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		__this->set_journeyLength2_13(L_10);
		GameObject_t1113636619 * L_11 = __this->get_dog_6();
		Transform_t3600365921 * L_12 = GameObject_get_transform_m1369836730(L_11, /*hidden argument*/NULL);
		Vector3_t3722313464  L_13 = Transform_get_position_m36019626(L_12, /*hidden argument*/NULL);
		Vector3_t3722313464  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m3353183577((&L_14), (-4.0f), (5.82f), (0.5f), /*hidden argument*/NULL);
		float L_15 = Vector3_Distance_m886789632(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		__this->set_journeyLength3_14(L_15);
		GameObject_t1113636619 * L_16 = __this->get_guy_7();
		Transform_t3600365921 * L_17 = GameObject_get_transform_m1369836730(L_16, /*hidden argument*/NULL);
		Vector3_t3722313464  L_18 = Transform_get_position_m36019626(L_17, /*hidden argument*/NULL);
		Vector3_t3722313464  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Vector3__ctor_m3353183577((&L_19), (6.64f), (-2.01f), (0.5f), /*hidden argument*/NULL);
		float L_20 = Vector3_Distance_m886789632(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		__this->set_journeyLength4_15(L_20);
		RuntimeObject* L_21 = Anim_TimeTracker_m1581750217(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_21, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Anim::Update()
extern "C"  void Anim_Update_m4207990122 (Anim_t271495542 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Anim_Update_m4207990122_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	{
		float L_0 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_startTime_11();
		float L_2 = __this->get_speed_10();
		V_0 = ((float)((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_0, (float)L_1)), (float)L_2))/(float)(10.0f)));
		float L_3 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = __this->get_startTime_11();
		float L_5 = __this->get_speed_10();
		V_1 = ((float)((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_3, (float)L_4)), (float)L_5))/(float)(10.0f)));
		float L_6 = V_0;
		float L_7 = __this->get_journeyLength_12();
		V_2 = ((float)((float)L_6/(float)L_7));
		float L_8 = V_0;
		float L_9 = __this->get_journeyLength2_13();
		V_3 = ((float)((float)L_8/(float)L_9));
		float L_10 = V_1;
		float L_11 = __this->get_journeyLength3_14();
		V_4 = ((float)((float)L_10/(float)L_11));
		float L_12 = V_1;
		float L_13 = __this->get_journeyLength4_15();
		V_5 = ((float)((float)L_12/(float)L_13));
		GameObject_t1113636619 * L_14 = __this->get_quad1_4();
		Transform_t3600365921 * L_15 = GameObject_get_transform_m1369836730(L_14, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_16 = __this->get_quad1_4();
		Transform_t3600365921 * L_17 = GameObject_get_transform_m1369836730(L_16, /*hidden argument*/NULL);
		Vector3_t3722313464  L_18 = Transform_get_position_m36019626(L_17, /*hidden argument*/NULL);
		Vector3_t3722313464  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Vector3__ctor_m3353183577((&L_19), (-16.55f), (-0.013f), (0.0f), /*hidden argument*/NULL);
		float L_20 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_21 = Vector3_Lerp_m407887542(NULL /*static, unused*/, L_18, L_19, L_20, /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_15, L_21, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_22 = __this->get_quad2_5();
		Transform_t3600365921 * L_23 = GameObject_get_transform_m1369836730(L_22, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_24 = __this->get_quad2_5();
		Transform_t3600365921 * L_25 = GameObject_get_transform_m1369836730(L_24, /*hidden argument*/NULL);
		Vector3_t3722313464  L_26 = Transform_get_position_m36019626(L_25, /*hidden argument*/NULL);
		Vector3_t3722313464  L_27;
		memset(&L_27, 0, sizeof(L_27));
		Vector3__ctor_m3353183577((&L_27), (16.42f), (-0.016f), (0.0f), /*hidden argument*/NULL);
		float L_28 = V_3;
		Vector3_t3722313464  L_29 = Vector3_Lerp_m407887542(NULL /*static, unused*/, L_26, L_27, L_28, /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_23, L_29, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_30 = __this->get_dog_6();
		Transform_t3600365921 * L_31 = GameObject_get_transform_m1369836730(L_30, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_32 = __this->get_dog_6();
		Transform_t3600365921 * L_33 = GameObject_get_transform_m1369836730(L_32, /*hidden argument*/NULL);
		Vector3_t3722313464  L_34 = Transform_get_position_m36019626(L_33, /*hidden argument*/NULL);
		Vector3_t3722313464  L_35;
		memset(&L_35, 0, sizeof(L_35));
		Vector3__ctor_m3353183577((&L_35), (-4.0f), (5.82f), (0.5f), /*hidden argument*/NULL);
		float L_36 = V_4;
		Vector3_t3722313464  L_37 = Vector3_Lerp_m407887542(NULL /*static, unused*/, L_34, L_35, L_36, /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_31, L_37, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_38 = __this->get_guy_7();
		Transform_t3600365921 * L_39 = GameObject_get_transform_m1369836730(L_38, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_40 = __this->get_guy_7();
		Transform_t3600365921 * L_41 = GameObject_get_transform_m1369836730(L_40, /*hidden argument*/NULL);
		Vector3_t3722313464  L_42 = Transform_get_position_m36019626(L_41, /*hidden argument*/NULL);
		Vector3_t3722313464  L_43;
		memset(&L_43, 0, sizeof(L_43));
		Vector3__ctor_m3353183577((&L_43), (6.64f), (-2.01f), (0.5f), /*hidden argument*/NULL);
		float L_44 = V_5;
		Vector3_t3722313464  L_45 = Vector3_Lerp_m407887542(NULL /*static, unused*/, L_42, L_43, ((float)il2cpp_codegen_multiply((float)L_44, (float)(2.0f))), /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_39, L_45, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Anim::TimeTracker()
extern "C"  RuntimeObject* Anim_TimeTracker_m1581750217 (Anim_t271495542 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Anim_TimeTracker_m1581750217_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CTimeTrackerU3Ec__Iterator0_t1744683265 * V_0 = NULL;
	{
		U3CTimeTrackerU3Ec__Iterator0_t1744683265 * L_0 = (U3CTimeTrackerU3Ec__Iterator0_t1744683265 *)il2cpp_codegen_object_new(U3CTimeTrackerU3Ec__Iterator0_t1744683265_il2cpp_TypeInfo_var);
		U3CTimeTrackerU3Ec__Iterator0__ctor_m1471372418(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CTimeTrackerU3Ec__Iterator0_t1744683265 * L_1 = V_0;
		L_1->set_U24this_0(__this);
		U3CTimeTrackerU3Ec__Iterator0_t1744683265 * L_2 = V_0;
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Anim/<TimeTracker>c__Iterator0::.ctor()
extern "C"  void U3CTimeTrackerU3Ec__Iterator0__ctor_m1471372418 (U3CTimeTrackerU3Ec__Iterator0_t1744683265 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Anim/<TimeTracker>c__Iterator0::MoveNext()
extern "C"  bool U3CTimeTrackerU3Ec__Iterator0_MoveNext_m3002021655 (U3CTimeTrackerU3Ec__Iterator0_t1744683265 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CTimeTrackerU3Ec__Iterator0_MoveNext_m3002021655_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_005d;
			}
		}
	}
	{
		goto IL_0081;
	}

IL_0021:
	{
		goto IL_005d;
	}

IL_0026:
	{
		Anim_t271495542 * L_2 = __this->get_U24this_0();
		Anim_t271495542 * L_3 = L_2;
		int32_t L_4 = L_3->get_i_9();
		L_3->set_i_9(((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1)));
		WaitForSeconds_t1699091251 * L_5 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_5, (1.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_5);
		bool L_6 = __this->get_U24disposing_2();
		if (L_6)
		{
			goto IL_0058;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0058:
	{
		goto IL_0083;
	}

IL_005d:
	{
		Anim_t271495542 * L_7 = __this->get_U24this_0();
		int32_t L_8 = L_7->get_i_9();
		if ((((int32_t)L_8) <= ((int32_t)4)))
		{
			goto IL_0026;
		}
	}
	{
		Anim_t271495542 * L_9 = __this->get_U24this_0();
		L_9->set_isDone_8((bool)1);
		__this->set_U24PC_3((-1));
	}

IL_0081:
	{
		return (bool)0;
	}

IL_0083:
	{
		return (bool)1;
	}
}
// System.Object Anim/<TimeTracker>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CTimeTrackerU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2336922308 (U3CTimeTrackerU3Ec__Iterator0_t1744683265 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object Anim/<TimeTracker>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CTimeTrackerU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2767393025 (U3CTimeTrackerU3Ec__Iterator0_t1744683265 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void Anim/<TimeTracker>c__Iterator0::Dispose()
extern "C"  void U3CTimeTrackerU3Ec__Iterator0_Dispose_m980699190 (U3CTimeTrackerU3Ec__Iterator0_t1744683265 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void Anim/<TimeTracker>c__Iterator0::Reset()
extern "C"  void U3CTimeTrackerU3Ec__Iterator0_Reset_m1833367939 (U3CTimeTrackerU3Ec__Iterator0_t1744683265 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CTimeTrackerU3Ec__Iterator0_Reset_m1833367939_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CTimeTrackerU3Ec__Iterator0_Reset_m1833367939_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Animimp::.ctor()
extern "C"  void Animimp__ctor_m1893832388 (Animimp_t3408691587 * __this, const RuntimeMethod* method)
{
	{
		__this->set_speed_10((1.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Animimp::Start()
extern "C"  void Animimp_Start_m1277239060 (Animimp_t3408691587 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Animimp_Start_m1277239060_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_startTime_11(L_0);
		GameObject_t1113636619 * L_1 = __this->get_quad1_4();
		Transform_t3600365921 * L_2 = GameObject_get_transform_m1369836730(L_1, /*hidden argument*/NULL);
		Vector3_t3722313464  L_3 = Transform_get_position_m36019626(L_2, /*hidden argument*/NULL);
		Vector3_t3722313464  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector3__ctor_m3353183577((&L_4), (-16.55f), (-0.013f), (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		float L_5 = Vector3_Distance_m886789632(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		__this->set_journeyLength_12(L_5);
		GameObject_t1113636619 * L_6 = __this->get_quad2_5();
		Transform_t3600365921 * L_7 = GameObject_get_transform_m1369836730(L_6, /*hidden argument*/NULL);
		Vector3_t3722313464  L_8 = Transform_get_position_m36019626(L_7, /*hidden argument*/NULL);
		Vector3_t3722313464  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m3353183577((&L_9), (16.42f), (-0.016f), (0.0f), /*hidden argument*/NULL);
		float L_10 = Vector3_Distance_m886789632(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		__this->set_journeyLength2_13(L_10);
		GameObject_t1113636619 * L_11 = __this->get_dog_6();
		Transform_t3600365921 * L_12 = GameObject_get_transform_m1369836730(L_11, /*hidden argument*/NULL);
		Vector3_t3722313464  L_13 = Transform_get_position_m36019626(L_12, /*hidden argument*/NULL);
		Vector3_t3722313464  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m3353183577((&L_14), (-4.0f), (5.82f), (0.5f), /*hidden argument*/NULL);
		float L_15 = Vector3_Distance_m886789632(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		__this->set_journeyLength3_14(L_15);
		GameObject_t1113636619 * L_16 = __this->get_guy_7();
		Transform_t3600365921 * L_17 = GameObject_get_transform_m1369836730(L_16, /*hidden argument*/NULL);
		Vector3_t3722313464  L_18 = Transform_get_position_m36019626(L_17, /*hidden argument*/NULL);
		Vector3_t3722313464  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Vector3__ctor_m3353183577((&L_19), (6.64f), (-2.01f), (0.5f), /*hidden argument*/NULL);
		float L_20 = Vector3_Distance_m886789632(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		__this->set_journeyLength4_15(L_20);
		RuntimeObject* L_21 = Animimp_TimeTracker_m2170675636(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_21, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Animimp::Update()
extern "C"  void Animimp_Update_m2181736325 (Animimp_t3408691587 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Animimp_Update_m2181736325_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	{
		float L_0 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_startTime_11();
		float L_2 = __this->get_speed_10();
		V_0 = ((float)((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_0, (float)L_1)), (float)L_2))/(float)(10.0f)));
		float L_3 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = __this->get_startTime_11();
		float L_5 = __this->get_speed_10();
		V_1 = ((float)((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_3, (float)L_4)), (float)L_5))/(float)(10.0f)));
		float L_6 = V_0;
		float L_7 = __this->get_journeyLength_12();
		V_2 = ((float)((float)L_6/(float)L_7));
		float L_8 = V_0;
		float L_9 = __this->get_journeyLength2_13();
		V_3 = ((float)((float)L_8/(float)L_9));
		float L_10 = V_1;
		float L_11 = __this->get_journeyLength3_14();
		V_4 = ((float)((float)L_10/(float)L_11));
		float L_12 = V_1;
		float L_13 = __this->get_journeyLength4_15();
		V_5 = ((float)((float)L_12/(float)L_13));
		GameObject_t1113636619 * L_14 = __this->get_quad1_4();
		Transform_t3600365921 * L_15 = GameObject_get_transform_m1369836730(L_14, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_16 = __this->get_quad1_4();
		Transform_t3600365921 * L_17 = GameObject_get_transform_m1369836730(L_16, /*hidden argument*/NULL);
		Vector3_t3722313464  L_18 = Transform_get_position_m36019626(L_17, /*hidden argument*/NULL);
		Vector3_t3722313464  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Vector3__ctor_m3353183577((&L_19), (-16.55f), (-0.013f), (0.0f), /*hidden argument*/NULL);
		float L_20 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_21 = Vector3_Lerp_m407887542(NULL /*static, unused*/, L_18, L_19, L_20, /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_15, L_21, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_22 = __this->get_quad2_5();
		Transform_t3600365921 * L_23 = GameObject_get_transform_m1369836730(L_22, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_24 = __this->get_quad2_5();
		Transform_t3600365921 * L_25 = GameObject_get_transform_m1369836730(L_24, /*hidden argument*/NULL);
		Vector3_t3722313464  L_26 = Transform_get_position_m36019626(L_25, /*hidden argument*/NULL);
		Vector3_t3722313464  L_27;
		memset(&L_27, 0, sizeof(L_27));
		Vector3__ctor_m3353183577((&L_27), (16.42f), (-0.016f), (0.0f), /*hidden argument*/NULL);
		float L_28 = V_3;
		Vector3_t3722313464  L_29 = Vector3_Lerp_m407887542(NULL /*static, unused*/, L_26, L_27, L_28, /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_23, L_29, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_30 = __this->get_dog_6();
		Transform_t3600365921 * L_31 = GameObject_get_transform_m1369836730(L_30, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_32 = __this->get_dog_6();
		Transform_t3600365921 * L_33 = GameObject_get_transform_m1369836730(L_32, /*hidden argument*/NULL);
		Vector3_t3722313464  L_34 = Transform_get_position_m36019626(L_33, /*hidden argument*/NULL);
		Vector3_t3722313464  L_35;
		memset(&L_35, 0, sizeof(L_35));
		Vector3__ctor_m3353183577((&L_35), (-4.0f), (5.82f), (0.5f), /*hidden argument*/NULL);
		float L_36 = V_4;
		Vector3_t3722313464  L_37 = Vector3_Lerp_m407887542(NULL /*static, unused*/, L_34, L_35, L_36, /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_31, L_37, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_38 = __this->get_guy_7();
		Transform_t3600365921 * L_39 = GameObject_get_transform_m1369836730(L_38, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_40 = __this->get_guy_7();
		Transform_t3600365921 * L_41 = GameObject_get_transform_m1369836730(L_40, /*hidden argument*/NULL);
		Vector3_t3722313464  L_42 = Transform_get_position_m36019626(L_41, /*hidden argument*/NULL);
		Vector3_t3722313464  L_43;
		memset(&L_43, 0, sizeof(L_43));
		Vector3__ctor_m3353183577((&L_43), (6.64f), (-2.01f), (0.5f), /*hidden argument*/NULL);
		float L_44 = V_5;
		Vector3_t3722313464  L_45 = Vector3_Lerp_m407887542(NULL /*static, unused*/, L_42, L_43, ((float)il2cpp_codegen_multiply((float)L_44, (float)(2.0f))), /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_39, L_45, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Animimp::TimeTracker()
extern "C"  RuntimeObject* Animimp_TimeTracker_m2170675636 (Animimp_t3408691587 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Animimp_TimeTracker_m2170675636_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CTimeTrackerU3Ec__Iterator0_t3157006910 * V_0 = NULL;
	{
		U3CTimeTrackerU3Ec__Iterator0_t3157006910 * L_0 = (U3CTimeTrackerU3Ec__Iterator0_t3157006910 *)il2cpp_codegen_object_new(U3CTimeTrackerU3Ec__Iterator0_t3157006910_il2cpp_TypeInfo_var);
		U3CTimeTrackerU3Ec__Iterator0__ctor_m2697886460(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CTimeTrackerU3Ec__Iterator0_t3157006910 * L_1 = V_0;
		L_1->set_U24this_0(__this);
		U3CTimeTrackerU3Ec__Iterator0_t3157006910 * L_2 = V_0;
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Animimp/<TimeTracker>c__Iterator0::.ctor()
extern "C"  void U3CTimeTrackerU3Ec__Iterator0__ctor_m2697886460 (U3CTimeTrackerU3Ec__Iterator0_t3157006910 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Animimp/<TimeTracker>c__Iterator0::MoveNext()
extern "C"  bool U3CTimeTrackerU3Ec__Iterator0_MoveNext_m993837992 (U3CTimeTrackerU3Ec__Iterator0_t3157006910 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CTimeTrackerU3Ec__Iterator0_MoveNext_m993837992_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_005d;
			}
		}
	}
	{
		goto IL_0081;
	}

IL_0021:
	{
		goto IL_005d;
	}

IL_0026:
	{
		Animimp_t3408691587 * L_2 = __this->get_U24this_0();
		Animimp_t3408691587 * L_3 = L_2;
		int32_t L_4 = L_3->get_i_9();
		L_3->set_i_9(((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1)));
		WaitForSeconds_t1699091251 * L_5 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_5, (1.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_5);
		bool L_6 = __this->get_U24disposing_2();
		if (L_6)
		{
			goto IL_0058;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0058:
	{
		goto IL_0083;
	}

IL_005d:
	{
		Animimp_t3408691587 * L_7 = __this->get_U24this_0();
		int32_t L_8 = L_7->get_i_9();
		if ((((int32_t)L_8) <= ((int32_t)4)))
		{
			goto IL_0026;
		}
	}
	{
		Animimp_t3408691587 * L_9 = __this->get_U24this_0();
		L_9->set_isDone_8((bool)1);
		__this->set_U24PC_3((-1));
	}

IL_0081:
	{
		return (bool)0;
	}

IL_0083:
	{
		return (bool)1;
	}
}
// System.Object Animimp/<TimeTracker>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CTimeTrackerU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3162898559 (U3CTimeTrackerU3Ec__Iterator0_t3157006910 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object Animimp/<TimeTracker>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CTimeTrackerU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1669310884 (U3CTimeTrackerU3Ec__Iterator0_t3157006910 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void Animimp/<TimeTracker>c__Iterator0::Dispose()
extern "C"  void U3CTimeTrackerU3Ec__Iterator0_Dispose_m2109433341 (U3CTimeTrackerU3Ec__Iterator0_t3157006910 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void Animimp/<TimeTracker>c__Iterator0::Reset()
extern "C"  void U3CTimeTrackerU3Ec__Iterator0_Reset_m885536348 (U3CTimeTrackerU3Ec__Iterator0_t3157006910 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CTimeTrackerU3Ec__Iterator0_Reset_m885536348_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CTimeTrackerU3Ec__Iterator0_Reset_m885536348_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Animinf::.ctor()
extern "C"  void Animinf__ctor_m4042988650 (Animinf_t989049961 * __this, const RuntimeMethod* method)
{
	{
		__this->set_speed_10((1.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Animinf::Start()
extern "C"  void Animinf_Start_m3911812142 (Animinf_t989049961 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Animinf_Start_m3911812142_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_startTime_11(L_0);
		GameObject_t1113636619 * L_1 = __this->get_quad1_4();
		Transform_t3600365921 * L_2 = GameObject_get_transform_m1369836730(L_1, /*hidden argument*/NULL);
		Vector3_t3722313464  L_3 = Transform_get_position_m36019626(L_2, /*hidden argument*/NULL);
		Vector3_t3722313464  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector3__ctor_m3353183577((&L_4), (-16.55f), (-0.013f), (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		float L_5 = Vector3_Distance_m886789632(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		__this->set_journeyLength_12(L_5);
		GameObject_t1113636619 * L_6 = __this->get_quad2_5();
		Transform_t3600365921 * L_7 = GameObject_get_transform_m1369836730(L_6, /*hidden argument*/NULL);
		Vector3_t3722313464  L_8 = Transform_get_position_m36019626(L_7, /*hidden argument*/NULL);
		Vector3_t3722313464  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m3353183577((&L_9), (16.42f), (-0.016f), (0.0f), /*hidden argument*/NULL);
		float L_10 = Vector3_Distance_m886789632(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		__this->set_journeyLength2_13(L_10);
		GameObject_t1113636619 * L_11 = __this->get_dog_6();
		Transform_t3600365921 * L_12 = GameObject_get_transform_m1369836730(L_11, /*hidden argument*/NULL);
		Vector3_t3722313464  L_13 = Transform_get_position_m36019626(L_12, /*hidden argument*/NULL);
		Vector3_t3722313464  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m3353183577((&L_14), (-4.0f), (5.82f), (0.5f), /*hidden argument*/NULL);
		float L_15 = Vector3_Distance_m886789632(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		__this->set_journeyLength3_14(L_15);
		GameObject_t1113636619 * L_16 = __this->get_guy_7();
		Transform_t3600365921 * L_17 = GameObject_get_transform_m1369836730(L_16, /*hidden argument*/NULL);
		Vector3_t3722313464  L_18 = Transform_get_position_m36019626(L_17, /*hidden argument*/NULL);
		Vector3_t3722313464  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Vector3__ctor_m3353183577((&L_19), (6.64f), (-2.01f), (0.5f), /*hidden argument*/NULL);
		float L_20 = Vector3_Distance_m886789632(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		__this->set_journeyLength4_15(L_20);
		RuntimeObject* L_21 = Animinf_TimeTracker_m1620902351(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_21, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Animinf::Update()
extern "C"  void Animinf_Update_m1260342309 (Animinf_t989049961 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Animinf_Update_m1260342309_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	{
		float L_0 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_startTime_11();
		float L_2 = __this->get_speed_10();
		V_0 = ((float)((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_0, (float)L_1)), (float)L_2))/(float)(10.0f)));
		float L_3 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = __this->get_startTime_11();
		float L_5 = __this->get_speed_10();
		V_1 = ((float)((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_3, (float)L_4)), (float)L_5))/(float)(10.0f)));
		float L_6 = V_0;
		float L_7 = __this->get_journeyLength_12();
		V_2 = ((float)((float)L_6/(float)L_7));
		float L_8 = V_0;
		float L_9 = __this->get_journeyLength2_13();
		V_3 = ((float)((float)L_8/(float)L_9));
		float L_10 = V_1;
		float L_11 = __this->get_journeyLength3_14();
		V_4 = ((float)((float)L_10/(float)L_11));
		float L_12 = V_1;
		float L_13 = __this->get_journeyLength4_15();
		V_5 = ((float)((float)L_12/(float)L_13));
		GameObject_t1113636619 * L_14 = __this->get_quad1_4();
		Transform_t3600365921 * L_15 = GameObject_get_transform_m1369836730(L_14, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_16 = __this->get_quad1_4();
		Transform_t3600365921 * L_17 = GameObject_get_transform_m1369836730(L_16, /*hidden argument*/NULL);
		Vector3_t3722313464  L_18 = Transform_get_position_m36019626(L_17, /*hidden argument*/NULL);
		Vector3_t3722313464  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Vector3__ctor_m3353183577((&L_19), (-16.55f), (-0.013f), (0.0f), /*hidden argument*/NULL);
		float L_20 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_21 = Vector3_Lerp_m407887542(NULL /*static, unused*/, L_18, L_19, L_20, /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_15, L_21, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_22 = __this->get_quad2_5();
		Transform_t3600365921 * L_23 = GameObject_get_transform_m1369836730(L_22, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_24 = __this->get_quad2_5();
		Transform_t3600365921 * L_25 = GameObject_get_transform_m1369836730(L_24, /*hidden argument*/NULL);
		Vector3_t3722313464  L_26 = Transform_get_position_m36019626(L_25, /*hidden argument*/NULL);
		Vector3_t3722313464  L_27;
		memset(&L_27, 0, sizeof(L_27));
		Vector3__ctor_m3353183577((&L_27), (16.42f), (-0.016f), (0.0f), /*hidden argument*/NULL);
		float L_28 = V_3;
		Vector3_t3722313464  L_29 = Vector3_Lerp_m407887542(NULL /*static, unused*/, L_26, L_27, L_28, /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_23, L_29, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_30 = __this->get_dog_6();
		Transform_t3600365921 * L_31 = GameObject_get_transform_m1369836730(L_30, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_32 = __this->get_dog_6();
		Transform_t3600365921 * L_33 = GameObject_get_transform_m1369836730(L_32, /*hidden argument*/NULL);
		Vector3_t3722313464  L_34 = Transform_get_position_m36019626(L_33, /*hidden argument*/NULL);
		Vector3_t3722313464  L_35;
		memset(&L_35, 0, sizeof(L_35));
		Vector3__ctor_m3353183577((&L_35), (-4.0f), (5.82f), (0.5f), /*hidden argument*/NULL);
		float L_36 = V_4;
		Vector3_t3722313464  L_37 = Vector3_Lerp_m407887542(NULL /*static, unused*/, L_34, L_35, L_36, /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_31, L_37, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_38 = __this->get_guy_7();
		Transform_t3600365921 * L_39 = GameObject_get_transform_m1369836730(L_38, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_40 = __this->get_guy_7();
		Transform_t3600365921 * L_41 = GameObject_get_transform_m1369836730(L_40, /*hidden argument*/NULL);
		Vector3_t3722313464  L_42 = Transform_get_position_m36019626(L_41, /*hidden argument*/NULL);
		Vector3_t3722313464  L_43;
		memset(&L_43, 0, sizeof(L_43));
		Vector3__ctor_m3353183577((&L_43), (6.64f), (-2.01f), (0.5f), /*hidden argument*/NULL);
		float L_44 = V_5;
		Vector3_t3722313464  L_45 = Vector3_Lerp_m407887542(NULL /*static, unused*/, L_42, L_43, ((float)il2cpp_codegen_multiply((float)L_44, (float)(2.0f))), /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_39, L_45, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Animinf::TimeTracker()
extern "C"  RuntimeObject* Animinf_TimeTracker_m1620902351 (Animinf_t989049961 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Animinf_TimeTracker_m1620902351_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CTimeTrackerU3Ec__Iterator0_t1213291940 * V_0 = NULL;
	{
		U3CTimeTrackerU3Ec__Iterator0_t1213291940 * L_0 = (U3CTimeTrackerU3Ec__Iterator0_t1213291940 *)il2cpp_codegen_object_new(U3CTimeTrackerU3Ec__Iterator0_t1213291940_il2cpp_TypeInfo_var);
		U3CTimeTrackerU3Ec__Iterator0__ctor_m184673188(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CTimeTrackerU3Ec__Iterator0_t1213291940 * L_1 = V_0;
		L_1->set_U24this_0(__this);
		U3CTimeTrackerU3Ec__Iterator0_t1213291940 * L_2 = V_0;
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Animinf/<TimeTracker>c__Iterator0::.ctor()
extern "C"  void U3CTimeTrackerU3Ec__Iterator0__ctor_m184673188 (U3CTimeTrackerU3Ec__Iterator0_t1213291940 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Animinf/<TimeTracker>c__Iterator0::MoveNext()
extern "C"  bool U3CTimeTrackerU3Ec__Iterator0_MoveNext_m2444601745 (U3CTimeTrackerU3Ec__Iterator0_t1213291940 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CTimeTrackerU3Ec__Iterator0_MoveNext_m2444601745_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_005d;
			}
		}
	}
	{
		goto IL_0081;
	}

IL_0021:
	{
		goto IL_005d;
	}

IL_0026:
	{
		Animinf_t989049961 * L_2 = __this->get_U24this_0();
		Animinf_t989049961 * L_3 = L_2;
		int32_t L_4 = L_3->get_i_9();
		L_3->set_i_9(((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1)));
		WaitForSeconds_t1699091251 * L_5 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_5, (1.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_5);
		bool L_6 = __this->get_U24disposing_2();
		if (L_6)
		{
			goto IL_0058;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0058:
	{
		goto IL_0083;
	}

IL_005d:
	{
		Animinf_t989049961 * L_7 = __this->get_U24this_0();
		int32_t L_8 = L_7->get_i_9();
		if ((((int32_t)L_8) <= ((int32_t)4)))
		{
			goto IL_0026;
		}
	}
	{
		Animinf_t989049961 * L_9 = __this->get_U24this_0();
		L_9->set_isDone_8((bool)1);
		__this->set_U24PC_3((-1));
	}

IL_0081:
	{
		return (bool)0;
	}

IL_0083:
	{
		return (bool)1;
	}
}
// System.Object Animinf/<TimeTracker>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CTimeTrackerU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m887396034 (U3CTimeTrackerU3Ec__Iterator0_t1213291940 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object Animinf/<TimeTracker>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CTimeTrackerU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3661444963 (U3CTimeTrackerU3Ec__Iterator0_t1213291940 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void Animinf/<TimeTracker>c__Iterator0::Dispose()
extern "C"  void U3CTimeTrackerU3Ec__Iterator0_Dispose_m1811340582 (U3CTimeTrackerU3Ec__Iterator0_t1213291940 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void Animinf/<TimeTracker>c__Iterator0::Reset()
extern "C"  void U3CTimeTrackerU3Ec__Iterator0_Reset_m1842370124 (U3CTimeTrackerU3Ec__Iterator0_t1213291940 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CTimeTrackerU3Ec__Iterator0_Reset_m1842370124_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CTimeTrackerU3Ec__Iterator0_Reset_m1842370124_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Battle::.ctor()
extern "C"  void Battle__ctor_m3420311181 (Battle_t2191861408 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Battle__ctor_m3420311181_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Random_t108471755 * L_0 = (Random_t108471755 *)il2cpp_codegen_object_new(Random_t108471755_il2cpp_TypeInfo_var);
		Random__ctor_m4122933043(L_0, /*hidden argument*/NULL);
		__this->set_rnd_16(L_0);
		__this->set_myTurn_17((bool)1);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single Battle::get_CurrentHealth()
extern "C"  float Battle_get_CurrentHealth_m2136656731 (Battle_t2191861408 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CCurrentHealthU3Ek__BackingField_23();
		return L_0;
	}
}
// System.Void Battle::set_CurrentHealth(System.Single)
extern "C"  void Battle_set_CurrentHealth_m4268259891 (Battle_t2191861408 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CCurrentHealthU3Ek__BackingField_23(L_0);
		return;
	}
}
// System.Single Battle::get_MaxHealth()
extern "C"  float Battle_get_MaxHealth_m2763667083 (Battle_t2191861408 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CMaxHealthU3Ek__BackingField_24();
		return L_0;
	}
}
// System.Void Battle::set_MaxHealth(System.Single)
extern "C"  void Battle_set_MaxHealth_m4182892573 (Battle_t2191861408 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CMaxHealthU3Ek__BackingField_24(L_0);
		return;
	}
}
// System.Single Battle::get_CurrentHealthGuy()
extern "C"  float Battle_get_CurrentHealthGuy_m1411648811 (Battle_t2191861408 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CCurrentHealthGuyU3Ek__BackingField_25();
		return L_0;
	}
}
// System.Void Battle::set_CurrentHealthGuy(System.Single)
extern "C"  void Battle_set_CurrentHealthGuy_m2682337132 (Battle_t2191861408 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CCurrentHealthGuyU3Ek__BackingField_25(L_0);
		return;
	}
}
// System.Single Battle::get_MaxHealthGuy()
extern "C"  float Battle_get_MaxHealthGuy_m876340381 (Battle_t2191861408 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CMaxHealthGuyU3Ek__BackingField_26();
		return L_0;
	}
}
// System.Void Battle::set_MaxHealthGuy(System.Single)
extern "C"  void Battle_set_MaxHealthGuy_m2194838754 (Battle_t2191861408 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CMaxHealthGuyU3Ek__BackingField_26(L_0);
		return;
	}
}
// System.Void Battle::Start()
extern "C"  void Battle_Start_m3292020095 (Battle_t2191861408 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Battle_Start_m3292020095_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		GameObject_t1113636619 * L_0 = GameObject_FindGameObjectWithTag_m2129039296(NULL /*static, unused*/, _stringLiteral531699260, /*hidden argument*/NULL);
		Anim_t271495542 * L_1 = GameObject_GetComponent_TisAnim_t271495542_m3559005964(L_0, /*hidden argument*/GameObject_GetComponent_TisAnim_t271495542_m3559005964_RuntimeMethod_var);
		__this->set_anim_15(L_1);
		GameObject_t1113636619 * L_2 = __this->get_GUI_13();
		GameObject_SetActive_m796801857(L_2, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_3 = __this->get_GameOver_14();
		GameObject_SetActive_m796801857(L_3, (bool)0, /*hidden argument*/NULL);
		__this->set_isDead_22((bool)0);
		Battle_set_MaxHealth_m4182892573(__this, (20.0f), /*hidden argument*/NULL);
		float L_4 = Battle_get_MaxHealth_m2763667083(__this, /*hidden argument*/NULL);
		Battle_set_CurrentHealth_m4268259891(__this, L_4, /*hidden argument*/NULL);
		TMP_Text_t2599618874 * L_5 = __this->get_dogHealth_7();
		float L_6 = Battle_get_CurrentHealth_m2136656731(__this, /*hidden argument*/NULL);
		V_0 = L_6;
		String_t* L_7 = Single_ToString_m3947131094((float*)(&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral3602717400, L_7, /*hidden argument*/NULL);
		TMP_Text_set_text_m1216996582(L_5, L_8, /*hidden argument*/NULL);
		Slider_t3903728902 * L_9 = __this->get_slider_4();
		float L_10 = Battle_CalcHealth_m4040825151(__this, /*hidden argument*/NULL);
		VirtActionInvoker1< float >::Invoke(47 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_9, L_10);
		Battle_set_MaxHealthGuy_m2194838754(__this, (20.0f), /*hidden argument*/NULL);
		float L_11 = Battle_get_MaxHealthGuy_m876340381(__this, /*hidden argument*/NULL);
		Battle_set_CurrentHealthGuy_m2682337132(__this, L_11, /*hidden argument*/NULL);
		TMP_Text_t2599618874 * L_12 = __this->get_guyHealth_8();
		float L_13 = Battle_get_CurrentHealthGuy_m1411648811(__this, /*hidden argument*/NULL);
		V_1 = L_13;
		String_t* L_14 = Single_ToString_m3947131094((float*)(&V_1), /*hidden argument*/NULL);
		String_t* L_15 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral576020035, L_14, /*hidden argument*/NULL);
		TMP_Text_set_text_m1216996582(L_12, L_15, /*hidden argument*/NULL);
		Slider_t3903728902 * L_16 = __this->get_guySlider_5();
		float L_17 = Battle_CalcHealthGuy_m3105725521(__this, /*hidden argument*/NULL);
		VirtActionInvoker1< float >::Invoke(47 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_16, L_17);
		return;
	}
}
// System.Void Battle::Update()
extern "C"  void Battle_Update_m683330005 (Battle_t2191861408 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Battle_Update_m683330005_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Anim_t271495542 * L_0 = __this->get_anim_15();
		bool L_1 = L_0->get_isDone_8();
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		bool L_2 = __this->get_isDone_21();
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		__this->set_isReady_20((bool)1);
	}

IL_0022:
	{
		bool L_3 = __this->get_isReady_20();
		if (!L_3)
		{
			goto IL_004b;
		}
	}
	{
		bool L_4 = __this->get_isDone_21();
		if (L_4)
		{
			goto IL_004b;
		}
	}
	{
		GameObject_t1113636619 * L_5 = __this->get_GUI_13();
		GameObject_SetActive_m796801857(L_5, (bool)1, /*hidden argument*/NULL);
		__this->set_isDone_21((bool)1);
	}

IL_004b:
	{
		bool L_6 = __this->get_myTurn_17();
		if (!L_6)
		{
			goto IL_005d;
		}
	}
	{
		__this->set_ableToAttack_18((bool)1);
	}

IL_005d:
	{
		bool L_7 = __this->get_myTurn_17();
		if (L_7)
		{
			goto IL_007c;
		}
	}
	{
		__this->set_enemyAbleToAttack_19((bool)1);
		RuntimeObject* L_8 = Battle_EnemyAttack_m1219160329(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_8, /*hidden argument*/NULL);
	}

IL_007c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_9 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, ((int32_t)27), /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_008e;
		}
	}
	{
		SceneManager_LoadScene_m3463216446(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_008e:
	{
		return;
	}
}
// System.Void Battle::Attack(System.String)
extern "C"  void Battle_Attack_m1873061951 (Battle_t2191861408 * __this, String_t* ___move0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Battle_Attack_m1873061951_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		bool L_0 = __this->get_ableToAttack_18();
		if (!L_0)
		{
			goto IL_00cc;
		}
	}
	{
		__this->set_myTurn_17((bool)0);
		__this->set_ableToAttack_18((bool)0);
		Random_t108471755 * L_1 = __this->get_rnd_16();
		int32_t L_2 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Random::Next(System.Int32,System.Int32) */, L_1, 1, 5);
		V_0 = (((float)((float)L_2)));
		float L_3 = V_0;
		if ((!(((float)L_3) >= ((float)(4.8f)))))
		{
			goto IL_004e;
		}
	}
	{
		TMP_Text_t2599618874 * L_4 = __this->get_attackText_6();
		String_t* L_5 = ___move0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral1378388746, L_5, _stringLiteral348006446, /*hidden argument*/NULL);
		TMP_Text_set_text_m1216996582(L_4, L_6, /*hidden argument*/NULL);
	}

IL_004e:
	{
		float L_7 = V_0;
		if ((!(((float)L_7) <= ((float)(4.0f)))))
		{
			goto IL_0074;
		}
	}
	{
		TMP_Text_t2599618874 * L_8 = __this->get_attackText_6();
		String_t* L_9 = ___move0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral1378388746, L_9, _stringLiteral877777334, /*hidden argument*/NULL);
		TMP_Text_set_text_m1216996582(L_8, L_10, /*hidden argument*/NULL);
	}

IL_0074:
	{
		float L_11 = V_0;
		if ((!(((float)L_11) <= ((float)(2.5f)))))
		{
			goto IL_009a;
		}
	}
	{
		TMP_Text_t2599618874 * L_12 = __this->get_attackText_6();
		String_t* L_13 = ___move0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral1378388746, L_13, _stringLiteral4242887721, /*hidden argument*/NULL);
		TMP_Text_set_text_m1216996582(L_12, L_14, /*hidden argument*/NULL);
	}

IL_009a:
	{
		float L_15 = V_0;
		if ((!(((float)L_15) <= ((float)(1.5f)))))
		{
			goto IL_00c0;
		}
	}
	{
		TMP_Text_t2599618874 * L_16 = __this->get_attackText_6();
		String_t* L_17 = ___move0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral1378388746, L_17, _stringLiteral2831055546, /*hidden argument*/NULL);
		TMP_Text_set_text_m1216996582(L_16, L_18, /*hidden argument*/NULL);
	}

IL_00c0:
	{
		float L_19 = V_0;
		Battle_DealDamage_m3118024972(__this, L_19, /*hidden argument*/NULL);
		goto IL_00cd;
	}

IL_00cc:
	{
		return;
	}

IL_00cd:
	{
		return;
	}
}
// System.Void Battle::DealDamage(System.Single)
extern "C"  void Battle_DealDamage_m3118024972 (Battle_t2191861408 * __this, float ___damageValue0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Battle_DealDamage_m3118024972_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = Battle_get_CurrentHealth_m2136656731(__this, /*hidden argument*/NULL);
		float L_1 = ___damageValue0;
		Battle_set_CurrentHealth_m4268259891(__this, ((float)il2cpp_codegen_subtract((float)L_0, (float)L_1)), /*hidden argument*/NULL);
		Slider_t3903728902 * L_2 = __this->get_slider_4();
		float L_3 = Battle_CalcHealth_m4040825151(__this, /*hidden argument*/NULL);
		VirtActionInvoker1< float >::Invoke(47 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_2, L_3);
		TMP_Text_t2599618874 * L_4 = __this->get_dogHealth_7();
		float L_5 = Battle_get_CurrentHealth_m2136656731(__this, /*hidden argument*/NULL);
		V_0 = L_5;
		String_t* L_6 = Single_ToString_m3947131094((float*)(&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral3602717400, L_6, /*hidden argument*/NULL);
		TMP_Text_set_text_m1216996582(L_4, L_7, /*hidden argument*/NULL);
		float L_8 = Battle_get_CurrentHealth_m2136656731(__this, /*hidden argument*/NULL);
		if ((!(((float)L_8) <= ((float)(0.0f)))))
		{
			goto IL_007f;
		}
	}
	{
		__this->set_myTurn_17((bool)1);
		__this->set_ableToAttack_18((bool)1);
		__this->set_enemyAbleToAttack_19((bool)0);
		GameObject_t1113636619 * L_9 = __this->get_GUI_13();
		GameObject_SetActive_m796801857(L_9, (bool)0, /*hidden argument*/NULL);
		Battle_Die_m2800284142(__this, /*hidden argument*/NULL);
	}

IL_007f:
	{
		Slider_t3903728902 * L_10 = __this->get_slider_4();
		float L_11 = VirtFuncInvoker0< float >::Invoke(46 /* System.Single UnityEngine.UI.Slider::get_value() */, L_10);
		if ((!(((float)L_11) <= ((float)(0.0f)))))
		{
			goto IL_00af;
		}
	}
	{
		__this->set_myTurn_17((bool)1);
		__this->set_ableToAttack_18((bool)1);
		__this->set_enemyAbleToAttack_19((bool)0);
		Battle_Die_m2800284142(__this, /*hidden argument*/NULL);
	}

IL_00af:
	{
		return;
	}
}
// System.Single Battle::CalcHealth()
extern "C"  float Battle_CalcHealth_m4040825151 (Battle_t2191861408 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = Battle_get_CurrentHealth_m2136656731(__this, /*hidden argument*/NULL);
		float L_1 = Battle_get_MaxHealth_m2763667083(__this, /*hidden argument*/NULL);
		return ((float)((float)L_0/(float)L_1));
	}
}
// System.Collections.IEnumerator Battle::EnemyAttack()
extern "C"  RuntimeObject* Battle_EnemyAttack_m1219160329 (Battle_t2191861408 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Battle_EnemyAttack_m1219160329_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CEnemyAttackU3Ec__Iterator0_t1682970516 * V_0 = NULL;
	{
		U3CEnemyAttackU3Ec__Iterator0_t1682970516 * L_0 = (U3CEnemyAttackU3Ec__Iterator0_t1682970516 *)il2cpp_codegen_object_new(U3CEnemyAttackU3Ec__Iterator0_t1682970516_il2cpp_TypeInfo_var);
		U3CEnemyAttackU3Ec__Iterator0__ctor_m3060783474(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CEnemyAttackU3Ec__Iterator0_t1682970516 * L_1 = V_0;
		L_1->set_U24this_1(__this);
		U3CEnemyAttackU3Ec__Iterator0_t1682970516 * L_2 = V_0;
		return L_2;
	}
}
// System.Single Battle::CalcHealthGuy()
extern "C"  float Battle_CalcHealthGuy_m3105725521 (Battle_t2191861408 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = Battle_get_CurrentHealthGuy_m1411648811(__this, /*hidden argument*/NULL);
		float L_1 = Battle_get_MaxHealthGuy_m876340381(__this, /*hidden argument*/NULL);
		return ((float)((float)L_0/(float)L_1));
	}
}
// System.Void Battle::EnemyDealDamage(System.Single)
extern "C"  void Battle_EnemyDealDamage_m2866959850 (Battle_t2191861408 * __this, float ___damageValue0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Battle_EnemyDealDamage_m2866959850_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = Battle_get_CurrentHealthGuy_m1411648811(__this, /*hidden argument*/NULL);
		float L_1 = ___damageValue0;
		Battle_set_CurrentHealthGuy_m2682337132(__this, ((float)il2cpp_codegen_subtract((float)L_0, (float)L_1)), /*hidden argument*/NULL);
		Slider_t3903728902 * L_2 = __this->get_guySlider_5();
		float L_3 = Battle_CalcHealthGuy_m3105725521(__this, /*hidden argument*/NULL);
		VirtActionInvoker1< float >::Invoke(47 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_2, L_3);
		TMP_Text_t2599618874 * L_4 = __this->get_guyHealth_8();
		float L_5 = Battle_get_CurrentHealthGuy_m1411648811(__this, /*hidden argument*/NULL);
		V_0 = L_5;
		String_t* L_6 = Single_ToString_m3947131094((float*)(&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral576020035, L_6, /*hidden argument*/NULL);
		TMP_Text_set_text_m1216996582(L_4, L_7, /*hidden argument*/NULL);
		float L_8 = Battle_get_CurrentHealthGuy_m1411648811(__this, /*hidden argument*/NULL);
		if ((!(((float)L_8) <= ((float)(0.0f)))))
		{
			goto IL_007f;
		}
	}
	{
		__this->set_myTurn_17((bool)1);
		__this->set_ableToAttack_18((bool)1);
		__this->set_enemyAbleToAttack_19((bool)0);
		GameObject_t1113636619 * L_9 = __this->get_GUI_13();
		GameObject_SetActive_m796801857(L_9, (bool)0, /*hidden argument*/NULL);
		Battle_DieGuy_m536536778(__this, /*hidden argument*/NULL);
	}

IL_007f:
	{
		Slider_t3903728902 * L_10 = __this->get_guySlider_5();
		float L_11 = VirtFuncInvoker0< float >::Invoke(46 /* System.Single UnityEngine.UI.Slider::get_value() */, L_10);
		if ((!(((float)L_11) <= ((float)(0.0f)))))
		{
			goto IL_00af;
		}
	}
	{
		__this->set_myTurn_17((bool)1);
		__this->set_ableToAttack_18((bool)1);
		__this->set_enemyAbleToAttack_19((bool)0);
		Battle_DieGuy_m536536778(__this, /*hidden argument*/NULL);
	}

IL_00af:
	{
		return;
	}
}
// System.Void Battle::Die()
extern "C"  void Battle_Die_m2800284142 (Battle_t2191861408 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Battle_Die_m2800284142_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioSource_t3935305588 * L_0 = __this->get_DogDieSFX_11();
		AudioSource_Play_m48294159(L_0, /*hidden argument*/NULL);
		TMP_Text_t2599618874 * L_1 = __this->get_attackText_6();
		TMP_Text_set_text_m1216996582(L_1, _stringLiteral4201971368, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_2 = __this->get_GUI_13();
		GameObject_SetActive_m796801857(L_2, (bool)0, /*hidden argument*/NULL);
		__this->set_isDead_22((bool)1);
		RuntimeObject* L_3 = Battle_Continue_m1384187413(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Battle::Continue()
extern "C"  RuntimeObject* Battle_Continue_m1384187413 (Battle_t2191861408 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Battle_Continue_m1384187413_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CContinueU3Ec__Iterator1_t642798636 * V_0 = NULL;
	{
		U3CContinueU3Ec__Iterator1_t642798636 * L_0 = (U3CContinueU3Ec__Iterator1_t642798636 *)il2cpp_codegen_object_new(U3CContinueU3Ec__Iterator1_t642798636_il2cpp_TypeInfo_var);
		U3CContinueU3Ec__Iterator1__ctor_m3300558351(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CContinueU3Ec__Iterator1_t642798636 * L_1 = V_0;
		L_1->set_U24this_0(__this);
		U3CContinueU3Ec__Iterator1_t642798636 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Battle::DieGuy()
extern "C"  void Battle_DieGuy_m536536778 (Battle_t2191861408 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Battle_DieGuy_m536536778_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioSource_t3935305588 * L_0 = __this->get_GuydDieSFX_12();
		AudioSource_Play_m48294159(L_0, /*hidden argument*/NULL);
		TMP_Text_t2599618874 * L_1 = __this->get_attackText_6();
		TMP_Text_set_text_m1216996582(L_1, _stringLiteral3005979166, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_2 = __this->get_GUI_13();
		GameObject_SetActive_m796801857(L_2, (bool)0, /*hidden argument*/NULL);
		RuntimeObject* L_3 = Battle_DIe_m2481910125(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Battle::DIe()
extern "C"  RuntimeObject* Battle_DIe_m2481910125 (Battle_t2191861408 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Battle_DIe_m2481910125_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CDIeU3Ec__Iterator2_t558276709 * V_0 = NULL;
	{
		U3CDIeU3Ec__Iterator2_t558276709 * L_0 = (U3CDIeU3Ec__Iterator2_t558276709 *)il2cpp_codegen_object_new(U3CDIeU3Ec__Iterator2_t558276709_il2cpp_TypeInfo_var);
		U3CDIeU3Ec__Iterator2__ctor_m2870459626(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDIeU3Ec__Iterator2_t558276709 * L_1 = V_0;
		L_1->set_U24this_0(__this);
		U3CDIeU3Ec__Iterator2_t558276709 * L_2 = V_0;
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Battle/<Continue>c__Iterator1::.ctor()
extern "C"  void U3CContinueU3Ec__Iterator1__ctor_m3300558351 (U3CContinueU3Ec__Iterator1_t642798636 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Battle/<Continue>c__Iterator1::MoveNext()
extern "C"  bool U3CContinueU3Ec__Iterator1_MoveNext_m3745180152 (U3CContinueU3Ec__Iterator1_t642798636 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CContinueU3Ec__Iterator1_MoveNext_m3745180152_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0025;
			}
			case 1:
			{
				goto IL_0049;
			}
			case 2:
			{
				goto IL_009f;
			}
		}
	}
	{
		goto IL_00ac;
	}

IL_0025:
	{
		WaitForSeconds_t1699091251 * L_2 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_2, (10.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		bool L_3 = __this->get_U24disposing_2();
		if (L_3)
		{
			goto IL_0044;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0044:
	{
		goto IL_00ae;
	}

IL_0049:
	{
		Battle_t2191861408 * L_4 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_5 = L_4->get_attackText_6();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		TMP_Text_set_text_m1216996582(L_5, L_6, /*hidden argument*/NULL);
		Battle_t2191861408 * L_7 = __this->get_U24this_0();
		Animator_t434523843 * L_8 = L_7->get_animator_10();
		Animator_SetBool_m234840832(L_8, _stringLiteral1985039568, (bool)1, /*hidden argument*/NULL);
		intptr_t L_9 = (intptr_t)U3CContinueU3Ec__Iterator1_U3CU3Em__0_m193092823_RuntimeMethod_var;
		Func_1_t3822001908 * L_10 = (Func_1_t3822001908 *)il2cpp_codegen_object_new(Func_1_t3822001908_il2cpp_TypeInfo_var);
		Func_1__ctor_m1399073142(L_10, __this, L_9, /*hidden argument*/Func_1__ctor_m1399073142_RuntimeMethod_var);
		WaitUntil_t3373419216 * L_11 = (WaitUntil_t3373419216 *)il2cpp_codegen_object_new(WaitUntil_t3373419216_il2cpp_TypeInfo_var);
		WaitUntil__ctor_m4227046299(L_11, L_10, /*hidden argument*/NULL);
		__this->set_U24current_1(L_11);
		bool L_12 = __this->get_U24disposing_2();
		if (L_12)
		{
			goto IL_009a;
		}
	}
	{
		__this->set_U24PC_3(2);
	}

IL_009a:
	{
		goto IL_00ae;
	}

IL_009f:
	{
		SceneManager_LoadScene_m3463216446(NULL /*static, unused*/, 5, /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_00ac:
	{
		return (bool)0;
	}

IL_00ae:
	{
		return (bool)1;
	}
}
// System.Object Battle/<Continue>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CContinueU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3216402593 (U3CContinueU3Ec__Iterator1_t642798636 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object Battle/<Continue>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CContinueU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m525921008 (U3CContinueU3Ec__Iterator1_t642798636 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void Battle/<Continue>c__Iterator1::Dispose()
extern "C"  void U3CContinueU3Ec__Iterator1_Dispose_m1436086715 (U3CContinueU3Ec__Iterator1_t642798636 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void Battle/<Continue>c__Iterator1::Reset()
extern "C"  void U3CContinueU3Ec__Iterator1_Reset_m1261977967 (U3CContinueU3Ec__Iterator1_t642798636 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CContinueU3Ec__Iterator1_Reset_m1261977967_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CContinueU3Ec__Iterator1_Reset_m1261977967_RuntimeMethod_var);
	}
}
// System.Boolean Battle/<Continue>c__Iterator1::<>m__0()
extern "C"  bool U3CContinueU3Ec__Iterator1_U3CU3Em__0_m193092823 (U3CContinueU3Ec__Iterator1_t642798636 * __this, const RuntimeMethod* method)
{
	Color_t2555686324  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Battle_t2191861408 * L_0 = __this->get_U24this_0();
		Image_t2670269651 * L_1 = L_0->get_black_9();
		Color_t2555686324  L_2 = VirtFuncInvoker0< Color_t2555686324  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_1);
		V_0 = L_2;
		float L_3 = (&V_0)->get_a_3();
		return (bool)((((float)L_3) == ((float)(1.0f)))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Battle/<DIe>c__Iterator2::.ctor()
extern "C"  void U3CDIeU3Ec__Iterator2__ctor_m2870459626 (U3CDIeU3Ec__Iterator2_t558276709 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Battle/<DIe>c__Iterator2::MoveNext()
extern "C"  bool U3CDIeU3Ec__Iterator2_MoveNext_m803567402 (U3CDIeU3Ec__Iterator2_t558276709 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDIeU3Ec__Iterator2_MoveNext_m803567402_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0025;
			}
			case 1:
			{
				goto IL_0049;
			}
			case 2:
			{
				goto IL_009f;
			}
		}
	}
	{
		goto IL_00ac;
	}

IL_0025:
	{
		WaitForSeconds_t1699091251 * L_2 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_2, (5.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		bool L_3 = __this->get_U24disposing_2();
		if (L_3)
		{
			goto IL_0044;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0044:
	{
		goto IL_00ae;
	}

IL_0049:
	{
		Battle_t2191861408 * L_4 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_5 = L_4->get_attackText_6();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		TMP_Text_set_text_m1216996582(L_5, L_6, /*hidden argument*/NULL);
		Battle_t2191861408 * L_7 = __this->get_U24this_0();
		Animator_t434523843 * L_8 = L_7->get_animator_10();
		Animator_SetBool_m234840832(L_8, _stringLiteral1985039568, (bool)1, /*hidden argument*/NULL);
		intptr_t L_9 = (intptr_t)U3CDIeU3Ec__Iterator2_U3CU3Em__0_m4217354996_RuntimeMethod_var;
		Func_1_t3822001908 * L_10 = (Func_1_t3822001908 *)il2cpp_codegen_object_new(Func_1_t3822001908_il2cpp_TypeInfo_var);
		Func_1__ctor_m1399073142(L_10, __this, L_9, /*hidden argument*/Func_1__ctor_m1399073142_RuntimeMethod_var);
		WaitUntil_t3373419216 * L_11 = (WaitUntil_t3373419216 *)il2cpp_codegen_object_new(WaitUntil_t3373419216_il2cpp_TypeInfo_var);
		WaitUntil__ctor_m4227046299(L_11, L_10, /*hidden argument*/NULL);
		__this->set_U24current_1(L_11);
		bool L_12 = __this->get_U24disposing_2();
		if (L_12)
		{
			goto IL_009a;
		}
	}
	{
		__this->set_U24PC_3(2);
	}

IL_009a:
	{
		goto IL_00ae;
	}

IL_009f:
	{
		SceneManager_LoadScene_m3463216446(NULL /*static, unused*/, 4, /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_00ac:
	{
		return (bool)0;
	}

IL_00ae:
	{
		return (bool)1;
	}
}
// System.Object Battle/<DIe>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CDIeU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1937283831 (U3CDIeU3Ec__Iterator2_t558276709 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object Battle/<DIe>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CDIeU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m1929401720 (U3CDIeU3Ec__Iterator2_t558276709 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void Battle/<DIe>c__Iterator2::Dispose()
extern "C"  void U3CDIeU3Ec__Iterator2_Dispose_m1546216359 (U3CDIeU3Ec__Iterator2_t558276709 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void Battle/<DIe>c__Iterator2::Reset()
extern "C"  void U3CDIeU3Ec__Iterator2_Reset_m615284703 (U3CDIeU3Ec__Iterator2_t558276709 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDIeU3Ec__Iterator2_Reset_m615284703_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CDIeU3Ec__Iterator2_Reset_m615284703_RuntimeMethod_var);
	}
}
// System.Boolean Battle/<DIe>c__Iterator2::<>m__0()
extern "C"  bool U3CDIeU3Ec__Iterator2_U3CU3Em__0_m4217354996 (U3CDIeU3Ec__Iterator2_t558276709 * __this, const RuntimeMethod* method)
{
	Color_t2555686324  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Battle_t2191861408 * L_0 = __this->get_U24this_0();
		Image_t2670269651 * L_1 = L_0->get_black_9();
		Color_t2555686324  L_2 = VirtFuncInvoker0< Color_t2555686324  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_1);
		V_0 = L_2;
		float L_3 = (&V_0)->get_a_3();
		return (bool)((((float)L_3) == ((float)(1.0f)))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Battle/<EnemyAttack>c__Iterator0::.ctor()
extern "C"  void U3CEnemyAttackU3Ec__Iterator0__ctor_m3060783474 (U3CEnemyAttackU3Ec__Iterator0_t1682970516 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Battle/<EnemyAttack>c__Iterator0::MoveNext()
extern "C"  bool U3CEnemyAttackU3Ec__Iterator0_MoveNext_m3082423899 (U3CEnemyAttackU3Ec__Iterator0_t1682970516 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CEnemyAttackU3Ec__Iterator0_MoveNext_m3082423899_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_006d;
			}
		}
	}
	{
		goto IL_0132;
	}

IL_0021:
	{
		Battle_t2191861408 * L_2 = __this->get_U24this_1();
		bool L_3 = L_2->get_enemyAbleToAttack_19();
		if (!L_3)
		{
			goto IL_012b;
		}
	}
	{
		Battle_t2191861408 * L_4 = __this->get_U24this_1();
		L_4->set_myTurn_17((bool)1);
		Battle_t2191861408 * L_5 = __this->get_U24this_1();
		L_5->set_enemyAbleToAttack_19((bool)0);
		WaitForSeconds_t1699091251 * L_6 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_6, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_6);
		bool L_7 = __this->get_U24disposing_3();
		if (L_7)
		{
			goto IL_0068;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_0068:
	{
		goto IL_0134;
	}

IL_006d:
	{
		Battle_t2191861408 * L_8 = __this->get_U24this_1();
		Random_t108471755 * L_9 = L_8->get_rnd_16();
		int32_t L_10 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Random::Next(System.Int32,System.Int32) */, L_9, 1, 5);
		__this->set_U3CrndDMGU3E__1_0((((float)((float)L_10))));
		float L_11 = __this->get_U3CrndDMGU3E__1_0();
		if ((!(((float)L_11) >= ((float)(4.8f)))))
		{
			goto IL_00ab;
		}
	}
	{
		Battle_t2191861408 * L_12 = __this->get_U24this_1();
		TMP_Text_t2599618874 * L_13 = L_12->get_attackText_6();
		TMP_Text_set_text_m1216996582(L_13, _stringLiteral1415876783, /*hidden argument*/NULL);
	}

IL_00ab:
	{
		float L_14 = __this->get_U3CrndDMGU3E__1_0();
		if ((!(((float)L_14) <= ((float)(4.0f)))))
		{
			goto IL_00d0;
		}
	}
	{
		Battle_t2191861408 * L_15 = __this->get_U24this_1();
		TMP_Text_t2599618874 * L_16 = L_15->get_attackText_6();
		TMP_Text_set_text_m1216996582(L_16, _stringLiteral231011766, /*hidden argument*/NULL);
	}

IL_00d0:
	{
		float L_17 = __this->get_U3CrndDMGU3E__1_0();
		if ((!(((float)L_17) <= ((float)(2.5f)))))
		{
			goto IL_00f5;
		}
	}
	{
		Battle_t2191861408 * L_18 = __this->get_U24this_1();
		TMP_Text_t2599618874 * L_19 = L_18->get_attackText_6();
		TMP_Text_set_text_m1216996582(L_19, _stringLiteral1635465982, /*hidden argument*/NULL);
	}

IL_00f5:
	{
		float L_20 = __this->get_U3CrndDMGU3E__1_0();
		if ((!(((float)L_20) <= ((float)(1.5f)))))
		{
			goto IL_011a;
		}
	}
	{
		Battle_t2191861408 * L_21 = __this->get_U24this_1();
		TMP_Text_t2599618874 * L_22 = L_21->get_attackText_6();
		TMP_Text_set_text_m1216996582(L_22, _stringLiteral681334598, /*hidden argument*/NULL);
	}

IL_011a:
	{
		Battle_t2191861408 * L_23 = __this->get_U24this_1();
		float L_24 = __this->get_U3CrndDMGU3E__1_0();
		Battle_EnemyDealDamage_m2866959850(L_23, L_24, /*hidden argument*/NULL);
	}

IL_012b:
	{
		__this->set_U24PC_4((-1));
	}

IL_0132:
	{
		return (bool)0;
	}

IL_0134:
	{
		return (bool)1;
	}
}
// System.Object Battle/<EnemyAttack>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CEnemyAttackU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2749074922 (U3CEnemyAttackU3Ec__Iterator0_t1682970516 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object Battle/<EnemyAttack>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CEnemyAttackU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m859837906 (U3CEnemyAttackU3Ec__Iterator0_t1682970516 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Void Battle/<EnemyAttack>c__Iterator0::Dispose()
extern "C"  void U3CEnemyAttackU3Ec__Iterator0_Dispose_m3557647722 (U3CEnemyAttackU3Ec__Iterator0_t1682970516 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void Battle/<EnemyAttack>c__Iterator0::Reset()
extern "C"  void U3CEnemyAttackU3Ec__Iterator0_Reset_m1767328419 (U3CEnemyAttackU3Ec__Iterator0_t1682970516 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CEnemyAttackU3Ec__Iterator0_Reset_m1767328419_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CEnemyAttackU3Ec__Iterator0_Reset_m1767328419_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Battleimp::.ctor()
extern "C"  void Battleimp__ctor_m1554581115 (Battleimp_t158148094 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Battleimp__ctor_m1554581115_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Random_t108471755 * L_0 = (Random_t108471755 *)il2cpp_codegen_object_new(Random_t108471755_il2cpp_TypeInfo_var);
		Random__ctor_m4122933043(L_0, /*hidden argument*/NULL);
		__this->set_rnd_16(L_0);
		__this->set_myTurn_17((bool)1);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single Battleimp::get_CurrentHealth()
extern "C"  float Battleimp_get_CurrentHealth_m1013476409 (Battleimp_t158148094 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CCurrentHealthU3Ek__BackingField_23();
		return L_0;
	}
}
// System.Void Battleimp::set_CurrentHealth(System.Single)
extern "C"  void Battleimp_set_CurrentHealth_m826649395 (Battleimp_t158148094 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CCurrentHealthU3Ek__BackingField_23(L_0);
		return;
	}
}
// System.Single Battleimp::get_MaxHealth()
extern "C"  float Battleimp_get_MaxHealth_m961871286 (Battleimp_t158148094 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CMaxHealthU3Ek__BackingField_24();
		return L_0;
	}
}
// System.Void Battleimp::set_MaxHealth(System.Single)
extern "C"  void Battleimp_set_MaxHealth_m603327789 (Battleimp_t158148094 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CMaxHealthU3Ek__BackingField_24(L_0);
		return;
	}
}
// System.Single Battleimp::get_CurrentHealthGuy()
extern "C"  float Battleimp_get_CurrentHealthGuy_m437652102 (Battleimp_t158148094 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CCurrentHealthGuyU3Ek__BackingField_25();
		return L_0;
	}
}
// System.Void Battleimp::set_CurrentHealthGuy(System.Single)
extern "C"  void Battleimp_set_CurrentHealthGuy_m2081096126 (Battleimp_t158148094 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CCurrentHealthGuyU3Ek__BackingField_25(L_0);
		return;
	}
}
// System.Single Battleimp::get_MaxHealthGuy()
extern "C"  float Battleimp_get_MaxHealthGuy_m3578614347 (Battleimp_t158148094 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CMaxHealthGuyU3Ek__BackingField_26();
		return L_0;
	}
}
// System.Void Battleimp::set_MaxHealthGuy(System.Single)
extern "C"  void Battleimp_set_MaxHealthGuy_m322227139 (Battleimp_t158148094 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CMaxHealthGuyU3Ek__BackingField_26(L_0);
		return;
	}
}
// System.Void Battleimp::Start()
extern "C"  void Battleimp_Start_m2224704123 (Battleimp_t158148094 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Battleimp_Start_m2224704123_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		GameObject_t1113636619 * L_0 = GameObject_FindGameObjectWithTag_m2129039296(NULL /*static, unused*/, _stringLiteral531699260, /*hidden argument*/NULL);
		Animimp_t3408691587 * L_1 = GameObject_GetComponent_TisAnimimp_t3408691587_m1937797106(L_0, /*hidden argument*/GameObject_GetComponent_TisAnimimp_t3408691587_m1937797106_RuntimeMethod_var);
		__this->set_anim_15(L_1);
		GameObject_t1113636619 * L_2 = __this->get_GUI_13();
		GameObject_SetActive_m796801857(L_2, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_3 = __this->get_GameOver_14();
		GameObject_SetActive_m796801857(L_3, (bool)0, /*hidden argument*/NULL);
		__this->set_isDead_22((bool)0);
		Battleimp_set_MaxHealth_m603327789(__this, (200.0f), /*hidden argument*/NULL);
		float L_4 = Battleimp_get_MaxHealth_m961871286(__this, /*hidden argument*/NULL);
		Battleimp_set_CurrentHealth_m826649395(__this, L_4, /*hidden argument*/NULL);
		TMP_Text_t2599618874 * L_5 = __this->get_dogHealth_7();
		float L_6 = Battleimp_get_CurrentHealth_m1013476409(__this, /*hidden argument*/NULL);
		V_0 = L_6;
		String_t* L_7 = Single_ToString_m3947131094((float*)(&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral3602717400, L_7, /*hidden argument*/NULL);
		TMP_Text_set_text_m1216996582(L_5, L_8, /*hidden argument*/NULL);
		Slider_t3903728902 * L_9 = __this->get_slider_4();
		float L_10 = Battleimp_CalcHealth_m3863819909(__this, /*hidden argument*/NULL);
		VirtActionInvoker1< float >::Invoke(47 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_9, L_10);
		Battleimp_set_MaxHealthGuy_m322227139(__this, (20.0f), /*hidden argument*/NULL);
		float L_11 = Battleimp_get_MaxHealthGuy_m3578614347(__this, /*hidden argument*/NULL);
		Battleimp_set_CurrentHealthGuy_m2081096126(__this, L_11, /*hidden argument*/NULL);
		TMP_Text_t2599618874 * L_12 = __this->get_guyHealth_8();
		float L_13 = Battleimp_get_CurrentHealthGuy_m437652102(__this, /*hidden argument*/NULL);
		V_1 = L_13;
		String_t* L_14 = Single_ToString_m3947131094((float*)(&V_1), /*hidden argument*/NULL);
		String_t* L_15 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral576020035, L_14, /*hidden argument*/NULL);
		TMP_Text_set_text_m1216996582(L_12, L_15, /*hidden argument*/NULL);
		Slider_t3903728902 * L_16 = __this->get_guySlider_5();
		float L_17 = Battleimp_CalcHealthGuy_m1665544502(__this, /*hidden argument*/NULL);
		VirtActionInvoker1< float >::Invoke(47 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_16, L_17);
		return;
	}
}
// System.Void Battleimp::Update()
extern "C"  void Battleimp_Update_m361311231 (Battleimp_t158148094 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Battleimp_Update_m361311231_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animimp_t3408691587 * L_0 = __this->get_anim_15();
		bool L_1 = L_0->get_isDone_8();
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		bool L_2 = __this->get_isDone_21();
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		__this->set_isReady_20((bool)1);
	}

IL_0022:
	{
		bool L_3 = __this->get_isReady_20();
		if (!L_3)
		{
			goto IL_004b;
		}
	}
	{
		bool L_4 = __this->get_isDone_21();
		if (L_4)
		{
			goto IL_004b;
		}
	}
	{
		GameObject_t1113636619 * L_5 = __this->get_GUI_13();
		GameObject_SetActive_m796801857(L_5, (bool)1, /*hidden argument*/NULL);
		__this->set_isDone_21((bool)1);
	}

IL_004b:
	{
		bool L_6 = __this->get_myTurn_17();
		if (!L_6)
		{
			goto IL_005d;
		}
	}
	{
		__this->set_ableToAttack_18((bool)1);
	}

IL_005d:
	{
		bool L_7 = __this->get_myTurn_17();
		if (L_7)
		{
			goto IL_007c;
		}
	}
	{
		__this->set_enemyAbleToAttack_19((bool)1);
		RuntimeObject* L_8 = Battleimp_EnemyAttack_m1443215274(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_8, /*hidden argument*/NULL);
	}

IL_007c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_9 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, ((int32_t)27), /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_008e;
		}
	}
	{
		SceneManager_LoadScene_m3463216446(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_008e:
	{
		return;
	}
}
// System.Void Battleimp::Attack(System.String)
extern "C"  void Battleimp_Attack_m1242176998 (Battleimp_t158148094 * __this, String_t* ___move0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Battleimp_Attack_m1242176998_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		bool L_0 = __this->get_ableToAttack_18();
		if (!L_0)
		{
			goto IL_00cc;
		}
	}
	{
		__this->set_myTurn_17((bool)0);
		__this->set_ableToAttack_18((bool)0);
		Random_t108471755 * L_1 = __this->get_rnd_16();
		int32_t L_2 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Random::Next(System.Int32,System.Int32) */, L_1, 1, 5);
		V_0 = (((float)((float)L_2)));
		float L_3 = V_0;
		if ((!(((float)L_3) >= ((float)(4.8f)))))
		{
			goto IL_004e;
		}
	}
	{
		TMP_Text_t2599618874 * L_4 = __this->get_attackText_6();
		String_t* L_5 = ___move0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral1378388746, L_5, _stringLiteral348006446, /*hidden argument*/NULL);
		TMP_Text_set_text_m1216996582(L_4, L_6, /*hidden argument*/NULL);
	}

IL_004e:
	{
		float L_7 = V_0;
		if ((!(((float)L_7) <= ((float)(4.0f)))))
		{
			goto IL_0074;
		}
	}
	{
		TMP_Text_t2599618874 * L_8 = __this->get_attackText_6();
		String_t* L_9 = ___move0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral1378388746, L_9, _stringLiteral877777334, /*hidden argument*/NULL);
		TMP_Text_set_text_m1216996582(L_8, L_10, /*hidden argument*/NULL);
	}

IL_0074:
	{
		float L_11 = V_0;
		if ((!(((float)L_11) <= ((float)(2.5f)))))
		{
			goto IL_009a;
		}
	}
	{
		TMP_Text_t2599618874 * L_12 = __this->get_attackText_6();
		String_t* L_13 = ___move0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral1378388746, L_13, _stringLiteral4242887721, /*hidden argument*/NULL);
		TMP_Text_set_text_m1216996582(L_12, L_14, /*hidden argument*/NULL);
	}

IL_009a:
	{
		float L_15 = V_0;
		if ((!(((float)L_15) <= ((float)(1.5f)))))
		{
			goto IL_00c0;
		}
	}
	{
		TMP_Text_t2599618874 * L_16 = __this->get_attackText_6();
		String_t* L_17 = ___move0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral1378388746, L_17, _stringLiteral2831055546, /*hidden argument*/NULL);
		TMP_Text_set_text_m1216996582(L_16, L_18, /*hidden argument*/NULL);
	}

IL_00c0:
	{
		float L_19 = V_0;
		Battleimp_DealDamage_m2481885304(__this, L_19, /*hidden argument*/NULL);
		goto IL_00cd;
	}

IL_00cc:
	{
		return;
	}

IL_00cd:
	{
		return;
	}
}
// System.Void Battleimp::DealDamage(System.Single)
extern "C"  void Battleimp_DealDamage_m2481885304 (Battleimp_t158148094 * __this, float ___damageValue0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Battleimp_DealDamage_m2481885304_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = Battleimp_get_CurrentHealth_m1013476409(__this, /*hidden argument*/NULL);
		float L_1 = ___damageValue0;
		Battleimp_set_CurrentHealth_m826649395(__this, ((float)il2cpp_codegen_subtract((float)L_0, (float)L_1)), /*hidden argument*/NULL);
		Slider_t3903728902 * L_2 = __this->get_slider_4();
		float L_3 = Battleimp_CalcHealth_m3863819909(__this, /*hidden argument*/NULL);
		VirtActionInvoker1< float >::Invoke(47 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_2, L_3);
		TMP_Text_t2599618874 * L_4 = __this->get_dogHealth_7();
		float L_5 = Battleimp_get_CurrentHealth_m1013476409(__this, /*hidden argument*/NULL);
		V_0 = L_5;
		String_t* L_6 = Single_ToString_m3947131094((float*)(&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral3602717400, L_6, /*hidden argument*/NULL);
		TMP_Text_set_text_m1216996582(L_4, L_7, /*hidden argument*/NULL);
		float L_8 = Battleimp_get_CurrentHealth_m1013476409(__this, /*hidden argument*/NULL);
		if ((!(((float)L_8) <= ((float)(0.0f)))))
		{
			goto IL_0079;
		}
	}
	{
		__this->set_myTurn_17((bool)1);
		__this->set_ableToAttack_18((bool)1);
		__this->set_enemyAbleToAttack_19((bool)0);
		GameObject_t1113636619 * L_9 = __this->get_GUI_13();
		GameObject_SetActive_m796801857(L_9, (bool)0, /*hidden argument*/NULL);
	}

IL_0079:
	{
		Slider_t3903728902 * L_10 = __this->get_slider_4();
		float L_11 = VirtFuncInvoker0< float >::Invoke(46 /* System.Single UnityEngine.UI.Slider::get_value() */, L_10);
		if ((!(((float)L_11) <= ((float)(0.0f)))))
		{
			goto IL_00a3;
		}
	}
	{
		__this->set_myTurn_17((bool)1);
		__this->set_ableToAttack_18((bool)1);
		__this->set_enemyAbleToAttack_19((bool)0);
	}

IL_00a3:
	{
		return;
	}
}
// System.Single Battleimp::CalcHealth()
extern "C"  float Battleimp_CalcHealth_m3863819909 (Battleimp_t158148094 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = Battleimp_get_CurrentHealth_m1013476409(__this, /*hidden argument*/NULL);
		float L_1 = Battleimp_get_MaxHealth_m961871286(__this, /*hidden argument*/NULL);
		return ((float)((float)L_0/(float)L_1));
	}
}
// System.Collections.IEnumerator Battleimp::EnemyAttack()
extern "C"  RuntimeObject* Battleimp_EnemyAttack_m1443215274 (Battleimp_t158148094 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Battleimp_EnemyAttack_m1443215274_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CEnemyAttackU3Ec__Iterator0_t889231986 * V_0 = NULL;
	{
		U3CEnemyAttackU3Ec__Iterator0_t889231986 * L_0 = (U3CEnemyAttackU3Ec__Iterator0_t889231986 *)il2cpp_codegen_object_new(U3CEnemyAttackU3Ec__Iterator0_t889231986_il2cpp_TypeInfo_var);
		U3CEnemyAttackU3Ec__Iterator0__ctor_m1449397385(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CEnemyAttackU3Ec__Iterator0_t889231986 * L_1 = V_0;
		L_1->set_U24this_1(__this);
		U3CEnemyAttackU3Ec__Iterator0_t889231986 * L_2 = V_0;
		return L_2;
	}
}
// System.Single Battleimp::CalcHealthGuy()
extern "C"  float Battleimp_CalcHealthGuy_m1665544502 (Battleimp_t158148094 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = Battleimp_get_CurrentHealthGuy_m437652102(__this, /*hidden argument*/NULL);
		float L_1 = Battleimp_get_MaxHealthGuy_m3578614347(__this, /*hidden argument*/NULL);
		return ((float)((float)L_0/(float)L_1));
	}
}
// System.Void Battleimp::EnemyDealDamage(System.Single)
extern "C"  void Battleimp_EnemyDealDamage_m1731020462 (Battleimp_t158148094 * __this, float ___damageValue0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Battleimp_EnemyDealDamage_m1731020462_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = Battleimp_get_CurrentHealthGuy_m437652102(__this, /*hidden argument*/NULL);
		float L_1 = ___damageValue0;
		Battleimp_set_CurrentHealthGuy_m2081096126(__this, ((float)il2cpp_codegen_subtract((float)L_0, (float)L_1)), /*hidden argument*/NULL);
		Slider_t3903728902 * L_2 = __this->get_guySlider_5();
		float L_3 = Battleimp_CalcHealthGuy_m1665544502(__this, /*hidden argument*/NULL);
		VirtActionInvoker1< float >::Invoke(47 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_2, L_3);
		TMP_Text_t2599618874 * L_4 = __this->get_guyHealth_8();
		float L_5 = Battleimp_get_CurrentHealthGuy_m437652102(__this, /*hidden argument*/NULL);
		V_0 = L_5;
		String_t* L_6 = Single_ToString_m3947131094((float*)(&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral576020035, L_6, /*hidden argument*/NULL);
		TMP_Text_set_text_m1216996582(L_4, L_7, /*hidden argument*/NULL);
		float L_8 = Battleimp_get_CurrentHealthGuy_m437652102(__this, /*hidden argument*/NULL);
		if ((!(((float)L_8) <= ((float)(0.0f)))))
		{
			goto IL_007f;
		}
	}
	{
		__this->set_myTurn_17((bool)1);
		__this->set_ableToAttack_18((bool)1);
		__this->set_enemyAbleToAttack_19((bool)0);
		GameObject_t1113636619 * L_9 = __this->get_GUI_13();
		GameObject_SetActive_m796801857(L_9, (bool)0, /*hidden argument*/NULL);
		Battleimp_DieGuy_m2108545423(__this, /*hidden argument*/NULL);
	}

IL_007f:
	{
		Slider_t3903728902 * L_10 = __this->get_guySlider_5();
		float L_11 = VirtFuncInvoker0< float >::Invoke(46 /* System.Single UnityEngine.UI.Slider::get_value() */, L_10);
		if ((!(((float)L_11) <= ((float)(0.0f)))))
		{
			goto IL_00af;
		}
	}
	{
		__this->set_myTurn_17((bool)1);
		__this->set_ableToAttack_18((bool)1);
		__this->set_enemyAbleToAttack_19((bool)0);
		Battleimp_DieGuy_m2108545423(__this, /*hidden argument*/NULL);
	}

IL_00af:
	{
		return;
	}
}
// System.Collections.IEnumerator Battleimp::Continue()
extern "C"  RuntimeObject* Battleimp_Continue_m2989872615 (Battleimp_t158148094 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Battleimp_Continue_m2989872615_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CContinueU3Ec__Iterator1_t2046304316 * V_0 = NULL;
	{
		U3CContinueU3Ec__Iterator1_t2046304316 * L_0 = (U3CContinueU3Ec__Iterator1_t2046304316 *)il2cpp_codegen_object_new(U3CContinueU3Ec__Iterator1_t2046304316_il2cpp_TypeInfo_var);
		U3CContinueU3Ec__Iterator1__ctor_m131153455(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CContinueU3Ec__Iterator1_t2046304316 * L_1 = V_0;
		L_1->set_U24this_0(__this);
		U3CContinueU3Ec__Iterator1_t2046304316 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Battleimp::DieGuy()
extern "C"  void Battleimp_DieGuy_m2108545423 (Battleimp_t158148094 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Battleimp_DieGuy_m2108545423_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioSource_t3935305588 * L_0 = __this->get_GuydDieSFX_12();
		AudioSource_Play_m48294159(L_0, /*hidden argument*/NULL);
		TMP_Text_t2599618874 * L_1 = __this->get_attackText_6();
		TMP_Text_set_text_m1216996582(L_1, _stringLiteral3005979166, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_2 = __this->get_GUI_13();
		GameObject_SetActive_m796801857(L_2, (bool)0, /*hidden argument*/NULL);
		RuntimeObject* L_3 = Battleimp_Continue_m2989872615(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Battleimp::Again()
extern "C"  void Battleimp_Again_m3270384833 (Battleimp_t158148094 * __this, const RuntimeMethod* method)
{
	{
		SceneManager_LoadScene_m3463216446(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Battleimp::Giveup()
extern "C"  void Battleimp_Giveup_m2416201425 (Battleimp_t158148094 * __this, const RuntimeMethod* method)
{
	{
		SceneManager_LoadScene_m3463216446(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Battleimp/<Continue>c__Iterator1::.ctor()
extern "C"  void U3CContinueU3Ec__Iterator1__ctor_m131153455 (U3CContinueU3Ec__Iterator1_t2046304316 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Battleimp/<Continue>c__Iterator1::MoveNext()
extern "C"  bool U3CContinueU3Ec__Iterator1_MoveNext_m1211867273 (U3CContinueU3Ec__Iterator1_t2046304316 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CContinueU3Ec__Iterator1_MoveNext_m1211867273_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0025;
			}
			case 1:
			{
				goto IL_0049;
			}
			case 2:
			{
				goto IL_008a;
			}
		}
	}
	{
		goto IL_0097;
	}

IL_0025:
	{
		WaitForSeconds_t1699091251 * L_2 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_2, (5.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		bool L_3 = __this->get_U24disposing_2();
		if (L_3)
		{
			goto IL_0044;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0044:
	{
		goto IL_0099;
	}

IL_0049:
	{
		Battleimp_t158148094 * L_4 = __this->get_U24this_0();
		Animator_t434523843 * L_5 = L_4->get_animator_10();
		Animator_SetBool_m234840832(L_5, _stringLiteral1985039568, (bool)1, /*hidden argument*/NULL);
		intptr_t L_6 = (intptr_t)U3CContinueU3Ec__Iterator1_U3CU3Em__0_m3660005984_RuntimeMethod_var;
		Func_1_t3822001908 * L_7 = (Func_1_t3822001908 *)il2cpp_codegen_object_new(Func_1_t3822001908_il2cpp_TypeInfo_var);
		Func_1__ctor_m1399073142(L_7, __this, L_6, /*hidden argument*/Func_1__ctor_m1399073142_RuntimeMethod_var);
		WaitUntil_t3373419216 * L_8 = (WaitUntil_t3373419216 *)il2cpp_codegen_object_new(WaitUntil_t3373419216_il2cpp_TypeInfo_var);
		WaitUntil__ctor_m4227046299(L_8, L_7, /*hidden argument*/NULL);
		__this->set_U24current_1(L_8);
		bool L_9 = __this->get_U24disposing_2();
		if (L_9)
		{
			goto IL_0085;
		}
	}
	{
		__this->set_U24PC_3(2);
	}

IL_0085:
	{
		goto IL_0099;
	}

IL_008a:
	{
		SceneManager_LoadScene_m3463216446(NULL /*static, unused*/, 3, /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_0097:
	{
		return (bool)0;
	}

IL_0099:
	{
		return (bool)1;
	}
}
// System.Object Battleimp/<Continue>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CContinueU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m246031950 (U3CContinueU3Ec__Iterator1_t2046304316 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object Battleimp/<Continue>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CContinueU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2531069349 (U3CContinueU3Ec__Iterator1_t2046304316 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void Battleimp/<Continue>c__Iterator1::Dispose()
extern "C"  void U3CContinueU3Ec__Iterator1_Dispose_m3804146309 (U3CContinueU3Ec__Iterator1_t2046304316 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void Battleimp/<Continue>c__Iterator1::Reset()
extern "C"  void U3CContinueU3Ec__Iterator1_Reset_m784469664 (U3CContinueU3Ec__Iterator1_t2046304316 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CContinueU3Ec__Iterator1_Reset_m784469664_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CContinueU3Ec__Iterator1_Reset_m784469664_RuntimeMethod_var);
	}
}
// System.Boolean Battleimp/<Continue>c__Iterator1::<>m__0()
extern "C"  bool U3CContinueU3Ec__Iterator1_U3CU3Em__0_m3660005984 (U3CContinueU3Ec__Iterator1_t2046304316 * __this, const RuntimeMethod* method)
{
	Color_t2555686324  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Battleimp_t158148094 * L_0 = __this->get_U24this_0();
		Image_t2670269651 * L_1 = L_0->get_black_9();
		Color_t2555686324  L_2 = VirtFuncInvoker0< Color_t2555686324  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_1);
		V_0 = L_2;
		float L_3 = (&V_0)->get_a_3();
		return (bool)((((float)L_3) == ((float)(1.0f)))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Battleimp/<EnemyAttack>c__Iterator0::.ctor()
extern "C"  void U3CEnemyAttackU3Ec__Iterator0__ctor_m1449397385 (U3CEnemyAttackU3Ec__Iterator0_t889231986 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Battleimp/<EnemyAttack>c__Iterator0::MoveNext()
extern "C"  bool U3CEnemyAttackU3Ec__Iterator0_MoveNext_m25122958 (U3CEnemyAttackU3Ec__Iterator0_t889231986 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CEnemyAttackU3Ec__Iterator0_MoveNext_m25122958_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_006d;
			}
		}
	}
	{
		goto IL_0132;
	}

IL_0021:
	{
		Battleimp_t158148094 * L_2 = __this->get_U24this_1();
		bool L_3 = L_2->get_enemyAbleToAttack_19();
		if (!L_3)
		{
			goto IL_012b;
		}
	}
	{
		Battleimp_t158148094 * L_4 = __this->get_U24this_1();
		L_4->set_myTurn_17((bool)1);
		Battleimp_t158148094 * L_5 = __this->get_U24this_1();
		L_5->set_enemyAbleToAttack_19((bool)0);
		WaitForSeconds_t1699091251 * L_6 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_6, (1.0f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_6);
		bool L_7 = __this->get_U24disposing_3();
		if (L_7)
		{
			goto IL_0068;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_0068:
	{
		goto IL_0134;
	}

IL_006d:
	{
		Battleimp_t158148094 * L_8 = __this->get_U24this_1();
		Random_t108471755 * L_9 = L_8->get_rnd_16();
		int32_t L_10 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Random::Next(System.Int32,System.Int32) */, L_9, 1, 5);
		__this->set_U3CrndDMGU3E__1_0((((float)((float)L_10))));
		float L_11 = __this->get_U3CrndDMGU3E__1_0();
		if ((!(((float)L_11) >= ((float)(4.8f)))))
		{
			goto IL_00ab;
		}
	}
	{
		Battleimp_t158148094 * L_12 = __this->get_U24this_1();
		TMP_Text_t2599618874 * L_13 = L_12->get_attackText_6();
		TMP_Text_set_text_m1216996582(L_13, _stringLiteral1415876783, /*hidden argument*/NULL);
	}

IL_00ab:
	{
		float L_14 = __this->get_U3CrndDMGU3E__1_0();
		if ((!(((float)L_14) <= ((float)(4.0f)))))
		{
			goto IL_00d0;
		}
	}
	{
		Battleimp_t158148094 * L_15 = __this->get_U24this_1();
		TMP_Text_t2599618874 * L_16 = L_15->get_attackText_6();
		TMP_Text_set_text_m1216996582(L_16, _stringLiteral231011766, /*hidden argument*/NULL);
	}

IL_00d0:
	{
		float L_17 = __this->get_U3CrndDMGU3E__1_0();
		if ((!(((float)L_17) <= ((float)(2.5f)))))
		{
			goto IL_00f5;
		}
	}
	{
		Battleimp_t158148094 * L_18 = __this->get_U24this_1();
		TMP_Text_t2599618874 * L_19 = L_18->get_attackText_6();
		TMP_Text_set_text_m1216996582(L_19, _stringLiteral1635465982, /*hidden argument*/NULL);
	}

IL_00f5:
	{
		float L_20 = __this->get_U3CrndDMGU3E__1_0();
		if ((!(((float)L_20) <= ((float)(1.5f)))))
		{
			goto IL_011a;
		}
	}
	{
		Battleimp_t158148094 * L_21 = __this->get_U24this_1();
		TMP_Text_t2599618874 * L_22 = L_21->get_attackText_6();
		TMP_Text_set_text_m1216996582(L_22, _stringLiteral681334598, /*hidden argument*/NULL);
	}

IL_011a:
	{
		Battleimp_t158148094 * L_23 = __this->get_U24this_1();
		float L_24 = __this->get_U3CrndDMGU3E__1_0();
		Battleimp_EnemyDealDamage_m1731020462(L_23, L_24, /*hidden argument*/NULL);
	}

IL_012b:
	{
		__this->set_U24PC_4((-1));
	}

IL_0132:
	{
		return (bool)0;
	}

IL_0134:
	{
		return (bool)1;
	}
}
// System.Object Battleimp/<EnemyAttack>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CEnemyAttackU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1012690893 (U3CEnemyAttackU3Ec__Iterator0_t889231986 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object Battleimp/<EnemyAttack>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CEnemyAttackU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2275745245 (U3CEnemyAttackU3Ec__Iterator0_t889231986 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Void Battleimp/<EnemyAttack>c__Iterator0::Dispose()
extern "C"  void U3CEnemyAttackU3Ec__Iterator0_Dispose_m864728332 (U3CEnemyAttackU3Ec__Iterator0_t889231986 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void Battleimp/<EnemyAttack>c__Iterator0::Reset()
extern "C"  void U3CEnemyAttackU3Ec__Iterator0_Reset_m196973293 (U3CEnemyAttackU3Ec__Iterator0_t889231986 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CEnemyAttackU3Ec__Iterator0_Reset_m196973293_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CEnemyAttackU3Ec__Iterator0_Reset_m196973293_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Battleinf::.ctor()
extern "C"  void Battleinf__ctor_m1987701741 (Battleinf_t1732126188 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Battleinf__ctor_m1987701741_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color32_t2600501292  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Random_t108471755 * L_0 = (Random_t108471755 *)il2cpp_codegen_object_new(Random_t108471755_il2cpp_TypeInfo_var);
		Random__ctor_m4122933043(L_0, /*hidden argument*/NULL);
		__this->set_rnd_18(L_0);
		__this->set_myTurn_19((bool)1);
		il2cpp_codegen_initobj((&V_0), sizeof(Color32_t2600501292 ));
		Color32_t2600501292  L_1 = V_0;
		__this->set_col_28(L_1);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single Battleinf::get_CurrentHealth()
extern "C"  float Battleinf_get_CurrentHealth_m3568673109 (Battleinf_t1732126188 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CCurrentHealthU3Ek__BackingField_30();
		return L_0;
	}
}
// System.Void Battleinf::set_CurrentHealth(System.Single)
extern "C"  void Battleinf_set_CurrentHealth_m3599131987 (Battleinf_t1732126188 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CCurrentHealthU3Ek__BackingField_30(L_0);
		return;
	}
}
// System.Single Battleinf::get_MaxHealth()
extern "C"  float Battleinf_get_MaxHealth_m3113673643 (Battleinf_t1732126188 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CMaxHealthU3Ek__BackingField_31();
		return L_0;
	}
}
// System.Void Battleinf::set_MaxHealth(System.Single)
extern "C"  void Battleinf_set_MaxHealth_m500050573 (Battleinf_t1732126188 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CMaxHealthU3Ek__BackingField_31(L_0);
		return;
	}
}
// System.Single Battleinf::get_CurrentHealthGuy()
extern "C"  float Battleinf_get_CurrentHealthGuy_m560545868 (Battleinf_t1732126188 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CCurrentHealthGuyU3Ek__BackingField_32();
		return L_0;
	}
}
// System.Void Battleinf::set_CurrentHealthGuy(System.Single)
extern "C"  void Battleinf_set_CurrentHealthGuy_m2983613390 (Battleinf_t1732126188 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CCurrentHealthGuyU3Ek__BackingField_32(L_0);
		return;
	}
}
// System.Single Battleinf::get_MaxHealthGuy()
extern "C"  float Battleinf_get_MaxHealthGuy_m2575423585 (Battleinf_t1732126188 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CMaxHealthGuyU3Ek__BackingField_33();
		return L_0;
	}
}
// System.Void Battleinf::set_MaxHealthGuy(System.Single)
extern "C"  void Battleinf_set_MaxHealthGuy_m3424096180 (Battleinf_t1732126188 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CMaxHealthGuyU3Ek__BackingField_33(L_0);
		return;
	}
}
// System.Void Battleinf::Start()
extern "C"  void Battleinf_Start_m1339897305 (Battleinf_t1732126188 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Battleinf_Start_m1339897305_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		GameObject_t1113636619 * L_0 = GameObject_FindGameObjectWithTag_m2129039296(NULL /*static, unused*/, _stringLiteral531699260, /*hidden argument*/NULL);
		Animinf_t989049961 * L_1 = GameObject_GetComponent_TisAniminf_t989049961_m1535312041(L_0, /*hidden argument*/GameObject_GetComponent_TisAniminf_t989049961_m1535312041_RuntimeMethod_var);
		__this->set_anim_17(L_1);
		GameObject_t1113636619 * L_2 = __this->get_GUI_15();
		GameObject_SetActive_m796801857(L_2, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_3 = __this->get_GameOver_16();
		GameObject_SetActive_m796801857(L_3, (bool)0, /*hidden argument*/NULL);
		__this->set_isDead_24((bool)0);
		Battleinf_set_MaxHealth_m500050573(__this, (20.0f), /*hidden argument*/NULL);
		float L_4 = Battleinf_get_MaxHealth_m3113673643(__this, /*hidden argument*/NULL);
		Battleinf_set_CurrentHealth_m3599131987(__this, L_4, /*hidden argument*/NULL);
		TMP_Text_t2599618874 * L_5 = __this->get_dogHealth_8();
		float L_6 = Battleinf_get_CurrentHealth_m3568673109(__this, /*hidden argument*/NULL);
		V_0 = L_6;
		String_t* L_7 = Single_ToString_m3947131094((float*)(&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral3602717400, L_7, /*hidden argument*/NULL);
		TMP_Text_set_text_m1216996582(L_5, L_8, /*hidden argument*/NULL);
		Slider_t3903728902 * L_9 = __this->get_slider_4();
		float L_10 = Battleinf_CalcHealth_m705597414(__this, /*hidden argument*/NULL);
		VirtActionInvoker1< float >::Invoke(47 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_9, L_10);
		Battleinf_set_MaxHealthGuy_m3424096180(__this, (20.0f), /*hidden argument*/NULL);
		float L_11 = Battleinf_get_MaxHealthGuy_m2575423585(__this, /*hidden argument*/NULL);
		Battleinf_set_CurrentHealthGuy_m2983613390(__this, L_11, /*hidden argument*/NULL);
		TMP_Text_t2599618874 * L_12 = __this->get_guyHealth_9();
		float L_13 = Battleinf_get_CurrentHealthGuy_m560545868(__this, /*hidden argument*/NULL);
		V_1 = L_13;
		String_t* L_14 = Single_ToString_m3947131094((float*)(&V_1), /*hidden argument*/NULL);
		String_t* L_15 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral576020035, L_14, /*hidden argument*/NULL);
		TMP_Text_set_text_m1216996582(L_12, L_15, /*hidden argument*/NULL);
		Slider_t3903728902 * L_16 = __this->get_guySlider_5();
		float L_17 = Battleinf_CalcHealthGuy_m1174535655(__this, /*hidden argument*/NULL);
		VirtActionInvoker1< float >::Invoke(47 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_16, L_17);
		Random_t108471755 * L_18 = __this->get_rnd_18();
		int32_t L_19 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Random::Next(System.Int32,System.Int32) */, L_18, 1, ((int32_t)255));
		__this->set_r_25((uint8_t)(((int32_t)((uint8_t)L_19))));
		Random_t108471755 * L_20 = __this->get_rnd_18();
		int32_t L_21 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Random::Next(System.Int32,System.Int32) */, L_20, 1, ((int32_t)255));
		__this->set_g_26((uint8_t)(((int32_t)((uint8_t)L_21))));
		Random_t108471755 * L_22 = __this->get_rnd_18();
		int32_t L_23 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Random::Next(System.Int32,System.Int32) */, L_22, 1, ((int32_t)255));
		__this->set_b_27((uint8_t)(((int32_t)((uint8_t)L_23))));
		Random_t108471755 * L_24 = __this->get_rnd_18();
		int32_t L_25 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Random::Next(System.Int32,System.Int32) */, L_24, 1, ((int32_t)10000));
		__this->set_n_29(L_25);
		TMP_Text_t2599618874 * L_26 = __this->get_nameText_7();
		int32_t L_27 = __this->get_n_29();
		int32_t L_28 = L_27;
		RuntimeObject * L_29 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_28);
		String_t* L_30 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral160657250, L_29, /*hidden argument*/NULL);
		TMP_Text_set_text_m1216996582(L_26, L_30, /*hidden argument*/NULL);
		Color32_t2600501292 * L_31 = __this->get_address_of_col_28();
		uint8_t L_32 = __this->get_r_25();
		L_31->set_r_1(L_32);
		Color32_t2600501292 * L_33 = __this->get_address_of_col_28();
		uint8_t L_34 = __this->get_g_26();
		L_33->set_g_2(L_34);
		Color32_t2600501292 * L_35 = __this->get_address_of_col_28();
		uint8_t L_36 = __this->get_b_27();
		L_35->set_b_3(L_36);
		Color32_t2600501292 * L_37 = __this->get_address_of_col_28();
		L_37->set_a_4((uint8_t)((int32_t)255));
		SpriteRenderer_t3235626157 * L_38 = __this->get_dogSprite_10();
		Color32_t2600501292  L_39 = __this->get_col_28();
		Color_t2555686324  L_40 = Color32_op_Implicit_m213813866(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		SpriteRenderer_set_color_m3056777566(L_38, L_40, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Battleinf::Update()
extern "C"  void Battleinf_Update_m3915901730 (Battleinf_t1732126188 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Battleinf_Update_m3915901730_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animinf_t989049961 * L_0 = __this->get_anim_17();
		bool L_1 = L_0->get_isDone_8();
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		bool L_2 = __this->get_isDone_23();
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		__this->set_isReady_22((bool)1);
	}

IL_0022:
	{
		bool L_3 = __this->get_isReady_22();
		if (!L_3)
		{
			goto IL_004b;
		}
	}
	{
		bool L_4 = __this->get_isDone_23();
		if (L_4)
		{
			goto IL_004b;
		}
	}
	{
		GameObject_t1113636619 * L_5 = __this->get_GUI_15();
		GameObject_SetActive_m796801857(L_5, (bool)1, /*hidden argument*/NULL);
		__this->set_isDone_23((bool)1);
	}

IL_004b:
	{
		bool L_6 = __this->get_myTurn_19();
		if (!L_6)
		{
			goto IL_005d;
		}
	}
	{
		__this->set_ableToAttack_20((bool)1);
	}

IL_005d:
	{
		bool L_7 = __this->get_myTurn_19();
		if (L_7)
		{
			goto IL_007c;
		}
	}
	{
		__this->set_enemyAbleToAttack_21((bool)1);
		RuntimeObject* L_8 = Battleinf_EnemyAttack_m3332119496(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_8, /*hidden argument*/NULL);
	}

IL_007c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_9 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, ((int32_t)27), /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_008e;
		}
	}
	{
		SceneManager_LoadScene_m3463216446(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_008e:
	{
		return;
	}
}
// System.Void Battleinf::Attack(System.String)
extern "C"  void Battleinf_Attack_m1533273237 (Battleinf_t1732126188 * __this, String_t* ___move0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Battleinf_Attack_m1533273237_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		bool L_0 = __this->get_ableToAttack_20();
		if (!L_0)
		{
			goto IL_00cc;
		}
	}
	{
		__this->set_myTurn_19((bool)0);
		__this->set_ableToAttack_20((bool)0);
		Random_t108471755 * L_1 = __this->get_rnd_18();
		int32_t L_2 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Random::Next(System.Int32,System.Int32) */, L_1, 1, 5);
		V_0 = (((float)((float)L_2)));
		float L_3 = V_0;
		if ((!(((float)L_3) >= ((float)(4.8f)))))
		{
			goto IL_004e;
		}
	}
	{
		TMP_Text_t2599618874 * L_4 = __this->get_attackText_6();
		String_t* L_5 = ___move0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral1378388746, L_5, _stringLiteral348006446, /*hidden argument*/NULL);
		TMP_Text_set_text_m1216996582(L_4, L_6, /*hidden argument*/NULL);
	}

IL_004e:
	{
		float L_7 = V_0;
		if ((!(((float)L_7) <= ((float)(4.0f)))))
		{
			goto IL_0074;
		}
	}
	{
		TMP_Text_t2599618874 * L_8 = __this->get_attackText_6();
		String_t* L_9 = ___move0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral1378388746, L_9, _stringLiteral877777334, /*hidden argument*/NULL);
		TMP_Text_set_text_m1216996582(L_8, L_10, /*hidden argument*/NULL);
	}

IL_0074:
	{
		float L_11 = V_0;
		if ((!(((float)L_11) <= ((float)(2.5f)))))
		{
			goto IL_009a;
		}
	}
	{
		TMP_Text_t2599618874 * L_12 = __this->get_attackText_6();
		String_t* L_13 = ___move0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral1378388746, L_13, _stringLiteral4242887721, /*hidden argument*/NULL);
		TMP_Text_set_text_m1216996582(L_12, L_14, /*hidden argument*/NULL);
	}

IL_009a:
	{
		float L_15 = V_0;
		if ((!(((float)L_15) <= ((float)(1.5f)))))
		{
			goto IL_00c0;
		}
	}
	{
		TMP_Text_t2599618874 * L_16 = __this->get_attackText_6();
		String_t* L_17 = ___move0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral1378388746, L_17, _stringLiteral2831055546, /*hidden argument*/NULL);
		TMP_Text_set_text_m1216996582(L_16, L_18, /*hidden argument*/NULL);
	}

IL_00c0:
	{
		float L_19 = V_0;
		Battleinf_DealDamage_m3678906129(__this, L_19, /*hidden argument*/NULL);
		goto IL_00cd;
	}

IL_00cc:
	{
		return;
	}

IL_00cd:
	{
		return;
	}
}
// System.Void Battleinf::DealDamage(System.Single)
extern "C"  void Battleinf_DealDamage_m3678906129 (Battleinf_t1732126188 * __this, float ___damageValue0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Battleinf_DealDamage_m3678906129_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = Battleinf_get_CurrentHealth_m3568673109(__this, /*hidden argument*/NULL);
		float L_1 = ___damageValue0;
		Battleinf_set_CurrentHealth_m3599131987(__this, ((float)il2cpp_codegen_subtract((float)L_0, (float)L_1)), /*hidden argument*/NULL);
		Slider_t3903728902 * L_2 = __this->get_slider_4();
		float L_3 = Battleinf_CalcHealth_m705597414(__this, /*hidden argument*/NULL);
		VirtActionInvoker1< float >::Invoke(47 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_2, L_3);
		TMP_Text_t2599618874 * L_4 = __this->get_dogHealth_8();
		float L_5 = Battleinf_get_CurrentHealth_m3568673109(__this, /*hidden argument*/NULL);
		V_0 = L_5;
		String_t* L_6 = Single_ToString_m3947131094((float*)(&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral3602717400, L_6, /*hidden argument*/NULL);
		TMP_Text_set_text_m1216996582(L_4, L_7, /*hidden argument*/NULL);
		float L_8 = Battleinf_get_CurrentHealth_m3568673109(__this, /*hidden argument*/NULL);
		if ((!(((float)L_8) <= ((float)(0.0f)))))
		{
			goto IL_007f;
		}
	}
	{
		__this->set_myTurn_19((bool)1);
		__this->set_ableToAttack_20((bool)1);
		__this->set_enemyAbleToAttack_21((bool)0);
		GameObject_t1113636619 * L_9 = __this->get_GUI_15();
		GameObject_SetActive_m796801857(L_9, (bool)0, /*hidden argument*/NULL);
		Battleinf_Die_m3352291207(__this, /*hidden argument*/NULL);
	}

IL_007f:
	{
		Slider_t3903728902 * L_10 = __this->get_slider_4();
		float L_11 = VirtFuncInvoker0< float >::Invoke(46 /* System.Single UnityEngine.UI.Slider::get_value() */, L_10);
		if ((!(((float)L_11) <= ((float)(0.0f)))))
		{
			goto IL_00af;
		}
	}
	{
		__this->set_myTurn_19((bool)1);
		__this->set_ableToAttack_20((bool)1);
		__this->set_enemyAbleToAttack_21((bool)0);
		Battleinf_Die_m3352291207(__this, /*hidden argument*/NULL);
	}

IL_00af:
	{
		return;
	}
}
// System.Single Battleinf::CalcHealth()
extern "C"  float Battleinf_CalcHealth_m705597414 (Battleinf_t1732126188 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = Battleinf_get_CurrentHealth_m3568673109(__this, /*hidden argument*/NULL);
		float L_1 = Battleinf_get_MaxHealth_m3113673643(__this, /*hidden argument*/NULL);
		return ((float)((float)L_0/(float)L_1));
	}
}
// System.Collections.IEnumerator Battleinf::EnemyAttack()
extern "C"  RuntimeObject* Battleinf_EnemyAttack_m3332119496 (Battleinf_t1732126188 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Battleinf_EnemyAttack_m3332119496_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CEnemyAttackU3Ec__Iterator0_t2884124416 * V_0 = NULL;
	{
		U3CEnemyAttackU3Ec__Iterator0_t2884124416 * L_0 = (U3CEnemyAttackU3Ec__Iterator0_t2884124416 *)il2cpp_codegen_object_new(U3CEnemyAttackU3Ec__Iterator0_t2884124416_il2cpp_TypeInfo_var);
		U3CEnemyAttackU3Ec__Iterator0__ctor_m3824501192(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CEnemyAttackU3Ec__Iterator0_t2884124416 * L_1 = V_0;
		L_1->set_U24this_1(__this);
		U3CEnemyAttackU3Ec__Iterator0_t2884124416 * L_2 = V_0;
		return L_2;
	}
}
// System.Single Battleinf::CalcHealthGuy()
extern "C"  float Battleinf_CalcHealthGuy_m1174535655 (Battleinf_t1732126188 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = Battleinf_get_CurrentHealthGuy_m560545868(__this, /*hidden argument*/NULL);
		float L_1 = Battleinf_get_MaxHealthGuy_m2575423585(__this, /*hidden argument*/NULL);
		return ((float)((float)L_0/(float)L_1));
	}
}
// System.Void Battleinf::EnemyDealDamage(System.Single)
extern "C"  void Battleinf_EnemyDealDamage_m3350442186 (Battleinf_t1732126188 * __this, float ___damageValue0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Battleinf_EnemyDealDamage_m3350442186_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = Battleinf_get_CurrentHealthGuy_m560545868(__this, /*hidden argument*/NULL);
		float L_1 = ___damageValue0;
		Battleinf_set_CurrentHealthGuy_m2983613390(__this, ((float)il2cpp_codegen_subtract((float)L_0, (float)L_1)), /*hidden argument*/NULL);
		Slider_t3903728902 * L_2 = __this->get_guySlider_5();
		float L_3 = Battleinf_CalcHealthGuy_m1174535655(__this, /*hidden argument*/NULL);
		VirtActionInvoker1< float >::Invoke(47 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_2, L_3);
		TMP_Text_t2599618874 * L_4 = __this->get_guyHealth_9();
		float L_5 = Battleinf_get_CurrentHealthGuy_m560545868(__this, /*hidden argument*/NULL);
		V_0 = L_5;
		String_t* L_6 = Single_ToString_m3947131094((float*)(&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral576020035, L_6, /*hidden argument*/NULL);
		TMP_Text_set_text_m1216996582(L_4, L_7, /*hidden argument*/NULL);
		float L_8 = Battleinf_get_CurrentHealthGuy_m560545868(__this, /*hidden argument*/NULL);
		if ((!(((float)L_8) <= ((float)(0.0f)))))
		{
			goto IL_007f;
		}
	}
	{
		__this->set_myTurn_19((bool)1);
		__this->set_ableToAttack_20((bool)1);
		__this->set_enemyAbleToAttack_21((bool)0);
		GameObject_t1113636619 * L_9 = __this->get_GUI_15();
		GameObject_SetActive_m796801857(L_9, (bool)0, /*hidden argument*/NULL);
		Battleinf_DieGuy_m3765556335(__this, /*hidden argument*/NULL);
	}

IL_007f:
	{
		Slider_t3903728902 * L_10 = __this->get_guySlider_5();
		float L_11 = VirtFuncInvoker0< float >::Invoke(46 /* System.Single UnityEngine.UI.Slider::get_value() */, L_10);
		if ((!(((float)L_11) <= ((float)(0.0f)))))
		{
			goto IL_00af;
		}
	}
	{
		__this->set_myTurn_19((bool)1);
		__this->set_ableToAttack_20((bool)1);
		__this->set_enemyAbleToAttack_21((bool)0);
		Battleinf_DieGuy_m3765556335(__this, /*hidden argument*/NULL);
	}

IL_00af:
	{
		return;
	}
}
// System.Void Battleinf::Die()
extern "C"  void Battleinf_Die_m3352291207 (Battleinf_t1732126188 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Battleinf_Die_m3352291207_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioSource_t3935305588 * L_0 = __this->get_DogDieSFX_13();
		AudioSource_Play_m48294159(L_0, /*hidden argument*/NULL);
		TMP_Text_t2599618874 * L_1 = __this->get_attackText_6();
		TMP_Text_set_text_m1216996582(L_1, _stringLiteral4201971368, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_2 = __this->get_GUI_15();
		GameObject_SetActive_m796801857(L_2, (bool)0, /*hidden argument*/NULL);
		__this->set_isDead_24((bool)1);
		RuntimeObject* L_3 = Battleinf_Continue_m985971343(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Battleinf::Continue()
extern "C"  RuntimeObject* Battleinf_Continue_m985971343 (Battleinf_t1732126188 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Battleinf_Continue_m985971343_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CContinueU3Ec__Iterator1_t1090546628 * V_0 = NULL;
	{
		U3CContinueU3Ec__Iterator1_t1090546628 * L_0 = (U3CContinueU3Ec__Iterator1_t1090546628 *)il2cpp_codegen_object_new(U3CContinueU3Ec__Iterator1_t1090546628_il2cpp_TypeInfo_var);
		U3CContinueU3Ec__Iterator1__ctor_m1400880857(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CContinueU3Ec__Iterator1_t1090546628 * L_1 = V_0;
		L_1->set_U24this_0(__this);
		U3CContinueU3Ec__Iterator1_t1090546628 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Battleinf::DieGuy()
extern "C"  void Battleinf_DieGuy_m3765556335 (Battleinf_t1732126188 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Battleinf_DieGuy_m3765556335_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioSource_t3935305588 * L_0 = __this->get_GuydDieSFX_14();
		AudioSource_Play_m48294159(L_0, /*hidden argument*/NULL);
		TMP_Text_t2599618874 * L_1 = __this->get_attackText_6();
		TMP_Text_set_text_m1216996582(L_1, _stringLiteral3005979166, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_2 = __this->get_GUI_15();
		GameObject_SetActive_m796801857(L_2, (bool)0, /*hidden argument*/NULL);
		RuntimeObject* L_3 = Battleinf_DIe_m2804138209(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Battleinf::DIe()
extern "C"  RuntimeObject* Battleinf_DIe_m2804138209 (Battleinf_t1732126188 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Battleinf_DIe_m2804138209_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CDIeU3Ec__Iterator2_t3004093797 * V_0 = NULL;
	{
		U3CDIeU3Ec__Iterator2_t3004093797 * L_0 = (U3CDIeU3Ec__Iterator2_t3004093797 *)il2cpp_codegen_object_new(U3CDIeU3Ec__Iterator2_t3004093797_il2cpp_TypeInfo_var);
		U3CDIeU3Ec__Iterator2__ctor_m4284698291(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDIeU3Ec__Iterator2_t3004093797 * L_1 = V_0;
		L_1->set_U24this_0(__this);
		U3CDIeU3Ec__Iterator2_t3004093797 * L_2 = V_0;
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Battleinf/<Continue>c__Iterator1::.ctor()
extern "C"  void U3CContinueU3Ec__Iterator1__ctor_m1400880857 (U3CContinueU3Ec__Iterator1_t1090546628 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Battleinf/<Continue>c__Iterator1::MoveNext()
extern "C"  bool U3CContinueU3Ec__Iterator1_MoveNext_m505267573 (U3CContinueU3Ec__Iterator1_t1090546628 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CContinueU3Ec__Iterator1_MoveNext_m505267573_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0025;
			}
			case 1:
			{
				goto IL_0049;
			}
			case 2:
			{
				goto IL_009f;
			}
		}
	}
	{
		goto IL_00ac;
	}

IL_0025:
	{
		WaitForSeconds_t1699091251 * L_2 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_2, (10.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		bool L_3 = __this->get_U24disposing_2();
		if (L_3)
		{
			goto IL_0044;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0044:
	{
		goto IL_00ae;
	}

IL_0049:
	{
		Battleinf_t1732126188 * L_4 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_5 = L_4->get_attackText_6();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		TMP_Text_set_text_m1216996582(L_5, L_6, /*hidden argument*/NULL);
		Battleinf_t1732126188 * L_7 = __this->get_U24this_0();
		Animator_t434523843 * L_8 = L_7->get_animator_12();
		Animator_SetBool_m234840832(L_8, _stringLiteral1985039568, (bool)1, /*hidden argument*/NULL);
		intptr_t L_9 = (intptr_t)U3CContinueU3Ec__Iterator1_U3CU3Em__0_m934966357_RuntimeMethod_var;
		Func_1_t3822001908 * L_10 = (Func_1_t3822001908 *)il2cpp_codegen_object_new(Func_1_t3822001908_il2cpp_TypeInfo_var);
		Func_1__ctor_m1399073142(L_10, __this, L_9, /*hidden argument*/Func_1__ctor_m1399073142_RuntimeMethod_var);
		WaitUntil_t3373419216 * L_11 = (WaitUntil_t3373419216 *)il2cpp_codegen_object_new(WaitUntil_t3373419216_il2cpp_TypeInfo_var);
		WaitUntil__ctor_m4227046299(L_11, L_10, /*hidden argument*/NULL);
		__this->set_U24current_1(L_11);
		bool L_12 = __this->get_U24disposing_2();
		if (L_12)
		{
			goto IL_009a;
		}
	}
	{
		__this->set_U24PC_3(2);
	}

IL_009a:
	{
		goto IL_00ae;
	}

IL_009f:
	{
		SceneManager_LoadScene_m3463216446(NULL /*static, unused*/, 6, /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_00ac:
	{
		return (bool)0;
	}

IL_00ae:
	{
		return (bool)1;
	}
}
// System.Object Battleinf/<Continue>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CContinueU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1404438338 (U3CContinueU3Ec__Iterator1_t1090546628 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object Battleinf/<Continue>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CContinueU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2310972383 (U3CContinueU3Ec__Iterator1_t1090546628 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void Battleinf/<Continue>c__Iterator1::Dispose()
extern "C"  void U3CContinueU3Ec__Iterator1_Dispose_m4052803795 (U3CContinueU3Ec__Iterator1_t1090546628 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void Battleinf/<Continue>c__Iterator1::Reset()
extern "C"  void U3CContinueU3Ec__Iterator1_Reset_m3351168776 (U3CContinueU3Ec__Iterator1_t1090546628 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CContinueU3Ec__Iterator1_Reset_m3351168776_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CContinueU3Ec__Iterator1_Reset_m3351168776_RuntimeMethod_var);
	}
}
// System.Boolean Battleinf/<Continue>c__Iterator1::<>m__0()
extern "C"  bool U3CContinueU3Ec__Iterator1_U3CU3Em__0_m934966357 (U3CContinueU3Ec__Iterator1_t1090546628 * __this, const RuntimeMethod* method)
{
	Color_t2555686324  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Battleinf_t1732126188 * L_0 = __this->get_U24this_0();
		Image_t2670269651 * L_1 = L_0->get_black_11();
		Color_t2555686324  L_2 = VirtFuncInvoker0< Color_t2555686324  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_1);
		V_0 = L_2;
		float L_3 = (&V_0)->get_a_3();
		return (bool)((((float)L_3) == ((float)(1.0f)))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Battleinf/<DIe>c__Iterator2::.ctor()
extern "C"  void U3CDIeU3Ec__Iterator2__ctor_m4284698291 (U3CDIeU3Ec__Iterator2_t3004093797 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Battleinf/<DIe>c__Iterator2::MoveNext()
extern "C"  bool U3CDIeU3Ec__Iterator2_MoveNext_m3837063140 (U3CDIeU3Ec__Iterator2_t3004093797 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDIeU3Ec__Iterator2_MoveNext_m3837063140_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0025;
			}
			case 1:
			{
				goto IL_0049;
			}
			case 2:
			{
				goto IL_009f;
			}
		}
	}
	{
		goto IL_00ac;
	}

IL_0025:
	{
		WaitForSeconds_t1699091251 * L_2 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_2, (5.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		bool L_3 = __this->get_U24disposing_2();
		if (L_3)
		{
			goto IL_0044;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0044:
	{
		goto IL_00ae;
	}

IL_0049:
	{
		Battleinf_t1732126188 * L_4 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_5 = L_4->get_attackText_6();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		TMP_Text_set_text_m1216996582(L_5, L_6, /*hidden argument*/NULL);
		Battleinf_t1732126188 * L_7 = __this->get_U24this_0();
		Animator_t434523843 * L_8 = L_7->get_animator_12();
		Animator_SetBool_m234840832(L_8, _stringLiteral1985039568, (bool)1, /*hidden argument*/NULL);
		intptr_t L_9 = (intptr_t)U3CDIeU3Ec__Iterator2_U3CU3Em__0_m911504022_RuntimeMethod_var;
		Func_1_t3822001908 * L_10 = (Func_1_t3822001908 *)il2cpp_codegen_object_new(Func_1_t3822001908_il2cpp_TypeInfo_var);
		Func_1__ctor_m1399073142(L_10, __this, L_9, /*hidden argument*/Func_1__ctor_m1399073142_RuntimeMethod_var);
		WaitUntil_t3373419216 * L_11 = (WaitUntil_t3373419216 *)il2cpp_codegen_object_new(WaitUntil_t3373419216_il2cpp_TypeInfo_var);
		WaitUntil__ctor_m4227046299(L_11, L_10, /*hidden argument*/NULL);
		__this->set_U24current_1(L_11);
		bool L_12 = __this->get_U24disposing_2();
		if (L_12)
		{
			goto IL_009a;
		}
	}
	{
		__this->set_U24PC_3(2);
	}

IL_009a:
	{
		goto IL_00ae;
	}

IL_009f:
	{
		SceneManager_LoadScene_m3463216446(NULL /*static, unused*/, 6, /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_00ac:
	{
		return (bool)0;
	}

IL_00ae:
	{
		return (bool)1;
	}
}
// System.Object Battleinf/<DIe>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CDIeU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3125119921 (U3CDIeU3Ec__Iterator2_t3004093797 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object Battleinf/<DIe>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CDIeU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2942581964 (U3CDIeU3Ec__Iterator2_t3004093797 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void Battleinf/<DIe>c__Iterator2::Dispose()
extern "C"  void U3CDIeU3Ec__Iterator2_Dispose_m2270532457 (U3CDIeU3Ec__Iterator2_t3004093797 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void Battleinf/<DIe>c__Iterator2::Reset()
extern "C"  void U3CDIeU3Ec__Iterator2_Reset_m2654355123 (U3CDIeU3Ec__Iterator2_t3004093797 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDIeU3Ec__Iterator2_Reset_m2654355123_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CDIeU3Ec__Iterator2_Reset_m2654355123_RuntimeMethod_var);
	}
}
// System.Boolean Battleinf/<DIe>c__Iterator2::<>m__0()
extern "C"  bool U3CDIeU3Ec__Iterator2_U3CU3Em__0_m911504022 (U3CDIeU3Ec__Iterator2_t3004093797 * __this, const RuntimeMethod* method)
{
	Color_t2555686324  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Battleinf_t1732126188 * L_0 = __this->get_U24this_0();
		Image_t2670269651 * L_1 = L_0->get_black_11();
		Color_t2555686324  L_2 = VirtFuncInvoker0< Color_t2555686324  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_1);
		V_0 = L_2;
		float L_3 = (&V_0)->get_a_3();
		return (bool)((((float)L_3) == ((float)(1.0f)))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Battleinf/<EnemyAttack>c__Iterator0::.ctor()
extern "C"  void U3CEnemyAttackU3Ec__Iterator0__ctor_m3824501192 (U3CEnemyAttackU3Ec__Iterator0_t2884124416 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Battleinf/<EnemyAttack>c__Iterator0::MoveNext()
extern "C"  bool U3CEnemyAttackU3Ec__Iterator0_MoveNext_m1362218421 (U3CEnemyAttackU3Ec__Iterator0_t2884124416 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CEnemyAttackU3Ec__Iterator0_MoveNext_m1362218421_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_006d;
			}
		}
	}
	{
		goto IL_0132;
	}

IL_0021:
	{
		Battleinf_t1732126188 * L_2 = __this->get_U24this_1();
		bool L_3 = L_2->get_enemyAbleToAttack_21();
		if (!L_3)
		{
			goto IL_012b;
		}
	}
	{
		Battleinf_t1732126188 * L_4 = __this->get_U24this_1();
		L_4->set_myTurn_19((bool)1);
		Battleinf_t1732126188 * L_5 = __this->get_U24this_1();
		L_5->set_enemyAbleToAttack_21((bool)0);
		WaitForSeconds_t1699091251 * L_6 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_6, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_6);
		bool L_7 = __this->get_U24disposing_3();
		if (L_7)
		{
			goto IL_0068;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_0068:
	{
		goto IL_0134;
	}

IL_006d:
	{
		Battleinf_t1732126188 * L_8 = __this->get_U24this_1();
		Random_t108471755 * L_9 = L_8->get_rnd_18();
		int32_t L_10 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Random::Next(System.Int32,System.Int32) */, L_9, 1, 5);
		__this->set_U3CrndDMGU3E__1_0((((float)((float)L_10))));
		float L_11 = __this->get_U3CrndDMGU3E__1_0();
		if ((!(((float)L_11) >= ((float)(4.8f)))))
		{
			goto IL_00ab;
		}
	}
	{
		Battleinf_t1732126188 * L_12 = __this->get_U24this_1();
		TMP_Text_t2599618874 * L_13 = L_12->get_attackText_6();
		TMP_Text_set_text_m1216996582(L_13, _stringLiteral1415876783, /*hidden argument*/NULL);
	}

IL_00ab:
	{
		float L_14 = __this->get_U3CrndDMGU3E__1_0();
		if ((!(((float)L_14) <= ((float)(4.0f)))))
		{
			goto IL_00d0;
		}
	}
	{
		Battleinf_t1732126188 * L_15 = __this->get_U24this_1();
		TMP_Text_t2599618874 * L_16 = L_15->get_attackText_6();
		TMP_Text_set_text_m1216996582(L_16, _stringLiteral231011766, /*hidden argument*/NULL);
	}

IL_00d0:
	{
		float L_17 = __this->get_U3CrndDMGU3E__1_0();
		if ((!(((float)L_17) <= ((float)(2.5f)))))
		{
			goto IL_00f5;
		}
	}
	{
		Battleinf_t1732126188 * L_18 = __this->get_U24this_1();
		TMP_Text_t2599618874 * L_19 = L_18->get_attackText_6();
		TMP_Text_set_text_m1216996582(L_19, _stringLiteral1635465982, /*hidden argument*/NULL);
	}

IL_00f5:
	{
		float L_20 = __this->get_U3CrndDMGU3E__1_0();
		if ((!(((float)L_20) <= ((float)(1.5f)))))
		{
			goto IL_011a;
		}
	}
	{
		Battleinf_t1732126188 * L_21 = __this->get_U24this_1();
		TMP_Text_t2599618874 * L_22 = L_21->get_attackText_6();
		TMP_Text_set_text_m1216996582(L_22, _stringLiteral681334598, /*hidden argument*/NULL);
	}

IL_011a:
	{
		Battleinf_t1732126188 * L_23 = __this->get_U24this_1();
		float L_24 = __this->get_U3CrndDMGU3E__1_0();
		Battleinf_EnemyDealDamage_m3350442186(L_23, L_24, /*hidden argument*/NULL);
	}

IL_012b:
	{
		__this->set_U24PC_4((-1));
	}

IL_0132:
	{
		return (bool)0;
	}

IL_0134:
	{
		return (bool)1;
	}
}
// System.Object Battleinf/<EnemyAttack>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CEnemyAttackU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3666862696 (U3CEnemyAttackU3Ec__Iterator0_t2884124416 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object Battleinf/<EnemyAttack>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CEnemyAttackU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1353786215 (U3CEnemyAttackU3Ec__Iterator0_t2884124416 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Void Battleinf/<EnemyAttack>c__Iterator0::Dispose()
extern "C"  void U3CEnemyAttackU3Ec__Iterator0_Dispose_m115427874 (U3CEnemyAttackU3Ec__Iterator0_t2884124416 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void Battleinf/<EnemyAttack>c__Iterator0::Reset()
extern "C"  void U3CEnemyAttackU3Ec__Iterator0_Reset_m2815185348 (U3CEnemyAttackU3Ec__Iterator0_t2884124416 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CEnemyAttackU3Ec__Iterator0_Reset_m2815185348_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CEnemyAttackU3Ec__Iterator0_Reset_m2815185348_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Color::.ctor()
extern "C"  void Color__ctor_m3372396398 (Color_t2591083401 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Color::Start()
extern "C"  void Color_Start_m1154564560 (Color_t2591083401 * __this, const RuntimeMethod* method)
{
	{
		Color32_t2600501292  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color32__ctor_m4150508762((&L_0), (uint8_t)((int32_t)20), (uint8_t)((int32_t)229), (uint8_t)((int32_t)106), (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		__this->set_col_6(L_0);
		return;
	}
}
// System.Void Color::Update()
extern "C"  void Color_Update_m3570421106 (Color_t2591083401 * __this, const RuntimeMethod* method)
{
	{
		Slider_t3903728902 * L_0 = __this->get_slider_4();
		float L_1 = VirtFuncInvoker0< float >::Invoke(46 /* System.Single UnityEngine.UI.Slider::get_value() */, L_0);
		__this->set_val_7(L_1);
		Slider_t3903728902 * L_2 = __this->get_slider_4();
		float L_3 = VirtFuncInvoker0< float >::Invoke(46 /* System.Single UnityEngine.UI.Slider::get_value() */, L_2);
		if ((!(((float)L_3) <= ((float)(1.0f)))))
		{
			goto IL_0055;
		}
	}
	{
		Color32_t2600501292  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Color32__ctor_m4150508762((&L_4), (uint8_t)((int32_t)20), (uint8_t)((int32_t)229), (uint8_t)((int32_t)106), (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		__this->set_col_6(L_4);
		Image_t2670269651 * L_5 = __this->get_img_5();
		Color32_t2600501292  L_6 = __this->get_col_6();
		Color_t2555686324  L_7 = Color32_op_Implicit_m213813866(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_5, L_7);
	}

IL_0055:
	{
		Slider_t3903728902 * L_8 = __this->get_slider_4();
		float L_9 = VirtFuncInvoker0< float >::Invoke(46 /* System.Single UnityEngine.UI.Slider::get_value() */, L_8);
		if ((!(((float)L_9) <= ((float)(0.4f)))))
		{
			goto IL_009c;
		}
	}
	{
		Color32_t2600501292  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Color32__ctor_m4150508762((&L_10), (uint8_t)((int32_t)219), (uint8_t)((int32_t)209), (uint8_t)((int32_t)73), (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		__this->set_col_6(L_10);
		Image_t2670269651 * L_11 = __this->get_img_5();
		Color32_t2600501292  L_12 = __this->get_col_6();
		Color_t2555686324  L_13 = Color32_op_Implicit_m213813866(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_11, L_13);
	}

IL_009c:
	{
		Slider_t3903728902 * L_14 = __this->get_slider_4();
		float L_15 = VirtFuncInvoker0< float >::Invoke(46 /* System.Single UnityEngine.UI.Slider::get_value() */, L_14);
		if ((!(((float)L_15) <= ((float)(0.2f)))))
		{
			goto IL_00e0;
		}
	}
	{
		Color32_t2600501292  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Color32__ctor_m4150508762((&L_16), (uint8_t)((int32_t)205), (uint8_t)((int32_t)32), (uint8_t)((int32_t)34), (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		__this->set_col_6(L_16);
		Image_t2670269651 * L_17 = __this->get_img_5();
		Color32_t2600501292  L_18 = __this->get_col_6();
		Color_t2555686324  L_19 = Color32_op_Implicit_m213813866(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_17, L_19);
	}

IL_00e0:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Colorimp::.ctor()
extern "C"  void Colorimp__ctor_m328385337 (Colorimp_t1969227374 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Colorimp::Start()
extern "C"  void Colorimp_Start_m1455244163 (Colorimp_t1969227374 * __this, const RuntimeMethod* method)
{
	{
		Color32_t2600501292  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color32__ctor_m4150508762((&L_0), (uint8_t)((int32_t)20), (uint8_t)((int32_t)229), (uint8_t)((int32_t)106), (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		__this->set_col_6(L_0);
		return;
	}
}
// System.Void Colorimp::Update()
extern "C"  void Colorimp_Update_m1322858297 (Colorimp_t1969227374 * __this, const RuntimeMethod* method)
{
	{
		Slider_t3903728902 * L_0 = __this->get_slider_4();
		float L_1 = VirtFuncInvoker0< float >::Invoke(46 /* System.Single UnityEngine.UI.Slider::get_value() */, L_0);
		__this->set_val_7(L_1);
		Slider_t3903728902 * L_2 = __this->get_slider_4();
		float L_3 = VirtFuncInvoker0< float >::Invoke(46 /* System.Single UnityEngine.UI.Slider::get_value() */, L_2);
		if ((!(((float)L_3) <= ((float)(1.0f)))))
		{
			goto IL_0055;
		}
	}
	{
		Color32_t2600501292  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Color32__ctor_m4150508762((&L_4), (uint8_t)((int32_t)20), (uint8_t)((int32_t)229), (uint8_t)((int32_t)106), (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		__this->set_col_6(L_4);
		Image_t2670269651 * L_5 = __this->get_img_5();
		Color32_t2600501292  L_6 = __this->get_col_6();
		Color_t2555686324  L_7 = Color32_op_Implicit_m213813866(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_5, L_7);
	}

IL_0055:
	{
		Slider_t3903728902 * L_8 = __this->get_slider_4();
		float L_9 = VirtFuncInvoker0< float >::Invoke(46 /* System.Single UnityEngine.UI.Slider::get_value() */, L_8);
		if ((!(((float)L_9) <= ((float)(0.4f)))))
		{
			goto IL_009c;
		}
	}
	{
		Color32_t2600501292  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Color32__ctor_m4150508762((&L_10), (uint8_t)((int32_t)219), (uint8_t)((int32_t)209), (uint8_t)((int32_t)73), (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		__this->set_col_6(L_10);
		Image_t2670269651 * L_11 = __this->get_img_5();
		Color32_t2600501292  L_12 = __this->get_col_6();
		Color_t2555686324  L_13 = Color32_op_Implicit_m213813866(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_11, L_13);
	}

IL_009c:
	{
		Slider_t3903728902 * L_14 = __this->get_slider_4();
		float L_15 = VirtFuncInvoker0< float >::Invoke(46 /* System.Single UnityEngine.UI.Slider::get_value() */, L_14);
		if ((!(((float)L_15) <= ((float)(0.2f)))))
		{
			goto IL_00e0;
		}
	}
	{
		Color32_t2600501292  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Color32__ctor_m4150508762((&L_16), (uint8_t)((int32_t)205), (uint8_t)((int32_t)32), (uint8_t)((int32_t)34), (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		__this->set_col_6(L_16);
		Image_t2670269651 * L_17 = __this->get_img_5();
		Color32_t2600501292  L_18 = __this->get_col_6();
		Color_t2555686324  L_19 = Color32_op_Implicit_m213813866(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_17, L_19);
	}

IL_00e0:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Colorinf::.ctor()
extern "C"  void Colorinf__ctor_m3318868928 (Colorinf_t4284197021 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Colorinf::Start()
extern "C"  void Colorinf_Start_m1389479124 (Colorinf_t4284197021 * __this, const RuntimeMethod* method)
{
	{
		Color32_t2600501292  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color32__ctor_m4150508762((&L_0), (uint8_t)((int32_t)20), (uint8_t)((int32_t)229), (uint8_t)((int32_t)106), (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		__this->set_col_6(L_0);
		return;
	}
}
// System.Void Colorinf::Update()
extern "C"  void Colorinf_Update_m333234786 (Colorinf_t4284197021 * __this, const RuntimeMethod* method)
{
	{
		Slider_t3903728902 * L_0 = __this->get_slider_4();
		float L_1 = VirtFuncInvoker0< float >::Invoke(46 /* System.Single UnityEngine.UI.Slider::get_value() */, L_0);
		__this->set_val_7(L_1);
		Slider_t3903728902 * L_2 = __this->get_slider_4();
		float L_3 = VirtFuncInvoker0< float >::Invoke(46 /* System.Single UnityEngine.UI.Slider::get_value() */, L_2);
		if ((!(((float)L_3) <= ((float)(1.0f)))))
		{
			goto IL_0055;
		}
	}
	{
		Color32_t2600501292  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Color32__ctor_m4150508762((&L_4), (uint8_t)((int32_t)20), (uint8_t)((int32_t)229), (uint8_t)((int32_t)106), (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		__this->set_col_6(L_4);
		Image_t2670269651 * L_5 = __this->get_img_5();
		Color32_t2600501292  L_6 = __this->get_col_6();
		Color_t2555686324  L_7 = Color32_op_Implicit_m213813866(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_5, L_7);
	}

IL_0055:
	{
		Slider_t3903728902 * L_8 = __this->get_slider_4();
		float L_9 = VirtFuncInvoker0< float >::Invoke(46 /* System.Single UnityEngine.UI.Slider::get_value() */, L_8);
		if ((!(((float)L_9) <= ((float)(0.4f)))))
		{
			goto IL_009c;
		}
	}
	{
		Color32_t2600501292  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Color32__ctor_m4150508762((&L_10), (uint8_t)((int32_t)219), (uint8_t)((int32_t)209), (uint8_t)((int32_t)73), (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		__this->set_col_6(L_10);
		Image_t2670269651 * L_11 = __this->get_img_5();
		Color32_t2600501292  L_12 = __this->get_col_6();
		Color_t2555686324  L_13 = Color32_op_Implicit_m213813866(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_11, L_13);
	}

IL_009c:
	{
		Slider_t3903728902 * L_14 = __this->get_slider_4();
		float L_15 = VirtFuncInvoker0< float >::Invoke(46 /* System.Single UnityEngine.UI.Slider::get_value() */, L_14);
		if ((!(((float)L_15) <= ((float)(0.2f)))))
		{
			goto IL_00e0;
		}
	}
	{
		Color32_t2600501292  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Color32__ctor_m4150508762((&L_16), (uint8_t)((int32_t)205), (uint8_t)((int32_t)32), (uint8_t)((int32_t)34), (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		__this->set_col_6(L_16);
		Image_t2670269651 * L_17 = __this->get_img_5();
		Color32_t2600501292  L_18 = __this->get_col_6();
		Color_t2555686324  L_19 = Color32_op_Implicit_m213813866(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_17, L_19);
	}

IL_00e0:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Harmony.BootSeq::.ctor()
extern "C"  void BootSeq__ctor_m3090604233 (BootSeq_t2506222928 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Harmony.BootSeq::.cctor()
extern "C"  void BootSeq__cctor_m79868158 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BootSeq__cctor_m79868158_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((BootSeq_t2506222928_StaticFields*)il2cpp_codegen_static_fields_for(BootSeq_t2506222928_il2cpp_TypeInfo_var))->set_BootSequence_4(_stringLiteral758536563);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Harmony.Difficulty::.ctor()
extern "C"  void Difficulty__ctor_m821776643 (Difficulty_t3960432064 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Harmony.Difficulty::.cctor()
extern "C"  void Difficulty__cctor_m919047636 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Difficulty__cctor_m919047636_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Difficulty_t3960432064_StaticFields*)il2cpp_codegen_static_fields_for(Difficulty_t3960432064_il2cpp_TypeInfo_var))->set_a_4(_stringLiteral626144933);
		((Difficulty_t3960432064_StaticFields*)il2cpp_codegen_static_fields_for(Difficulty_t3960432064_il2cpp_TypeInfo_var))->set_b_5(_stringLiteral3225350139);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Harmony.Doggo::.ctor()
extern "C"  void Doggo__ctor_m3065458288 (Doggo_t4070438711 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Harmony.Doggo::.cctor()
extern "C"  void Doggo__cctor_m183777946 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Doggo__cctor_m183777946_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Doggo_t4070438711_StaticFields*)il2cpp_codegen_static_fields_for(Doggo_t4070438711_il2cpp_TypeInfo_var))->set_a_4(_stringLiteral1830413752);
		((Doggo_t4070438711_StaticFields*)il2cpp_codegen_static_fields_for(Doggo_t4070438711_il2cpp_TypeInfo_var))->set_b_5(_stringLiteral2710407981);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Harmony.Input::.ctor()
extern "C"  void Input__ctor_m601865817 (Input_t3716682282 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Harmony.Input::.cctor()
extern "C"  void Input__cctor_m615191921 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Input__cctor_m615191921_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Input_t3716682282_StaticFields*)il2cpp_codegen_static_fields_for(Input_t3716682282_il2cpp_TypeInfo_var))->set_a_4(_stringLiteral640182759);
		((Input_t3716682282_StaticFields*)il2cpp_codegen_static_fields_for(Input_t3716682282_il2cpp_TypeInfo_var))->set_b_5(_stringLiteral3063217984);
		((Input_t3716682282_StaticFields*)il2cpp_codegen_static_fields_for(Input_t3716682282_il2cpp_TypeInfo_var))->set_c_6(_stringLiteral1168549832);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Harmony.Program::.ctor()
extern "C"  void Program__ctor_m1934142545 (Program_t1866495201 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Harmony.Program::Start()
extern "C"  void Program_Start_m3266403965 (Program_t1866495201 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = Program_Loading_m47061019(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Harmony.Program::Update()
extern "C"  void Program_Update_m4123468737 (Program_t1866495201 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Program_Update_m4123468737_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, ((int32_t)27), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		SceneManager_LoadScene_m3463216446(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Collections.IEnumerator Harmony.Program::Loading()
extern "C"  RuntimeObject* Program_Loading_m47061019 (Program_t1866495201 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Program_Loading_m47061019_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CLoadingU3Ec__Iterator0_t3071600265 * V_0 = NULL;
	{
		U3CLoadingU3Ec__Iterator0_t3071600265 * L_0 = (U3CLoadingU3Ec__Iterator0_t3071600265 *)il2cpp_codegen_object_new(U3CLoadingU3Ec__Iterator0_t3071600265_il2cpp_TypeInfo_var);
		U3CLoadingU3Ec__Iterator0__ctor_m2551927464(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoadingU3Ec__Iterator0_t3071600265 * L_1 = V_0;
		L_1->set_U24this_0(__this);
		U3CLoadingU3Ec__Iterator0_t3071600265 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator Harmony.Program::PlayBootSFX()
extern "C"  RuntimeObject* Program_PlayBootSFX_m781471345 (Program_t1866495201 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Program_PlayBootSFX_m781471345_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CPlayBootSFXU3Ec__Iterator1_t3195710289 * V_0 = NULL;
	{
		U3CPlayBootSFXU3Ec__Iterator1_t3195710289 * L_0 = (U3CPlayBootSFXU3Ec__Iterator1_t3195710289 *)il2cpp_codegen_object_new(U3CPlayBootSFXU3Ec__Iterator1_t3195710289_il2cpp_TypeInfo_var);
		U3CPlayBootSFXU3Ec__Iterator1__ctor_m3619584200(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPlayBootSFXU3Ec__Iterator1_t3195710289 * L_1 = V_0;
		L_1->set_U24this_0(__this);
		U3CPlayBootSFXU3Ec__Iterator1_t3195710289 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Harmony.Program::.cctor()
extern "C"  void Program__cctor_m4132216779 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Program__cctor_m4132216779_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Program_t1866495201_StaticFields*)il2cpp_codegen_static_fields_for(Program_t1866495201_il2cpp_TypeInfo_var))->set_normTypeSpeed_7((0.03f));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Harmony.Program/<Loading>c__Iterator0::.ctor()
extern "C"  void U3CLoadingU3Ec__Iterator0__ctor_m2551927464 (U3CLoadingU3Ec__Iterator0_t3071600265 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Harmony.Program/<Loading>c__Iterator0::MoveNext()
extern "C"  bool U3CLoadingU3Ec__Iterator0_MoveNext_m3161337628 (U3CLoadingU3Ec__Iterator0_t3071600265 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadingU3Ec__Iterator0_MoveNext_m3161337628_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0079;
			}
			case 1:
			{
				goto IL_009d;
			}
			case 2:
			{
				goto IL_00f7;
			}
			case 3:
			{
				goto IL_0151;
			}
			case 4:
			{
				goto IL_018a;
			}
			case 5:
			{
				goto IL_0201;
			}
			case 6:
			{
				goto IL_025b;
			}
			case 7:
			{
				goto IL_0294;
			}
			case 8:
			{
				goto IL_030b;
			}
			case 9:
			{
				goto IL_0366;
			}
			case 10:
			{
				goto IL_03a0;
			}
			case 11:
			{
				goto IL_040f;
			}
			case 12:
			{
				goto IL_0449;
			}
			case 13:
			{
				goto IL_0483;
			}
			case 14:
			{
				goto IL_04bd;
			}
			case 15:
			{
				goto IL_04f7;
			}
			case 16:
			{
				goto IL_0531;
			}
			case 17:
			{
				goto IL_056b;
			}
			case 18:
			{
				goto IL_05a5;
			}
			case 19:
			{
				goto IL_05df;
			}
			case 20:
			{
				goto IL_0619;
			}
			case 21:
			{
				goto IL_0653;
			}
			case 22:
			{
				goto IL_068d;
			}
			case 23:
			{
				goto IL_071a;
			}
		}
	}
	{
		goto IL_0737;
	}

IL_0079:
	{
		WaitForSeconds_t1699091251 * L_2 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_2, (1.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		bool L_3 = __this->get_U24disposing_2();
		if (L_3)
		{
			goto IL_0098;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0098:
	{
		goto IL_0739;
	}

IL_009d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(BootSeq_t2506222928_il2cpp_TypeInfo_var);
		String_t* L_4 = ((BootSeq_t2506222928_StaticFields*)il2cpp_codegen_static_fields_for(BootSeq_t2506222928_il2cpp_TypeInfo_var))->get_BootSequence_4();
		Program_t1866495201 * L_5 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_6 = L_5->get_startText_9();
		Type_TypeText_m4150723822(NULL /*static, unused*/, L_4, L_6, (0.001f), /*hidden argument*/NULL);
		Program_t1866495201 * L_7 = __this->get_U24this_0();
		Program_t1866495201 * L_8 = __this->get_U24this_0();
		RuntimeObject* L_9 = Program_PlayBootSFX_m781471345(L_8, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(L_7, L_9, /*hidden argument*/NULL);
		goto IL_00f7;
	}

IL_00d3:
	{
		WaitForSeconds_t1699091251 * L_10 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_10, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_10);
		bool L_11 = __this->get_U24disposing_2();
		if (L_11)
		{
			goto IL_00f2;
		}
	}
	{
		__this->set_U24PC_3(2);
	}

IL_00f2:
	{
		goto IL_0739;
	}

IL_00f7:
	{
		Program_t1866495201 * L_12 = __this->get_U24this_0();
		bool L_13 = L_12->get_inCoroutine_8();
		if (L_13)
		{
			goto IL_00d3;
		}
	}
	{
		Program_t1866495201 * L_14 = __this->get_U24this_0();
		L_14->set_i_10(0);
		goto IL_019d;
	}

IL_0118:
	{
		Program_t1866495201 * L_15 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_16 = L_15->get_startText_9();
		TMP_Text_set_text_m1216996582(L_16, _stringLiteral1089347067, /*hidden argument*/NULL);
		WaitForSeconds_t1699091251 * L_17 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_17, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_17);
		bool L_18 = __this->get_U24disposing_2();
		if (L_18)
		{
			goto IL_014c;
		}
	}
	{
		__this->set_U24PC_3(3);
	}

IL_014c:
	{
		goto IL_0739;
	}

IL_0151:
	{
		Program_t1866495201 * L_19 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_20 = L_19->get_startText_9();
		TMP_Text_set_text_m1216996582(L_20, _stringLiteral2277005576, /*hidden argument*/NULL);
		WaitForSeconds_t1699091251 * L_21 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_21, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_21);
		bool L_22 = __this->get_U24disposing_2();
		if (L_22)
		{
			goto IL_0185;
		}
	}
	{
		__this->set_U24PC_3(4);
	}

IL_0185:
	{
		goto IL_0739;
	}

IL_018a:
	{
		Program_t1866495201 * L_23 = __this->get_U24this_0();
		Program_t1866495201 * L_24 = L_23;
		int32_t L_25 = L_24->get_i_10();
		L_24->set_i_10(((int32_t)il2cpp_codegen_add((int32_t)L_25, (int32_t)1)));
	}

IL_019d:
	{
		Program_t1866495201 * L_26 = __this->get_U24this_0();
		int32_t L_27 = L_26->get_i_10();
		if ((((int32_t)L_27) <= ((int32_t)3)))
		{
			goto IL_0118;
		}
	}
	{
		Program_t1866495201 * L_28 = __this->get_U24this_0();
		AudioSource_t3935305588 * L_29 = L_28->get_messageSFX_5();
		AudioSource_Play_m48294159(L_29, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t3716682282_il2cpp_TypeInfo_var);
		String_t* L_30 = ((Input_t3716682282_StaticFields*)il2cpp_codegen_static_fields_for(Input_t3716682282_il2cpp_TypeInfo_var))->get_a_4();
		Program_t1866495201 * L_31 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_32 = L_31->get_startText_9();
		IL2CPP_RUNTIME_CLASS_INIT(Program_t1866495201_il2cpp_TypeInfo_var);
		float L_33 = ((Program_t1866495201_StaticFields*)il2cpp_codegen_static_fields_for(Program_t1866495201_il2cpp_TypeInfo_var))->get_normTypeSpeed_7();
		Type_TypeText_m4150723822(NULL /*static, unused*/, L_30, L_32, L_33, /*hidden argument*/NULL);
		goto IL_0201;
	}

IL_01dd:
	{
		WaitForSeconds_t1699091251 * L_34 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_34, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_34);
		bool L_35 = __this->get_U24disposing_2();
		if (L_35)
		{
			goto IL_01fc;
		}
	}
	{
		__this->set_U24PC_3(5);
	}

IL_01fc:
	{
		goto IL_0739;
	}

IL_0201:
	{
		Program_t1866495201 * L_36 = __this->get_U24this_0();
		bool L_37 = L_36->get_inCoroutine_8();
		if (L_37)
		{
			goto IL_01dd;
		}
	}
	{
		Program_t1866495201 * L_38 = __this->get_U24this_0();
		L_38->set_i_10(0);
		goto IL_02a7;
	}

IL_0222:
	{
		Program_t1866495201 * L_39 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_40 = L_39->get_startText_9();
		TMP_Text_set_text_m1216996582(L_40, _stringLiteral1462988390, /*hidden argument*/NULL);
		WaitForSeconds_t1699091251 * L_41 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_41, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_41);
		bool L_42 = __this->get_U24disposing_2();
		if (L_42)
		{
			goto IL_0256;
		}
	}
	{
		__this->set_U24PC_3(6);
	}

IL_0256:
	{
		goto IL_0739;
	}

IL_025b:
	{
		Program_t1866495201 * L_43 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_44 = L_43->get_startText_9();
		TMP_Text_set_text_m1216996582(L_44, _stringLiteral436365799, /*hidden argument*/NULL);
		WaitForSeconds_t1699091251 * L_45 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_45, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_45);
		bool L_46 = __this->get_U24disposing_2();
		if (L_46)
		{
			goto IL_028f;
		}
	}
	{
		__this->set_U24PC_3(7);
	}

IL_028f:
	{
		goto IL_0739;
	}

IL_0294:
	{
		Program_t1866495201 * L_47 = __this->get_U24this_0();
		Program_t1866495201 * L_48 = L_47;
		int32_t L_49 = L_48->get_i_10();
		L_48->set_i_10(((int32_t)il2cpp_codegen_add((int32_t)L_49, (int32_t)1)));
	}

IL_02a7:
	{
		Program_t1866495201 * L_50 = __this->get_U24this_0();
		int32_t L_51 = L_50->get_i_10();
		if ((((int32_t)L_51) <= ((int32_t)5)))
		{
			goto IL_0222;
		}
	}
	{
		Program_t1866495201 * L_52 = __this->get_U24this_0();
		AudioSource_t3935305588 * L_53 = L_52->get_messageSFX_5();
		AudioSource_Play_m48294159(L_53, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t3716682282_il2cpp_TypeInfo_var);
		String_t* L_54 = ((Input_t3716682282_StaticFields*)il2cpp_codegen_static_fields_for(Input_t3716682282_il2cpp_TypeInfo_var))->get_b_5();
		Program_t1866495201 * L_55 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_56 = L_55->get_startText_9();
		IL2CPP_RUNTIME_CLASS_INIT(Program_t1866495201_il2cpp_TypeInfo_var);
		float L_57 = ((Program_t1866495201_StaticFields*)il2cpp_codegen_static_fields_for(Program_t1866495201_il2cpp_TypeInfo_var))->get_normTypeSpeed_7();
		Type_TypeText_m4150723822(NULL /*static, unused*/, L_54, L_56, L_57, /*hidden argument*/NULL);
		goto IL_030b;
	}

IL_02e7:
	{
		WaitForSeconds_t1699091251 * L_58 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_58, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_58);
		bool L_59 = __this->get_U24disposing_2();
		if (L_59)
		{
			goto IL_0306;
		}
	}
	{
		__this->set_U24PC_3(8);
	}

IL_0306:
	{
		goto IL_0739;
	}

IL_030b:
	{
		Program_t1866495201 * L_60 = __this->get_U24this_0();
		bool L_61 = L_60->get_inCoroutine_8();
		if (L_61)
		{
			goto IL_02e7;
		}
	}
	{
		Program_t1866495201 * L_62 = __this->get_U24this_0();
		L_62->set_i_10(0);
		goto IL_03b3;
	}

IL_032c:
	{
		Program_t1866495201 * L_63 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_64 = L_63->get_startText_9();
		TMP_Text_set_text_m1216996582(L_64, _stringLiteral3017060061, /*hidden argument*/NULL);
		WaitForSeconds_t1699091251 * L_65 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_65, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_65);
		bool L_66 = __this->get_U24disposing_2();
		if (L_66)
		{
			goto IL_0361;
		}
	}
	{
		__this->set_U24PC_3(((int32_t)9));
	}

IL_0361:
	{
		goto IL_0739;
	}

IL_0366:
	{
		Program_t1866495201 * L_67 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_68 = L_67->get_startText_9();
		TMP_Text_set_text_m1216996582(L_68, _stringLiteral3021844189, /*hidden argument*/NULL);
		WaitForSeconds_t1699091251 * L_69 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_69, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_69);
		bool L_70 = __this->get_U24disposing_2();
		if (L_70)
		{
			goto IL_039b;
		}
	}
	{
		__this->set_U24PC_3(((int32_t)10));
	}

IL_039b:
	{
		goto IL_0739;
	}

IL_03a0:
	{
		Program_t1866495201 * L_71 = __this->get_U24this_0();
		Program_t1866495201 * L_72 = L_71;
		int32_t L_73 = L_72->get_i_10();
		L_72->set_i_10(((int32_t)il2cpp_codegen_add((int32_t)L_73, (int32_t)1)));
	}

IL_03b3:
	{
		Program_t1866495201 * L_74 = __this->get_U24this_0();
		int32_t L_75 = L_74->get_i_10();
		if ((((int32_t)L_75) <= ((int32_t)7)))
		{
			goto IL_032c;
		}
	}
	{
		Program_t1866495201 * L_76 = __this->get_U24this_0();
		L_76->set_i_10(0);
		goto IL_06b5;
	}

IL_03d5:
	{
		Program_t1866495201 * L_77 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_78 = L_77->get_startText_9();
		TMP_Text_set_text_m1216996582(L_78, _stringLiteral3015749341, /*hidden argument*/NULL);
		WaitForSeconds_t1699091251 * L_79 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_79, (0.3f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_79);
		bool L_80 = __this->get_U24disposing_2();
		if (L_80)
		{
			goto IL_040a;
		}
	}
	{
		__this->set_U24PC_3(((int32_t)11));
	}

IL_040a:
	{
		goto IL_0739;
	}

IL_040f:
	{
		Program_t1866495201 * L_81 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_82 = L_81->get_startText_9();
		TMP_Text_set_text_m1216996582(L_82, _stringLiteral1301065091, /*hidden argument*/NULL);
		WaitForSeconds_t1699091251 * L_83 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_83, (0.3f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_83);
		bool L_84 = __this->get_U24disposing_2();
		if (L_84)
		{
			goto IL_0444;
		}
	}
	{
		__this->set_U24PC_3(((int32_t)12));
	}

IL_0444:
	{
		goto IL_0739;
	}

IL_0449:
	{
		Program_t1866495201 * L_85 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_86 = L_85->get_startText_9();
		TMP_Text_set_text_m1216996582(L_86, _stringLiteral2085072259, /*hidden argument*/NULL);
		WaitForSeconds_t1699091251 * L_87 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_87, (0.3f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_87);
		bool L_88 = __this->get_U24disposing_2();
		if (L_88)
		{
			goto IL_047e;
		}
	}
	{
		__this->set_U24PC_3(((int32_t)13));
	}

IL_047e:
	{
		goto IL_0739;
	}

IL_0483:
	{
		Program_t1866495201 * L_89 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_90 = L_89->get_startText_9();
		TMP_Text_set_text_m1216996582(L_90, _stringLiteral524701805, /*hidden argument*/NULL);
		WaitForSeconds_t1699091251 * L_91 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_91, (0.3f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_91);
		bool L_92 = __this->get_U24disposing_2();
		if (L_92)
		{
			goto IL_04b8;
		}
	}
	{
		__this->set_U24PC_3(((int32_t)14));
	}

IL_04b8:
	{
		goto IL_0739;
	}

IL_04bd:
	{
		Program_t1866495201 * L_93 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_94 = L_93->get_startText_9();
		TMP_Text_set_text_m1216996582(L_94, _stringLiteral2085072259, /*hidden argument*/NULL);
		WaitForSeconds_t1699091251 * L_95 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_95, (0.3f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_95);
		bool L_96 = __this->get_U24disposing_2();
		if (L_96)
		{
			goto IL_04f2;
		}
	}
	{
		__this->set_U24PC_3(((int32_t)15));
	}

IL_04f2:
	{
		goto IL_0739;
	}

IL_04f7:
	{
		Program_t1866495201 * L_97 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_98 = L_97->get_startText_9();
		TMP_Text_set_text_m1216996582(L_98, _stringLiteral1301065091, /*hidden argument*/NULL);
		WaitForSeconds_t1699091251 * L_99 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_99, (0.3f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_99);
		bool L_100 = __this->get_U24disposing_2();
		if (L_100)
		{
			goto IL_052c;
		}
	}
	{
		__this->set_U24PC_3(((int32_t)16));
	}

IL_052c:
	{
		goto IL_0739;
	}

IL_0531:
	{
		Program_t1866495201 * L_101 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_102 = L_101->get_startText_9();
		TMP_Text_set_text_m1216996582(L_102, _stringLiteral1320398211, /*hidden argument*/NULL);
		WaitForSeconds_t1699091251 * L_103 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_103, (0.3f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_103);
		bool L_104 = __this->get_U24disposing_2();
		if (L_104)
		{
			goto IL_0566;
		}
	}
	{
		__this->set_U24PC_3(((int32_t)17));
	}

IL_0566:
	{
		goto IL_0739;
	}

IL_056b:
	{
		Program_t1866495201 * L_105 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_106 = L_105->get_startText_9();
		TMP_Text_set_text_m1216996582(L_106, _stringLiteral4054995049, /*hidden argument*/NULL);
		WaitForSeconds_t1699091251 * L_107 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_107, (0.3f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_107);
		bool L_108 = __this->get_U24disposing_2();
		if (L_108)
		{
			goto IL_05a0;
		}
	}
	{
		__this->set_U24PC_3(((int32_t)18));
	}

IL_05a0:
	{
		goto IL_0739;
	}

IL_05a5:
	{
		Program_t1866495201 * L_109 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_110 = L_109->get_startText_9();
		TMP_Text_set_text_m1216996582(L_110, _stringLiteral4060106857, /*hidden argument*/NULL);
		WaitForSeconds_t1699091251 * L_111 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_111, (0.3f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_111);
		bool L_112 = __this->get_U24disposing_2();
		if (L_112)
		{
			goto IL_05da;
		}
	}
	{
		__this->set_U24PC_3(((int32_t)19));
	}

IL_05da:
	{
		goto IL_0739;
	}

IL_05df:
	{
		Program_t1866495201 * L_113 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_114 = L_113->get_startText_9();
		TMP_Text_set_text_m1216996582(L_114, _stringLiteral3457922962, /*hidden argument*/NULL);
		WaitForSeconds_t1699091251 * L_115 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_115, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_115);
		bool L_116 = __this->get_U24disposing_2();
		if (L_116)
		{
			goto IL_0614;
		}
	}
	{
		__this->set_U24PC_3(((int32_t)20));
	}

IL_0614:
	{
		goto IL_0739;
	}

IL_0619:
	{
		Program_t1866495201 * L_117 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_118 = L_117->get_startText_9();
		TMP_Text_set_text_m1216996582(L_118, _stringLiteral3452614546, /*hidden argument*/NULL);
		WaitForSeconds_t1699091251 * L_119 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_119, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_119);
		bool L_120 = __this->get_U24disposing_2();
		if (L_120)
		{
			goto IL_064e;
		}
	}
	{
		__this->set_U24PC_3(((int32_t)21));
	}

IL_064e:
	{
		goto IL_0739;
	}

IL_0653:
	{
		Program_t1866495201 * L_121 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_122 = L_121->get_startText_9();
		TMP_Text_set_text_m1216996582(L_122, _stringLiteral3457922962, /*hidden argument*/NULL);
		WaitForSeconds_t1699091251 * L_123 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_123, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_123);
		bool L_124 = __this->get_U24disposing_2();
		if (L_124)
		{
			goto IL_0688;
		}
	}
	{
		__this->set_U24PC_3(((int32_t)22));
	}

IL_0688:
	{
		goto IL_0739;
	}

IL_068d:
	{
		Program_t1866495201 * L_125 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_126 = L_125->get_startText_9();
		TMP_Text_set_text_m1216996582(L_126, _stringLiteral3452614546, /*hidden argument*/NULL);
		Program_t1866495201 * L_127 = __this->get_U24this_0();
		Program_t1866495201 * L_128 = L_127;
		int32_t L_129 = L_128->get_i_10();
		L_128->set_i_10(((int32_t)il2cpp_codegen_add((int32_t)L_129, (int32_t)1)));
	}

IL_06b5:
	{
		Program_t1866495201 * L_130 = __this->get_U24this_0();
		int32_t L_131 = L_130->get_i_10();
		if ((((int32_t)L_131) < ((int32_t)1)))
		{
			goto IL_03d5;
		}
	}
	{
		Program_t1866495201 * L_132 = __this->get_U24this_0();
		AudioSource_t3935305588 * L_133 = L_132->get_leetSFX_6();
		AudioSource_Play_m48294159(L_133, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t3716682282_il2cpp_TypeInfo_var);
		String_t* L_134 = ((Input_t3716682282_StaticFields*)il2cpp_codegen_static_fields_for(Input_t3716682282_il2cpp_TypeInfo_var))->get_c_6();
		Program_t1866495201 * L_135 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_136 = L_135->get_startText_9();
		Type_TypeText_m4150723822(NULL /*static, unused*/, L_134, L_136, (0.5f), /*hidden argument*/NULL);
		goto IL_071a;
	}

IL_06f5:
	{
		WaitForSeconds_t1699091251 * L_137 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_137, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_137);
		bool L_138 = __this->get_U24disposing_2();
		if (L_138)
		{
			goto IL_0715;
		}
	}
	{
		__this->set_U24PC_3(((int32_t)23));
	}

IL_0715:
	{
		goto IL_0739;
	}

IL_071a:
	{
		Program_t1866495201 * L_139 = __this->get_U24this_0();
		bool L_140 = L_139->get_inCoroutine_8();
		if (L_140)
		{
			goto IL_06f5;
		}
	}
	{
		SceneManager_LoadScene_m3463216446(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_0737:
	{
		return (bool)0;
	}

IL_0739:
	{
		return (bool)1;
	}
}
// System.Object Harmony.Program/<Loading>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CLoadingU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4171897058 (U3CLoadingU3Ec__Iterator0_t3071600265 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object Harmony.Program/<Loading>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CLoadingU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m634181839 (U3CLoadingU3Ec__Iterator0_t3071600265 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void Harmony.Program/<Loading>c__Iterator0::Dispose()
extern "C"  void U3CLoadingU3Ec__Iterator0_Dispose_m2277303762 (U3CLoadingU3Ec__Iterator0_t3071600265 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void Harmony.Program/<Loading>c__Iterator0::Reset()
extern "C"  void U3CLoadingU3Ec__Iterator0_Reset_m423204432 (U3CLoadingU3Ec__Iterator0_t3071600265 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadingU3Ec__Iterator0_Reset_m423204432_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CLoadingU3Ec__Iterator0_Reset_m423204432_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Harmony.Program/<PlayBootSFX>c__Iterator1::.ctor()
extern "C"  void U3CPlayBootSFXU3Ec__Iterator1__ctor_m3619584200 (U3CPlayBootSFXU3Ec__Iterator1_t3195710289 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Harmony.Program/<PlayBootSFX>c__Iterator1::MoveNext()
extern "C"  bool U3CPlayBootSFXU3Ec__Iterator1_MoveNext_m2963412815 (U3CPlayBootSFXU3Ec__Iterator1_t3195710289 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPlayBootSFXU3Ec__Iterator1_MoveNext_m2963412815_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0045;
			}
		}
	}
	{
		goto IL_005c;
	}

IL_0021:
	{
		WaitForSeconds_t1699091251 * L_2 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_2, (14.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		bool L_3 = __this->get_U24disposing_2();
		if (L_3)
		{
			goto IL_0040;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0040:
	{
		goto IL_005e;
	}

IL_0045:
	{
		Program_t1866495201 * L_4 = __this->get_U24this_0();
		AudioSource_t3935305588 * L_5 = L_4->get_bootSFX_4();
		AudioSource_Play_m48294159(L_5, /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_005c:
	{
		return (bool)0;
	}

IL_005e:
	{
		return (bool)1;
	}
}
// System.Object Harmony.Program/<PlayBootSFX>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CPlayBootSFXU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m881421217 (U3CPlayBootSFXU3Ec__Iterator1_t3195710289 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object Harmony.Program/<PlayBootSFX>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CPlayBootSFXU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1465316736 (U3CPlayBootSFXU3Ec__Iterator1_t3195710289 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void Harmony.Program/<PlayBootSFX>c__Iterator1::Dispose()
extern "C"  void U3CPlayBootSFXU3Ec__Iterator1_Dispose_m4145516049 (U3CPlayBootSFXU3Ec__Iterator1_t3195710289 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void Harmony.Program/<PlayBootSFX>c__Iterator1::Reset()
extern "C"  void U3CPlayBootSFXU3Ec__Iterator1_Reset_m3273131816 (U3CPlayBootSFXU3Ec__Iterator1_t3195710289 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPlayBootSFXU3Ec__Iterator1_Reset_m3273131816_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CPlayBootSFXU3Ec__Iterator1_Reset_m3273131816_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Harmony.Program2::.ctor()
extern "C"  void Program2__ctor_m1621325272 (Program2_t3485103329 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Harmony.Program2::Start()
extern "C"  void Program2_Start_m262601170 (Program2_t3485103329 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = Program2_Loading_m3138989863(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Harmony.Program2::Update()
extern "C"  void Program2_Update_m3502782264 (Program2_t3485103329 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Program2_Update_m3502782264_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, ((int32_t)27), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		SceneManager_LoadScene_m3463216446(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Collections.IEnumerator Harmony.Program2::Loading()
extern "C"  RuntimeObject* Program2_Loading_m3138989863 (Program2_t3485103329 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Program2_Loading_m3138989863_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CLoadingU3Ec__Iterator0_t2822561722 * V_0 = NULL;
	{
		U3CLoadingU3Ec__Iterator0_t2822561722 * L_0 = (U3CLoadingU3Ec__Iterator0_t2822561722 *)il2cpp_codegen_object_new(U3CLoadingU3Ec__Iterator0_t2822561722_il2cpp_TypeInfo_var);
		U3CLoadingU3Ec__Iterator0__ctor_m4231034442(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoadingU3Ec__Iterator0_t2822561722 * L_1 = V_0;
		L_1->set_U24this_0(__this);
		U3CLoadingU3Ec__Iterator0_t2822561722 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Harmony.Program2::.cctor()
extern "C"  void Program2__cctor_m2483671483 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Program2__cctor_m2483671483_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Program2_t3485103329_StaticFields*)il2cpp_codegen_static_fields_for(Program2_t3485103329_il2cpp_TypeInfo_var))->set_normTypeSpeed_6((0.03f));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Harmony.Program2/<Loading>c__Iterator0::.ctor()
extern "C"  void U3CLoadingU3Ec__Iterator0__ctor_m4231034442 (U3CLoadingU3Ec__Iterator0_t2822561722 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Harmony.Program2/<Loading>c__Iterator0::MoveNext()
extern "C"  bool U3CLoadingU3Ec__Iterator0_MoveNext_m4187657732 (U3CLoadingU3Ec__Iterator0_t2822561722 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadingU3Ec__Iterator0_MoveNext_m4187657732_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0031;
			}
			case 1:
			{
				goto IL_007b;
			}
			case 2:
			{
				goto IL_00b4;
			}
			case 3:
			{
				goto IL_012b;
			}
			case 4:
			{
				goto IL_015f;
			}
			case 5:
			{
				goto IL_01b2;
			}
		}
	}
	{
		goto IL_01cf;
	}

IL_0031:
	{
		Program2_t3485103329 * L_2 = __this->get_U24this_0();
		L_2->set_i_9(0);
		goto IL_00c7;
	}

IL_0042:
	{
		Program2_t3485103329 * L_3 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_4 = L_3->get_startText_8();
		TMP_Text_set_text_m1216996582(L_4, _stringLiteral2527013464, /*hidden argument*/NULL);
		WaitForSeconds_t1699091251 * L_5 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_5, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_5);
		bool L_6 = __this->get_U24disposing_2();
		if (L_6)
		{
			goto IL_0076;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0076:
	{
		goto IL_01d1;
	}

IL_007b:
	{
		Program2_t3485103329 * L_7 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_8 = L_7->get_startText_8();
		TMP_Text_set_text_m1216996582(L_8, _stringLiteral2522622552, /*hidden argument*/NULL);
		WaitForSeconds_t1699091251 * L_9 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_9, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_9);
		bool L_10 = __this->get_U24disposing_2();
		if (L_10)
		{
			goto IL_00af;
		}
	}
	{
		__this->set_U24PC_3(2);
	}

IL_00af:
	{
		goto IL_01d1;
	}

IL_00b4:
	{
		Program2_t3485103329 * L_11 = __this->get_U24this_0();
		Program2_t3485103329 * L_12 = L_11;
		int32_t L_13 = L_12->get_i_9();
		L_12->set_i_9(((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1)));
	}

IL_00c7:
	{
		Program2_t3485103329 * L_14 = __this->get_U24this_0();
		int32_t L_15 = L_14->get_i_9();
		if ((((int32_t)L_15) <= ((int32_t)4)))
		{
			goto IL_0042;
		}
	}
	{
		Program2_t3485103329 * L_16 = __this->get_U24this_0();
		AudioSource_t3935305588 * L_17 = L_16->get_messageSFX_4();
		AudioSource_Play_m48294159(L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Difficulty_t3960432064_il2cpp_TypeInfo_var);
		String_t* L_18 = ((Difficulty_t3960432064_StaticFields*)il2cpp_codegen_static_fields_for(Difficulty_t3960432064_il2cpp_TypeInfo_var))->get_a_4();
		Program2_t3485103329 * L_19 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_20 = L_19->get_startText_8();
		IL2CPP_RUNTIME_CLASS_INIT(Program2_t3485103329_il2cpp_TypeInfo_var);
		float L_21 = ((Program2_t3485103329_StaticFields*)il2cpp_codegen_static_fields_for(Program2_t3485103329_il2cpp_TypeInfo_var))->get_normTypeSpeed_6();
		Type2_TypeText_m1698800569(NULL /*static, unused*/, L_18, L_20, L_21, /*hidden argument*/NULL);
		goto IL_012b;
	}

IL_0107:
	{
		WaitForSeconds_t1699091251 * L_22 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_22, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_22);
		bool L_23 = __this->get_U24disposing_2();
		if (L_23)
		{
			goto IL_0126;
		}
	}
	{
		__this->set_U24PC_3(3);
	}

IL_0126:
	{
		goto IL_01d1;
	}

IL_012b:
	{
		Program2_t3485103329 * L_24 = __this->get_U24this_0();
		bool L_25 = L_24->get_inCoroutine_7();
		if (L_25)
		{
			goto IL_0107;
		}
	}
	{
		WaitForSeconds_t1699091251 * L_26 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_26, (5.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_26);
		bool L_27 = __this->get_U24disposing_2();
		if (L_27)
		{
			goto IL_015a;
		}
	}
	{
		__this->set_U24PC_3(4);
	}

IL_015a:
	{
		goto IL_01d1;
	}

IL_015f:
	{
		Program2_t3485103329 * L_28 = __this->get_U24this_0();
		AudioSource_t3935305588 * L_29 = L_28->get_leetSFX_5();
		AudioSource_Play_m48294159(L_29, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Difficulty_t3960432064_il2cpp_TypeInfo_var);
		String_t* L_30 = ((Difficulty_t3960432064_StaticFields*)il2cpp_codegen_static_fields_for(Difficulty_t3960432064_il2cpp_TypeInfo_var))->get_b_5();
		Program2_t3485103329 * L_31 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_32 = L_31->get_startText_8();
		Type2_TypeText_m1698800569(NULL /*static, unused*/, L_30, L_32, (0.5f), /*hidden argument*/NULL);
		goto IL_01b2;
	}

IL_018e:
	{
		WaitForSeconds_t1699091251 * L_33 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_33, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_33);
		bool L_34 = __this->get_U24disposing_2();
		if (L_34)
		{
			goto IL_01ad;
		}
	}
	{
		__this->set_U24PC_3(5);
	}

IL_01ad:
	{
		goto IL_01d1;
	}

IL_01b2:
	{
		Program2_t3485103329 * L_35 = __this->get_U24this_0();
		bool L_36 = L_35->get_inCoroutine_7();
		if (L_36)
		{
			goto IL_018e;
		}
	}
	{
		SceneManager_LoadScene_m3463216446(NULL /*static, unused*/, 4, /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_01cf:
	{
		return (bool)0;
	}

IL_01d1:
	{
		return (bool)1;
	}
}
// System.Object Harmony.Program2/<Loading>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CLoadingU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3598751173 (U3CLoadingU3Ec__Iterator0_t2822561722 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object Harmony.Program2/<Loading>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CLoadingU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2897406357 (U3CLoadingU3Ec__Iterator0_t2822561722 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void Harmony.Program2/<Loading>c__Iterator0::Dispose()
extern "C"  void U3CLoadingU3Ec__Iterator0_Dispose_m1301709815 (U3CLoadingU3Ec__Iterator0_t2822561722 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void Harmony.Program2/<Loading>c__Iterator0::Reset()
extern "C"  void U3CLoadingU3Ec__Iterator0_Reset_m3445375883 (U3CLoadingU3Ec__Iterator0_t2822561722 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadingU3Ec__Iterator0_Reset_m3445375883_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CLoadingU3Ec__Iterator0_Reset_m3445375883_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Harmony.Program3::.ctor()
extern "C"  void Program3__ctor_m1497920984 (Program3_t1528788193 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Harmony.Program3::Start()
extern "C"  void Program3_Start_m406976978 (Program3_t1528788193 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = Program3_Loading_m1854714262(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Harmony.Program3::Update()
extern "C"  void Program3_Update_m3264821048 (Program3_t1528788193 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Program3_Update_m3264821048_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, ((int32_t)27), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		SceneManager_LoadScene_m3463216446(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Collections.IEnumerator Harmony.Program3::Loading()
extern "C"  RuntimeObject* Program3_Loading_m1854714262 (Program3_t1528788193 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Program3_Loading_m1854714262_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CLoadingU3Ec__Iterator0_t2437702791 * V_0 = NULL;
	{
		U3CLoadingU3Ec__Iterator0_t2437702791 * L_0 = (U3CLoadingU3Ec__Iterator0_t2437702791 *)il2cpp_codegen_object_new(U3CLoadingU3Ec__Iterator0_t2437702791_il2cpp_TypeInfo_var);
		U3CLoadingU3Ec__Iterator0__ctor_m1320084545(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoadingU3Ec__Iterator0_t2437702791 * L_1 = V_0;
		L_1->set_U24this_0(__this);
		U3CLoadingU3Ec__Iterator0_t2437702791 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Harmony.Program3::.cctor()
extern "C"  void Program3__cctor_m295358907 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Program3__cctor_m295358907_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Program3_t1528788193_StaticFields*)il2cpp_codegen_static_fields_for(Program3_t1528788193_il2cpp_TypeInfo_var))->set_normTypeSpeed_6((0.03f));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Harmony.Program3/<Loading>c__Iterator0::.ctor()
extern "C"  void U3CLoadingU3Ec__Iterator0__ctor_m1320084545 (U3CLoadingU3Ec__Iterator0_t2437702791 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Harmony.Program3/<Loading>c__Iterator0::MoveNext()
extern "C"  bool U3CLoadingU3Ec__Iterator0_MoveNext_m1813361301 (U3CLoadingU3Ec__Iterator0_t2437702791 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadingU3Ec__Iterator0_MoveNext_m1813361301_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0031;
			}
			case 1:
			{
				goto IL_007b;
			}
			case 2:
			{
				goto IL_00b4;
			}
			case 3:
			{
				goto IL_012b;
			}
			case 4:
			{
				goto IL_015f;
			}
			case 5:
			{
				goto IL_01b2;
			}
		}
	}
	{
		goto IL_01df;
	}

IL_0031:
	{
		Program3_t1528788193 * L_2 = __this->get_U24this_0();
		L_2->set_i_9(0);
		goto IL_00c7;
	}

IL_0042:
	{
		Program3_t1528788193 * L_3 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_4 = L_3->get_startText_8();
		TMP_Text_set_text_m1216996582(L_4, _stringLiteral2527013464, /*hidden argument*/NULL);
		WaitForSeconds_t1699091251 * L_5 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_5, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_5);
		bool L_6 = __this->get_U24disposing_2();
		if (L_6)
		{
			goto IL_0076;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0076:
	{
		goto IL_01e1;
	}

IL_007b:
	{
		Program3_t1528788193 * L_7 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_8 = L_7->get_startText_8();
		TMP_Text_set_text_m1216996582(L_8, _stringLiteral2522622552, /*hidden argument*/NULL);
		WaitForSeconds_t1699091251 * L_9 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_9, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_9);
		bool L_10 = __this->get_U24disposing_2();
		if (L_10)
		{
			goto IL_00af;
		}
	}
	{
		__this->set_U24PC_3(2);
	}

IL_00af:
	{
		goto IL_01e1;
	}

IL_00b4:
	{
		Program3_t1528788193 * L_11 = __this->get_U24this_0();
		Program3_t1528788193 * L_12 = L_11;
		int32_t L_13 = L_12->get_i_9();
		L_12->set_i_9(((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1)));
	}

IL_00c7:
	{
		Program3_t1528788193 * L_14 = __this->get_U24this_0();
		int32_t L_15 = L_14->get_i_9();
		if ((((int32_t)L_15) <= ((int32_t)4)))
		{
			goto IL_0042;
		}
	}
	{
		Program3_t1528788193 * L_16 = __this->get_U24this_0();
		AudioSource_t3935305588 * L_17 = L_16->get_messageSFX_4();
		AudioSource_Play_m48294159(L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Doggo_t4070438711_il2cpp_TypeInfo_var);
		String_t* L_18 = ((Doggo_t4070438711_StaticFields*)il2cpp_codegen_static_fields_for(Doggo_t4070438711_il2cpp_TypeInfo_var))->get_a_4();
		Program3_t1528788193 * L_19 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_20 = L_19->get_startText_8();
		IL2CPP_RUNTIME_CLASS_INIT(Program3_t1528788193_il2cpp_TypeInfo_var);
		float L_21 = ((Program3_t1528788193_StaticFields*)il2cpp_codegen_static_fields_for(Program3_t1528788193_il2cpp_TypeInfo_var))->get_normTypeSpeed_6();
		Type3_TypeText_m2630213857(NULL /*static, unused*/, L_18, L_20, L_21, /*hidden argument*/NULL);
		goto IL_012b;
	}

IL_0107:
	{
		WaitForSeconds_t1699091251 * L_22 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_22, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_22);
		bool L_23 = __this->get_U24disposing_2();
		if (L_23)
		{
			goto IL_0126;
		}
	}
	{
		__this->set_U24PC_3(3);
	}

IL_0126:
	{
		goto IL_01e1;
	}

IL_012b:
	{
		Program3_t1528788193 * L_24 = __this->get_U24this_0();
		bool L_25 = L_24->get_inCoroutine_7();
		if (L_25)
		{
			goto IL_0107;
		}
	}
	{
		WaitForSeconds_t1699091251 * L_26 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_26, (11.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_26);
		bool L_27 = __this->get_U24disposing_2();
		if (L_27)
		{
			goto IL_015a;
		}
	}
	{
		__this->set_U24PC_3(4);
	}

IL_015a:
	{
		goto IL_01e1;
	}

IL_015f:
	{
		Program3_t1528788193 * L_28 = __this->get_U24this_0();
		AudioSource_t3935305588 * L_29 = L_28->get_doggoSFX_5();
		AudioSource_Play_m48294159(L_29, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Doggo_t4070438711_il2cpp_TypeInfo_var);
		String_t* L_30 = ((Doggo_t4070438711_StaticFields*)il2cpp_codegen_static_fields_for(Doggo_t4070438711_il2cpp_TypeInfo_var))->get_b_5();
		Program3_t1528788193 * L_31 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_32 = L_31->get_startText_8();
		Type3_TypeText_m2630213857(NULL /*static, unused*/, L_30, L_32, (0.5f), /*hidden argument*/NULL);
		goto IL_01b2;
	}

IL_018e:
	{
		WaitForSeconds_t1699091251 * L_33 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_33, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_33);
		bool L_34 = __this->get_U24disposing_2();
		if (L_34)
		{
			goto IL_01ad;
		}
	}
	{
		__this->set_U24PC_3(5);
	}

IL_01ad:
	{
		goto IL_01e1;
	}

IL_01b2:
	{
		Program3_t1528788193 * L_35 = __this->get_U24this_0();
		bool L_36 = L_35->get_inCoroutine_7();
		if (L_36)
		{
			goto IL_018e;
		}
	}
	{
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral1125101099, 1, /*hidden argument*/NULL);
		PlayerPrefs_Save_m2701929462(NULL /*static, unused*/, /*hidden argument*/NULL);
		SceneManager_LoadScene_m3463216446(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_01df:
	{
		return (bool)0;
	}

IL_01e1:
	{
		return (bool)1;
	}
}
// System.Object Harmony.Program3/<Loading>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CLoadingU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2223838234 (U3CLoadingU3Ec__Iterator0_t2437702791 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object Harmony.Program3/<Loading>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CLoadingU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1683515430 (U3CLoadingU3Ec__Iterator0_t2437702791 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void Harmony.Program3/<Loading>c__Iterator0::Dispose()
extern "C"  void U3CLoadingU3Ec__Iterator0_Dispose_m1511611660 (U3CLoadingU3Ec__Iterator0_t2437702791 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void Harmony.Program3/<Loading>c__Iterator0::Reset()
extern "C"  void U3CLoadingU3Ec__Iterator0_Reset_m3655277728 (U3CLoadingU3Ec__Iterator0_t2437702791 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadingU3Ec__Iterator0_Reset_m3655277728_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CLoadingU3Ec__Iterator0_Reset_m3655277728_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Harmony.Type::.ctor()
extern "C"  void Type__ctor_m902691241 (Type_t4058249690 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Harmony.Type::Awake()
extern "C"  void Type_Awake_m1861083554 (Type_t4058249690 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Type_Awake_m1861083554_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = GameObject_FindGameObjectWithTag_m2129039296(NULL /*static, unused*/, _stringLiteral531699260, /*hidden argument*/NULL);
		Program_t1866495201 * L_1 = GameObject_GetComponent_TisProgram_t1866495201_m3502787894(L_0, /*hidden argument*/GameObject_GetComponent_TisProgram_t1866495201_m3502787894_RuntimeMethod_var);
		((Type_t4058249690_StaticFields*)il2cpp_codegen_static_fields_for(Type_t4058249690_il2cpp_TypeInfo_var))->set_program_5(L_1);
		((Type_t4058249690_StaticFields*)il2cpp_codegen_static_fields_for(Type_t4058249690_il2cpp_TypeInfo_var))->set_instance_4(__this);
		return;
	}
}
// System.Void Harmony.Type::TypeText(System.String,TMPro.TMP_Text,System.Single)
extern "C"  void Type_TypeText_m4150723822 (RuntimeObject * __this /* static, unused */, String_t* ___message0, TMP_Text_t2599618874 * ___textDes1, float ___speed2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Type_TypeText_m4150723822_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Type_t4058249690 * L_0 = ((Type_t4058249690_StaticFields*)il2cpp_codegen_static_fields_for(Type_t4058249690_il2cpp_TypeInfo_var))->get_instance_4();
		String_t* L_1 = ___message0;
		TMP_Text_t2599618874 * L_2 = ___textDes1;
		float L_3 = ___speed2;
		RuntimeObject* L_4 = Type_AnimateText_m2819578080(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(L_0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Harmony.Type::AnimateText(System.String,TMPro.TMP_Text,System.Single)
extern "C"  RuntimeObject* Type_AnimateText_m2819578080 (RuntimeObject * __this /* static, unused */, String_t* ___message0, TMP_Text_t2599618874 * ___textDes1, float ___speed2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Type_AnimateText_m2819578080_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CAnimateTextU3Ec__Iterator0_t429204900 * V_0 = NULL;
	{
		U3CAnimateTextU3Ec__Iterator0_t429204900 * L_0 = (U3CAnimateTextU3Ec__Iterator0_t429204900 *)il2cpp_codegen_object_new(U3CAnimateTextU3Ec__Iterator0_t429204900_il2cpp_TypeInfo_var);
		U3CAnimateTextU3Ec__Iterator0__ctor_m4276049717(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CAnimateTextU3Ec__Iterator0_t429204900 * L_1 = V_0;
		String_t* L_2 = ___message0;
		L_1->set_message_1(L_2);
		U3CAnimateTextU3Ec__Iterator0_t429204900 * L_3 = V_0;
		TMP_Text_t2599618874 * L_4 = ___textDes1;
		L_3->set_textDes_2(L_4);
		U3CAnimateTextU3Ec__Iterator0_t429204900 * L_5 = V_0;
		float L_6 = ___speed2;
		L_5->set_speed_3(L_6);
		U3CAnimateTextU3Ec__Iterator0_t429204900 * L_7 = V_0;
		return L_7;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Harmony.Type/<AnimateText>c__Iterator0::.ctor()
extern "C"  void U3CAnimateTextU3Ec__Iterator0__ctor_m4276049717 (U3CAnimateTextU3Ec__Iterator0_t429204900 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Harmony.Type/<AnimateText>c__Iterator0::MoveNext()
extern "C"  bool U3CAnimateTextU3Ec__Iterator0_MoveNext_m1796649191 (U3CAnimateTextU3Ec__Iterator0_t429204900 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CAnimateTextU3Ec__Iterator0_MoveNext_m1796649191_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_007a;
			}
		}
	}
	{
		goto IL_00b2;
	}

IL_0021:
	{
		Program_t1866495201 * L_2 = ((Type_t4058249690_StaticFields*)il2cpp_codegen_static_fields_for(Type_t4058249690_il2cpp_TypeInfo_var))->get_program_5();
		L_2->set_inCoroutine_8((bool)1);
		__this->set_U3CiU3E__1_0(0);
		goto IL_0088;
	}

IL_0038:
	{
		TMP_Text_t2599618874 * L_3 = __this->get_textDes_2();
		String_t* L_4 = __this->get_message_1();
		int32_t L_5 = __this->get_U3CiU3E__1_0();
		String_t* L_6 = String_Substring_m1610150815(L_4, 0, L_5, /*hidden argument*/NULL);
		TMP_Text_set_text_m1216996582(L_3, L_6, /*hidden argument*/NULL);
		float L_7 = __this->get_speed_3();
		WaitForSeconds_t1699091251 * L_8 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_8, L_7, /*hidden argument*/NULL);
		__this->set_U24current_4(L_8);
		bool L_9 = __this->get_U24disposing_5();
		if (L_9)
		{
			goto IL_0075;
		}
	}
	{
		__this->set_U24PC_6(1);
	}

IL_0075:
	{
		goto IL_00b4;
	}

IL_007a:
	{
		int32_t L_10 = __this->get_U3CiU3E__1_0();
		__this->set_U3CiU3E__1_0(((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1)));
	}

IL_0088:
	{
		int32_t L_11 = __this->get_U3CiU3E__1_0();
		String_t* L_12 = __this->get_message_1();
		int32_t L_13 = String_get_Length_m3847582255(L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_11) < ((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1)))))
		{
			goto IL_0038;
		}
	}
	{
		Program_t1866495201 * L_14 = ((Type_t4058249690_StaticFields*)il2cpp_codegen_static_fields_for(Type_t4058249690_il2cpp_TypeInfo_var))->get_program_5();
		L_14->set_inCoroutine_8((bool)0);
		__this->set_U24PC_6((-1));
	}

IL_00b2:
	{
		return (bool)0;
	}

IL_00b4:
	{
		return (bool)1;
	}
}
// System.Object Harmony.Type/<AnimateText>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CAnimateTextU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2866024280 (U3CAnimateTextU3Ec__Iterator0_t429204900 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object Harmony.Type/<AnimateText>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CAnimateTextU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m483692309 (U3CAnimateTextU3Ec__Iterator0_t429204900 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Void Harmony.Type/<AnimateText>c__Iterator0::Dispose()
extern "C"  void U3CAnimateTextU3Ec__Iterator0_Dispose_m2084414412 (U3CAnimateTextU3Ec__Iterator0_t429204900 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_5((bool)1);
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void Harmony.Type/<AnimateText>c__Iterator0::Reset()
extern "C"  void U3CAnimateTextU3Ec__Iterator0_Reset_m3627900720 (U3CAnimateTextU3Ec__Iterator0_t429204900 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CAnimateTextU3Ec__Iterator0_Reset_m3627900720_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CAnimateTextU3Ec__Iterator0_Reset_m3627900720_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Harmony.Type2::.ctor()
extern "C"  void Type2__ctor_m3517361998 (Type2_t2499874421 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Harmony.Type2::Awake()
extern "C"  void Type2_Awake_m2046811758 (Type2_t2499874421 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Type2_Awake_m2046811758_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = GameObject_FindGameObjectWithTag_m2129039296(NULL /*static, unused*/, _stringLiteral531699260, /*hidden argument*/NULL);
		Program2_t3485103329 * L_1 = GameObject_GetComponent_TisProgram2_t3485103329_m3599024581(L_0, /*hidden argument*/GameObject_GetComponent_TisProgram2_t3485103329_m3599024581_RuntimeMethod_var);
		((Type2_t2499874421_StaticFields*)il2cpp_codegen_static_fields_for(Type2_t2499874421_il2cpp_TypeInfo_var))->set_program2_5(L_1);
		((Type2_t2499874421_StaticFields*)il2cpp_codegen_static_fields_for(Type2_t2499874421_il2cpp_TypeInfo_var))->set_instance_4(__this);
		return;
	}
}
// System.Void Harmony.Type2::TypeText(System.String,TMPro.TMP_Text,System.Single)
extern "C"  void Type2_TypeText_m1698800569 (RuntimeObject * __this /* static, unused */, String_t* ___message0, TMP_Text_t2599618874 * ___textDes1, float ___speed2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Type2_TypeText_m1698800569_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Type2_t2499874421 * L_0 = ((Type2_t2499874421_StaticFields*)il2cpp_codegen_static_fields_for(Type2_t2499874421_il2cpp_TypeInfo_var))->get_instance_4();
		String_t* L_1 = ___message0;
		TMP_Text_t2599618874 * L_2 = ___textDes1;
		float L_3 = ___speed2;
		RuntimeObject* L_4 = Type2_AnimateText_m909537142(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(L_0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Harmony.Type2::AnimateText(System.String,TMPro.TMP_Text,System.Single)
extern "C"  RuntimeObject* Type2_AnimateText_m909537142 (RuntimeObject * __this /* static, unused */, String_t* ___message0, TMP_Text_t2599618874 * ___textDes1, float ___speed2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Type2_AnimateText_m909537142_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CAnimateTextU3Ec__Iterator0_t589720723 * V_0 = NULL;
	{
		U3CAnimateTextU3Ec__Iterator0_t589720723 * L_0 = (U3CAnimateTextU3Ec__Iterator0_t589720723 *)il2cpp_codegen_object_new(U3CAnimateTextU3Ec__Iterator0_t589720723_il2cpp_TypeInfo_var);
		U3CAnimateTextU3Ec__Iterator0__ctor_m3226696527(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CAnimateTextU3Ec__Iterator0_t589720723 * L_1 = V_0;
		String_t* L_2 = ___message0;
		L_1->set_message_1(L_2);
		U3CAnimateTextU3Ec__Iterator0_t589720723 * L_3 = V_0;
		TMP_Text_t2599618874 * L_4 = ___textDes1;
		L_3->set_textDes_2(L_4);
		U3CAnimateTextU3Ec__Iterator0_t589720723 * L_5 = V_0;
		float L_6 = ___speed2;
		L_5->set_speed_3(L_6);
		U3CAnimateTextU3Ec__Iterator0_t589720723 * L_7 = V_0;
		return L_7;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Harmony.Type2/<AnimateText>c__Iterator0::.ctor()
extern "C"  void U3CAnimateTextU3Ec__Iterator0__ctor_m3226696527 (U3CAnimateTextU3Ec__Iterator0_t589720723 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Harmony.Type2/<AnimateText>c__Iterator0::MoveNext()
extern "C"  bool U3CAnimateTextU3Ec__Iterator0_MoveNext_m2416659315 (U3CAnimateTextU3Ec__Iterator0_t589720723 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CAnimateTextU3Ec__Iterator0_MoveNext_m2416659315_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_007a;
			}
		}
	}
	{
		goto IL_00b2;
	}

IL_0021:
	{
		Program2_t3485103329 * L_2 = ((Type2_t2499874421_StaticFields*)il2cpp_codegen_static_fields_for(Type2_t2499874421_il2cpp_TypeInfo_var))->get_program2_5();
		L_2->set_inCoroutine_7((bool)1);
		__this->set_U3CiU3E__1_0(0);
		goto IL_0088;
	}

IL_0038:
	{
		TMP_Text_t2599618874 * L_3 = __this->get_textDes_2();
		String_t* L_4 = __this->get_message_1();
		int32_t L_5 = __this->get_U3CiU3E__1_0();
		String_t* L_6 = String_Substring_m1610150815(L_4, 0, L_5, /*hidden argument*/NULL);
		TMP_Text_set_text_m1216996582(L_3, L_6, /*hidden argument*/NULL);
		float L_7 = __this->get_speed_3();
		WaitForSeconds_t1699091251 * L_8 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_8, L_7, /*hidden argument*/NULL);
		__this->set_U24current_4(L_8);
		bool L_9 = __this->get_U24disposing_5();
		if (L_9)
		{
			goto IL_0075;
		}
	}
	{
		__this->set_U24PC_6(1);
	}

IL_0075:
	{
		goto IL_00b4;
	}

IL_007a:
	{
		int32_t L_10 = __this->get_U3CiU3E__1_0();
		__this->set_U3CiU3E__1_0(((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1)));
	}

IL_0088:
	{
		int32_t L_11 = __this->get_U3CiU3E__1_0();
		String_t* L_12 = __this->get_message_1();
		int32_t L_13 = String_get_Length_m3847582255(L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_11) < ((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1)))))
		{
			goto IL_0038;
		}
	}
	{
		Program2_t3485103329 * L_14 = ((Type2_t2499874421_StaticFields*)il2cpp_codegen_static_fields_for(Type2_t2499874421_il2cpp_TypeInfo_var))->get_program2_5();
		L_14->set_inCoroutine_7((bool)0);
		__this->set_U24PC_6((-1));
	}

IL_00b2:
	{
		return (bool)0;
	}

IL_00b4:
	{
		return (bool)1;
	}
}
// System.Object Harmony.Type2/<AnimateText>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CAnimateTextU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1387176536 (U3CAnimateTextU3Ec__Iterator0_t589720723 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object Harmony.Type2/<AnimateText>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CAnimateTextU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m4213044948 (U3CAnimateTextU3Ec__Iterator0_t589720723 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Void Harmony.Type2/<AnimateText>c__Iterator0::Dispose()
extern "C"  void U3CAnimateTextU3Ec__Iterator0_Dispose_m1211778999 (U3CAnimateTextU3Ec__Iterator0_t589720723 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_5((bool)1);
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void Harmony.Type2/<AnimateText>c__Iterator0::Reset()
extern "C"  void U3CAnimateTextU3Ec__Iterator0_Reset_m2745469755 (U3CAnimateTextU3Ec__Iterator0_t589720723 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CAnimateTextU3Ec__Iterator0_Reset_m2745469755_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CAnimateTextU3Ec__Iterator0_Reset_m2745469755_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Harmony.Type3::.ctor()
extern "C"  void Type3__ctor_m3517361189 (Type3_t2499874422 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Harmony.Type3::Awake()
extern "C"  void Type3_Awake_m2046810157 (Type3_t2499874422 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Type3_Awake_m2046810157_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = GameObject_FindGameObjectWithTag_m2129039296(NULL /*static, unused*/, _stringLiteral531699260, /*hidden argument*/NULL);
		Program3_t1528788193 * L_1 = GameObject_GetComponent_TisProgram3_t1528788193_m3599024580(L_0, /*hidden argument*/GameObject_GetComponent_TisProgram3_t1528788193_m3599024580_RuntimeMethod_var);
		((Type3_t2499874422_StaticFields*)il2cpp_codegen_static_fields_for(Type3_t2499874422_il2cpp_TypeInfo_var))->set_program3_5(L_1);
		((Type3_t2499874422_StaticFields*)il2cpp_codegen_static_fields_for(Type3_t2499874422_il2cpp_TypeInfo_var))->set_instance_4(__this);
		return;
	}
}
// System.Void Harmony.Type3::TypeText(System.String,TMPro.TMP_Text,System.Single)
extern "C"  void Type3_TypeText_m2630213857 (RuntimeObject * __this /* static, unused */, String_t* ___message0, TMP_Text_t2599618874 * ___textDes1, float ___speed2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Type3_TypeText_m2630213857_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Type3_t2499874422 * L_0 = ((Type3_t2499874422_StaticFields*)il2cpp_codegen_static_fields_for(Type3_t2499874422_il2cpp_TypeInfo_var))->get_instance_4();
		String_t* L_1 = ___message0;
		TMP_Text_t2599618874 * L_2 = ___textDes1;
		float L_3 = ___speed2;
		RuntimeObject* L_4 = Type3_AnimateText_m1584460279(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(L_0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Harmony.Type3::AnimateText(System.String,TMPro.TMP_Text,System.Single)
extern "C"  RuntimeObject* Type3_AnimateText_m1584460279 (RuntimeObject * __this /* static, unused */, String_t* ___message0, TMP_Text_t2599618874 * ___textDes1, float ___speed2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Type3_AnimateText_m1584460279_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CAnimateTextU3Ec__Iterator0_t772139121 * V_0 = NULL;
	{
		U3CAnimateTextU3Ec__Iterator0_t772139121 * L_0 = (U3CAnimateTextU3Ec__Iterator0_t772139121 *)il2cpp_codegen_object_new(U3CAnimateTextU3Ec__Iterator0_t772139121_il2cpp_TypeInfo_var);
		U3CAnimateTextU3Ec__Iterator0__ctor_m4028396701(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CAnimateTextU3Ec__Iterator0_t772139121 * L_1 = V_0;
		String_t* L_2 = ___message0;
		L_1->set_message_1(L_2);
		U3CAnimateTextU3Ec__Iterator0_t772139121 * L_3 = V_0;
		TMP_Text_t2599618874 * L_4 = ___textDes1;
		L_3->set_textDes_2(L_4);
		U3CAnimateTextU3Ec__Iterator0_t772139121 * L_5 = V_0;
		float L_6 = ___speed2;
		L_5->set_speed_3(L_6);
		U3CAnimateTextU3Ec__Iterator0_t772139121 * L_7 = V_0;
		return L_7;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Harmony.Type3/<AnimateText>c__Iterator0::.ctor()
extern "C"  void U3CAnimateTextU3Ec__Iterator0__ctor_m4028396701 (U3CAnimateTextU3Ec__Iterator0_t772139121 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Harmony.Type3/<AnimateText>c__Iterator0::MoveNext()
extern "C"  bool U3CAnimateTextU3Ec__Iterator0_MoveNext_m1115575760 (U3CAnimateTextU3Ec__Iterator0_t772139121 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CAnimateTextU3Ec__Iterator0_MoveNext_m1115575760_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_007a;
			}
		}
	}
	{
		goto IL_00b2;
	}

IL_0021:
	{
		Program3_t1528788193 * L_2 = ((Type3_t2499874422_StaticFields*)il2cpp_codegen_static_fields_for(Type3_t2499874422_il2cpp_TypeInfo_var))->get_program3_5();
		L_2->set_inCoroutine_7((bool)1);
		__this->set_U3CiU3E__1_0(0);
		goto IL_0088;
	}

IL_0038:
	{
		TMP_Text_t2599618874 * L_3 = __this->get_textDes_2();
		String_t* L_4 = __this->get_message_1();
		int32_t L_5 = __this->get_U3CiU3E__1_0();
		String_t* L_6 = String_Substring_m1610150815(L_4, 0, L_5, /*hidden argument*/NULL);
		TMP_Text_set_text_m1216996582(L_3, L_6, /*hidden argument*/NULL);
		float L_7 = __this->get_speed_3();
		WaitForSeconds_t1699091251 * L_8 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_8, L_7, /*hidden argument*/NULL);
		__this->set_U24current_4(L_8);
		bool L_9 = __this->get_U24disposing_5();
		if (L_9)
		{
			goto IL_0075;
		}
	}
	{
		__this->set_U24PC_6(1);
	}

IL_0075:
	{
		goto IL_00b4;
	}

IL_007a:
	{
		int32_t L_10 = __this->get_U3CiU3E__1_0();
		__this->set_U3CiU3E__1_0(((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1)));
	}

IL_0088:
	{
		int32_t L_11 = __this->get_U3CiU3E__1_0();
		String_t* L_12 = __this->get_message_1();
		int32_t L_13 = String_get_Length_m3847582255(L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_11) < ((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1)))))
		{
			goto IL_0038;
		}
	}
	{
		Program3_t1528788193 * L_14 = ((Type3_t2499874422_StaticFields*)il2cpp_codegen_static_fields_for(Type3_t2499874422_il2cpp_TypeInfo_var))->get_program3_5();
		L_14->set_inCoroutine_7((bool)0);
		__this->set_U24PC_6((-1));
	}

IL_00b2:
	{
		return (bool)0;
	}

IL_00b4:
	{
		return (bool)1;
	}
}
// System.Object Harmony.Type3/<AnimateText>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CAnimateTextU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3580546422 (U3CAnimateTextU3Ec__Iterator0_t772139121 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object Harmony.Type3/<AnimateText>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CAnimateTextU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2111125214 (U3CAnimateTextU3Ec__Iterator0_t772139121 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Void Harmony.Type3/<AnimateText>c__Iterator0::Dispose()
extern "C"  void U3CAnimateTextU3Ec__Iterator0_Dispose_m1655465821 (U3CAnimateTextU3Ec__Iterator0_t772139121 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_5((bool)1);
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void Harmony.Type3/<AnimateText>c__Iterator0::Reset()
extern "C"  void U3CAnimateTextU3Ec__Iterator0_Reset_m3302327437 (U3CAnimateTextU3Ec__Iterator0_t772139121 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CAnimateTextU3Ec__Iterator0_Reset_m3302327437_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CAnimateTextU3Ec__Iterator0_Reset_m3302327437_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Harmony.menuProgram::.ctor()
extern "C"  void menuProgram__ctor_m2271585197 (menuProgram_t465836879 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Harmony.menuProgram::Start()
extern "C"  void menuProgram_Start_m202465803 (menuProgram_t465836879 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (menuProgram_Start_m202465803_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_str1_6(_stringLiteral2527013464);
		__this->set_str2_7(_stringLiteral2522622552);
		GameObject_t1113636619 * L_0 = __this->get_doggoHuntBTN_5();
		GameObject_SetActive_m796801857(L_0, (bool)0, /*hidden argument*/NULL);
		RuntimeObject* L_1 = menuProgram_Loading_m616370398(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Harmony.menuProgram::Update()
extern "C"  void menuProgram_Update_m85793366 (menuProgram_t465836879 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (menuProgram_Update_m85793366_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral1125101099, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_0032;
		}
	}
	{
		__this->set_str1_6(_stringLiteral432624090);
		__this->set_str2_7(_stringLiteral2807073954);
		GameObject_t1113636619 * L_1 = __this->get_doggoHuntBTN_5();
		GameObject_SetActive_m796801857(L_1, (bool)1, /*hidden argument*/NULL);
	}

IL_0032:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, ((int32_t)27), /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0043;
		}
	}
	{
		Application_Quit_m470877999(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0043:
	{
		return;
	}
}
// System.Collections.IEnumerator Harmony.menuProgram::Loading()
extern "C"  RuntimeObject* menuProgram_Loading_m616370398 (menuProgram_t465836879 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (menuProgram_Loading_m616370398_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CLoadingU3Ec__Iterator0_t2464126437 * V_0 = NULL;
	{
		U3CLoadingU3Ec__Iterator0_t2464126437 * L_0 = (U3CLoadingU3Ec__Iterator0_t2464126437 *)il2cpp_codegen_object_new(U3CLoadingU3Ec__Iterator0_t2464126437_il2cpp_TypeInfo_var);
		U3CLoadingU3Ec__Iterator0__ctor_m2397844334(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoadingU3Ec__Iterator0_t2464126437 * L_1 = V_0;
		L_1->set_U24this_0(__this);
		U3CLoadingU3Ec__Iterator0_t2464126437 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Harmony.menuProgram::StartGame()
extern "C"  void menuProgram_StartGame_m2657410957 (menuProgram_t465836879 * __this, const RuntimeMethod* method)
{
	{
		SceneManager_LoadScene_m3463216446(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Harmony.menuProgram::QuitGame()
extern "C"  void menuProgram_QuitGame_m4257273585 (menuProgram_t465836879 * __this, const RuntimeMethod* method)
{
	{
		Application_Quit_m470877999(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Harmony.menuProgram::DoggoHunt()
extern "C"  void menuProgram_DoggoHunt_m1418726496 (menuProgram_t465836879 * __this, const RuntimeMethod* method)
{
	{
		SceneManager_LoadScene_m3463216446(NULL /*static, unused*/, 6, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Harmony.menuProgram/<Loading>c__Iterator0::.ctor()
extern "C"  void U3CLoadingU3Ec__Iterator0__ctor_m2397844334 (U3CLoadingU3Ec__Iterator0_t2464126437 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Harmony.menuProgram/<Loading>c__Iterator0::MoveNext()
extern "C"  bool U3CLoadingU3Ec__Iterator0_MoveNext_m2655573207 (U3CLoadingU3Ec__Iterator0_t2464126437 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadingU3Ec__Iterator0_MoveNext_m2655573207_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0025;
			}
			case 1:
			{
				goto IL_0064;
			}
			case 2:
			{
				goto IL_00a3;
			}
		}
	}
	{
		goto IL_00af;
	}

IL_0025:
	{
		menuProgram_t465836879 * L_2 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_3 = L_2->get_startText_4();
		menuProgram_t465836879 * L_4 = __this->get_U24this_0();
		String_t* L_5 = L_4->get_str1_6();
		TMP_Text_set_text_m1216996582(L_3, L_5, /*hidden argument*/NULL);
		WaitForSeconds_t1699091251 * L_6 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_6, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_6);
		bool L_7 = __this->get_U24disposing_2();
		if (L_7)
		{
			goto IL_005f;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_005f:
	{
		goto IL_00b1;
	}

IL_0064:
	{
		menuProgram_t465836879 * L_8 = __this->get_U24this_0();
		TMP_Text_t2599618874 * L_9 = L_8->get_startText_4();
		menuProgram_t465836879 * L_10 = __this->get_U24this_0();
		String_t* L_11 = L_10->get_str2_7();
		TMP_Text_set_text_m1216996582(L_9, L_11, /*hidden argument*/NULL);
		WaitForSeconds_t1699091251 * L_12 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_12, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_12);
		bool L_13 = __this->get_U24disposing_2();
		if (L_13)
		{
			goto IL_009e;
		}
	}
	{
		__this->set_U24PC_3(2);
	}

IL_009e:
	{
		goto IL_00b1;
	}

IL_00a3:
	{
		goto IL_0025;
	}
	// Dead block : IL_00a8: ldarg.0

IL_00af:
	{
		return (bool)0;
	}

IL_00b1:
	{
		return (bool)1;
	}
}
// System.Object Harmony.menuProgram/<Loading>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CLoadingU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4274645884 (U3CLoadingU3Ec__Iterator0_t2464126437 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object Harmony.menuProgram/<Loading>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CLoadingU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3984828058 (U3CLoadingU3Ec__Iterator0_t2464126437 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void Harmony.menuProgram/<Loading>c__Iterator0::Dispose()
extern "C"  void U3CLoadingU3Ec__Iterator0_Dispose_m3311496141 (U3CLoadingU3Ec__Iterator0_t2464126437 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void Harmony.menuProgram/<Loading>c__Iterator0::Reset()
extern "C"  void U3CLoadingU3Ec__Iterator0_Reset_m3353854054 (U3CLoadingU3Ec__Iterator0_t2464126437 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadingU3Ec__Iterator0_Reset_m3353854054_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CLoadingU3Ec__Iterator0_Reset_m3353854054_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sprite::.ctor()
extern "C"  void Sprite__ctor_m4073386695 (Sprite_t2249211761 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sprite::Start()
extern "C"  void Sprite_Start_m1984966497 (Sprite_t2249211761 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Sprite_Start_m1984966497_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = GameObject_FindGameObjectWithTag_m2129039296(NULL /*static, unused*/, _stringLiteral531699260, /*hidden argument*/NULL);
		Battle_t2191861408 * L_1 = GameObject_GetComponent_TisBattle_t2191861408_m3384264100(L_0, /*hidden argument*/GameObject_GetComponent_TisBattle_t2191861408_m3384264100_RuntimeMethod_var);
		__this->set_battle_4(L_1);
		return;
	}
}
// System.Void Sprite::Update()
extern "C"  void Sprite_Update_m3398220587 (Sprite_t2249211761 * __this, const RuntimeMethod* method)
{
	{
		Battle_t2191861408 * L_0 = __this->get_battle_4();
		bool L_1 = L_0->get_isDead_22();
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		bool L_2 = __this->get_stahp_6();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		Sprite_PlayAnim_m2275555825(__this, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Void Sprite::PlayAnim()
extern "C"  void Sprite_PlayAnim_m2275555825 (Sprite_t2249211761 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Sprite_PlayAnim_m2275555825_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_stahp_6((bool)1);
		Animator_t434523843 * L_0 = __this->get_anim_5();
		Animator_Play_m1697843332(L_0, _stringLiteral1700381005, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Spriteimp::.ctor()
extern "C"  void Spriteimp__ctor_m1281705163 (Spriteimp_t3952321968 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Spriteimp::Start()
extern "C"  void Spriteimp_Start_m27523747 (Spriteimp_t3952321968 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Spriteimp_Start_m27523747_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = GameObject_FindGameObjectWithTag_m2129039296(NULL /*static, unused*/, _stringLiteral531699260, /*hidden argument*/NULL);
		Battleimp_t158148094 * L_1 = GameObject_GetComponent_TisBattleimp_t158148094_m3358208344(L_0, /*hidden argument*/GameObject_GetComponent_TisBattleimp_t158148094_m3358208344_RuntimeMethod_var);
		__this->set_battle_4(L_1);
		return;
	}
}
// System.Void Spriteimp::Update()
extern "C"  void Spriteimp_Update_m1534474277 (Spriteimp_t3952321968 * __this, const RuntimeMethod* method)
{
	{
		Battleimp_t158148094 * L_0 = __this->get_battle_4();
		bool L_1 = L_0->get_isDead_22();
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		bool L_2 = __this->get_stahp_6();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		Spriteimp_PlayAnim_m1100634416(__this, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Void Spriteimp::PlayAnim()
extern "C"  void Spriteimp_PlayAnim_m1100634416 (Spriteimp_t3952321968 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Spriteimp_PlayAnim_m1100634416_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_stahp_6((bool)1);
		Animator_t434523843 * L_0 = __this->get_anim_5();
		Animator_Play_m1697843332(L_0, _stringLiteral1700381005, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Spriteinf::.ctor()
extern "C"  void Spriteinf__ctor_m3322314561 (Spriteinf_t2378343838 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Spriteinf::Start()
extern "C"  void Spriteinf_Start_m996226461 (Spriteinf_t2378343838 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Spriteinf_Start_m996226461_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = GameObject_FindGameObjectWithTag_m2129039296(NULL /*static, unused*/, _stringLiteral531699260, /*hidden argument*/NULL);
		Battleinf_t1732126188 * L_1 = GameObject_GetComponent_TisBattleinf_t1732126188_m3397136827(L_0, /*hidden argument*/GameObject_GetComponent_TisBattleinf_t1732126188_m3397136827_RuntimeMethod_var);
		__this->set_battle_4(L_1);
		return;
	}
}
// System.Void Spriteinf::Update()
extern "C"  void Spriteinf_Update_m4016605689 (Spriteinf_t2378343838 * __this, const RuntimeMethod* method)
{
	{
		Battleinf_t1732126188 * L_0 = __this->get_battle_4();
		bool L_1 = L_0->get_isDead_24();
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		bool L_2 = __this->get_stahp_6();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		Spriteinf_PlayAnim_m139970992(__this, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Void Spriteinf::PlayAnim()
extern "C"  void Spriteinf_PlayAnim_m139970992 (Spriteinf_t2378343838 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Spriteinf_PlayAnim_m139970992_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_stahp_6((bool)1);
		Animator_t434523843 * L_0 = __this->get_anim_5();
		Animator_Play_m1697843332(L_0, _stringLiteral1700381005, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
